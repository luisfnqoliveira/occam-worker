import pytest
from unittest import mock

from tests import helper

# All tests will capture the Database
@pytest.yield_fixture(autouse=True)
def setup_db():
  db_session_mock = helper.db_session()
  yield

# All tests will capture file open
@pytest.yield_fixture(autouse=True)
def setup_open():
  mocks = []
  open_file_mock = helper.open(getMock=mocks)
  yield

# All tests will capture Popen
@pytest.yield_fixture(autouse=True)
def setup_subprocess():
  mocks = []
  mock = helper.subprocess()
  yield

# All tests will fail to find files
@pytest.yield_fixture(autouse=True)
def setup_os_paths():
  mock.patch('os.path.exists', False)
  yield
