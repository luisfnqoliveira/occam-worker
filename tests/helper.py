from flexmock import flexmock
from unittest import mock
import pytest

from uuid import uuid4

from collections import namedtuple
import subprocess as Real_subprocess

import random
import re
import sys
import os as RealOS
import json as RealJSON

from lib.db import DB

# Do not allow anything to make a directory
flexmock(RealOS)
flexmock(RealOS.path)
RealOS.should_receive('mkdir').and_return(None)

# Make joins consistent
def joiner(*args, **kwargs):
  return '/'.join(args)
RealOS.path.should_receive('join').replace_with(joiner)

def json(expected_file=None, result={}):
  mock = flexmock(RealJSON)
  RealJSON.should_receive('load').with_args(expected_file).and_return(result)

  return mock

def exists(expected_path):
  mock = flexmock(RealOS.path)

  RealOS.path.should_receive('exists').with_args(expected_path).and_return(True)

  return mock

def mkdir(expected_path=None):
  mock = flexmock(RealOS)

  RealOS.should_receive('mkdir').with_args(expected_path).and_return(None)

  return mock

def open(expected_filename=None, expected_mode=None, content="CONTENT", getMock=None):
  mock = flexmock(sys.modules['builtins'])

  file_mock = flexmock(
    read=lambda: content,
    close=lambda: None
  )

  if expected_filename is not None:
    if expected_mode is not None:
      m = (mock.should_receive('open')
         .with_args(expected_filename, expected_mode)
         .and_return(file_mock))
    else:
      m = (mock.should_receive('open')
         .with_args(expected_filename)
         .and_return(file_mock))
  else:
    m =(mock.should_receive('open')
       .and_return(file_mock))

  if getMock is not None:
    getMock.append(m)

  return file_mock

def setup_directory(structure={}):
  """
  Sets up the standard library to pretend that the given
  directory structure exists. The structure will always
  be on top of the directory: /test
  """
  flexmock(RealOS)
  flexmock(RealOS.path)
  RealOS.path.should_receive('exists').and_return(False)

  def setup_path(path, base_path, inner_structure):
    for k,v in inner_structure.items():
      flexmock(RealOS)
      flexmock(RealOS.path)

      inner_path = RealOS.path.join(path, k)
      inner_base_path = RealOS.path.join(base_path, k)

      if base_path == "":
        inner_base_path = k

      if isinstance(v, dict):

        print("setting up directory %s" % (k))
        setup_path(inner_path, inner_base_path, v)

        RealOS.path.should_receive('isdir').with_args(inner_path).and_return(True)
        RealOS.path.should_receive('isdir').with_args(inner_base_path).and_return(True)
        RealOS.should_receive('listdir').with_args(inner_path).and_return(list(v.keys()))
        RealOS.should_receive('listdir').with_args(inner_base_path).and_return(list(v.keys()))
      else:
        # This is a file with the contents of 'v'
        open(path, None, v)
        open(base_path, None, v)

        RealOS.path.should_receive('isdir').with_args(inner_base_path).and_return(False)
        RealOS.path.should_receive('isdir').with_args(inner_path).and_return(False)

    print(path)
    print(base_path)
    RealOS.path.should_receive('exists').with_args(path).and_return(True)
    RealOS.path.should_receive('exists').with_args(base_path).and_return(True)
    RealOS.path.should_receive('splitdrive').with_args(path).and_return(['', path])
    #RealOS.path.should_receive('realpath').with_args(path).and_return(path)
    #RealOS.path.should_receive('realpath').with_args(base_path).and_return(path)

  # Go through structure and set up 'exists' routines and split paths etc
  setup_path("/test", "", structure)
  RealOS.should_receive('getcwd').and_return("/test")

  RealOS.path.should_receive('isdir').with_args('/test').and_return(True)

def subprocess(stdout="", stderr="", stdin="", code=0):
  """
  Mock out a subprocess.Popen call.
  """

  import io

  mocked_process = mock.MagicMock()

  mocked_process.stdout = io.StringIO(stdout)
  mocked_process.stderr = io.StringIO(stderr)
  mocked_process.stdin  = io.StringIO(stdin)
  mocked_process.wait   = lambda *a, **kw: code

  m = mock.patch('subprocess.Popen', mocked_process)
  m.start()
  m.process = mocked_process

  return m

def db_session():
  # Mock the Database session
  db_mock = flexmock(DB)
  db_session = flexmock(
    commit = lambda *a, **kw: None,
    add    = lambda *a, **kw: None,
    flush  = lambda *a, **kw: None,
  )
  m = mock.patch('lib.db.DB.session', db_session)
  m.start()
  return db_mock

def DBObject(name="name", type="type", uid=None):
  """
  This represents a mocked DB object. (models/object.py)
  """

  uid = uid or uuid()

  mock = flexmock()

  mock.name        = name
  mock.object_type = type
  mock.uid         = uid
  mock.id          = random.randint(0, 100000)

  if type == "workset":
    mock.workset = DBObject()

  if type == "experiment":
    mock.experiment = DBObject()

  if type == "person":
    mock.person = DBObject()

  if type == "group":
    mock.group = DBObject()

  return mock

def Object(name="name", type="type", uid=None, revision=None, info=None, parent=None, workset=None):
  """
  This represents a mocked object. (/lib/object.py)
  """

  uid = uid or uuid()
  info = info or {}

  mock = flexmock()

  mock.uuid       = uid
  mock.path       = "OBJECT_PATH"
  mock.revision   = revision

  object_info = {
    "name": name,
    "type": type,
    "id":   uid
  }

  object_info.update(info)

  mock.should_receive('parent').and_return(parent)
  mock.should_receive('workset').and_return(workset)
  mock.should_receive('clone').and_return(None)

  mock.should_receive('objectInfo').with_args().and_return(object_info)

  return mock

def Task(generator, generator_revision=None, name="name", uid=None, revision=None):
  """
  This represents in particular an occam/task Object.
  """

  generator_info = generator.objectInfo().copy()
  generator_revision = generator_revision or generator.revision
  generator_info['revision'] = generator_revision

  mock = Object(name     = name,
                type     = "occam/task",
                uid      = uid,
                revision = revision,
                info     = {
                  "generator": generator_info
                })

  return mock

def Occam(known_objects=None, current_person_id = None):
  """
  This mocks out the general Occam class. (/lib/occam.py)
  """

  known_objects = known_objects or []
  directory = {}

  for obj in known_objects:
    if isinstance(obj, str):
      directory[obj] = Object(uid=obj)
    else:
      directory[obj.uuid] = obj

  occam = flexmock()

  occam.should_receive('objectAt').and_return(None)
  occam.should_receive('currentPersonId').and_return(current_person_id)

  def searchObjects(uid):
    if uid in directory:
      return flexmock(first=lambda *a, **kw:DBObject(uid=uid, type=directory[uid].objectInfo()['type']))
    else:
      return flexmock(first=lambda *a, **kw:None)
  occam.searchObjects = searchObjects

  def searchPeople(uid):
    if uid in directory:
      return flexmock(first=lambda *a, **kw:DBObject(uid=uid, type="person"))
    else:
      return flexmock(first=lambda *a, **kw:None)
  occam.searchPeople = searchPeople

  def retrieve(uid, type, revision=None):
    if uid in directory:
      return directory[uid]
    else:
      return None
  occam.retrieve = retrieve

  def temporaryClone(uid, revision, withinUuid=None, withinRevision=None):
    obj = None
    workset_obj = None

    if withinUuid in directory:
      workset_obj = directory[withinUuid]
      workset_obj.revision = withinRevision

    if uid in directory:
      obj = directory[uid]
      obj.revision = revision

    return workset_obj, obj, "PATH"
  occam.temporaryClone = temporaryClone

  return occam

def uuid():
  """
  A UUID factory to produce fuzzy, random uuids.
  """

  # UUID4 produces a uniformly random uuid
  return str(uuid4())
