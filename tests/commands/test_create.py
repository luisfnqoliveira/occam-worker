from flexmock import flexmock
import pytest

from lib.commands.create 	import Create
#from lib.log            	import Log

class TestCreate(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Create.initParser(fake) == fake


	'''@pytest.mark.parametrize("args,errormsg,expected", [
		(['zero'],'no name given',-1),
		(['zero','one'],None, None),
		(['zero','one','two'], None, None),
		(['zero','one','two','three','four'], None, None),
		#(['zero','one','two','three'], 0),
		#(['zero','one','two','three','four','five'], 0)
	])
	def test_do(self,args,errormsg,expected):
		flexmock(Log)
		Log.should_receive('header').once()
		fake_occam = flexmock()
		fake_self=flexmock(args=args,occam=fake_occam)
		
		if len(args) > 1:
			fake_occam.should_receive('addWorkset').once()
			Log.should_receive('done').with_args("Successfully created workset %s" % (args[1])).once()
		if len(args) > 2:
			Log.should_receive('write').with_args("placing object in %s" % (args[2])).once()
		if not errormsg is None:
			Log.should_receive('error').with_args(errormsg).at_least().once()
		assert Create.do(fake_self) == expected'''
	class TestDo(object):

		def test_do_no_name_given(self):
			fake_self = flexmock(args=['zero'])
			assert Create.do(fake_self) == -1

		def test_do_default_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one'],occam=fake_occam)
			fake_occam.should_receive('addWorkset').with_args('./one','one').once()
			assert Create.do(fake_self) == None

		def test_do_name_and_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one','two'],occam=fake_occam)
			fake_occam.should_receive('addWorkset').with_args('two','one').once()
			assert Create.do(fake_self) == None

		def test_do_extra_args(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one','two','three','four'],occam=fake_occam)
			fake_occam.should_receive('addWorkset').with_args('two','one').once()
			assert Create.do(fake_self) == None