from flexmock import flexmock
import pytest

#from lib.log            	import Log
from lib.occam             	import Occam
from lib.commands.append 	import Append
from lib.object 			import Object

class TestAppend(object):
	def test_initParser(self):
		assert Append.initParser(1) == 1

	class TestDo(object):

		def test_do_not_enough_args(self):
			fake_self = flexmock(args=['zero','one'])
			assert Append.do(fake_self) == -1

		def test_do_3_args_value_exists_in_list_of_key(self):
			fake_occam=flexmock()
			fake_self = flexmock(args=['zero','one','two'],occam=fake_occam)
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({'one':['two']})
			assert Append.do(fake_self) == 0

		def test_do_3_args_value_not_exists_in_list_of_key(self):
			fake_occam=flexmock()
			fake_self = flexmock(args=['zero','one','two'],occam=fake_occam)
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('updateObject').with_args({'one':['two']},'appending value to one')
			assert Append.do(fake_self) == 0

		def test_do_more_than_3_args_and_odd_number(self):
			fake_self = flexmock(args=['zero','one','two','three','four'])
			assert Append.do(fake_self) == -1

		def test_do_more_than_3_args_value_exists_in_list_of_key(self):
			fake_occam=flexmock()
			fake_self = flexmock(args=['zero','one','two','three'],occam=fake_occam)
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({'one':[{'two':'three'}]})
			assert Append.do(fake_self) == 0

		def test_do_more_than_3_args_value_not_exists_in_list_of_key(self):
			fake_occam=flexmock()
			fake_self = flexmock(args=['zero','one','two','three'],occam=fake_occam)
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('updateObject').with_args({'one':[{'two':'three'}]},'appending value to one')
			assert Append.do(fake_self) == 0