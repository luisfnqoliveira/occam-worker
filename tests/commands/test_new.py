from flexmock import flexmock
import pytest

from lib.commands.new 	import New
from lib.person     import Person

import getpass     # For entering a password for person create
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

import os

class TestNew(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert New.initParser(fake) == fake

	class TestDo(object):

		def test_do_1_arg_no_type_of_object_given(self):
			with pytest.raises(Exception) as ex:
				fake_options = None
				fake_args=['zero']
				fake_occam=None
				New(fake_options,fake_args,fake_occam).do()
				assert 'no type of object given' in str(ex.value)
		def test_do_2_args_no_name_given(self):
			with pytest.raises(Exception) as ex:
				fake_options = None
				fake_args=['zero','one']
				fake_occam=None
				New(fake_options,fake_args,fake_occam).do()
				assert 'no name given' in str(ex.value)

		@pytest.mark.parametrize("author_uuid",[
			('AUUID'),
			(None)
		])
		def test_do_3_args_type_is_workset_to_object_in_options_author_uuid_in_or_not_in_options_successful_create(self,author_uuid):
			fake_options = flexmock(author_uuid=author_uuid,revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['zero','workset','two']
			fake_occam=flexmock()

			fake_obj=flexmock(objectInfo=lambda:{'type':'TYPE','name':'NAME'})
			#fake_workset=flexmock(objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_obj,'.')).once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('.').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','workset-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()


			os.path.should_receive('join').with_args('RPATH','two').and_return('JJPATH').once()
			os.path.should_receive('realpath').with_args('JJPATH').and_return('OOPATH').once()

			fake_workset=flexmock(path='WPATH',objectInfo=lambda:{'id':'ID'})
			fake_person=flexmock(objectInfo=lambda:{'name':'NAME'})

			fake_occam.should_receive('addWorkset').with_args('OOPATH','two').and_return(fake_workset).once()
			
			if author_uuid:
				fake_occam.should_receive('objectPath').with_args(author_uuid).and_return('OOOPATH').once()
				flexmock(Person)
				Person.should_receive('__init__').with_args('OOOPATH',occam=fake_occam).once()
				Person.should_receive('objectInfo').and_return({'name':'NAME'}).once()
				fake_occam.should_receive('createLocalLink').with_args('WPATH',fake_workset,Person).once()
				fake_workset.should_receive('addAuthor').with_args(Person).once()
			else:
				fake_occam.should_receive('currentPerson').and_return(fake_person).once()
				fake_occam.should_receive('createLocalLink').with_args('WPATH',fake_workset,fake_person).once()
				fake_workset.should_receive('addAuthor').with_args(fake_person).once()
			assert New(fake_options,fake_args,fake_occam).do() == 0

		@pytest.mark.parametrize("password,received",[
			('PASSWORD','PASSWORD'),
			(None,'GEN_PASS')
		])
		def test_do_4_args_given_path_type_is_person_to_object_in_options_password_in_or_not_in_options_successful_create(self,password,received):
			fake_options = flexmock(password=password,revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['zero','person','two','three']
			fake_occam=flexmock()

			fake_obj=flexmock(objectInfo=lambda:{'type':'TYPE','name':'NAME'})
			#fake_workset=flexmock(objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_obj,'.')).once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('.').and_return('RPATH').once()
			os.path.should_receive('join').with_args('three','person-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			flexmock(getpass)

			getpass.should_receive('getpass').with_args('\npassword for two: ').and_return('GEN_PASS')

			fake_occam.should_receive('addPerson').with_args('two').and_return(flexmock(objectInfo=lambda:{'id':'ID'})).once()
			fake_occam.should_receive('addAuthentication').with_args('ID',received).once()

			assert New(fake_options,fake_args,fake_occam).do() == 0


		def test_do_3_args_internal_in_options_type_is_collaborator_cannot_find_the_person(self):
			fake_options = flexmock(internal='internal',revision=None,to_object=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			flexmock(tempfile)
			tempfile.should_receive('mkdtemp').with_args(prefix="occam-").and_return('TPATH').once()
			flexmock(os.path).should_receive('realpath').with_args('TPATH').and_return('PATH').once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('PATH').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','collaborator-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='')

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			fake_first=flexmock()
			fake_occam.should_receive('searchPeople').with_args(keyword = 'two').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()

			assert New(fake_options,fake_args,fake_occam).do() == -1


		def test_do_3_args_internal_in_options_type_is_collaborator_addCollaborator_failure(self):
			fake_options = flexmock(internal='internal',revision=None,to_object=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			flexmock(tempfile)
			tempfile.should_receive('mkdtemp').with_args(prefix="occam-").and_return('TPATH').once()
			flexmock(os.path).should_receive('realpath').with_args('TPATH').and_return('PATH').once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('PATH').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','collaborator-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='')

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			fake_first=flexmock()
			fake_occam.should_receive('searchPeople').with_args(keyword = 'two').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(path='PPATH')).once()
			flexmock(Person)
			Person.should_receive('__init__').with_args('PPATH').once()
			fake_workset.should_receive('addCollaborator').with_args(Person).replace_with(lambda:blah()).once()

			assert New(fake_options,fake_args,fake_occam).do() == -1

		def test_do_3_args_internal_in_options_type_is_collaborator_addCollaborator_success(self):
			fake_options = flexmock(internal='internal',revision=None,to_object=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			flexmock(tempfile)
			tempfile.should_receive('mkdtemp').with_args(prefix="occam-").and_return('TPATH').once()
			flexmock(os.path).should_receive('realpath').with_args('TPATH').and_return('PATH').once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('PATH').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','collaborator-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='')

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			fake_first=flexmock()
			fake_occam.should_receive('searchPeople').with_args(keyword = 'two').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(path='PPATH')).once()
			flexmock(Person)
			Person.should_receive('__init__').with_args('PPATH').once()
			fake_workset.should_receive('addCollaborator').with_args(Person).and_return(True).once()

			assert New(fake_options,fake_args,fake_occam).do() == 0

		def test_do_3_args_internal_in_options_type_is_author_addAuthor_success(self):
			fake_options = flexmock(internal='internal',revision=None,to_object=None)
			fake_args=['zero','author','two']
			fake_occam=flexmock()
			flexmock(tempfile)
			tempfile.should_receive('mkdtemp').with_args(prefix="occam-").and_return('TPATH').once()
			flexmock(os.path).should_receive('realpath').with_args('TPATH').and_return('PATH').once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('PATH').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','author-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='')

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			fake_first=flexmock()
			fake_occam.should_receive('searchPeople').with_args(keyword = 'two').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(path='PPATH')).once()
			flexmock(Person)
			Person.should_receive('__init__').with_args('PPATH').once()
			fake_workset.should_receive('addAuthor').with_args(Person).and_return(True).once()

			assert New(fake_options,fake_args,fake_occam).do() == 0


		def test_do_3_args_internal_in_options_type_is_other_addObject_success(self):
			fake_options = flexmock(internal='internal',revision=None,to_object=None)
			fake_args=['zero','other','two']
			fake_occam=flexmock()
			flexmock(tempfile)
			tempfile.should_receive('mkdtemp').with_args(prefix="occam-").and_return('TPATH').once()
			flexmock(os.path).should_receive('realpath').with_args('TPATH').and_return('PATH').once()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('PATH').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','other-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME','type':'workset'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='',git=flexmock(head=lambda:None))

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			#fake_first=flexmock()
			#fake_occam.should_receive('searchPeople').with_args(keyword = 'two').and_return(fake_first).once()
			#fake_first.should_receive('first').and_return(flexmock(path='PPATH')).once()
			#flexmock(Person)
			#Person.should_receive('__init__').with_args('PPATH').once()
			#fake_workset.should_receive('addAuthor').with_args(Person).and_return(True).once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('OPATH').and_return(False).once()

			fake_new_obj=flexmock(objectInfo=lambda:{'id':'ID'})

			fake_obj.should_receive('addObject').with_args('two','other').and_return(fake_new_obj).once()

			flexmock(shutil)
			shutil.should_receive('rmtree').with_args('RPATH').once()

			assert New(fake_options,fake_args,fake_occam).do() == 0


		def test_do_3_args_nothing_in_options_type_is_other_directory_already_exists(self):
			fake_options = flexmock(internal=None,revision=None,to_object=None)
			fake_args=['zero','other','two']
			fake_occam=flexmock()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('.').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','other-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME','type':'workset'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='')

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('OPATH').and_return(True).once()

			assert New(fake_options,fake_args,fake_occam).do() == -1


		def test_do_3_args_nothing_in_options_type_is_group_addGroup_success(self):
			fake_options = flexmock(internal=None,revision=None,to_object=None)
			fake_args=['zero','group','two']
			fake_occam=flexmock()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('.').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','group-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME','type':'workset'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='',git=flexmock(head=lambda:None))

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('OPATH').and_return(False).once()

			fake_new_obj=flexmock(objectInfo=lambda:{'id':'ID'})
			fake_obj.should_receive('addGroup').with_args('two').and_return(fake_new_obj).once()

			assert New(fake_options,fake_args,fake_occam).do() == 0


		def test_do_3_args_nothing_in_options_type_is_experiment_addExperiment_success(self):
			fake_options = flexmock(internal=None,revision=None,to_object=None)
			fake_args=['zero','experiment','two']
			fake_occam=flexmock()

			flexmock(os.path)
			os.path.should_receive('realpath').with_args('.').and_return('RPATH').once()
			os.path.should_receive('join').with_args('RPATH','experiment-two').and_return('JPATH').once()
			os.path.should_receive('realpath').with_args('JPATH').and_return('OPATH').once()

			fake_workset = flexmock(objectInfo=lambda:{'name':'NAME','type':'workset'})

			fake_occam.should_receive('objectAt').with_args('RPATH', searchFor='workset').and_return(fake_workset).once()

			fake_obj = flexmock(root='',git=flexmock(head=lambda:None))

			fake_occam.should_receive('objectAt').with_args('RPATH', recurse=True).and_return(fake_obj).at_least().once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('OPATH').and_return(False).once()

			fake_new_obj=flexmock(objectInfo=lambda:{'id':'ID'})
			fake_obj.should_receive('addExperiment').with_args('two','RPATH').and_return(fake_new_obj).once()

			assert New(fake_options,fake_args,fake_occam).do() == 0