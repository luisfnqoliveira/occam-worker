from flexmock import flexmock
import pytest

from lib.commands.view 		import View
from lib.log            	import Log
from lib.object         import Object
import re

class TestView(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert View.initParser(fake) == fake



	class TestDo(object):

		def test_do_1_arg_url_not_in_options(self):
			fake_options = flexmock(revision=None,url=None)
			fake_args=['zero']
			fake_occam = None
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',revision='HEAD',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({}).once()

			assert View(fake_options,fake_args,fake_occam).do() == None

		def test_do_too_many_args(self):
			fake_options = flexmock(revision='REV')
			fake_args=['zero','one','two','three']
			fake_occam = None
			assert View(fake_options,fake_args,fake_occam).do() == -1

		def test_do_default_revision_url_in_options_key_not_in_obj_info(self):
			fake_options = flexmock(revision=None,url='URL')
			fake_args=['zero','one','two']
			fake_occam = None

			flexmock(Object)
			Object.should_receive('__init__').with_args('URL',revision='HEAD',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({}).once()
			
			assert View(fake_options,fake_args,fake_occam).do() == None

		def test_do_default_revision_url_in_options_match_not_a_list(self):
			fake_options = flexmock(revision=None,url='URL')
			fake_args=['0','1','2']
			fake_occam = None

			flexmock(Object)
			Object.should_receive('__init__').with_args('URL',revision='HEAD',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({'1':'not_a_list'}).once()
			flexmock(re)
			re.should_receive('match').and_return(flexmock(group=lambda x:str(x))).once()

			assert View(fake_options,fake_args,fake_occam).do() == -1

		def test_do_default_revision_url_not_in_options_match_item_length_invalid(self):
			fake_options = flexmock(revision=None,url=None)
			fake_args=['0','1','2']
			fake_occam = flexmock()

			flexmock(Object)
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='1').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(uid='OID')).once()
			
			fake_occam.should_receive('objectPath').with_args('OID').and_return('OP').once()
			Object.should_receive('__init__').with_args('OP',revision='HEAD',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({'1':[]}).once()

			flexmock(re)
			re.should_receive('match').and_return(flexmock(group=lambda x:str(x))).once()

			assert View(fake_options,fake_args,fake_occam).do() == -1


		@pytest.mark.parametrize("typ",[
			(['X','X','X']),
			([1,1,1])
		])
		def test_do_default_revision_url_not_in_options_match_item_length_valid_type_str_or_not_str(self,typ):
			fake_options = flexmock(revision=None,url=None)
			fake_args=['0','1','2']
			fake_occam = flexmock()

			flexmock(Object)
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='1').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(uid='OID')).once()
			
			fake_occam.should_receive('objectPath').with_args('OID').and_return('OP').once()
			Object.should_receive('__init__').with_args('OP',revision='HEAD',occam=fake_occam).once()
			Object.should_receive('objectInfo').and_return({'1':typ}).once()

			flexmock(re)
			re.should_receive('match').and_return(flexmock(group=lambda x:str(x))).once()

			assert View(fake_options,fake_args,fake_occam).do() == None


	'''
	@pytest.mark.parametrize("args,url,group1,group2,info,result,warning,outputBlock,output,objPath,header",[#,auth,exists,result", [
		#(['zero'],None,None,-1),
		#(['zero','one'],False,False,-1),
		#(['zero','one'],True,True,None)
		(['zero','one','two','three'],None,None,None,None,-1,False,False,False,False,False),
		(['zero','one','two'],'URL','1','2',{'3':'3'},None,True,False,False,False,False),
		(['zero','one','two'],'URL','1','2',{'1':'1'},-1,False,False,False,False,False),
		(['zero','one','two'],'URL','1','2',{'1':['1']},-1,False,False,False,False,False),
		(['zero','one','two'],'URL','1','2',{'1':['1','11','111']},None,False,True,False,False,False),
		(['zero','one','two'],'URL','1','2',{'1':['1','11',111]},None,False,False,True,False,False),
		(['zero','one'],None,None,None,None,None,False,False,True,True,True),
		#(['zero']),
		#(['zero','one']),
		#(['zero','one','two']),

	])
	def test_do(self,args,url,group1,group2,info,result,warning,outputBlock,output,objPath,header):
		fake_occam = flexmock()
		fake_self=flexmock(occam=fake_occam,args=args,options=flexmock(revision='REV',url=url))
		flexmock(Log)
		if result == -1:
			Log.should_receive('error').once()
		if warning:
			Log.should_receive('warning').once()
		if outputBlock:
			Log.should_receive('outputBlock').once()
		if output:
			Log.should_receive('output').once()
		if objPath:
			fake_occam.should_receive('searchObjects.first').and_return(flexmock(uid='UID')).once()
			fake_occam.should_receive('objectPath').once()
		if header:
			Log.should_receive('header').once()
		flexmock(Object)
		Object.new_instances(flexmock(objectInfo=lambda :info))
		flexmock(re)
		re.should_receive('match').and_return(flexmock(group=lambda x: group1 if x==1 else group2))

		assert View.do(fake_self) == result'''