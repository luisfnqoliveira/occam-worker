from flexmock import flexmock
import pytest

from lib.commands.search 	import Search
from lib.log            	import Log 
from lib.object 			import Object
from lib.db         		import DB

class TestSearch(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Search.initParser(fake) == fake

	class TestDo(object):
		def test_do_None_as_key_object_type_person(self):
			fake_occam = flexmock()
			fake_self = flexmock(	args=['zero'],
									options=flexmock(	object_type='person',
														name='NAME',
														uuid='UUID'),
									occam=fake_occam)
			fake_objs = flexmock(name="NAME",uid="UID",count=lambda :10)
			fake_occam.should_receive('searchPeople').with_args(name='NAME',uuid='UUID',keyword=None).and_return(fake_objs).once()
			assert Search.do(fake_self) == 0

		def test_do_real_key_object_type_person(self):
			fake_occam = flexmock()
			fake_self = flexmock(	args=['zero','one'],
									options=flexmock(	object_type='person',
														name='NAME',
														uuid='UUID'),
									occam=fake_occam)
			fake_objs = flexmock(name="NAME",uid="UID",count=lambda :10)
			fake_occam.should_receive('searchPeople').with_args(name='NAME',uuid='UUID',keyword='one').and_return(fake_objs).once()
			assert Search.do(fake_self) == 0

		def test_do_object_type_not_person(self):
			fake_occam = flexmock()
			fake_self = flexmock(	args=['zero'],
									options=flexmock(	object_type='not_person',
														name='NAME',
														uuid='UUID'),
									occam=fake_occam)
			fake_objs = flexmock(name="NAME",uid="UID",count=lambda :10)
			fake_occam.should_receive('searchObjects').with_args(name='NAME',uuid='UUID',object_type='not_person').and_return(fake_objs).once()
			assert Search.do(fake_self) == 0
		'''
		@pytest.mark.parametrize("args,options,method,expected", [
			(['zero'],flexmock(object_type="person",name="NAME",uuid="UUID"),'searchPeople',-1),
			(['zero','one'],flexmock(object_type="other",name="NAME",uuid="UUID"),'searchObjects', None),
			#(['zero','one','two'], None, None),
			#(['zero','one','two','three','four'], None, None),
			#(['zero','one','two','three'], 0),
			#(['zero','one','two','three','four','five'], 0)
		])
		def test_do(self,args,options,method,expected):

			flexmock(Log)
			Log.should_receive('header').once()

			fake_occam = flexmock()

			fake_self=flexmock(args=args,options = options,occam=fake_occam)
			fake_objs = flexmock(name="NAME",uid="UID",count=lambda :10)
			fake_occam.should_receive(method).and_return(fake_objs).once()
			Log.should_receive('output').at_least.once()
			Log.should_receive('write').with_args("found 10 object(s)")
			Search.do(fake_self)'''