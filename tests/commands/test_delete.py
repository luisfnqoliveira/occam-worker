from flexmock import flexmock
import pytest

from lib.commands.delete 	import Delete
from lib.log            	import Log 
from lib.object 			import Object
from lib.db         		import DB

class TestDelete(object):
	def test_initParser(self):
		assert Delete.initParser(1) == 1

	class TestDo(object):
		def test_do_no_uuid(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero'],occam=fake_occam)
			fake_first = flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid=None).and_return(fake_first)
			fake_first.should_receive('first').and_return(None).once()
			assert Delete.do(fake_self) == -1

		def test_do_successful_delete(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one'],occam=fake_occam)
			fake_first = flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='one').and_return(fake_first)
			fake_first.should_receive('first').and_return(flexmock(name='NAME',path='PATH')).once()
			flexmock(Object)
			Object.should_receive('__init__').with_args('PATH',occam=fake_occam).once()
			fake_occam.should_receive('deleteObject').once()
			assert Delete.do(fake_self) == None

		'''@pytest.mark.parametrize("args,errormsg,searchresult,expected", [
			(['zero'],'Cannot find None',None,-1),
			(['zero','one'],None,flexmock(name='NAME',path='PATH'), None),
			#(['zero','one','two'], None, None),
			#(['zero','one','two','three','four'], None, None),
			#(['zero','one','two','three'], 0),
			#(['zero','one','two','three','four','five'], 0)
		])
		def test_do(self,args,errormsg,searchresult,expected):
			flexmock(Log)
			Log.should_receive('header').once()
			fake_occam = flexmock()
			fake_occam.should_receive('searchObjects.first').and_return(searchresult)
			#.replace_with(lambda uuid:flexmock(first=lambda : None) if uuid is None else flexmock(first= lambda : flexmock(name='NAME',path='PATH')))

			fake_self=flexmock(args=args,occam=fake_occam)

			if not errormsg is None:
				Log.should_receive('error').with_args(errormsg).once()
			if len(args) > 1:
				Log.should_receive('write').once()
				flexmock(Object)
				Object.should_receive('__init__').once()
				fake_occam.should_receive('deleteObject').once()
			assert Delete.do(fake_self) == expected'''

		def test_force_deleteObject_exception(self):
			#flexmock(Log)
			#Log.should_receive('header').once()
			fake_occam = flexmock()
			fake_obj = flexmock(name='NAME',path='PATH')
			fake_first = flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='one').and_return(fake_first)
			fake_first.should_receive('first').and_return(fake_obj).once()

			fake_self=flexmock(args=['zero','one'],occam=fake_occam)
			#Log.should_receive('write').once()
			flexmock(Object)
			Object.should_receive('__init__').with_args('PATH',occam=fake_occam).once()
			fake_occam.should_receive('deleteObject').replace_with(lambda : blah())

			flexmock(DB)
			fake_session = flexmock(delete= lambda x:x,commit=lambda :None)
			DB.should_receive('session').and_return(fake_session)
			fake_session.should_receive('delete').with_args(fake_obj).once()
			fake_session.should_receive('commit').once()

			Delete.do(fake_self)