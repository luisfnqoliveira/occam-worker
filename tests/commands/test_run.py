from flexmock import flexmock
import pytest

from tests.commands.helper import options
from tests import helper

from lib.commands.run   import Run

import re
import os

class TestRun:
  class TestInitParser:
    @pytest.yield_fixture(autouse=True)
    def setup(self):
      # Hack our mock to keep track of the arguments added
      keys = {}

      def f(a, b, **kw):
        keys[a] = True
        keys[b] = True

      self.fake = flexmock(add_option=f, keys=keys)
      yield

    def test_should_accept_dispatch_argument(self):
      Run.initParser(self.fake)
      assert '-d' in self.fake.keys and '--dispatch' in self.fake.keys

    def test_should_accept_revision_argument(self):
      Run.initParser(self.fake)
      assert '-r' in self.fake.keys and '--revision' in self.fake.keys

    def test_should_accept_to_argument(self):
      Run.initParser(self.fake)
      assert '-t' in self.fake.keys and '--to' in self.fake.keys

    def test_should_accept_within_argument(self):
      Run.initParser(self.fake)
      assert '-w' in self.fake.keys and '--within' in self.fake.keys

    def test_should_accept_within_revision_argument(self):
      Run.initParser(self.fake)
      assert '-W' in self.fake.keys and '--within-revision' in self.fake.keys

    def test_should_accept_as_argument(self):
      Run.initParser(self.fake)
      assert '-a' in self.fake.keys and '--as' in self.fake.keys

    def test_should_return_passed_parser(self):
      assert Run.initParser(self.fake) == self.fake

  class TestDetermineRevision:
    def test_uses_revision_option_argument(self):
      run = Run(options({"revision": "REVISION"}), [], None)
      assert run.determineRevision() == "REVISION"

    def test_should_return_None_when_argument_is_not_given(self):
      run = Run(options({}), [], None)
      assert run.determineRevision() == None

  class TestDetermineWorkset:
    def test_should_simply_call_workset_on_object(self):
      run = Run(options({}), [], None)
      object = flexmock()
      object.should_receive('workset')
      run.determineWorkset(object)

    def test_should_return_workset(self):
      run = Run(options({}), [], None)
      object = flexmock(workset=lambda *a:'WORKSET')
      assert run.determineWorkset(object) == "WORKSET"

  class TestDetermineObject:
    def test_should_find_object_at_cwd_when_no_object_argument(self):
      occam = helper.Occam()
      occam.should_receive('objectAt').with_args('.', revision="REVISION")
      run = Run(options({}), [], occam)
      run.determineObject("REVISION")

    def test_should_return_object_at_cwd_when_no_object_argument(self):
      occam = helper.Occam()
      occam.objectAt = lambda *a, **kw:"OBJECT"
      run = Run(options({}), [], occam)
      assert run.determineObject("REVISION") == "OBJECT"

    def test_should_request_object_at_given_revision_when_no_object_argument(self):
      occam = helper.Occam()
      occam.should_receive('objectAt').with_args('.', revision="REVISION")
      run = Run(options({}), [], occam)
      run.determineObject("REVISION")

    def test_should_retrieve_the_given_object(self):
      some_uuid = helper.uuid()
      occam = helper.Occam([some_uuid])

      run = Run(options({"to_object": some_uuid}), [], occam)
      assert run.determineObject("REVISION").uuid == some_uuid

    def test_should_clone_the_given_object(self):
      some_uuid = helper.uuid()
      occam = helper.Occam([some_uuid])
      occam.should_receive('temporaryClone').at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid}), [], occam)
      run.determineObject("REVISION")

    def test_should_clone_the_given_object(self):
      some_uuid = helper.uuid()
      occam = helper.Occam([some_uuid])
      occam.should_receive('temporaryClone').with_args(some_uuid, None, None, None).at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid}), [], occam)
      run.determineObject(None)

    def test_should_clone_the_given_object_at_the_given_revision(self):
      some_uuid = helper.uuid()
      occam = helper.Occam([some_uuid])
      occam.should_receive('temporaryClone').with_args(some_uuid, "REVISION", None, None).at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid}), [], occam)
      run.determineObject("REVISION")

    def test_should_clone_the_given_object_with_the_given_workset(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([some_uuid, workset_uuid])
      occam.should_receive('temporaryClone').with_args(some_uuid, "REVISION", workset_uuid, None).at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid}), [], occam)
      run.determineObject("REVISION")

    def test_should_clone_the_given_object_with_the_given_workset_and_revision(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([some_uuid, workset_uuid])
      occam.should_receive('temporaryClone').with_args(some_uuid, "REVISION", workset_uuid, "WORKSET_REVISION").at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)
      run.determineObject("REVISION")

    def test_should_return_the_clone_of_the_given_object(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([some_uuid, workset_uuid])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      assert run.determineObject(None).uuid == some_uuid

    def test_should_return_the_clone_of_the_given_object_at_given_revision(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([some_uuid, workset_uuid])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      assert run.determineObject("REVISION").revision == "REVISION"

    def test_should_return_None_if_object_is_not_found(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([workset_uuid])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      assert run.determineObject("REVISION") == None

    def test_should_return_None_if_object_cannot_be_cloned(self):
      some_uuid = helper.uuid()
      workset_uuid = helper.uuid()
      occam = helper.Occam([some_uuid, workset_uuid])

      occam.should_receive('temporaryClone').at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": some_uuid,
                         "within_object": workset_uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      assert run.determineObject("REVISION") == None

    def test_should_special_case_task_objects_and_use_their_experiment(self):
      experiment = helper.Object(type='experiment')
      obj = helper.Task(experiment, "EXP_REVISION")
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, obj, workset])

      occam.should_receive('temporaryClone').with_args(experiment.uuid, "EXP_REVISION", workset.uuid, "WORKSET_REVISION").at_least().once().and_return([None, None, None])

      run = Run(options({"to_object": obj.uuid,
                         "within_object": workset.uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      run.determineObject("REVISION")

    def test_should_special_case_task_objects_and_return_their_experiment(self):
      experiment = helper.Object(type='experiment')
      obj = helper.Task(experiment, "REVISION")
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, obj, workset])

      run = Run(options({"to_object": obj.uuid,
                         "within_object": workset.uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      assert run.determineObject("REVISION").uuid == experiment.uuid

    def test_should_special_case_task_objects_and_clone_them_within_experiment(self):
      experiment = helper.Object(type='experiment')
      obj = helper.Task(experiment, "REVISION")
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, obj, workset])

      obj.should_receive('clone').with_args("PATH", "REVISION")

      run = Run(options({"to_object": obj.uuid,
                         "within_object": workset.uuid,
                         "within_revision": "WORKSET_REVISION"}), [], occam)

      run.determineObject("REVISION")

  class TestRakeOutputList:
    def test_should_not_append_outputs_for_a_nonrunning_object(self):
      occam = helper.Occam([])

      run = Run(options({}), [], occam)

      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   "UUID",
        "index": 0
      }, False)

      assert len(outputs) == 0

    def test_should_append_output_for_stdout_when_command_is_run(self):
      occam = helper.Occam([])

      run = Run(options({}), [], occam)

      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   "UUID",
        "index": 0,
        "run": {
        }
      }, False)

      truth = False
      for output in outputs:
        if output['file'].endswith("run-out.raw") and output['type'] == 'text/plain':
          truth = True

      assert truth

    def test_should_append_output_for_stderr_when_command_is_run(self):
      occam = helper.Occam([])

      run = Run(options({}), [], occam)

      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   "UUID",
        "index": 0,
        "run": {
        }
      }, False)

      truth = False
      for output in outputs:
        if output['file'].endswith("run-err.raw") and output['type'] == 'text/plain':
          truth = True

      assert truth

    def test_should_append_output_for_stdout_when_finish_command_is_run(self):
      occam = helper.Occam([])

      run = Run(options({}), [], occam)

      # Pretend the object made an 'initialize' file
      # with a 'finish' section:
      file_mock = helper.open(content="INIT_JSON")
      json_mock = helper.json(file_mock, {
        "finish": "FINISH_COMMAND"
      })

      # Ensure that we 'see' the initialization file
      flexmock(os.path)
      os.path.should_receive('exists').with_args("VM_PATH/initializations/object-UUID-0/run.json").and_return(True)

      # Then, give it a valid object:
      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   "UUID",
        "index": 0,
        "run": {
        }
      }, False)

      truth = False
      for output in outputs:
        if output['file'].endswith("run-finish-err.raw") and output['type'] == 'text/plain':
          truth = True

      assert truth

    def test_should_append_output_for_stderr_when_finish_command_is_run(self):
      occam = helper.Occam([])

      run = Run(options({}), [], occam)

      # Pretend the object made an 'initialize' file
      # with a 'finish' section:
      file_mock  = helper.open(content="INIT_JSON")
      json_mock = helper.json(file_mock, {
        "finish": "FINISH_COMMAND"
      })

      # Ensure that we 'see' the initialization file
      helper.exists("VM_PATH/initializations/object-UUID-0/run.json")

      # Then, give it a valid object:
      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   "UUID",
        "index": 0,
        "run": {
        }
      }, False)

      truth = False
      for output in outputs:
        if output['file'].endswith("run-finish-out.raw") and output['type'] == 'text/plain':
          truth = True

      assert truth

    def test_should_append_output_for_each_output_described_by_running_object(self):
      running_object = helper.Object(name='running_object', type='object', revision="abcd")
      occam = helper.Occam([running_object])

      run = Run(options({}), [], occam)

      # Then, give it a valid object:
      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   running_object.uuid,
        "revision": "abcd",
        "index": 0,
        "outputs": [
          {
            'name': "An output for a thing",
            'type': 'text/plain',
            'file': 'our_output.txt'
          },
          {
            'name': "Another output",
            'type': 'image/png',
            'file': 'our_output.png'
          }
        ],
        "run": {
        }
      }, False)

      outputs_found = 0
      for output in outputs:
        if output['file'].endswith("our_output.txt") and output['type'] == 'text/plain':
          outputs_found += 1
        if output['file'].endswith("our_output.png") and output['type'] == 'image/png':
          outputs_found += 1

      assert outputs_found == 2

    def test_should_retain_output_information_given_by_running_object(self):
      running_object = helper.Object(name='running_object', type='object', revision="abcd")
      occam = helper.Occam([running_object])

      run = Run(options({}), [], occam)

      # Then, give it a valid object:
      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   running_object.uuid,
        "revision": "abcd",
        "index": 0,
        "outputs": [
          {
            'name': "An output for a thing",
            'type': 'text/plain',
            'file': 'our_output.txt',
            'bogus!!': 'should be kept!'
          },
          {
            'name': "Another output",
            'type': 'image/png',
            'file': 'our_output.png',
            'bogus!!': 'should be kept!!!'
          }
        ],
        "run": {
        }
      }, False)

      outputs_found = 0
      for output in outputs:
        if 'bogus!!' in output:
          outputs_found += 1

      assert outputs_found == 2

    def test_should_add_revision_to_build_object_of_new_output_object(self):
      running_object = helper.Object(name='running_object', type='object', revision="abcd")
      occam = helper.Occam([running_object])

      run = Run(options({}), [], occam)

      # Then, give it a valid object:
      outputs = run.rakeOutputList("VM_PATH", {
        "type": "object",
        "name": "running_object",
        "id":   running_object.uuid,
        "revision": "abcd",
        "index": 0,
        "outputs": [
          {
            'name': "An output for a thing",
            'type': 'text/plain',
            'file': 'our_output.txt',
            'build': {
              'using': {
                'type': 'object',
                'name': 'running_object',
                'id':   running_object.uuid
              }
            }
          }
        ],
        "run": {
        }
      }, False)

      outputs_found = 0
      for output in outputs:
        if 'build' in output:
          if 'using' in output['build']:
            if output['build']['using']['revision'] == 'abcd':
              outputs_found += 1

      assert outputs_found == 1

  class TestCaptureOutput:
    def test_foo(self):
      pass

  class TestRunVMs:
    # Fixtures hate me
    def setup(self):
      self.mock = helper.subprocess()
      helper.setup_directory({
        "runs": {
          "0": {
            "jobs": {
              "0": {
                "vms": {
                  "0": {
      } } } } } } })


    def test_should_call_out_to_occam_to_run_each_vm(self):
      self.setup()

      experiment = helper.Object(type='experiment')
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, workset])

      run = Run(options({}), [], occam)

      job_path = os.path.join("runs", "0", "jobs", "0")
      run.runVMs(job_path, workset, experiment)

      popen_args = self.mock.process.call_args[0][0]

      assert popen_args[0] == "occam" and popen_args[1] == "run"

    def test_should_use_an_absolute_vm_path_to_run_each_vm(self):
      self.setup()

      experiment = helper.Object(type='experiment')
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, workset])

      run = Run(options({}), [], occam)

      job_path = os.path.join("runs", "0", "jobs", "0")
      run.runVMs(job_path, workset, experiment)

      popen_cwd  = self.mock.process.call_args[1].get('cwd')

      assert popen_cwd == os.path.realpath(os.path.join(job_path, "vms", "0"))

    def test_should_create_a_Run_record_when_dispatching_a_vm(self):
      self.setup()

      experiment = helper.Object(type='experiment')
      workset = helper.Object(type='workset')

      occam = helper.Occam([experiment, workset])

      run = Run(options({"dispatch": True}), [], occam)

      job_path = os.path.join("runs", "0", "jobs", "0")
      run.runVMs(job_path, workset, experiment)

      popen_cwd  = mock.process.call_args[1].get('cwd')

      assert popen_cwd == os.path.realpath(os.path.join(job_path, "vms", "0"))

  class TestRunTask:
    def test_foo(self):
      pass

  class Do(object):
    def test_do_to_object_in_options_obj_type_is_experiment_run_finished(self):
      fake_options = flexmock(revision='REV',to_object='TOOBJ',within_object=None,within_revision=None,dispatch='DISPATCH')
      fake_args=['zero']
      fake_occam=flexmock()

      fake_obj=flexmock(objectInfo=lambda:{'id':'ID','type':'occam/task','generator':{'id':'GID','revision':'GREV'}})
      fake_occam.should_receive('retrieve').with_args('TOOBJ').and_return(fake_obj).once()
      fake_workset=flexmock(revision='WREV')
      fake_experiment_info={'id':'EID','name':'NAME','type':'experiment'}
      fake_experiment=flexmock(revision='EREV',path='EPATH',objectInfo=lambda:fake_experiment_info)
      fake_occam.should_receive('temporaryClone').with_args('GID','GREV',None,None).and_return((fake_workset,fake_experiment,'WPATH')).once()
      flexmock(os.path)
      os.path.should_receive('join').with_args('EPATH','ID').and_return('GPATH').once()
      fake_obj.should_receive('clone').with_args('GPATH','REV').once()

      flexmock(os.path)
      os.path.should_receive('join').with_args('GPATH','object.json').and_return('JJSON').once()
      os.path.should_receive('exists').with_args('JJSON').and_return(True).once()
      flexmock(json)
      #119
      fake_obj_info={'id':'ID','type':'experiment','outputs':[{'file':{}}]}
      flexmock(sys.modules['builtins']).should_receive('open').with_args('JJSON').and_return('JJJSON').once()
      json.should_receive('load').with_args('JJJSON').and_return(fake_obj_info).once()

      flexmock(RunBuilder)
      RunBuilder.should_receive('__init__').with_args(fake_args,fake_options,'GPATH',fake_occam)
      RunBuilder.should_receive('do').and_return([1]).once()

      fake_first=flexmock()
      fake_workset_info={'id':'WID'}
      fake_workset.should_receive('objectInfo').and_return(fake_workset_info)
      fake_occam.should_receive('searchObjects').with_args(uuid='WID').and_return(fake_first).once()
      fake_first.should_receive('first').and_return(flexmock(id='WDBID')).once()
      fake_first_2=flexmock()
      fake_occam.should_receive('searchObjects').with_args(uuid='EID').and_return(fake_first_2).once()
      fake_first_2.should_receive('first').and_return(flexmock(id='EDBID')).once()

      flexmock(TaskBuilder)
      TaskBuilder.should_receive('__init__').with_args(fake_args,fake_options,'GPATH',fake_occam).once()
      TaskBuilder.should_receive('do').with_args(runPaths=[1]).and_return([10]).once()

      os.path.should_receive('join').with_args('GPATH','vms').and_return('VMSPATH').once()

      os.path.should_receive('exists').with_args('VMSPATH').and_return(True).once
      flexmock(os)
      os.should_receive('listdir').with_args('VMSPATH').and_return(['VMPATH']).once()
      os.path.should_receive('join').with_args('VMSPATH','VMPATH').and_return('JVPATH').once()
      os.path.should_receive('realpath').with_args('JVPATH').and_return('VMPATH').once()
      os.path.should_receive('isdir').with_args('VMPATH').and_return(True).once()
      fake_paf=flexmock()
      os.path.should_receive('splitdrive').with_args('VMPATH').and_return(('DRIVE',fake_paf)).once()
      os.sep='SEP'
      fake_paf.should_receive('split').with_args('SEP').and_return([5,4,3,2,1]).once()

      os.path.should_receive('join').with_args('VMPATH', '..', '..', '..', '..').and_return('JVMPATH').once()
      os.path.should_receive('realpath').with_args('JVMPATH').and_return('RUNPATH').once()
      os.path.should_receive('join').with_args('RUNPATH','run.json').and_return('RUNOBJPATH').once()
      os.path.should_receive('exists').with_args('RUNOBJPATH').and_return(True).once()

      fake_file=flexmock()
      flexmock(sys.modules['builtins']).should_receive('open').with_args('RUNOBJPATH','r').and_return(fake_file).once()
      json.should_receive('load').with_args(fake_file).and_return({'id':'RID'}).once()
      fake_file.should_receive('close').once()
      fake_first_3=flexmock()
      fake_occam.should_receive('searchRuns').with_args(uuid='RID').and_return(fake_first_3).once()
      fake_first_3.should_receive('first').and_return(flexmock(id='RUNID')).once()

      flexmock(Object)
      Object.should_receive('__init__').with_args('VMPATH',occam=fake_occam).once()
      Object.should_receive('objectInfo').and_return({'id':'VMID','run':{'index':'VMINDEX'}})
      flexmock(DB)
      fake_job=flexmock()
      flexmock(Job)
      Job.new_instances(fake_job)
      fake_occam.should_receive('currentPersonId').and_return('CURPID').once()
      fake_session=flexmock()
      DB.should_receive('session').and_return(fake_session).twice()

      fake_session.should_receive('add').with_args(fake_job).once()
      fake_session.should_receive('commit').once()

      flexmock(shutil)
      shutil.should_receive('rmtree').with_args('GPATH').once()

      assert Run(fake_options,fake_args,fake_occam).do() == None


    def test_do_to_object_in_options_obj_type_is_workset_run_finished(self):
      fake_options = flexmock(revision='REV',to_object='TOOBJ',within_object=None,within_revision=None)
      fake_args=['zero']
      fake_occam=flexmock()

      fake_obj=flexmock(objectInfo=lambda:{'id':'ID','type':'occam/task','generator':{'id':'GID','revision':'GREV'}})
      fake_occam.should_receive('retrieve').with_args('TOOBJ').and_return(fake_obj).once()
      fake_workset=flexmock()
      fake_experiment_info={'id':'ID','name':'NAME','type':'experiment'}
      fake_experiment=flexmock(path='EPATH',objectInfo=lambda:fake_experiment_info)
      fake_occam.should_receive('temporaryClone').with_args('GID','GREV',None,None).and_return((fake_workset,fake_experiment,'WPATH')).once()
      flexmock(os.path)
      os.path.should_receive('join').with_args('EPATH','ID').and_return('GPATH').once()
      fake_obj.should_receive('clone').with_args('GPATH','REV').once()

      flexmock(os.path)
      os.path.should_receive('join').with_args('GPATH','object.json').and_return('JJSON').once()
      os.path.should_receive('exists').with_args('JJSON').and_return(True).once()
      flexmock(json)
      #119
      fake_obj_info={'id':'ID','type':'workset','outputs':[{'file':{}}]}
      flexmock(sys.modules['builtins']).should_receive('open').with_args('JJSON').and_return('JJJSON').once()
      json.should_receive('load').with_args('JJJSON').and_return(fake_obj_info).once()

      flexmock(os)
      os.should_receive('listdir').with_args('GPATH').and_return(['..']).once()
      flexmock(os.path)
      os.path.should_receive('join').with_args('GPATH','..').and_return('SSUBPATH').once()
      os.path.should_receive('isdir').with_args('SSUBPATH').and_return(True).once()
      os.path.should_receive('join').with_args('SSUBPATH','object.json').and_return('JSONPATH').once()
      os.path.should_receive('exists').with_args('JSONPATH').and_return(True).once()
      flexmock(sys.modules['builtins']).should_receive('open').with_args('JSONPATH','r').and_return('JJJJSON').once()
      json.should_receive('load').with_args('JJJJSON').and_return({'type':'experiment'}).once()
      flexmock(Runner)
      Runner.should_receive('__init__').with_args(fake_options,fake_args,'SSUBPATH').once()
      Runner.should_receive('do').with_args(True).once()

      flexmock(shutil)
      shutil.should_receive('rmtree').with_args('GPATH').once()

      assert Run(fake_options,fake_args,fake_occam).do() == None

    def test_do_to_object_not_in_options_obj_type_is_simulator_run_finished(self):
      fake_options = flexmock(revision='REV',to_object=None)
      fake_args=['zero']
      fake_occam=flexmock()

      fake_experiment_info = {'id':'ID','type':'not_experiment','generator':{'id':'GID'}}
      fake_experiment = flexmock(objectInfo=lambda:fake_experiment_info)
      fake_workset = flexmock()
      fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()
      fake_experiment.should_receive('workset').and_return('workset').once()

      flexmock(os.path)
      os.path.should_receive('join').with_args('.','object.json').and_return('JJSON').once()
      os.path.should_receive('exists').with_args('JJSON').and_return(True).once()
      flexmock(json)
      #119
      fake_obj_info={'id':'ID','outputs':[{'file':{}}]}
      flexmock(sys.modules['builtins']).should_receive('open').with_args('JJSON').and_return('JJJSON').once()
      json.should_receive('load').with_args('JJJSON').and_return(fake_obj_info).once()

      flexmock(Object)
      Object.should_receive('__init__').with_args('.',occam=fake_occam).at_least().twice()
      Object.should_receive('head').once()
      Object.should_receive('storeLocalChanges').once()
      #Object.revision='OREV'
      fake_occam.should_receive('update').with_args(Object,build_path = '.', localHash='local-abc').and_return(False).once()
      fake_occam.should_receive('build').with_args(Object,build_path = '.', localHash='local-abc').once()
      flexmock(Docker)
      Docker.should_receive('docker_name').with_args('ID').and_return('HNAME').once()

      os.path.should_receive('join').with_args('.','objects').and_return('IOPATH').once()
      os.path.should_receive('exists').with_args('IOPATH').and_return(False).once()
      flexmock(os)
      os.should_receive('mkdir').with_args('IOPATH').once()

      os.path.should_receive('join').with_args('IOPATH','HNAME').and_return('IIOPATH').once()
      os.path.should_receive('exists').with_args('IIOPATH').and_return(False).once()
      os.should_receive('mkdir').with_args('IIOPATH').once()


      #329
      fake_objectInfo={  'type':'TYPE','name':'NAME',
                'outputs':[{'file':'FILE'}],
                'run':{'id':'ID','index':'INDEX','type':'TYPE','name':'NAME','revision':'OBJREV','schema':'SCHEMA','outputs':[{'file':{},'schema':'SCHEMA'}]}}
      Object.should_receive('objectInfo').and_return(fake_objectInfo)
      Docker.should_receive('run').with_args('TYPE','NAME',None,'.',local=True).once()

      os.path.should_receive('join').with_args('.',{}).and_return('OPPATH').once()
      fake_file=flexmock()
      flexmock(sys.modules['builtins']).should_receive('open').with_args('OPPATH').and_return(fake_file).once()

      json.should_receive('load').with_args(fake_file).and_return('OUTPUT').once()
      fake_file.should_receive('close').once()

      fake_experiment_info_2 = {'id':'ID','name':'NAME','type':'experiment','generator':{'id':'GID'}}
      fake_experiment_2=flexmock(objectInfo=lambda:fake_experiment_info_2)
      fake_occam.should_receive('retrieve').with_args('GID').and_return(fake_experiment_2).once()
      fake_experiment_2.revision='EREV'

      os.path.should_receive('join').with_args('.','objects','ID-INDEX').and_return('OBJPATH').once()
      os.path.should_receive('join').with_args('.','initializations','ID-INDEX','run.json').and_return('INITPATH').once()

      os.path.should_receive('exists').with_args('INITPATH').and_return(True).once()
      fake_file_2=flexmock()
      flexmock(sys.modules['builtins']).should_receive('open').with_args('INITPATH','r').and_return(fake_file_2).once()
      json.should_receive('load').with_args(fake_file_2).and_return({'finish':'FINISH'}).once()
      fake_file_2.should_receive('close').once()

      os.path.should_receive('join').with_args('OBJPATH',{}).and_return('JFILE').once()
      fake_git_obj=flexmock()
      fake_occam.should_receive('retrieve').with_args('ID',revision='OBJREV').and_return(fake_git_obj).once()
      #Object.should_receive('__init__').with_args('.',occam=fake_occam).once()

      os.path.should_receive('join').with_args('.','outputs').and_return('OUTPUTPATH').once()
      os.path.should_receive('exists').with_args('OUTPUTPATH').and_return(False).once()
      os.should_receive('mkdir').with_args('OUTPUTPATH').once()

      Object.should_receive('uuid').with_args('text/plain').and_return('UUID').times(5)
      os.path.should_receive('join').with_args('OUTPUTPATH','UUID').and_return('JOPPATH').times(5)
      os.path.should_receive('exists').with_args('JOPPATH').and_return(False).times(5)
      os.should_receive('mkdir').with_args('JOPPATH').times(5)

      os.path.should_receive('exists').with_args('OBJPATH/run-out.raw').and_return(False).once()
      os.path.should_receive('exists').with_args('OBJPATH/run-err.raw').and_return(False).once()
      os.path.should_receive('exists').with_args('OBJPATH/run-finish-out.raw').and_return(False).once()
      os.path.should_receive('exists').with_args('OBJPATH/run-finish-err.raw').and_return(False).once()
      os.path.should_receive('exists').with_args('JFILE').and_return(False).once()

      #flexmock(shutil)
      #shutil.should_receive('copy').with_args('OBJPATH/run-out.raw','JOPPATH',follow_symlinks=True).once()
      #shutil.should_receive('copy').with_args('OBJPATH/run-err.raw','JOPPATH',follow_symlinks=True).once()
      #shutil.should_receive('copy').with_args('OBJPATH/run-finish-out.raw','JOPPATH',follow_symlinks=True).once()
      #shutil.should_receive('copy').with_args('OBJPATH/run-finish-err.raw','JOPPATH',follow_symlinks=True).once()
      #shutil.should_receive('copy').with_args('JFILE','JOPPATH',follow_symlinks=True).once()

      fake_git_obj.git=flexmock()
      fake_git_obj.git.should_receive('retrieveJSON').with_args('SCHEMA').and_return('SSCHEMA').once()
      os.path.should_receive('join').with_args('JOPPATH','SCHEMA').and_return('JSCHEMA').once()
      fake_file_3=flexmock()
      flexmock(sys.modules['builtins']).should_receive('open').with_args('JSCHEMA','w+').and_return(fake_file_3).once()
      json.should_receive('dumps').with_args('SSCHEMA').and_return('DUMPS').once()
      fake_file_3.should_receive('write').with_args('DUMPS').once()
      fake_file_3.should_receive('close').once()

      fake_output_object_info={
        'generator':{
          "id": 'ID',
          "type": 'experiment',
          "name": 'NAME',
          "revision": 'EREV'
        },
        'file':'BASE'
      }

      fake_output_object_info_schema={
        'generator':{
          "id": 'ID',
          "type": 'experiment',
          "name": 'NAME',
          "revision": 'EREV'
        },
        'file':'BASE',
        'schema':'BASE'
      }

      fake_output_object=flexmock(objectInfo=lambda:fake_output_object_info)
      Object.should_receive('create').with_args('JOPPATH','stdout for TYPE NAME','text/plain',uuid='UUID',createPath=False, occam=fake_occam).and_return(fake_output_object).once()
      Object.should_receive('create').with_args('JOPPATH','stderr for TYPE NAME','text/plain',uuid='UUID',createPath=False, occam=fake_occam).and_return(fake_output_object).once()
      Object.should_receive('create').with_args('JOPPATH','stdout (Finish) for TYPE NAME','text/plain',uuid='UUID',createPath=False, occam=fake_occam).and_return(fake_output_object).once()
      Object.should_receive('create').with_args('JOPPATH','stderr (Finish) for TYPE NAME','text/plain',uuid='UUID',createPath=False, occam=fake_occam).and_return(fake_output_object).once()
      Object.should_receive('create').with_args('JOPPATH','output','text/plain',uuid='UUID',createPath=False, occam=fake_occam).and_return(fake_output_object).once()

      os.path.should_receive('basename')
      os.path.should_receive('basename').with_args('OBJPATH/run-out.raw').and_return('BASE').once()
      os.path.should_receive('basename').with_args('OBJPATH/run-err.raw').and_return('BASE').once()
      os.path.should_receive('basename').with_args('OBJPATH/run-finish-out.raw').and_return('BASE').once()
      os.path.should_receive('basename').with_args('OBJPATH/run-finish-err.raw').and_return('BASE').once()
      os.path.should_receive('basename').with_args('JFILE').and_return('BASE').once()
      os.path.should_receive('basename').with_args('SCHEMA').and_return('BASE').once()


      fake_output_object.should_receive('updateObject').with_args(fake_output_object_info,'Creating Output Object').times(4)
      fake_output_object.should_receive('updateObject').with_args(fake_output_object_info_schema,'Creating Output Object').once()
      fake_output_object.should_receive('head').times(5)

      fake_experiment_2.should_receive('updateObject').with_args(fake_experiment_info_2,'Ran and added generated outputs').once()

      assert Run(fake_options,fake_args,fake_occam).do() == None
