from flexmock import flexmock
import pytest

from lib.log            	import Log
from lib.db             	import DB
from lib.object         	import Object
from lib.workset        	import Workset

from subprocess				import Popen, PIPE

from lib.commands.attach 	import Attach
from lib.occam             	import Occam
from lib.experiment 		import Experiment

import shutil
import os
import sys
import json

class TestAttach(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Attach.initParser(fake) == fake

	class TestDo(object):

		def test_do_no_type_of_object_given(self):
			with pytest.raises(Exception) as ex:
				fake_options = flexmock(creates_object_type=None,object_id=None)
				fake_args=['zero']
				fake_occam=None

				Attach(fake_options,fake_args,fake_occam).do()

				assert 'no type of object given' in str(ex.value)

		def test_do_no_name_given(self):
			with pytest.raises(Exception) as ex:
				fake_options = flexmock(creates_object_type=None,object_id=None)
				fake_args=['zero','one']
				fake_occam=None

				Attach(fake_options,fake_args,fake_occam).do()

				assert 'no name given' in str(ex.value)

		def test_do_not_within_an_experiment(self):
			fake_options = flexmock(creates_object_type=None,object_id=None,to_object=None,revision=None,object_revision=None)
			fake_args=['0','1','2']
			fake_occam=flexmock()

			fake_experiment = flexmock(objectInfo=lambda :{'type':'not_experiment'},workset=lambda:None)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()

			assert Attach(fake_options,fake_args,fake_occam).do() == -1

		def test_do_cant_find_object(self):
			fake_options = flexmock(creates_object_type=None,object_id=None,to_object=None,revision=None,object_revision=None)
			fake_args=['0','1','2']
			fake_occam=flexmock()
			fake_workset = flexmock()
			fake_experiment = flexmock(objectInfo=lambda :{'type':'experiment'},workset=lambda:fake_workset)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()
			fake_experiment.should_receive('copyOnWrite').with_args(fake_workset).and_return(fake_experiment).once()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(object_type='1', name='2').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()

			assert Attach(fake_options,fake_args,fake_occam).do() == -1


		def test_do_must_specify_what_object_to_connect_this_to(self):
			fake_options = flexmock(creates_object_type=None,object_id=None,to_object=None,revision=None,object_revision=None,base=True)
			fake_args=['0','one','2']
			fake_occam=flexmock()
			fake_workset = flexmock()
			fake_experiment = flexmock(objectInfo=lambda :{'type':'experiment','workflow':{'connections':[1,2,3]}},workset=lambda:fake_workset)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()
			fake_experiment.should_receive('copyOnWrite').with_args(fake_workset).and_return(fake_experiment).once()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(object_type='one', name='2').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(uid='UID')).once()

			fake_occam.should_receive('objectPath').with_args('UID').and_return('OBJPATH').once()
			flexmock(Object).should_receive('__init__').with_args('OBJPATH',uuid='UID',occam=fake_occam).once()
			Object.should_receive('head').and_return('GREV').once()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return(flexmock(objects=lambda:None)).once()
			assert Attach(fake_options,fake_args,fake_occam).do() == -1

		def test_do_attached_creation_of_object(self):
			fake_options = flexmock(creates_object_group='GROUP',creates_object_type='experiment',object_id='OBJID',to_object='TOOBJ',revision='REV',object_revision=None,within_object=None,within_revision=None)
			fake_args=['0','1','2']
			fake_occam=flexmock()

			connection = {
				"object": {
					"type"    : 'experiment',
					"name"    : '2',
					"created" : True,
					"group"	  : 'GROUP'
				},
				"to": '1'
			}

			fake_experiment_info = {'type':'experiment','name':'NAME','workflow':{'connections':[connection]}}
			fake_experiment=flexmock(objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ','REV',None,None).and_return(('workset',fake_experiment,'PATH')).once()
			fake_experiment.should_receive('copyOnWrite').with_args('workset').and_return(fake_experiment).once()
			fake_experiment.should_receive('purgeGenerates').once()

			fake_experiment.should_receive('updateObject').with_args(fake_experiment_info,'attached created experiment object\n\nOCCAM automated commit').once()

			flexmock(shutil)
			shutil.should_receive('rmtree').with_args('PATH').once()

			assert Attach(fake_options,fake_args,fake_occam).do() == 0

		def test_do_creates_object_type_not_in_options_object_id_in_options_successful_attached_object(self):
			fake_options=flexmock(creates_object_type=None,to_object='TOOBJ',revision='REV',object_revision='OREV',object_id='OBJID',base=False,within_revision=None,within_object=None)
			fake_args=['0','1','2']
			fake_occam=flexmock()

			fake_experiment_info = {'type':'experiment','name':'NAME','workflow':{'connections':[]}}
			fake_experiment=flexmock(path='EPATH',objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ','REV',None,None).and_return(('workset',fake_experiment,'PATH')).once()
			fake_experiment.should_receive('copyOnWrite').with_args('workset').and_return(fake_experiment).once()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='OBJID').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(uid='UID')).once()

			fake_occam.should_receive('objectPath').with_args('UID').and_return('OBJPATH').once()
			flexmock(Object).should_receive('__init__').with_args('OBJPATH',uuid='UID',occam=fake_occam).once()
			Object.should_receive('fullRevision').with_args('OREV').and_return('FREV').once()

			fake_occam.should_receive('objectAt').with_args('PATH',searchFor='workset').and_return(None).once()
			flexmock(os.path)
			os.path.should_receive('join').with_args('PATH','..').and_return('JPATH').once()
			fake_parent_obj=flexmock(path='PATH',objectInfo=lambda:{'name':'2','type':'1','id':'ID','group':'GROUP',
																	'configurations':[{'file':'FILE','name':'NAME','schema':'SCHEMA'}]})
			fake_occam.should_receive('objectAt').with_args('JPATH', recurse=True).and_return(fake_parent_obj).once()
			fake_parent_obj.should_receive('fullRevision').with_args('OREV').and_return('OOREV').once()
			
			os.path.should_receive('join').with_args('EPATH','configurations').and_return('JJPATH').once()
			os.path.should_receive('exists').with_args('JJPATH').and_return(False).once()
			flexmock(os)
			os.should_receive('mkdir').with_args('JJPATH').once()

			os.path.should_receive('join').with_args('JJPATH','0').and_return('JJJPATH').once()
			os.path.should_receive('realpath').with_args('JJJPATH').and_return('RPATH').once()
			os.path.should_receive('exists').with_args('RPATH').and_return(False).once()
			os.should_receive('mkdir').with_args('RPATH').once()

			fake_parent_obj.git=flexmock()

			fake_parent_obj.git.should_receive('retrieveJSON').with_args('SCHEMA').and_return({'type':'not_dict'}).once()
			os.path.should_receive('join').with_args('RPATH','FILE').and_return('CFPATH').once()
			os.path.should_receive('exists').with_args('CFPATH').and_return(False).once()

			fake_file=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('CFPATH','w+').and_return(fake_file).once()

			flexmock(json)
			json.should_receive('dumps').with_args({}).and_return('DUMPS').once()
			fake_file.should_receive('write').with_args('DUMPS').once()
			fake_file.should_receive('close').once()

			fake_experiment.should_receive('purgeGenerates').once()
			fake_experiment.git=flexmock()

			fake_experiment.git.should_receive('add').with_args('RPATH').once()
			fake_experiment.should_receive('commit').with_args('Adds configuration for 1 2').once()

			fake_experiment.should_receive('updateObject').with_args(fake_experiment_info,'attached 1 2 (OOREV)\n\nOCCAM automated commit')

			flexmock(shutil)
			shutil.should_receive('rmtree').with_args('PATH').once()

			assert Attach(fake_options,fake_args,fake_occam).do() == 0

		#@pytest.mark.parametrize("options,args,exception,msg,expected", [
		#	(flexmock(creates_object_type=None,object_id=None),
		#		['zero'],
		#			Exception,
		#				'no type of object given',
		#					-1),
		#	(flexmock(creates_object_type=None,object_id=None),
		#		['zero','one'],
		#			Exception,
		#				'no name given',
		#					-1),
		#	(flexmock(creates_object_type='COBJTYPE',object_id='OBJID',revision='REV',object_revision='OBJREV',to_object='TOOBJ',within_object="WOBJ",within_revision="WREV",creates_object_group="COBJGP"),
		#		['zero'],
		#			None,
		#				None,
		#				-1),
			#(flexmock(creates_object_type=None,object_id='OBJID',revision='REV',object_revision='OBJREV'),['zero','one','two'],None,None,-1),
			#(flexmock(creates_object_type="TYTYT"),['zero'], -1),
		    #({},['zero','one'], -1),
		    #({},['zero','one','two'], 0),
		    #({},['zero','one','two','three','four'], -1),
		    #({},['zero','one','two','three'], 0),
		    #({},['zero','one','two','three','four','five'], 0)
		#])
		def _do(self,options,args,exception,msg,expected):
			# type at 1
			# name at 2
			# name None type None
			# node_id = int(type) fail-> -1
			if exception is None:
				if options.to_object:
					flexmock(Occam)
					flexmock(Attach)
					fake_workset = flexmock(object=lambda x:x)
					fake_experiment = flexmock(path="PATH")
					#Experiment.new_instances(flexmock(path="PATH",objectInfo= lambda :{'type':'TYPE','name':'NAME'}))

					fake_experiment.should_receive('objectInfo').and_return({'type':'experiment','name':'NAME'}).at_least().once()
					fake_experiment.should_receive('copyOnWrite').and_return(fake_experiment).at_least().once()
					#fake_experiment = flexmock(objectInfo=lambda x:{'type':'TYPE','name':'NAME','generates':[],'workflow':{'connections':[]}},workset=lambda x:x,copyOnWrite=lambda x:fake_experiment,)
					Occam.should_receive('temporaryClone').and_return((fake_workset,fake_experiment,"PATH"))
					Attach.new_instances(flexmock(options=options,args=args,occam=Occam()))
					assert Attach.do(Attach()) == expected
			else:

				with pytest.raises(exception) as ex:
					flexmock(Attach)
					Attach.new_instances(flexmock(options=options,args=args,occam=flexmock(Occam)))
					Attach.do(Attach())
					assert msg in str(ex.value)
