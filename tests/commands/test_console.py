from flexmock import flexmock
import pytest

from lib.commands.console 		import Console
from lib.log            		import Log

from lib.git          import Git

from subprocess import Popen, PIPE

class TestView(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Console.initParser(fake) == fake


	class TestDo(object):

		def test_do_1_arg_uuid_None_local_build_obj_revision_not_in_options(self):
			fake_options = flexmock(to_object=None,revision=None)
			fake_args = ['zero']
			fake_occam = flexmock()
			
			flexmock(Popen)
			fake_stdout = flexmock(read=lambda:flexmock(decode=lambda:flexmock(strip=lambda:None)))
			fake_popen = flexmock(stdout=fake_stdout,wait=lambda:None)
			Popen.new_instances(fake_popen)
			#Popen.should_receive('__init__').with_args(['git', 'rev-parse', 'HEAD'], stdout=PIPE, cwd='.').once()
			fake_popen.should_receive('wait').once()
			fake_stdout.should_receive('read.decode.strip').once()
			fake_objInfo = flexmock(objectInfo=lambda:{'name':'NAME','type':'TYPE'})
			fake_occam.should_receive('objectAt').with_args('.',revision='').and_return(fake_objInfo).once()
			fake_occam.should_receive('console').with_args(fake_objInfo,build_path = '.', localHash=None).and_return(True).once()
			assert Console(fake_options,fake_args,fake_occam).do() == None

		def test_do_2_args_given_uuid_obj_revision_not_in_options_cant_find_buildable_object(self):
			fake_options = flexmock(to_object=None,revision=None)
			fake_args = ['zero','one']
			fake_occam = flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='one').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()

			assert Console(fake_options,fake_args,fake_occam).do() == -1


		def test_do_3_args_given_obj_type_and_name_obj_revision_not_in_options_cant_find_buildable_object(self):
			fake_options = flexmock(to_object=None,revision=None)
			fake_args = ['zero','one','two']
			fake_occam = flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(	object_type = 'one',
                                        							name        = 'two',
                                        							uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()

			assert Console(fake_options,fake_args,fake_occam).do() == -1

		@pytest.mark.parametrize("orev,rev",[
			('REV','REV'),
			(None,'OREV')
		])
		def test_do_2_args_given_uuid_obj_revision_in_or_not_in_options(self,orev,rev):
			fake_options = flexmock(to_object=None,revision=orev)
			fake_args = ['zero','one']
			fake_occam = flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='one').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(revision='OREV',uid='UID')).once()
			fake_occam.should_receive('objectPath').with_args('UID').and_return('PATH').once()

			fake_objInfo = flexmock(objectInfo=lambda:{'name':'NAME','type':'TYPE'})
			fake_occam.should_receive('objectAt').with_args('PATH',revision=rev).and_return(fake_objInfo).once()

			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH',rev).and_return('FREV').once()
			Git.should_receive('__init__').with_args('PATH','FREV').once()
			Git.should_receive('objectInfo').and_return('INFO').once()
			fake_occam.should_receive('console').with_args(fake_objInfo).and_return(True).once()

			assert Console(fake_options,fake_args,fake_occam).do() == None

		@pytest.mark.parametrize("orev,rev",[
			('REV','REV'),
			(None,'OREV')
		])
		def test_do_3_args_given_obj_type_and_name_revision_in_or_not_in_revision_not_found(self,orev,rev):
			fake_options = flexmock(to_object=None,revision=orev)
			fake_args = ['zero','one','two']
			fake_occam = flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(	object_type = 'one',
                                        							name        = 'two',
                                        							uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(revision='OREV',uid='UID')).once()
			fake_occam.should_receive('objectPath').with_args('UID').and_return('PATH').once()

			fake_occam.should_receive('objectAt').with_args('PATH',revision=rev).and_return(None).once()
			
			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH',rev).and_return('FREV').once()
			Git.should_receive('__init__').with_args('PATH','FREV').once()
			Git.should_receive('objectInfo').and_return(None).once()
			
			assert Console(fake_options,fake_args,fake_occam).do() == -1
	'''
	#@pytest.mark.parametrize("args,searchObj,result",[#,auth,exists,result", [
		#(['zero'],None,None,-1),
		#(['zero','one'],False,False,-1),
		#(['zero','one'],True,True,None)
		#(['zero','one','two','three'],None,-1),
		#(['zero','one','two'],'URL',-1)
		(['zero'],None,-1),
		#(['zero','one']),
		#(['zero','one','two']),

	#])
	def _do(self,args,searchObj,result):
		flexmock(Log)
		Log.should_receive('header').once()
		fake_occam = flexmock()
		fake_self = flexmock(occam = fake_occam, options=flexmock(to_object='UUID',revision='REV'))
		if searchObj:
			fake_occam.should_receive('searchObjects.first').and_return(searchObjects)


		assert Console._do(fake_self) == result'''
