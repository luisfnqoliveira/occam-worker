from flexmock import flexmock
import pytest

from lib.commands.update 		import Update
from lib.log            		import Log
from lib.git            import Git

from subprocess import Popen, PIPE

class TestUpdate(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Update.initParser(fake) == fake


	class TestDo(object):

		def test_do_3_args_given_obj_type_and_name_cant_find_buildable_object(self):
			fake_options = None
			fake_args=['zero','one','two']
			fake_occam=flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(	object_type = 'one',
                                     								name        = 'two',
                                     								uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()

			assert Update(fake_options,fake_args,fake_occam).do() == -1

		def test_do_3_args_given_obj_type_and_name_revision_not_in_options_not_local_build_and_successful_update(self):
			fake_options = None
			fake_args=['zero','one','two']
			fake_occam=flexmock()
			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(	object_type = 'one',
                                     								name        = 'two',
                                     								uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(revision='OREV',path='OPATH')).once()

			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('OPATH',revision='OREV').and_return(fake_obj).once()

			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH','OREV').and_return('OOREV').once()
			Git.should_receive('__init__').with_args('PATH','OOREV').once()
			Git.should_receive('objectInfo').and_return('INFO').once()
			fake_occam.should_receive('update').with_args(fake_obj).and_return(True).once()

			assert Update(fake_options,fake_args,fake_occam).do() == None

		def test_do_2_args_no_obj_type_given_name_revision_in_options_revision_not_found(self):
			fake_options = flexmock(revision='REV')
			fake_args=['zero','one']
			fake_occam=flexmock()

			fake_occam.should_receive('objectAt').with_args('one',revision='REV').and_return(flexmock(revision='RREV',path='PATH')).once()
			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH','REV').and_return(None).once()

			assert Update(fake_options,fake_args,fake_occam).do() == -1

		def test_do_1_arg_no_obj_type_no_name_path_None_local_build_revision_not_in_options_successful_update(self):
			fake_options = None
			fake_args=['zero']
			fake_occam=flexmock()

			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})
			fake_occam.should_receive('objectAt').with_args('.',revision='').and_return(fake_obj).once()

			flexmock(Popen)
			fake_stdout = flexmock(read=lambda:flexmock(decode=lambda:flexmock(strip=lambda:None)))
			fake_popen = flexmock(stdout=fake_stdout,wait=lambda:None)
			Popen.new_instances(fake_popen)
			#Popen.should_receive('__init__').with_args(['git', 'rev-parse', 'HEAD'], stdout=PIPE, cwd='.').once()
			fake_popen.should_receive('wait').once()
			fake_stdout.should_receive('read.decode.strip').once()

			fake_occam.should_receive('update').with_args(fake_obj,build_path = '.', localHash=None).and_return(True).once()

			assert Update(fake_options,fake_args,fake_occam).do() == None

	'''
	@pytest.mark.parametrize("args,searchObj,fr,frv,update,result",[#,auth,exists,result", [
		#(['zero'],None,None,-1),
		#(['zero','one'],False,False,-1),
		#(['zero','one'],True,True,None)
		#(['zero','one','two','three'],None,-1),
		#(['zero','one','two'],'URL',-1)

		(['zero','one','two'],False,False,None,False,-1),
		(['zero','one','two'],True,True,'FREV',True,None),
		(['zero','one','two'],True,True,None,False,-1),
		
		#(['zero','one','two'],False,{'type':'TYPE','name':'NAME'},-1),
		#(['zero','one']),
		#(['zero','one','two']),

	])
	def test_do(self,args,searchObj,fr,frv,update,result):
		flexmock(Log)
		Log.should_receive('header').once()
		fake_occam = flexmock()
		fake_self = flexmock(args=args,occam = fake_occam, options=flexmock(revision='REV'))
		if searchObj:
			fake_occam.should_receive('searchObjects.first').and_return(flexmock(path='PATH',revision='REVO'))
		else:
			fake_occam.should_receive('searchObjects.first').and_return(None)
		
		fake_occam.should_receive('objectAt').and_return(flexmock(path='PATH',objectInfo=lambda :{'type':'TYPE','name':'NAME'}))
		
		if fr:
			flexmock(Git)
			Git.should_receive('fullRevision').and_return(frv).once()
			Git.should_receive('objectInfo').and_return('OBJINFO')
		if update:
			fake_occam.should_receive('update').and_return(True)
			Log.should_receive('done').once()
		if result==-1:
			Log.should_receive('error').once()
		assert Update.do(fake_self) == result'''