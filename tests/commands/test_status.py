from flexmock import flexmock
import pytest

from lib.commands.status 		import Status
from lib.log            		import Log
#from lib.git            		import Git

class TestStatus(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Status.initParser(fake) == fake

	class TestDo(object):

		#@pytest.mark.parametrize("workset,obj",[
		#	(None,'obj'),
		#	('workset',None)
		#])
		def test_do_not_within_a_workset_or_object(self):#,workset,obj):
			fake_options = None
			fake_args=['zero']
			fake_occam=flexmock()

			fake_occam.should_receive('objectAt').with_args('.').and_return(None).once()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return(None).once()

			assert Status(fake_options,fake_args,fake_occam).do() == -1

		@pytest.mark.parametrize("paths",[
			(None),
			('paths')
		])
		def test_do_workset_and_obj_both_exist__groups_in_options(self,paths):
			fake_options = flexmock(groups='groups',paths=paths)
			fake_args=['zero']
			fake_occam=flexmock()

			fake_group=flexmock(path='GP',objectInfo=lambda:{'name':'GNAME'})
			fake_obj=flexmock(groups=lambda:[fake_group,fake_group,fake_group],experiments=lambda:None,objects=lambda:None)

			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_obj).once()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return('workset').once()

			assert Status(fake_options,fake_args,fake_occam).do() == 0
	
		@pytest.mark.parametrize("paths",[
			(None),
			('paths')
		])
		def test_do_workset_and_obj_both_exist__groups_not_in_options__experiments_in_options(self,paths):
			fake_options = flexmock(groups=None,experiments='experiments',paths=paths)
			fake_args=['zero']
			fake_occam=flexmock()

			fake_exp=flexmock(path='EP',objectInfo=lambda:{'name':'ENAME'})
			fake_obj=flexmock(groups=lambda:None,experiments=lambda:[fake_exp,fake_exp,fake_exp],objects=lambda:None)

			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_obj).once()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return('workset').once()

			assert Status(fake_options,fake_args,fake_occam).do() == 0


		@pytest.mark.parametrize("paths",[
			(None),
			('paths')
		])
		def test_do_successful_status(self,paths):
			fake_options = flexmock(groups=None,experiments=None,paths=paths)
			fake_args=['zero']
			fake_occam=flexmock()

			fake_group=flexmock(path='GP',objectInfo=lambda:{'name':'GNAME'})
			fake_exp=flexmock(path='EP',objectInfo=lambda:{'name':'ENAME'})
			fake_o=flexmock(path='OP',objectInfo=lambda:{'name':'ONAME','type':'OTYPE','state':'OSTATE','revision':'OREV'})
			
			fake_conn1={'local':1,'object':{'type':'type1','name':'name1','revision':'revision1'}}
			fake_conn2={'created':1,'object':{'type':'type2','name':'name2','revision':'revision2'}}
			fake_conn3={'local':1,'object':{'type':'type3','name':'name3','revision':'revision3'}}


			fake_obj=flexmock(	parent=lambda:None,
								head=lambda:None,
								groups=lambda:[fake_group,fake_group,fake_group],
								experiments=lambda:[fake_exp,fake_exp,fake_exp],
								objects=lambda:[fake_o,fake_o,fake_o],
								objectInfo=lambda:{	'type':'TYPE',
													'name':'NAME',
													'id':'ID',
													'workflow':{'connections':[fake_conn1,fake_conn2,fake_conn3]}})

			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_obj).once()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return('workset').once()

			assert Status(fake_options,fake_args,fake_occam).do() == 0