from flexmock import flexmock
import pytest

from lib.commands.logs 		import Logs
from lib.db             import DB

import os
import select
import sys

class TestLogs(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Logs.initParser(fake) == fake

	class TestDo(object):

		def test_do_no_job_in_options(self):
			assert Logs(flexmock(job=None),[],None).do() == -1

		def test_do_cant_find_job_with_id(self):
			flexmock(DB)
			fake_options=flexmock(job='JOB')
			DB.should_receive('initialize').with_args(fake_options).once()
			fake_first=flexmock()
			DB.should_receive('session.query.filter_by').with_args(id='JOB').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()
			assert Logs(fake_options,[],None).do() == -1

		def test_do_cant_find_log_for_job_with_id(self):
			flexmock(DB)
			fake_options=flexmock(job='JOB')
			DB.should_receive('initialize').with_args(fake_options).once()
			fake_first=flexmock()
			DB.should_receive('session.query.filter_by').with_args(id='JOB').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(id='JID')).once()

			fake_occam=flexmock()
			fake_jobs_path={'paths':{'jobs':'JOBSPATH'}}
			fake_occam.should_receive('configuration').and_return(fake_jobs_path).once()
			flexmock(os.path)
			os.path.should_receive('join').with_args('JOBSPATH','0','job-JID').and_return('JOBPATH').once()
			os.path.should_receive('exists').with_args('JOBPATH').and_return(False).once()
			assert Logs(fake_options,[],fake_occam).do() == -1

		def test_do_cant_find_log_for_job_with_id_2(self):
			flexmock(DB)
			fake_options=flexmock(job='JOB')
			DB.should_receive('initialize').with_args(fake_options).once()
			fake_first=flexmock()
			DB.should_receive('session.query.filter_by').with_args(id='JOB').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(id='JID')).once()

			fake_occam=flexmock()
			fake_jobs_path={'paths':{'jobs':'JOBSPATH'}}
			fake_occam.should_receive('configuration').and_return(fake_jobs_path).once()
			flexmock(os.path)
			os.path.should_receive('join').with_args('JOBSPATH','0','job-JID').and_return('JOBPATH').once()
			os.path.should_receive('exists').with_args('JOBPATH').and_return(True).once()

			os.path.should_receive('join').with_args('JOBPATH','job-JID.log').and_return('LOGPATH').once()
			os.path.should_receive('exists').with_args('LOGPATH').and_return(False).once()

			assert Logs(fake_options,[],fake_occam).do() == -1

		def test_do_successful_logs_with_no_tail(self):
			flexmock(DB)
			fake_options=flexmock(job='JOB',seek='SEEK',tail=None)
			DB.should_receive('initialize').with_args(fake_options).once()
			fake_first=flexmock()
			DB.should_receive('session.query.filter_by').with_args(id='JOB').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(id='JID')).once()

			fake_occam=flexmock()
			fake_jobs_path={'paths':{'jobs':'JOBSPATH'}}
			fake_occam.should_receive('configuration').and_return(fake_jobs_path).once()
			flexmock(os.path)
			os.path.should_receive('join').with_args('JOBSPATH','0','job-JID').and_return('JOBPATH').once()
			os.path.should_receive('exists').with_args('JOBPATH').and_return(True).once()

			os.path.should_receive('join').with_args('JOBPATH','job-JID.log').and_return('LOGPATH').once()
			os.path.should_receive('exists').with_args('LOGPATH').and_return(True).once()

			fake_file = flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('LOGPATH','r').and_return(fake_file).once()
			fake_file.should_receive('seek').with_args('SEEK').once()
			fake_file.should_receive('read').once()
			fake_file.should_receive('close').once()

			assert Logs(fake_options,[],fake_occam).do() == None

		def _do_successful_logs_with_tail(self):
			flexmock(DB)
			fake_options=flexmock(job='JOB',seek='SEEK',tail='TAIL')
			DB.should_receive('initialize').with_args(fake_options).once()
			fake_first=flexmock()
			DB.should_receive('session.query.filter_by').with_args(id='JOB').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(id='JID',status='running')).once()

			fake_occam=flexmock()
			fake_jobs_path={'paths':{'jobs':'JOBSPATH'}}
			fake_occam.should_receive('configuration').and_return(fake_jobs_path).once()
			flexmock(os.path)
			os.path.should_receive('join').with_args('JOBSPATH','0','job-JID').and_return('JOBPATH').once()
			os.path.should_receive('exists').with_args('JOBPATH').and_return(True).once()

			os.path.should_receive('join').with_args('JOBPATH','job-JID.log').and_return('LOGPATH').once()
			os.path.should_receive('exists').with_args('LOGPATH').and_return(True).once()

			fake_file = flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('LOGPATH','r').and_return(fake_file).once()
			fake_file.should_receive('seek').with_args('SEEK').once()
			fake_file.should_receive('read').at_least().twice()
			flexmock(select)
			select.should_receive('select').with_args([fake_file],[],[fake_file]).and_return(([1,2,3],None,None)).at_least().once()
			fake_file.should_receive('close').once()

			assert Logs(fake_options,[],fake_occam).do() == None