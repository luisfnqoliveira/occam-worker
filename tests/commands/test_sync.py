from flexmock import flexmock
import pytest

from lib.commands.sync 	import Sync
from lib.log            	import Log 

class TestSync(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Sync.initParser(fake) == fake



	class TestDo(object):
		def test_do_revision_exists(self):
			fake_occam = flexmock()
			fake_self = flexmock(occam=fake_occam,options=flexmock(revision='REV'))
			fake_workset = flexmock()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return(fake_workset).once()
			fake_workset.should_receive('sync').with_args('REV').once()

			Sync.do(fake_self)

		def test_do_revision_not_exists(self):
			fake_occam = flexmock()
			fake_self = flexmock(occam=fake_occam,options=flexmock(revision=None))
			fake_workset = flexmock()
			fake_occam.should_receive('objectAt').with_args('.',searchFor='workset').and_return(fake_workset).once()
			
			fake_workset.should_receive('objectInfo').and_return({'id':'ID'})

			# NOTE: if searchObjects.first return None, revision = None.revision
			fake_first=flexmock(first=lambda :flexmock(revision=None))
			fake_occam.should_receive('searchObjects').with_args(uuid='ID').and_return(fake_first).once()

			fake_workset.should_receive('sync').with_args(None).once()

			Sync.do(fake_self)

	'''@pytest.mark.parametrize("revision,searchresult,syncarg", [
		(None,flexmock(revision="DBREV"),"DBREV"),
		("REV",None,"REV")
	])
	def test_do(self,revision,searchresult,syncarg):

		fake_occam = flexmock()
		fake_self = flexmock(occam=fake_occam,options=flexmock(revision=revision))
		fake_workset = flexmock(objectInfo=lambda :{'id':'ID'},sync=lambda x:x)
		fake_occam.should_receive('objectAt').and_return(fake_workset)
		fake_occam.should_receive('searchObjects.first').and_return(searchresult)
		fake_workset.should_receive('sync').with_args(syncarg).once()

		flexmock(Log).should_receive('done').once()
		Sync.do(fake_self)'''