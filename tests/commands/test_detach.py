from flexmock import flexmock
import pytest

from lib.commands.detach 		import Detach
from lib.log            		import Log

import shutil      # For deleting that temp dir (grr python)

class TestDetach(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Detach.initParser(fake) == fake



	class TestDo(object):
		def test_do_1_arg__node_identifier_required(self):
			fake_options = flexmock(revision=None,to_object=None)
			fake_args=['zero']
			fake_occam=None

			assert Detach(fake_options,fake_args,fake_occam).do() == -1

		def test_do_2_args__to_object_not_in_options__type_isnt_experiment__not_within_an_experiment(self):
			fake_options = flexmock(revision=None,to_object=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_exp_info = flexmock(objectInfo=lambda:{'type':'not_experiment'})
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_exp_info).once()

			assert Detach(fake_options,fake_args,fake_occam).do() == -1


		# NOT PASSED: local variable 'workset' referenced before assignment
		def _do_2_args__to_object_not_in_options__type_is_experiment__type_not_in_experiment_info__not_within_an_experiment(self):
			fake_options = flexmock(revision=None,to_object=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_exp = flexmock(objectInfo=lambda:{'type':'experiment'})
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_exp).once()

			fake_exp_2 =flexmock(objectInfo=lambda:{})
			fake_exp.should_receive('copyOnWrite').with_args(None).and_return(fake_exp_2).once()

			assert Detach(fake_options,fake_args,fake_occam).do() == -1


		@pytest.mark.parametrize("info",[

			({}),
			({'type':'not_experiment'})

		])
		def test_do_2_arg__to_object_in_options__type_is_experiment__not_within_an_experiment(self,info):
			fake_options = flexmock(revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_exp = flexmock(objectInfo=lambda:{'type':'experiment'})
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_exp,None)).once()

			#
			#fake_occam.should_receive('objectAt').with_args('.').and_return(fake_exp).once()

			fake_exp_2 =flexmock(objectInfo=lambda:info)
			fake_exp.should_receive('copyOnWrite').with_args(None).and_return(fake_exp_2).once()

			assert Detach(fake_options,fake_args,fake_occam).do() == -1

		@pytest.mark.parametrize("info",[

			({'type':'experiment','workflow':{}}),
			({'type':'experiment'})

		])
		def test_do_2_arg__to_object_in_options__type_is_experiment__within_an_experiment__experiment_has_no_workflow(self,info):
			fake_options = flexmock(revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_exp = flexmock(objectInfo=lambda:{'type':'experiment'})
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_exp,None)).once()

			#
			#fake_occam.should_receive('objectAt').with_args('.').and_return(fake_exp).once()

			#fake_exp_2 =flexmock(objectInfo=lambda:info)
			fake_exp.should_receive('copyOnWrite').with_args(None).and_return(fake_exp).once()

			assert Detach(fake_options,fake_args,fake_occam).do() == -1

		def test_do_2_arg__to_object_in_options__type_is_experiment__within_an_experiment__experiment_has_workflow__node_identifier_invalid(self):
			fake_options = flexmock(revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_exp = flexmock(objectInfo=lambda:{'type':'experiment','workflow':{'connections':[]}})
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_exp,None)).once()

			#
			#fake_occam.should_receive('objectAt').with_args('.').and_return(fake_exp).once()

			#fake_exp_2 =flexmock(objectInfo=lambda:info)
			fake_exp.should_receive('copyOnWrite').with_args(None).and_return(fake_exp).once()

			assert Detach(fake_options,fake_args,fake_occam).do() == -1

		def test_do_2_arg__to_object_in_options__type_is_experiment__within_an_experiment__experiment_has_workflow__node_identifier_valid(self):
			fake_options = flexmock(revision=None,to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['0','1']
			fake_occam=flexmock()

			fake_conn1={'to':2,'object':{'type':'type1','name':'name1','revision':'revision1'}}
			fake_conn2={'to':2,'object':{'type':'type2','name':'name2','revision':'revision2'}}
			fake_conn3={'to':2,'object':{'type':'type3','name':'name3','revision':'revision3'}}


			fake_info={'type':'experiment','workflow':{'connections':[fake_conn1,fake_conn2,fake_conn3]}}
			fake_exp = flexmock(objectInfo=lambda:fake_info)
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ',None,None,None).and_return((None,fake_exp,'PATH')).once()
			
			fake_exp.should_receive('copyOnWrite').with_args(None).and_return(fake_exp).once()

			expected_msg="detached type2 name2 (revision2)\n\nOCCAM automated commit"

			fake_exp.should_receive('updateObject').with_args(fake_info,expected_msg).once()

			flexmock(shutil)
			shutil.should_receive('rmtree').with_args('PATH').once()

			assert Detach(fake_options,fake_args,fake_occam).do() == 0

	'''
	@pytest.mark.parametrize("args,result",[#,auth,exists,result", [
		#(['zero'],None,None,-1),
		#(['zero','one'],False,False,-1),
		#(['zero','one'],True,True,None)
		#(['zero','one','two','three'],None,-1),
		#(['zero','one','two'],'URL',-1)

		(['zero'],-1),
		(['0','1'],None)
		
		#(['zero','one','two'],False,{'type':'TYPE','name':'NAME'},-1),
		#(['zero','one']),
		#(['zero','one','two']),

	])
	def _do(self,args,result):
		flexmock(Log)
		Log.should_receive('header').once()
		fake_occam=flexmock()
		fake_self=flexmock(occam=fake_occam, args=args,options=flexmock(revision='REV',to_object='TOOBJ'))
		fake_occam.should_receive('temporaryClone').and_return(('WORKSET',flexmock(objectInfo=lambda :{}),'PATH'))
		if result==-1:
			Log.should_receive('error').once()

		assert Detach.do(fake_self) == result'''