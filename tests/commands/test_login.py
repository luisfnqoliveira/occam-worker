from flexmock import flexmock
import pytest
import os          # path functions

from lib.commands.login 		import Login
from lib.log            		import Log 

import getpass
import sys
import json

class TestLogin(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Login.initParser(fake) == fake


	@pytest.mark.parametrize("exists,result", [
		(True,True),
		(False,False)
	])
	def test_is_authenticated(self,exists,result):
		flexmock(os)
		flexmock(os.path)
		os.path.should_receive('expanduser').with_args('~/.occam').and_return('~/.occam').once()
		os.path.should_receive('exists').and_return(exists)
		if not exists:
			os.should_receive('mkdir').with_args('~/.occam').once()

		Login.is_authenticated() == result


	class TestDo(object):

		def test_do_no_username(self):
			assert Login({},['zero'],None).do() == -1

		def test_do_username_or_password_invalid(self):
			flexmock(getpass)
			getpass.should_receive('getpass').and_return('PASS').once()
			fake_occam=flexmock()
			fake_occam.should_receive('authenticate').with_args('one',password='PASS').and_return(False).once()

			assert Login({},['zero','one'],fake_occam).do() == -1

		def test_do_successful_login(self):
			flexmock(getpass)
			getpass.should_receive('getpass').and_return('PASS').once()
			fake_occam=flexmock()
			fake_occam.should_receive('authenticate').with_args('one',password='PASS').and_return(True).once()
			flexmock(os.path)
			flexmock(os)
			os.path.should_receive('expanduser').with_args('~/.occam').and_return('OCP').once()
			os.path.should_receive('exists').with_args('OCP').and_return(False).once()
			os.should_receive('mkdir').with_args('OCP').once()
			fake_first=flexmock()
			fake_occam.should_receive('searchAccounts').with_args(username='one').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(person=flexmock(uid='UID'))).once()
			fake_file=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('OCP/auth_token','w+').and_return(fake_file).once()
			flexmock(json)
			token = {
      			"username": "one",
				"person": "UID",
				"timestamp": "foo"
			}
			json.should_receive('dumps').with_args(token).and_return('DP').once()
			fake_file.should_receive('write').with_args('DP').once()
			fake_file.should_receive('close').once()

			assert Login({},['zero','one'],fake_occam).do() == None

	'''@pytest.mark.parametrize("args,auth,exists,result", [
		(['zero'],None,None,-1),
		(['zero','one'],False,False,-1),
		(['zero','one'],True,True,None)
	])
	def test_do(self,args,auth,exists,result):
		flexmock(Log)
		Log.should_receive('header').once()
		fake_occam=flexmock()
		fake_occam.should_receive('authenticate').and_return(auth)
		fake_self = flexmock(args=args,occam=fake_occam)

		
		if result == -1:
			if len(args) > 1:
				flexmock(getpass)
				getpass.should_receive('getpass').and_return('PASS').once()
			Log.should_receive('error').once()
		else:
			flexmock(getpass)
			getpass.should_receive('getpass').and_return('PASS').once()
			Log.should_receive('write').once()

			flexmock(os.path)
			os.path.should_receive('expanduser').with_args('~/.occam').and_return('~/.occam').once()
			os.path.should_receive('exists').and_return(exists)
			if not exists:
				os.should_receive('mkdir').with_args('~/.occam').once()
			fake_occam.should_receive('searchAccounts.first').and_return(flexmock(person=flexmock(uid='UID'))).once()

			mock = flexmock(sys.modules['builtins'])
			fake_f = flexmock(write=lambda x:x,close= lambda:None)
			mock.should_receive('open').and_return(fake_f)
			fake_f.should_receive('write').once()
			fake_f.should_receive('close').once()
			Log.should_receive('done').once()
		assert Login.do(fake_self) == result'''
