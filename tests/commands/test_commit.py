from flexmock import flexmock
import pytest

from lib.log            	import Log
from lib.occam             	import Occam
from lib.commands.commit 	import Commit
from lib.db         		import DB

class TestCommit(object):
	def test_initParser(self):
		assert Commit.initParser(1) == 1
	def test_do(self):
		flexmock(Occam)

		fake_obj = flexmock()
		fake_dbobj = flexmock(revision=None)
		Occam.should_receive('objectAt').with_args('.').and_return(fake_obj).once()
		fake_obj.should_receive('head').at_least().once()
		fake_obj.should_receive('updateParents').once()
		fake_obj.should_receive('updateStore').once()
		fake_obj.should_receive('objectInfo').and_return({'id':None}).once()
		Occam.should_receive('searchObjects.first').and_return(fake_dbobj).once()
		flexmock(DB)
		fake_session = flexmock()
		DB.should_receive('session').and_return(fake_session)
		fake_session.should_receive('add').with_args(fake_dbobj).once()
		fake_session.should_receive('commit').once()
		fake_self=flexmock(occam=Occam())
		Commit.do(fake_self)