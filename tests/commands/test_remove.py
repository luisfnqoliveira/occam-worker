from flexmock import flexmock
import pytest

from lib.commands.remove 	import Remove
from lib.commands.status       import Status

from lib.db              import DB
from lib.group      import Group
from lib.workset         import Workset
from lib.experiment         import Experiment
from lib.commands.workflow         import Workflow

import sys
import json
import os

from subprocess import Popen, PIPE

from models.system           import System

class TestRemove(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Remove.initParser(fake) == fake

	class TestDo(object):

		def test_do_occam_system_not_initialized(self):
			with pytest.raises(SystemExit):
				fake_options = None
				fake_args=['zero']
				fake_occam=None

				flexmock(Status)
				Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[5,6]]).once()
				flexmock(DB)
				DB.should_receive('session.query.first').and_return(None).once()
				Remove(fake_options,fake_args,fake_occam).do()

		def test_do_no_type_of_object_given(self):
			with pytest.raises(SystemExit):
				fake_options = flexmock(rootPath=None,objectPath=None)
				fake_args=['zero']
				fake_occam=flexmock()
				fake_occam.configuration={'paths':{'objects':'OBJS'}}

				flexmock(Status)
				Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[5,6]]).once()
				flexmock(DB)
				DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
				Remove(fake_options,fake_args,fake_occam).do()

		def test_do_no_name_given(self):
			with pytest.raises(SystemExit):
				fake_options = flexmock(rootPath=None,objectPath=None)
				fake_args=['zero','one']
				fake_occam=flexmock()
				fake_occam.configuration={'paths':{'objects':'OBJS'}}

				flexmock(Status)
				Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[5,6]]).once()
				flexmock(DB)
				DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
				Remove(fake_options,fake_args,fake_occam).do()

		def test_do_not_within_a_workset(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[{'type':'not_workset'},6]]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			assert Remove(fake_options,fake_args,fake_occam).do() == -1

		def test_do_is_not_a_collaborator_in_workset(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[{'type':'workset','name':'NAME'},6]]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			assert Remove(fake_options,fake_args,fake_occam).do() == 0

		def test_do_removed_as_a_collaborator_to_workset(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','collaborator','two']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			fake_cobj = {'type':'workset','name':'NAME','collaborators':['two']}
			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[fake_cobj,'CPATH']]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			
			fake_file=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('CPATH/object.json','w+').and_return(fake_file).once()
			flexmock(json)
			json.should_receive('dumps').with_args(fake_cobj).and_return('DUMPS').once()
			fake_file.should_receive('write').with_args('DUMPS').once()
			fake_file.should_receive('close').once()

			assert Remove(fake_options,fake_args,fake_occam).do() == 0

		def test_do_not_within_a_workset_or_object(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','not_collaborator','two']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[{'type':'not_workset'},6]]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			assert Remove(fake_options,fake_args,fake_occam).do() == -1

		def test_do_not_within_a_workset_or_object_2(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','not_collaborator','two']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[1,2],[3,4],[None,6]]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			assert Remove(fake_options,fake_args,fake_occam).do() == -1

		def test_do_object_path_and_object_json_path_exist_do_nothing(self):
			fake_options = flexmock(rootPath=None,objectPath=None)
			fake_args=['zero','not_collaborator','two','three']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[{'type':'group','name':'NAME'},'GPATH'],[3,4],[{'type':'workset','name':'NAME'},6]]).once()
			flexmock(DB)
			DB.should_receive('session.query.first').and_return(flexmock(jobs_path='JBPATH')).once()
			
			flexmock(os.path)
			os.path.should_receive('exists').with_args('three/not_collaborator-two').and_return(True).once()
			os.path.should_receive('exists').with_args('three/not_collaborator-two/object.json').and_return(True).once()
			assert Remove(fake_options,fake_args,fake_occam).do() == None


		@pytest.mark.parametrize("type,clazz,id,path",[
			('group',Group,'GID','GPATH'),
			('experiment',Experiment,'EID','EPATH')
		])
		def test_do_object_path_and_object_json_path_both_dont_exist(self,type,clazz,id,path):
			fake_options = flexmock(rootPath=None,objectPath=None,no_db=None)
			fake_args=['zero',type,'two','three']
			fake_occam=flexmock()
			fake_occam.configuration={'paths':{'objects':'OBJS'}}

			fake_cobj = {'type':'workset','name':'NAME','id':'CID'}
			fake_gobj = {'type':'group','name':'NAME','id':'GID'}

			flexmock(Status)
			Status.should_receive('findContextWithPaths').with_args('.').and_return([[fake_gobj,path],[3,4],[fake_cobj,path]]).once()
			flexmock(DB)
			fake_first_0=flexmock()
			fake_session=flexmock()
			DB.should_receive('session').and_return(fake_session)#.at_least().once()
			fake_session.should_receive('query').with_args(System).and_return(fake_first_0).once()
			#DB.should_receive('session.query').with_args(System).and_return(fake_first_0).once()

			fake_first_0.should_receive('first').and_return(flexmock(jobs_path='JBPATH')).once()
			
			flexmock(os)
			flexmock(os.path)
			os.path.should_receive('exists').with_args('three/%s-two'%type).and_return(False).once()
			os.should_receive('mkdir').with_args('three/%s-two'%type).once()
			os.path.should_receive('exists').with_args('three/%s-two/object.json'%type).and_return(False).once()

			flexmock(clazz)
			clazz.should_receive('__init__').once()
			clazz.id=id
			flexmock(DB)
			fake_q=flexmock()
			fake_q_2=flexmock()
			fake_session.should_receive('query').with_args(Workset).and_return(fake_q).once()
			#DB.should_receive('session.query').with_args(Workset).and_return(fake_q).once()
			fake_first=flexmock()
			fake_first_2=flexmock()
			fake_q.should_receive('filter_by').with_args(id='CID').and_return(fake_first).once()
			fake_session.should_receive('query').with_args(Group).and_return(fake_q_2).once()
			#DB.should_receive('session.query').with_args(Group).and_return(fake_q).once()
			fake_q_2.should_receive('filter_by').with_args(id='GID').and_return(fake_first_2).once()
			fake_first.should_receive('first').and_return(flexmock(id='WID')).once()
			fake_first_2.should_receive('first').and_return(flexmock(id='GID')).once()
			fake_session.should_receive('add').with_args(clazz).once()
			#DB.should_receive('session.add').with_args(Group).once()
			#fake_session.should_receive('commit').once()
			#DB.should_receive('session.commit').once()
			flexmock(Workflow)
			Workflow.should_receive('__init__').once()
			fake_session.should_receive('add').with_args(Workflow).once()
			#DB.should_receive('session.add').with_args(Workflow).once()
			fake_session.should_receive('commit').twice()
			#DB.should_receive('session.commit').once()

			fake_file=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('three/%s-two/object.json'%type,'w').and_return(fake_file).once()

			flexmock(json)
			json.should_receive('dumps').with_args({'name':'two','type':type,'id':id}).and_return('DUMPS').once()
			fake_file.should_receive('write').with_args('DUMPS').once()
			fake_file.should_receive('close').once()
			fake_file_2=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('three/%s-two/.gitignore'%type,'w+').and_return(fake_file_2).once()
			if type=='experiment':
				fake_file_2.should_receive('write').with_args('runs\n').once()
			fake_file_2.should_receive('close').once()

			fake_file_3=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('%s/.gitignore'%path,'a+').and_return(fake_file_3).once()
			fake_file_3.should_receive('write').with_args('%s-two'%type).once()
			fake_file_3.should_receive('close').once()

			flexmock(Popen)
			#Popen.should_receive('__init__').with_args(['git', 'commit', '-a', '--file=-'], stdin=PIPE, stdout=PIPE, cwd='GPATH').once()
			fake_popen = flexmock()
			Popen.new_instances(fake_popen)
			fake_popen.stdin = flexmock()
			message = 'adds %s two\n\nOCCAM automated commit'%type
			fake_popen.stdin.should_receive('write').with_args(message.encode('utf-8')).once()
			fake_popen.stdin.should_receive('close').once()
			#fake_popen.should_receive('wait').once()

			#Popen.should_receive('__init__').with_args(['git', 'rev-parse', 'HEAD'], stdout=PIPE, cwd='GPATH')
			fake_popen.should_receive('wait').at_least().once()
			fake_popen.stdout=flexmock()
			fake_popen.stdout.should_receive('read.decode.strip').twice()

			assert Remove(fake_options,fake_args,fake_occam).do() == None