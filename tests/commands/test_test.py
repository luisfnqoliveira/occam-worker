from flexmock import flexmock
import pytest

from tests.commands.helper import options
from tests import helper

from lib.commands.test import Test

class TestTest:
  class TestInitParser:
    @pytest.yield_fixture(autouse=True)
    def setup(self):
      # Hack our mock to keep track of the arguments added
      keys = {}

      def f(a, b, **kw):
        keys[a] = True
        keys[b] = True

      self.fake = flexmock(add_option=f, keys=keys)
      yield

    def test_should_return_passed_parser(self):
      assert Test.initParser(self.fake) == self.fake

  class TestDo:
    def test_should_invoke_pytest(self):
      mock = helper.subprocess()

      Test(options({}), [], None).do()

      assert mock.process.call_args[0][0][0] == "py.test"

    def test_should_log_pytest_result_to_stderr(self):
      mock = helper.subprocess()

      Test(options({}), [], None).do()

      assert '--result-log' in mock.process.call_args[0][0]

      index = mock.process.call_args[0][0].index('--result-log')
      index += 1

      assert index < len(mock.process.call_args[0][0]) and mock.process.call_args[0][0][index] == "/dev/stderr"

    def test_should_pass_a_reader_of_stderr_off_to_parseStream(self):
      import codecs
      import io

      flexmock(codecs).should_receive('getreader').with_args('utf-8').and_return(lambda x:io.StringIO(""))

      Test(options({}), [], None).do()

    def test_should_wait_for_pytest_to_complete(self):
      mock = helper.subprocess()
      mock.process.should_receive('wait').and_return(0).once()

      Test(options({}), [], None).do()
