from flexmock import flexmock
import pytest

from collections import namedtuple

def options(info):
  # Turn the hash given into an object with attributes
  class OptionsResult:
    # This class is just going to represent the dictionary passed in
    def __init__(self, kw):
      self.__dict__.update(kw)

    # Anything unknown will report None
    def __getattr__(self, name):
      return None

  return OptionsResult(info)
