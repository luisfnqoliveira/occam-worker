from flexmock import flexmock
import pytest

from lib.occam               import Occam
from lib.commands.initialize import Initialize

class TestInitialize:
  class TestDo:
    def test_should_call_occam_initialize(self):
      """
      It should call Occam.initialize
      """

      flexmock(Occam).should_receive('initialize').once()

      Initialize({}, [], None).do()

    def test_should_pass_options_to_occam_class(self):
      """
      It should call Occam.initialize with options.
      """

      flexmock(Occam).should_receive('__init__').with_args(options="options").once()
      flexmock(Occam).should_receive('initialize').once()
      Initialize("options", "arguments", "occam").do()
