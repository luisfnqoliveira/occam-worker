from flexmock import flexmock
import pytest

from lib.commands.build 	import Build
from lib.git          import Git

from subprocess import Popen, PIPE


class TestBuild(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Build.initParser(fake) == fake


	class TestDo(object):
		def test_do_3_args_obj_type_is_base_to_object_not_in_options(self):
			fake_options = None
			fake_args=['zero','base','two']
			fake_occam=None

			flexmock(Docker)
			Docker.should_receive('build').with_args('base', 'two', None, None).once()

			assert Build(fake_options,fake_args,fake_occam).do() == 0

		def test_do_3_args_obj_type_isnt_base_to_object_and_revision_not_in_options_cannot_find_buildable_object(self):
			fake_options = None
			fake_args=['zero','not_base','two']
			fake_occam=flexmock()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(object_type = 'not_base',
                                     name        = 'two',
                                     uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()
			assert Build(fake_options,fake_args,fake_occam).do() == -1


		#@pytest.mark.parametrize("build",[
		#	(True),
		#	(False)
		#])
		def test_do_3_args_obj_type_isnt_base_to_object_and_revision_not_in_options_successful_not_local_build(self):
			fake_options = None
			fake_args=['zero','not_base','two']
			fake_occam=flexmock()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(object_type = 'not_base',
                                     name        = 'two',
                                     uuid        = None).and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(revision='OREV',path='OPATH')).once()
			
			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('OPATH',revision='OREV').and_return(fake_obj).once()

			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH','OREV').and_return('OOREV').once()
			Git.should_receive('__init__').with_args('PATH','OOREV').once()
			Git.should_receive('objectInfo').and_return('INFO').once()

			fake_occam.should_receive('build').with_args(fake_obj).and_return(True).once()

			assert Build(fake_options,fake_args,fake_occam).do() == None


		def test_do_1_arg_to_object_in_options_cannot_find_buildable_object(self):
			fake_options = flexmock(to_object='UUID')
			fake_args=['zero']
			fake_occam=flexmock()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='UUID').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(None).once()
			assert Build(fake_options,fake_args,fake_occam).do() == -1


		def test_do_2_args_no_obj_type_given_name_cannot_find_buildable_object(self):
			fake_options = None
			fake_args=['zero','one']
			fake_occam=flexmock()

			fake_occam.should_receive('objectAt').with_args('one',revision='').and_return(None).once()

			assert Build(fake_options,fake_args,fake_occam).do() == -1

		def test_do_2_args_no_obj_type_given_name_revision_in_options_revision_not_found(self):
			fake_options = flexmock(revision='REV')
			fake_args=['zero','one']
			fake_occam=flexmock()

			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('one',revision='REV').and_return(fake_obj).once()

			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH','REV').and_return(None).once()

			assert Build(fake_options,fake_args,fake_occam).do() == -1

		def test_do_1_arg_to_object_in_options_cannot_find_buildable_object(self):
			fake_options = flexmock(revision='OREV')
			fake_args=['zero']
			fake_occam=flexmock()

			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('.',revision='OREV').and_return(fake_obj).once()

			flexmock(Git)
			Git.should_receive('fullRevision').with_args('PATH','OREV').and_return('OOREV').once()
			Git.should_receive('__init__').with_args('PATH','OOREV').once()
			Git.should_receive('objectInfo').and_return('INFO').once()

			fake_occam.should_receive('build').with_args(fake_obj,build_path = '.', localHash=None).and_return(True).once()
			
			assert Build(fake_options,fake_args,fake_occam).do() == None


		def test_do_1_arg_to_object_in_options_cannot_find_buildable_object(self):
			fake_options = flexmock(to_object='UUID')
			fake_args=['zero']
			fake_occam=flexmock()

			fake_first=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='UUID').and_return(fake_first).once()
			fake_first.should_receive('first').and_return(flexmock(uid='UID',revision='')).once()
			fake_occam.should_receive('objectPath').with_args('UID').and_return('PPATH').once()


			fake_obj = flexmock(path='PATH',objectInfo=lambda:{'type':'TYPE','name':'NAME'})

			fake_occam.should_receive('objectAt').with_args('PPATH',revision='').and_return(fake_obj).once()

			flexmock(Popen)
			fake_stdout = flexmock(read=lambda:flexmock(decode=lambda:flexmock(strip=lambda:None)))
			fake_popen = flexmock(stdout=fake_stdout,wait=lambda:None)
			Popen.new_instances(fake_popen)
			#Popen.should_receive('__init__').with_args(['git', 'rev-parse', 'HEAD'], stdout=PIPE, cwd='.').once()
			fake_popen.should_receive('wait').once()
			fake_stdout.should_receive('read.decode.strip').once()

			fake_occam.should_receive('build').with_args(fake_obj).and_return(False).once()
			
			assert Build(fake_options,fake_args,fake_occam).do() == None
