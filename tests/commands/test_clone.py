from flexmock import flexmock
import pytest

from lib.commands.clone 	import Clone

class TestClone(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Clone.initParser(fake) == fake


	class TestDo(object):

		def _do_no_object_id_given(self):
			fake_options = flexmock(revision=None,to_object=None)
			fake_args=['zero']
			fake_occam=None

			assert Clone(fake_options,fake_args,fake_occam).do() == -1