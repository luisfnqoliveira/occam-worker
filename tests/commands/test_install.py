from flexmock import flexmock
import pytest

from lib.commands.install 	import Install
from lib.log            	import Log
from lib.object 			import Object
from lib.db 				import DB

from models.recipe 						import Recipe
import os
import sys
import json

class TestInstall(object):
	def test_initParser(self):
		assert Install.initParser(1) == 1


	class TestDo(object):
		def test_do_obj_type_name_both_None_default_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero'],occam=fake_occam)

			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()

			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('install').once()
			Install.do(fake_self)

		def test_do_obj_type_name_both_None_given_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero'],occam=fake_occam)

			flexmock(Object)
			Object.should_receive('__init__').with_args('PATH',occam=fake_occam).once()

			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('install').once()
			Install.do(fake_self,'PATH')

		def test_do_obj_type_None_default_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one'],occam=fake_occam)

			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()

			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('install').once()
			Install.do(fake_self)

		def test_do_obj_type_None_given_path(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one'],occam=fake_occam)

			flexmock(Object)
			Object.should_receive('__init__').with_args('PATH',occam=fake_occam).once()

			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('install').once()
			Install.do(fake_self,'PATH')

		def test_do_obj_type_name_both_exist(self):
			fake_occam = flexmock()
			fake_self = flexmock(args=['zero','one','two'],occam=fake_occam)
			fake_first = flexmock(first=lambda:flexmock(uid='UID'))
			fake_occam.should_receive('objectPath').with_args('UID').and_return('UIDD').once()
			fake_occam.should_receive('searchObjects').with_args(object_type='one',name='two').and_return(fake_first).once()

			flexmock(Object)
			Object.should_receive('__init__').with_args('UIDD',occam=fake_occam).once()

			Object.should_receive('objectInfo').and_return({})
			Object.should_receive('install').once()
			# path doesn't matter for this case
			Install.do(fake_self)

	class TestRakeRecipes(object):

		def test_rake_recipes_path_not_exists(self):
			flexmock(os.path)

			os.path.should_receive('exists').and_return(False).once()
			Install.rake_recipes(None,None,None,None,None)

		def test_rake_recipes_path_exists_all_files(self):
			flexmock(os.path)
			os.path.should_receive('exists').and_return(True).once()
			flexmock(os)
			os.should_receive('listdir').and_return(['a','b.json','c']).once()
			os.path.should_receive('isdir').and_return(False).at_least().once()

			flexmock(sys.modules['builtins']).should_receive('open').with_args('None/b.json','r').and_return(None).once()
			flexmock(json)
			json.should_receive('load').and_return({'name':'NAME','section':'SEC','description':'DESC','configuration':'CONF'}).once()

			flexmock(Recipe)
			fake_recipe = flexmock(name=None,occam_object_id=None,section=None,description=None,configuration_document_id=None)
			Recipe.new_instances(fake_recipe)

			flexmock(DB)
			fake_session = flexmock()
			DB.should_receive('session').and_return(fake_session)
			fake_session.should_receive('add').with_args(fake_recipe).once()

			Install.rake_recipes(None,None,None,None,flexmock(id='ID'))

		'''def _rake_recipes_path_exists_all_dirs(self):
			flexmock(os.path)
			os.path.should_receive('exists').and_return(True).at_least().once()
			flexmock(os)
			os.should_receive('listdir').and_return([None,None,None]).at_least().once()
			os.path.should_receive('isdir').and_return(True).at_least().once()
			flexmock(Install)
			Install.should_receive('rake_recipes').with_args(None,None,"None\\None",None).at_least().once()

			Install.rake_recipes(None,None,None,None)'''
