from flexmock import flexmock
import pytest
import re
import os 
import json        # For parsing recipes
import sys

# For pulling network http resources
from urllib.request import Request  # Python 3
import urllib.request

from subprocess import Popen, PIPE

from lib.commands.pull 		import Pull
#from lib.log            	import Log
from lib.object         	import Object
from lib.git            import Git

class TestPull(object):
	def test_initParser(self):
		assert Pull.initParser(1) == 1

	class TestDo(object):

		def test_do_no_url_given(self):
			fake_options = None
			fake_args=['zero']
			fake_occam=None

			assert Pull(fake_options,fake_args,fake_occam).do() == -1

		# NOT PASSED: if not "self" in object_info: object_info is None
		def _do_2_args_url_given_no_path_not_match_local_json_path_not_exists(self):
			fake_options = None
			fake_args=['zero','one']
			fake_occam=flexmock()

			flexmock(re)
			re.should_receive('match').with_args(r'^[a-z]+://', 'one').and_return(False).once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('one/object.json').and_return(False).once()

			assert Pull(fake_options,fake_args,fake_occam).do() == 0

		def test_do_2_args_url_given_no_path_not_match_local_json_path_exists(self):
			fake_options = None
			fake_args=['zero','one']
			fake_occam=flexmock()

			flexmock(re)
			re.should_receive('match').with_args(r'^[a-z]+://', 'one').and_return(False).once()

			flexmock(os.path)

			os.path.should_receive('exists').with_args('one/object.json').and_return(True).once()
			
			flexmock(sys.modules['builtins']).should_receive('open').with_args('one/object.json').and_return('TOLOAD').once()
			flexmock(json)

			fake_info={'type':'TYPE','name':'NAME','id':'ID'}#,'self':[{},{'git':'GP1'},{'git':'GP2'}]}

			json.should_receive('load').with_args('TOLOAD').and_return(fake_info).once()

			flexmock(Object)
			Object.should_receive('__init__').with_args('one',occam=fake_occam).once()
			flexmock(Git)
			Git.should_receive('__init__').with_args('one').once()
			fake_occam.should_receive('storeObject').with_args(Object).once()
			fake_occam.should_receive('retrieve').with_args('ID','TYPE','HEAD').once()
			assert Pull(fake_options,fake_args,fake_occam).do() == 0

	@pytest.mark.parametrize("args,match,po,exists,store,selv,gitpath,error,result",[ #,options,method,expected", [
		#(['zero'],flexmock(object_type="person",name="NAME",uuid="UUID"),'searchPeople',-1),
		#(['zero','one'],flexmock(object_type="other",name="NAME",uuid="UUID"),'searchObjects', None),
		#(['zero','one','two'], None, None),
		#(['zero','one','two','three','four'], None, None),
		#(['zero','one','two','three'], 0),
		#(['zero','one','two','three','four','five'], 0)

		# Log.error("no url given")
		(['zero'],None,None,None,None,{},None,None,-1),
		
		# NOT PASSED: if not "self" in object_info: # This is the only possibility for git_path to be None
		#(['zero','one'],False,None,False,True,None,None,None,-1),
		
		(['zero','one'],False,None,True,True,{'self':[{},{},{'git':'PATH'}]},'one',None,0),
		
		# NOT PASSED: local variable 'response' referenced before assignment @ 107
		#(['zero','one'],True,0,None,True,{'self':[{},{},{'git':'PATH'}]},'one',None,0),
		
		(['zero','one'],True,-1,None,None,{'self':[{},{},{'git':'PATH'}]},'one',SystemExit,0),
	])
	def test_do(self,args,match,po,exists,store,selv,gitpath,error,result):
		
		#flexmock(Log)
		flexmock(re)
		flexmock(Object)
		flexmock(os.path)
		flexmock(json)
		flexmock(Git)
		flexmock(sys.modules['builtins']).should_receive('open').and_return(None)
		flexmock(Request)
		flexmock(Popen)

		''' flexmock has trouble supporting keyword-only args (PEP 3102)
			so urlopen can't be mocked for now
		'''
		#flexmock(urllib.request)
		#urllib.request.should_receive('urlopen').and_return(flexmock(getcode= lambda :'CODE'))

		#Log.should_receive('header').at_least().once()

		re.should_receive('match').and_return(match)
		os.path.should_receive('exists').and_return(exists)

		load_info = {'id':'ID','name':'NAME','type':'TYPE'}
		load_info.update(selv)
		json.should_receive('load').and_return(load_info)

		Git.should_receive('objectInfo').and_return(load_info)

		Request.should_receive('add_header')
		Request.should_receive('__init__')

		Popen.new_instances(flexmock(wait=lambda :po))

		fake_occam = flexmock()
		fake_self = flexmock(args = args,occam=fake_occam)

		#if result == -1:
			#Log.should_receive('error').once()
		#el
		if store:
			#Log.should_receive('done').once()
			Object.should_receive('__init__').with_args(gitpath,occam=fake_occam).once()
			Git.should_receive('__init__').with_args(gitpath).once()
			fake_occam.should_receive('storeObject').once()
			fake_occam.should_receive('retrieve').once()

		if error:
			with pytest.raises(error):
				assert Pull.do(fake_self) == result
		else:
			assert Pull.do(fake_self) == result