from flexmock import flexmock
import pytest

from lib.commands.set 		import Set
from lib.log            		import Log
from lib.object         import Object

class TestSet(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Set.initParser(fake) == fake


	class TestDo(object):

		def test_do_nothing_in_options_no_object_id_given(self):
			fake_options = flexmock(revision=None,to_object=None)
			fake_args=['zero']
			fake_occam=None
			flexmock(Object)
			Object.should_receive('__init__').with_args('.',occam=fake_occam).once()

			assert Set(fake_options,fake_args,fake_occam).do() == -1

		def test_do_3_args_to_object_in_options_do_nothing(self):
			fake_options = flexmock(revision=None,to_object='OBJID',within_object=None,within_revision=None)
			fake_args=['zero','one','two']
			fake_occam=flexmock()
			fake_occam.should_receive('temporaryClone').with_args('OBJID','HEAD',None,None).and_return((None,None,None)).once()

			assert Set(fake_options,fake_args,fake_occam).do() == None

		def test_do_4_args_to_object_and_revision_in_options(self):
			fake_options = flexmock(revision=None,to_object='OBJID',within_object=None,within_revision=None)
			fake_args=['z.z\\z.ee\\rr.oo.o','one','t.t\\t.ww\\o','three']
			fake_occam=flexmock()
			fake_info={}
			fake_obj=flexmock(objectInfo=lambda:fake_info)
			fake_occam.should_receive('temporaryClone').with_args('OBJID','HEAD',None,None).and_return((None,fake_obj,None)).once()

			fake_obj.should_receive('updateObject').with_args(fake_info, 'changing value of one').once()

			assert Set(fake_options,fake_args,fake_occam).do() == None

	'''
	@pytest.mark.parametrize("args,oprev,opto,tempc,result",[#,auth,exists,result", [
		#(['zero'],None,None,-1),
		#(['zero','one'],False,False,-1),
		#(['zero','one'],True,True,None)
		#(['zero','one','two','three'],None,-1),
		#(['zero','one','two'],'URL',-1)

		([],None,None,None,-1),
		
		
		#(['zero','one','two'],False,{'type':'TYPE','name':'NAME'},-1),
		#(['zero','one']),
		#(['zero','one','two']),

	])
	def _do(self,args,oprev,opto,tempc,result):
		fake_occam=flexmock()
		fake_self=flexmock(occam =fake_occam, options=flexmock(revision=oprev,to_object=opto,within_object='BLAH',within_revision='BLAH',))
		flexmock(Log)
		if result==-1:
			Log.should_receive('error').once()

		assert Set.do(fake_self) == result'''