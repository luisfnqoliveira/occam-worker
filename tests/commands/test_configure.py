from flexmock import flexmock
import pytest

from lib.commands.configure import Configure

from lib.object         import Object
from lib.git            import Git

import os
import sys
import json


class TestConfigure(object):
	def test_initParser(self):
		fake = flexmock(add_option=lambda x:x)
		fake.should_receive('add_option').at_least().once()
		assert Configure.initParser(fake) == fake

	def test_validateTest(self):
		# inner class
		pass

	def test_rake_ranges(self):
		# recursive call
		pass

	class TestValidate(object):

		def test_validate(self):
			#recursive call
			pass

	class TestDo(object):

		def test_do_not_within_an_experiment(self):
			fake_options = flexmock(revision='REV',to_object=None,within_object=None,within_revision=None)
			fake_args=['zero','one','two','three']
			fake_occam=flexmock()

			fake_occam.should_receive('objectAt').with_args('.').and_return(None).once()

			assert Configure(fake_options,fake_args,fake_occam).do() == -1

		def test_do_experiment_has_no_workflow(self):
			fake_options = flexmock(revision='REV',to_object=None,within_object=None,within_revision=None)
			fake_args=['zero','one','two','three']
			fake_occam=flexmock()

			fake_experiment_info={'type':'experiment'}#,'workflow':{'connections':{}}}
			fake_experiment=flexmock(objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()

			assert Configure(fake_options,fake_args,fake_occam).do() == -1

		def test_do_node_identifier_required(self):
			fake_options = flexmock(revision='REV',to_object=None,within_object=None,within_revision=None)
			fake_args=['zero']
			fake_occam=flexmock()

			fake_experiment_info={'type':'experiment','workflow':{'connections':[]}}
			fake_experiment=flexmock(objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()

			assert Configure(fake_options,fake_args,fake_occam).do() == -1

		def test_do_node_identifier_invalid(self):
			fake_options = flexmock(revision='REV',to_object=None,within_object=None,within_revision=None)
			fake_args=['zero','1','two','three']
			fake_occam=flexmock()

			fake_experiment_info={'type':'experiment','workflow':{'connections':[1]}}
			fake_experiment=flexmock(objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()

			assert Configure(fake_options,fake_args,fake_occam).do() == -1

		def test_do_cant_find_object(self):
			with pytest.raises(SystemExit) as ex:

				fake_options = flexmock(revision='REV',to_object=None,within_object=None,within_revision=None)
				fake_args=['zero','1','two','three']
				fake_occam=flexmock()

				fake_experiment_info={'type':'experiment','workflow':{'connections':[1,
																					{'object':{	'type':'TYPE',
																								'name':'NAME',
																								'revision':'OREV',
																								'id':'ID'}}]}}
				fake_experiment=flexmock(objectInfo=lambda:fake_experiment_info)
				fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()
				fake_first_344=flexmock()
				fake_occam.should_receive('searchObjects').with_args(uuid='ID').and_return(fake_first_344).once()
				fake_first_344.should_receive('first').and_return(None).once()

				Configure(fake_options,fake_args,fake_occam).do()

		def test_do_successful_configured_object(self):
			fake_options = flexmock(revision='REV',to_object='TOOBJ',within_object=None,within_revision=None)
			fake_args=['zero','1','two','three']
			fake_occam=flexmock()

			fake_workset=flexmock()
			fake_experiment_info={'type':'experiment','workflow':{'connections':[1,
																					{'object':{	'type':'TYPE',
																								'name':'NAME',
																								'revision':'OREV',
																								'id':'ID'}}]}}
			fake_experiment=flexmock(path='EPATH',objectInfo=lambda:fake_experiment_info)
			fake_occam.should_receive('temporaryClone').with_args('TOOBJ','REV',None,None).and_return((fake_workset,fake_experiment,'WPATH')).once()
			#fake_occam.should_receive('objectAt').with_args('.').and_return(fake_experiment).once()
			
			fake_first_344=flexmock()
			fake_occam.should_receive('searchObjects').with_args(uuid='ID').and_return(fake_first_344).once()
			fake_first_344.should_receive('first').and_return(flexmock(uid='UID')).once()
			fake_occam.should_receive('objectPath').with_args('UID').and_return('OBJPATH').once()
			flexmock(Object)
			Object.should_receive('__init__').with_args('OBJPATH',revision='OREV', occam=fake_occam).once()
			flexmock(Git)
			Git.should_receive('fullRevision').with_args('OBJPATH','OREV').and_return(True).once()
			fake_obj_info={'configurations':[{'file':'FILE','name':'NAME','schema':'SCHEMA'}]}
			Object.should_receive('objectInfo').and_return(fake_obj_info)
			flexmock(os.path)
			os.path.should_receive('join').with_args('EPATH','configurations').and_return('CPATH').once()
			os.path.should_receive('exists').with_args('CPATH').and_return(False).once()
			flexmock(os)
			os.should_receive('mkdir').with_args('CPATH').once()
			
			os.path.should_receive('join').with_args('CPATH','1').and_return('CCPATH').once()
			os.path.should_receive('exists').with_args('CCPATH').and_return(False).once()
			flexmock(os)
			os.should_receive('mkdir').with_args('CCPATH').once()
			Object.git=flexmock()
			fake_schema={
				'key':{'type':'TYPE','default':'default'}
			}
			Object.git.should_receive('retrieveJSON').with_args('SCHEMA').and_return(fake_schema).twice()
			os.path.should_receive('join').with_args('CCPATH','FILE').and_return('CCCPATH').twice()
			os.path.should_receive('exists').with_args('CCCPATH').and_return(False).twice()

			fake_file=flexmock()
			flexmock(sys.modules['builtins']).should_receive('open').with_args('CCCPATH','w+').and_return(fake_file).once()
			flexmock(json)
			json.should_receive('dumps').with_args({'key':'default'}).and_return('DUMPS').once()
			fake_file.should_receive('write').with_args('DUMPS').once()
			fake_file.should_receive('close').once()

			fake_experiment.should_receive('purgeGenerates').once()
			fake_experiment.git=flexmock()
			fake_experiment.git.should_receive('add').with_args('CCPATH').once()
			fake_experiment.should_receive('commit').with_args('Updates configuration').once()

			#os.path.should_receive('join').with_args('CCPATH','FILE').and_return('CCCPATH').once()
			#os.path.should_receive('exists').with_args('CCCCPATH').and_return(True).once()

			#flexmock(sys.modules['builtins']).should_receive('open').with_args('CCCCPATH','r').and_return('CCCCPATHFILE').once()
			#json.should_receive('load').with_args('CCCCPATHFILE').and_return({}).once()

			#fake_file_558=flexmock()
			#flexmock(sys.modules['builtins']).should_receive('open').with_args('CCCCPATH','w+').and_return(fake_file_558).once()
			#json.should_receive('dumps').with_args({}).and_return('CONFIGDUMPS').once()
			#fake_file_558.should_receive('write').with_args('CONFIGDUMPS').once()
			#fake_file_558.should_receive('close').once()
			#fake_experiment.git.should_receive('add').with_args('CCCCPATH').once()
			#fake_experiment.should_receive('commit').with_args('Updates configuration','CCCCPATH').once()

			assert Configure(fake_options,fake_args,fake_occam).do() == 0

	def test_form_range(self):
		# WORK!
		pass
