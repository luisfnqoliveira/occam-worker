# Import these:
from flexmock import flexmock
import pytest

# Run with:
# python -m pytest tests/example_test.py

# Run all tests:
# python -m pytest

# Also can invoke with:
# pyenv exec py.test

# Normal Testing
# ==============

# Function to test (presumedly imported from another file):
def func(x):
  return x + 1

# Test: (test_x <-- test functions have test_ prefixes)
def test_answer():
  assert func(3) == 4

# Testing Exception Throws
# ========================

# Function to test: (again, this would be imported or mocked):
def f():
  raise SystemExit(1)

# Test that the above function raises some exception:
def test_mytest():
  with pytest.raises(SystemExit):
    f()

# Creating a Temporary Directory
# ==============================

# Test function arguments have special semantics
# Type `pyenv exec py.test --fixtures` to see a list of available ones to use
# In this case, tmpdir will give you a temporary directory:
def test_needsfiles(tmpdir):
  assert True

# Encapsulating Tests
# ===================

# You can place tests within a class (prefixed with Test):
class TestExample:
  # Now we can use instance methods (prefixed with test_) to write our tests:
  def test_one(self):
    assert True

  # These tests are related, so this is a good way to organize the tests:
  def test_two(self):
    assert True

# Mocks
# =====

# To make sure that a function is not _actually_ called, you can use a mock.

# We are using flexmock, a library that makes this a bit easier because we do
# a lot of heavy storage and processing stuff we don't _actually_ want to do
# to test cohesion etc.
class Foo:
  def some_function(self):
    return "hello"

# We are testing this function, which makes use of some other class.
# We are NOT testing the Foo class, so we should mock that class instead of
# running its code. We don't care about Foo. We care about blah! haha.
def blah():
  return Foo().some_function()

# Test function:
def test_foo():
  # Let's change the actual return value just to illustrate
  # But let's NOT allow it to call the actual function
  # That is, this will make it so when this function is called, it simply
  # returns the value.
  flexmock(Foo).should_receive('some_function').and_return("nope")
  assert blah() == "nope"

# Mocking Python Stdlib
# =====================

# Here we are using open(): which is not really something we want to actually
# call.
def foo():
  f = open('/some/file')
  return f.read()

# Here is how we would mock open():
import sys # import sys at the top of the file for sys.modules
def test_mock():
  mock = flexmock(sys.modules['builtins'])
  (mock.should_receive('open')
     .with_args('/some/file')
     .and_return(flexmock(read=lambda: 'file contents')))
  assert foo() == "file contents"
