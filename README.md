# OCCAM Worker Node

This project makes use of the standard OCCAM database schema to issue simulation work on a compute node.

## Project Layout

```
:::text

lib               - application code
\- db.py          - database management
\- worker.py      - Worker class that spawns processes

models            - data models and representation
\- account.py     - user accounts and authorization
\- experiment.py  - experiment information
\- simulator.py   - simulator information
\- job.py         - contains the jobs for the workers
```

## Usage

Clone this repo:

```
:::text
git clone https://bitbucket.org/occam/occam-worker
```

Inside the occam-worker directory, run:

```
:::text
cd occam-worker
./occam-worker
```

## Command Line Options

For a listing of command line options, type:

```
:::text
./occam-worker --help
```

## Development

Install python 3 if you haven't already.

Clone this repo:

```
:::text
git clone https://bitbucket.org/occam/occam-worker
```

Inside the occam-worker directory, install python dependencies:

```
:::text
cd occam-worker
pip install -r requirements.txt
```

The requirements.txt file contains a list of all of the libraries we will
use. Add a line to this whenever you require a new library.

You'll need a database that works with [occam-web](http://bitbucket.org/occam/occam-web).
You can pull it from occam-web. (Install ruby and bundler per instructions at the
occam-web repo.)

The directories `occam-web` and `occam-worker` should be in the same directory (for instance
both should be inside your home directory.)

```
:::text
cd ..      # go out of occam-worker to its parent directory.
git clone https://bitbucket.org/occam/occam-web
cd occam-web
bundle install
rackup
```

Open the webpage at `http://localhost:9292/` and play with the site to add
new simulators, experiments, etc.

### Migrations

We use Alembic. To auto-generate a database migration from any edits to the files in `models` type:

```
:::text
alembic revision --autogenerate -m "Commit message!"
```

And to run any and all migrations:

```
:::text
alembic upgrade head
```

## License

This software is licensed under the AGPL 3.0. See `LICENSE.txt` for a full description of the license.
Images and art are licensed under a CC-0 license unless otherwise specified.
