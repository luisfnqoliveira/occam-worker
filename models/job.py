# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float, DateTime

from models.run import Run
from models.experiment import Experiment
from models.workset import Workset
from models.person import Person

class Job(Base):
  __tablename__ = 'jobs'

  id                = Column(Integer, primary_key=True)

  command           = Column(String)

  run_id            = Column(Integer, ForeignKey('runs.id'))

  run_index         = Column(Integer)
  job_index         = Column(Integer)
  vm_index          = Column(Integer)

  task_uid          = Column(String)

  codependant       = Column(Integer)
  has_dependencies  = Column(Integer)

  uid               = Column(String)

  root_job_id       = Column(Integer)

  status            = Column(String)
  kind              = Column(String)

  experiment_id       = Column(Integer, ForeignKey('experiments.id'))
  experiment_revision = Column(String)
  workset_id          = Column(Integer, ForeignKey('worksets.id'))
  workset_revision    = Column(String)
  person_id           = Column(Integer, ForeignKey('people.id'))

  connections         = Column(String)

  log_file          = Column(String)
  elapsed_time      = Column(Float)

  published           = Column(DateTime)
  updated             = Column(DateTime)
