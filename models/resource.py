# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Text, DateTime

class Resource(Base):
  """
  This model represents an instance of a Resource. This is generally some
  file that is retrieved from the given source. This will have a revision
  tag (a hash, perhaps) that identifies the instance of the resource. Some
  resources version themselves, such as mercurial or git repositories.
  """

  __tablename__ = 'resources'

  id              = Column(Integer, primary_key=True)

  # A reference to the object that provides the environment
  occam_object_id = Column(Integer, ForeignKey('objects.id'))

  # The known revision (or hash, etc)
  revision        = Column(String(128))
