# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, DateTime
from sqlalchemy.orm import relationship, backref

class Node(Base):
  __tablename__ = 'nodes'

  id            = Column(Integer, primary_key=True)

  # Whether or not this is an external node or part of our cluster
  external      = Column(Integer, default=1)

  # The name of the host for this node
  host          = Column(String(128))

  # A friendlier name for this node
  name          = Column(String(128))

  # The port for http requests
  http_port     = Column(Integer, default=80)

  # Whether or not http requests are using HTTPS
  http_ssl      = Column(Integer, default=0)

  # The port for AMQP requests
  amqp_port     = Column(Integer, default=0)

  # The username for AMQP access
  amqp_username = Column(String(128), default="")

  # The password for AMQP access
  amqp_password = Column(String(128), default="")

  # Capabilities (such as 'git', 'http', 'amqp', 'worker') delimited by a ';'
  capabilities  = Column(String(256), default="http")
