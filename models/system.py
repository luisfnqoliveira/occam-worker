# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from lib.log    import Log
from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Float

class System(Base):
  __tablename__ = 'systems'

  id                = Column(Integer, primary_key=True)

  # The hostname of this node to the outside world
  host              = Column(String)

  # The port of this node to the outside world
  port              = Column(Integer)

  # The default roles to accounts created through the web portal sign-up
  default_roles     = Column(String(128))

  moderate_objects  = Column(Integer)
  moderate_accounts = Column(Integer)

  curate_git        = Column(Integer)
  curate_hg         = Column(Integer)
  curate_svn        = Column(Integer)
  curate_store      = Column(Integer)
