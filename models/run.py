# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, DateTime, Float

from models.person   import Person
from models.object   import Object
from models.workset  import Workset
from models.experiment import Experiment

class Run(Base):
  __tablename__ = 'runs'

  id                  = Column(Integer, primary_key=True)
  uid                 = Column(String)

  person_id           = Column(Integer, ForeignKey('people.id'))

  experiment_id       = Column(Integer, ForeignKey('experiments.id'))
  experiment_revision = Column(String)

  workset_id          = Column(Integer, ForeignKey('worksets.id'))
  workset_revision    = Column(String)

  status              = Column(String)

  published           = Column(DateTime)
  updated             = Column(DateTime)

  archived            = Column(Integer)

  elapsed_time        = Column(Float)
