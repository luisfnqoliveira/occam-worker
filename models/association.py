# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Text, DateTime

class Association(Base):
  """
  This model represents a mapping from an object type to a viewer. This can be
  attached to a particular Person such that this is the preferred viewer for
  just them. The Person id can be null such that this is the default system-wide
  association.
  """

  __tablename__ = 'associations'

  id              = Column(Integer, primary_key=True)

  # The Viewer Object type and subtype (subtype can be null)
  views_type      = Column(String(128))
  views_subtype   = Column(String(128))

  # A reference to the object that provides the environment
  occam_object_id = Column(Integer, ForeignKey('objects.id'))

  # The known revision (or hash, etc) Can be null to use the latest.
  revision        = Column(String(128))

  # Which person recently used it
  person_id       = Column(Integer, ForeignKey('people.id'))
