# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text

from models.workset import Workset
from models.object  import Object

class LocalLink(Base):
  __tablename__ = 'local_links'

  id         = Column(Integer, primary_key=True)
  path       = Column(String(128))
  workset_id = Column(Integer, ForeignKey('worksets.id'))
  person_id  = Column(Integer, ForeignKey('people.id'))
