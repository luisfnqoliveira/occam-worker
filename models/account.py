# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bcrypt

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, DateTime

from models.person   import Person

class Account(Base):
  __tablename__ = 'accounts'

  id              = Column(Integer, primary_key=True)

  username        = Column(String(128), unique=True)
  hashed_password = Column(String(128))
  roles           = Column(String(128))
  active          = Column(Integer)
  email           = Column(String(128))
  email_public    = Column(Integer, default=0)
  person_id       = Column(Integer, ForeignKey('people.id'))
  reset_token     = Column(String(128))
  expired         = Column(DateTime)
  activated       = Column(Integer, default=0)

  published       = Column(DateTime)
  updated         = Column(DateTime)
  private_key     = Column(Text)
