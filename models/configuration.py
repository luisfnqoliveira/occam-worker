# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

from sqlalchemy.orm import relationship
from models.configuration_schema import ConfigurationSchema
from models.object import Object

from bson.objectid         import ObjectId

class Configuration(Base):
  __tablename__ = "configurations"

  # Primary key
  id                        = Column(Integer, primary_key=True)

  # The file that will be created with the configuration options.
  file                      = Column(String(128))

  # The name of this configuration
  name                      = Column(String(128))

  # The file that contains the schema in the script repo.
  schema_file               = Column(String(128))

  # The schema document foreign key.
  schema_document_id        = Column(String(128))

  # The OCCAM object foreign key that these configurations are for.
  occam_object_id = Column(Integer, ForeignKey('objects.id'))
  occam_object = relationship("Object")

  def schema(self):
    data = ConfigurationSchema.query().find_one({"_id": ObjectId(self.schema_document_id)})
    data.pop("_id", None)
    return data
