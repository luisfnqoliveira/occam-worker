# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base
from lib.config import Config

from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, Text, Text, DateTime
from sqlalchemy import Index, func

class Object(Base):
  __tablename__ = 'objects'

  id               = Column(Integer, primary_key=True)

  name             = Column(String(128))
  uid              = Column(String(36))
  object_type      = Column(String(128))
  object_type_safe = Column(String(128))
  subtype          = Column(String(128))
  revision         = Column(String(128))
  organization     = Column(String(128))
  resource         = Column(String(128))
  source           = Column(String(128))

  description      = Column(Text)
  runnable         = Column(Integer)

  configures       = Column(String(128))

  owner_uid        = Column(String(128))

  tags             = Column(Text, default=";;")

  built            = Column(Integer)
  active           = Column(Integer)
  private          = Column(Integer, default=0)

  published        = Column(DateTime)
  updated          = Column(DateTime)
  locked           = Column(Integer, default=0)

  environment      = Column(String(128))
  architecture     = Column(String(128))
  capabilities     = Column(Text, default=";;")

  # Associations

  workset          = relationship("Workset",    uselist=False, backref="object")
  group            = relationship("Group",      uselist=False, backref="object")
  person           = relationship("Person",     uselist=False, backref="object")
  experiment       = relationship("Experiment", uselist=False, backref="object")
  provider         = relationship("Provider",   uselist=False, backref="object")
