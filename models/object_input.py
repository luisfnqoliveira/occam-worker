# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.db     import Base

from sqlalchemy import Column, Integer, String, ForeignKey, Text, Boolean

from models.object import Object

class ObjectInput(Base):
  __tablename__ = 'object_inputs'

  # Primary key
  id                        = Column(Integer, primary_key=True)

  # The type of object
  object_type               = Column(String(128))
  object_type_safe          = Column(String(128))

  # The subtype for this object
  subtype                   = Column(String(128))

  # The revision of the parent object
  revision                  = Column(String(128))

  # The OCCAM object foreign key that these configurations are for.
  occam_object_id           = Column(Integer, ForeignKey('objects.id'))

  # Whether or not this is a corunning object
  fifo                      = Column(Integer)
