#!/usr/bin/env python3

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Lib: Simulator (lists/edits simulator description)
# Lib: Build     (build details) - generate docker file
# Lib: Runner    (can run the simulator through docker or natively)
# Lib: Option    (lists/edits job options)

# occam describe name=sniper
# name: sniper

# occam describe
import sys

from lib.cli import CLI
from lib.log import Log

def main(argv=None):
  if argv is None:
    argv = sys.argv

  if len(argv) == 1:
    Log.error("No command given")
    return -1

  # The command is the first argument
  # For instance, "occam pull", argv[1] would be 'pull'
  return CLI.execute(argv[1:])

if __name__ == "__main__":
  exit(main())
