#!/usr/bin/env python

import sys

from optparse      import OptionParser

from lib.log           import Log
from lib.occam          import Occam

from lib.amqp      import AMQP

class Usage(Exception):
  def __init__(self, msg):
    self.msg = msg

def main(argv=None):
  if argv is None:
    argv = sys.argv

  parser = OptionParser()
  parser.add_option("-v", "--verbose",  action = "store_true",
                                        dest   = "verbose",
                                        help   = "prints all messages")

  parser.add_option("-r", "--rootPath", action = "store",
                                        type   = "string",
                                        dest   = "rootPath",
                                        help   = "the root path for occam")

  parser.add_option("-d", "--daemon",   action = "store_true",
                                        dest   = "daemon",
                                        help   = "runs as a daemon")

  parser.add_option("-D", "--development", action = "store_true",
                                           dest   = "development",
                                           help   = "use an sqlite database")

  parser.add_option("-P", "--mongo-port", action = "store",
                                          type   = "int",
                                          dest   = "mongo_port",
                                          help   = "mongo database port")

  parser.add_option("-N", "--mongo-name", action = "store",
                                          type   = "string",
                                          dest   = "mongo_name",
                                          help   = "mongo database name")

  parser.add_option("-H", "--mongo-host", action = "store",
                                          type   = "string",
                                          dest   = "mongo_host",
                                          help   = "mongo database host")
  parser.add_option("-L", "--logtype", action = "store",
                                       dest   = "logtype",
                                       help   = "the logging text for commands. Possible options are: 'text' (default), and 'json'.")

  opts, args = parser.parse_args()
  occam = Occam(options=opts)

  # Initialize Logger
  Log.initialize(opts)

  if not occam.initialized():
    Log.error("OCCAM is not initialized.")
    return -1

  AMQP(opts, occam).run()

  return 0

if __name__ == "__main__":
  sys.exit(main())
