# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log        import Log
from lib.git        import Git
from lib.group      import Group
from lib.object     import Object
from lib.experiment import Experiment

import os
import json

from uuid import uuid1

class Workset():
  """
  This class represents an OCCAM workset.
  """

  @staticmethod
  def create(path, name, occam=None):
    """
    Creates the workset on disk.
    """

    Object.create(path, name, "workset", root=None, build=False, occam=occam, belongsTo=None, createPath=True)
    return Workset(path, occam=occam)

  def __init__(self, path, occam=None, revision="HEAD"):
    """
    Creates an instance of a workset wrapping an existing workset at the
    given path. It can take any path and will search for the workset in
    parent paths until finding a workset object.
    """

    self.object = Object(path, revision=revision, occam=occam, root=None)

  def __getattr__(self, method_name):
    """
    This will override method calls which don't exist on Experiment and
    pass them to the internal Object.
    """
    return getattr(self.object, method_name)

  def experiments(self):
    """
    Pull out all experiment instances for this workset at this revision.
    """

    # For every experiment in our dependencies, generate a Group instance to either
    # its place on our local disk, or the OCCAM cache.
    info = self.objectInfo()

    experiments = []
    info['contains'] = info.get('contains', [])

    for dependency in info['contains']:
      if dependency.get('type') == 'experiment':
        # Group ids are experiment slugs followed by a UUID. The directory name will
        # be the part preceding the UUID within this experiment id:
        #   experiment-foo-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX

        # Therefore, the substring from 0 to len(str) - 37 is the default experiment
        # directory
        experiment_id = (dependency.get('id')
          or "experiment-unnamed-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        experiment_slug = experiment_id[0 : len(experiment_id) - 37]
        experiment_path = os.path.join(self.path, experiment_slug)

        # Check if this experiment has this id
        found = False
        if os.path.exists(experiment_path):
          experiment = Group(experiment_path)
          if experiment.objectInfo().get('id') == experiment_id:
            experiments.append(Group(experiment_path, dependency['revision']))
            found = True

        if not found and self.occam:
          experiment_path = self.occam.objectPath(experiment_id)
          if os.path.exists(experiment_path):
            experiment = Group(experiment_path)
            if experiment.objectInfo().get('id') == experiment_id:
              experiments.append(Group(experiment_path, dependency['revision']))
              found = True

    return experiments

  def groups(self):
    """
    Pull out all group instances for this workset at this revision.
    """

    # For every group in our dependencies, generate a Group instance to either
    # its place on our local disk, or the OCCAM cache.
    info = self.objectInfo()

    groups = []
    info['contains'] = info.get('contains', [])

    for dependency in info['contains']:
      if dependency.get('type') == 'group':
        # Group ids are group slugs followed by a UUID. The directory name will
        # be the part preceding the UUID within this group id:
        #   group-foo-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX

        # Therefore, the substring from 0 to len(str) - 37 is the default group
        # directory
        group_id = (dependency.get('id')
          or "group-unnamed-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        group_slug = group_id[0 : len(group_id) - 37]
        group_path = os.path.join(self.path, group_slug)

        # Check if this group has this id
        found = False
        if os.path.exists(group_path):
          group = Group(group_path)
          if group.objectInfo().get('id') == group_id:
            groups.append(Group(group_path, dependency['revision']))
            found = True

        if not found and self.occam:
          group_path = self.occam.objectPath(group_id)
          if os.path.exists(group_path):
            group = Group(group_path)
            if group.objectInfo().get('id') == group_id:
              groups.append(Group(group_path, dependency['revision']))
              found = True

    return groups

  def objects(self):
    """
    Pull out all object instances for this workset at this revision.
    """

    # For every object in our dependencies, generate a Object instance to either
    # its place on our local disk, or the OCCAM cache.
    info = self.objectInfo()

    objects = []
    info['contains'] = info.get('contains') or []

    for dependency in info['contains']:
      if not dependency.get('type') in ['group', 'experiment', 'workset']:
        # Object ids are object slugs followed by a UUID. The directory name will
        # be the part preceding the UUID within this object id:
        # type-foo-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX

        # Therefore, the substring from 0 to len(str) - 37 is the default object
        # directory
        object_id = (dependency.get('id')
          or "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        object_slug = "%s-%s" % (dependency.get('type'), object_id)
        object_path = os.path.join(self.path, object_slug)

        # Check if this object has this id
        found = False
        if os.path.exists(object_path):
          obj = Object(object_path)
          if obj.objectInfo().get('id') == object_id:
            objects.append(Object(object_path, dependency['revision']))
            found = True

        if not found and self.occam:
          object_path = self.occam.objects.pathFor(dependency.get('id'))
          if os.path.exists(object_path):
            obj = Object(object_path)
            if obj.objectInfo().get('id') == object_id:
              objects.append(Object(object_path, dependency['revision']))
              found = True

    return objects

  def addObject(self, name, object_type, path=None):
    """
    Creates a new object on disk.
    """

    # Create directory, if it doesn't exist, warn if it does
    if path is None:
      slug = "%s-%s" % (Object.slugFor(object_type), Object.slugFor(name))
      path = os.path.realpath(os.path.join(self.path, slug))

    obj = Object.create(path, name, object_type, belongsTo=self, root=self)

    obj.placeWithin(self)

    self.occam.objects.store(obj)

    # Update workset object to ignore object in repository
    f = open('%s/.gitignore' % (self.path), 'a+')
    f.write('%s\n' % (slug))
    f.close()

    revision = obj.head()
    uuid = obj.objectInfo()['id']

    # Update this new object as a dependency
    if not self.dependency(uuid, category='contains'):
      revisions = self.addDependency(object_type = object_type,
                                     name        = name,
                                     revision    = revision,
                                     uuid        = uuid,
                                     category    = 'contains')
    else:
      revisions = self.updateDependency(object_type = object_type,
                                        name        = name,
                                        revision    = revision,
                                        uuid        = uuid,
                                        category    = 'contains')

    # Update root revision
    new_root = Workset(self.path, occam=self.occam, revision=revisions[0])
    return Object(path, occam=self.occam, root=new_root)

  def addGroup(self, name, path=None):
    """
    Creates a new group on disk.
    """

    # Create directory, if it doesn't exist, warn if it does
    if path is None:
      path = os.path.realpath(os.path.join(self.path, "group-%s" % (Object.slugFor(name))))

    group = Group.create(path, name, belongsTo=self, root=self)

    group.placeWithin(self)

    self.occam.objects.store(group)

    # Update workset object to ignore group in repository
    f = open('%s/.gitignore' % (self.path), 'a+')
    f.write('group-%s\n' % (name))
    f.close()

    revision = group.head()
    uuid = group.objectInfo()['id']

    # Update this new group as a dependency
    if not self.dependency(uuid, category = 'contains'):
      revisions = self.addDependency(object_type = 'group',
                                     name        = name,
                                     revision    = revision,
                                     uuid        = uuid,
                                     category    = 'contains')
    else:
      revisions = self.updateDependency(object_type = 'group',
                                        name        = name,
                                        revision    = revision,
                                        uuid        = uuid,
                                        category    = 'contains')

    Log.write('ok')
    Log.write(revisions)

    # Update root revision
    new_root = Workset(self.path, occam=self.occam, revision=revisions[0])
    return Group(path, occam=self.occam, root=new_root)

  def addExperiment(self, name, path=None):
    """
    Creates a new experiment on disk.
    """

    # Create directory, if it doesn't exist, warn if it does
    path = os.path.realpath(os.path.join(self.path, "experiment-%s" % (name)))

    experiment = Experiment.create(path, name, belongsTo=self, root=self)

    experiment.placeWithin(self)

    self.occam.objects.store(experiment)

    # Update workset object to ignore experiment in repository
    f = open('%s/.gitignore' % (self.path), 'a+')
    f.write('experiment-%s\n' % (name))
    f.close()

    revision = experiment.head()
    uuid = experiment.objectInfo()['id']

    # Update this new experiment as a dependency
    if not self.dependency(uuid, category='contains'):
      self.addDependency(object_type = 'experiment',
                         name        = name,
                         revision    = revision,
                         uuid        = uuid,
                         category    = 'contains')
    else:
      self.updateDependency(object_type = 'experiment',
                            name        = name,
                            revision    = revision,
                            uuid        = uuid,
                            category    = 'contains')

    return Experiment(path, occam=self.occam, root=self)

  def addAuthor(self, person):
    """
    Add author to workset.
    """

    info = self.objectInfo()

    uuid = person.objectInfo()['id']
    name = person.objectInfo()['name']

    info['authors'] = (info.get('authors') or [])
    if not uuid in info['authors']:
      info['authors'].append(uuid)

      # Create collaboratorship
      self.occam.addAuthor(self, person)
    else:
      Log.warning("%s is already an author in workset %s" % (name, info['name']))
      return False

    # TODO: handle revisions?

    self.updateObject(info, "adds %s as author" % (name))

    return True

  def addCollaborator(self, person):
    """
    Add collaborator to workset.
    """

    info = self.objectInfo()

    uuid = person.objectInfo()['id']
    name = person.objectInfo()['name']

    info['collaborators'] = (info.get('collaborators') or [])
    if not uuid in info['collaborators']:
      info['collaborators'].append(uuid)

      # Create collaboratorship
      self.occam.addCollaborator(self, person)
    else:
      Log.warning("%s is already a collaborator in workset %s" % (name, info['name']))
      return False

    # TODO: handle revisions?

    self.updateObject(info, "adds %s as collaborator" % (name))

    return True

  def parent(self):
    return None

  def workset(self):
    return self
