# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from optparse          import OptionParser
import codecs
import select
import base64
import subprocess

# For pulling network http resources
try:
  from urllib.parse import urlparse
except:
  from urllib2 import urlparse

from lib.occam                 import Occam
from lib.log                   import Log

from lib.commands.initialize   import Initialize
from lib.commands.pull         import Pull
from lib.commands.manifest     import Manifest
from lib.commands.logs         import Logs
from lib.commands.sync         import Sync
from lib.commands.commit       import Commit
from lib.commands.run          import Run
from lib.commands.set          import Set
from lib.commands.test         import Test
from lib.commands.discover     import Discover
from lib.commands.console      import Console
from lib.commands.build        import Build
from lib.commands.attach       import Attach
from lib.commands.detach       import Detach
from lib.commands.configure    import Configure
from lib.commands.login        import Login
from lib.commands.clone        import Clone
from lib.commands.status       import Status
from lib.commands.new          import New
from lib.commands.remove       import Remove
from lib.commands.append       import Append
from lib.commands.view         import View
from lib.commands.search       import Search
from lib.commands.delete       import Delete
from lib.commands.install      import Install
from lib.commands.email        import Email

class Usage(Exception):
  def __init__(self, msg):
    self.msg = msg

def applyGenericOptions(parser):
  parser.add_option("-B", "--base64", action = "store_true",
                                      dest   = "base64",
                                      help   = "whether or not all arguments are specified in base64")

  parser.add_option("-P", "--mongo-port", action = "store",
                                          type   = "int",
                                          dest   = "mongo_port",
                                          help   = "mongo database port")

  parser.add_option("-N", "--mongo-name", action = "store",
                                          type   = "string",
                                          dest   = "mongo_name",
                                          help   = "mongo database name")

  parser.add_option("-H", "--mongo-host", action = "store",
                                          type   = "string",
                                          dest   = "mongo_host",
                                          help   = "mongo database host")

  parser.add_option("-R", "--rootPath", action = "store",
                                        type   = "string",
                                        dest   = "rootPath",
                                        help   = "the root path for occam")
  parser.add_option("-D", "--development", action = "store_true",
                                           dest   = "development",
                                           help   = "use an sqlite database")
  parser.add_option("-v", "--verbose",  action = "store_true",
                                        dest   = "verbose",
                                        help   = "prints all messages")
  parser.add_option("-L", "--logtype", action = "store",
                                       dest   = "logtype",
                                       help   = "the logging text for commands. Possible options are: 'text' (default), and 'json'.")
  parser.add_option("-O", "--on", action = "store",
                                  dest   = "remote_machine",
                                  help   = "runs the command on the given machine written as: username@host:port")

class CLI:
  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  @staticmethod
  def execute(args):
    command = args[0]

    # We will look for the corresponding class handling the command with
    # the following table. These classes will have an initParser() function
    # that will add other possible arguments that people can use.
    # We will just instantiate the handler and call the do() function.
    commands = {
      # OCCAM System
      "initialize":   Initialize,  # Initializes an OCCAM node
      "test":         Test,        # Runs software test suite
      "discover":     Discover,    # Discovers other OCCAM nodes

      # Object Management
      "new":          New,         # Creates a new object/group/experiment/workset
      "remove":       Remove,      # Removes things that are 'added'
      "pull":         Pull,        # Pulls an object from a remote OCCAM server
      "sync":         Sync,        # Syncs the local view of an object
      "clone":        Clone,       # Clones an existing object to create a new branch
      "DELETE":       Delete,      # Deletes an object from our OCCAM knowledge (volatile)
      "commit":       Commit,      # Push changes to the store and update dependencies.

      # Workflow Management
      "attach":       Attach,      # Attaches an existing object to a workflow
      "detach":       Detach,      # Detaches objects from a workflow

      # Object Modification
      "configure":    Configure,   # Handles object configuration within a workset
      "append":       Append,      # Adds an entry to Object meta-data
      "set":          Set,         # Sets an entry in Object meta-data

      # Listing
      "status":       Status,      # Displays the contents of the workset
      "view":         View,        # Lists information about a particular object
      "search":       Search,      # Queries objects known to the system
      "logs":         Logs,        # Prints out the log for the given job

      # Object Execution
      "install":      Install,     # Installs related artifacts for the object
      "build":        Build,       # Builds an object
      "run":          Run,         # Runs a built object
      "console":      Console,     # Presents a console running the object
      "manifest":     Manifest,    # Generates a task manifest and outputs it

      # Remote Authentication
      "login":        Login,        # Logs in
      "email":        Email
    }

    # If the command is valid, invoke it.
    if command in commands:
      # Form command line parser
      parser = OptionParser("occam %s" % (command))
      applyGenericOptions(parser)

      # Retrieve python class handling this command
      handler = commands[command]

      # Add specialized arguments for this command and parse
      old_args = args
      parser = handler.initParser(parser)
      opts, args = parser.parse_args(args)

      # We can encode arguments in base64 for shell safety paranoia
      if opts.base64:
        # Reform arguments and re-parse
        args = []
        args.append(command)
        for arg in old_args[1:]:
          if arg[0] != "-":
            decoded_arg = base64.b64decode(arg + "====")
            try:
              arg = decoded_arg.decode('utf8')
            except:
              pass
          args.append(arg)

        parser = OptionParser("occam %s" % (command))
        applyGenericOptions(parser)
        parser = handler.initParser(parser)
        opts, args = parser.parse_args(args)

      if opts.remote_machine:
        # Ok, give up and just invoke on another machine with ssh or w/e

        # Parse URL
        urlparts = urlparse(opts.remote_machine)

        if urlparts.scheme is None or urlparts.scheme == "ssh":
          ssh_command = ['ssh', '-t']

          if urlparts.port:
            ssh_command.append('-p')
            ssh_command.append(str(urlparts.port))

          ssh_command.append("%s@%s" % (urlparts.username, urlparts.hostname))

          # TODO: better handle removing this argument
          remote_arg = old_args.index('--on') or old_args.index('-O')
          old_args[remote_arg:remote_arg+2] = []

          ssh_command.append('/bin/bash -lc "occam %s"' % (' '.join(old_args)))

          stdout = subprocess.PIPE
          stderr = subprocess.PIPE
          cwd = "/tmp"
          process = CLI.popen(ssh_command, cwd=cwd, stdout=stdout, stderr=stderr)
          stdout = process.stdout
          stderr = process.stderr

          log = ""
          utfDecoder = codecs.getreader('utf-8')
          totalRead = 0

          while True:
            readable, writable, exceptional = select.select([stdout, stderr], [], [stdout, stderr])

            numRead = 0

            for reader in readable:
              read = utfDecoder(reader).read(1)
              if (read == "\n"):
                Log.external(log, urlparts.hostname)
                log = ""
              else:
                log += read

              numRead += len(read)
              totalRead += len(read)

            if len(readable) == 0 and process.poll() is not None:
              break

            if numRead == 0:
              break

          if len(log) > 0:
            Log.external(log, urlparts.hostname)

          process.communicate()
          return process.returncode

        Log.error("Unrecognized scheme")
        return -1

      occam = Occam(options=opts)

      if not occam.initialized() and not command == "initialize":
        Log.error("OCCAM is not initialized.")
        return -1

      # Initialize Logger
      Log.initialize(opts)

      # Generate the handler and run command
      #try:
      code = handler(opts, args, occam).do()
      #except Exception as e:
      #  Log.error(e)
      #  return -1

      return code

    # Otherwise, command was not found
    Log.error("command not found: %s" % (command))
    return -1
