# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log import Log

import codecs
import select
import subprocess
import os
import time
import json
import shutil
import base64

# runCommandFor(obj)

# for tasks:
#   runcommand will run a series of lines for combined elements
#   runcommands may be cached as a Configuration + Input pair
#   volumes will be determined from those too

#   Object needs the ability to pull all depend(a|e)nt objects
#   such that this backend can build/append volumes.

#   that would be: all 'input', 'output', 'with', and itself
#   well, 'with' would just impose an input relationship, but whatever

class Docker:
  """
  Implements the logic to build/run objects as Docker containers.
  """

  def __init__(self, occam):
    """
    Initializes a Docker backend.
    """
    self.occam = occam
    self.options = occam.options

  @staticmethod
  def provides():
    """
    Returns a list of all possible environments and architecture pairs that
    can be created and used by this backend.
    """

    # Generally, we should look up docker-container environment objects.

    # Look up docker-container objects and list the possible objects as
    # environments.

    # Search for type 'docker-container' and look at ['metadata']['docker']
    # field for 'environment' and 'architecture'

    # Record those in the database as possible environments

    # We only have to rake the object pool when we see that the number of
    # docker-container objects has changed. (although it may not see
    # updates)

    return [
      ['docker', 'x86-64']
    ]

  @staticmethod
  def name():
    return "docker"

  @staticmethod
  def canProvide(environment, architecture):
    """
    Returns True if this backend can build/run objects with the given
    environment and architecture.
    """

    return [environment, architecture] in Docker.provides()

  # TODO: purgeBuiltImage
  def purge(self, obj):
    """
    Deletes any local cached copy of the built object.
    """

    # TODO: this D: D: D:
    return False

  def exists(self, obj):
    """
    Returns True if the given object exists as a cached docker volume.
    """

    name = self.imageNameFor(obj)
    command = ['docker', 'inspect', '%s' % (name)]

    return self.executeDockerCommand(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

  def detect(self):
    """
    Returns True when docker is available.
    """

    # Just check that docker is in PATH and runs
    # Even when it does exist, we check the error code to see if the docker
    # service is running. It should post a non-zero when the docker service
    # has failed.

    command = ['docker']

    return self.executeDockerCommand(command)

  def canRun(self, obj):
    """
    Returns True when the given object can be run on this backend.
    """

    objInfo = obj.objectInfo()
    environment  = objInfo.get('environment')
    architecture = objInfo.get('architecture')

    return Docker.canProvide(environment, architecture)

  # TODO: export
  def export(self, obj):
    """
    """
    return False

  def pull(self, url, uuid, cert=None, revision=None):
    """
    Pulls the container representing the given object from an existing node.
    Returns True when a container is pulled and False otherwise.
    """

    return

    # Get the docker resource URL at the other node
    nodeToken = self.occam.nodes.partialFromURL(url).replace(',', ':')
    resourceURL = "https://%s/resources/docker-container/%s/%s" % (nodeToken, uuid, revision)

    # Tell that server our intent (so it caches the docker container)
    Log.noisy("pinging intent to pull container at %s" % (resourceURL))
    response, content_type, size = self.occam.network.get(resourceURL)
    if response is None:
      return False

    # form docker's prefered resource URI
    source = nodeToken + "/" + uuid + ":" + revision

    # Add certificate to global certs list for Docker (needs permission!)
    # TODO: what the hell does docker do on Windows. especially wrt ':' since
    #       those chars aren't allowed in filenames
    if cert:
      localCertPath = os.path.join('/', 'etc', 'docker', 'certs.d')

      localCertPath = os.path.join(localCertPath, nodeToken)
      if not os.path.exists(localCertPath):
        os.mkdir(localCertPath)

      localCertFile = os.path.join(localCertPath, 'ca.crt')

      # Copy node certificate to this location
      shutil.copyfile(cert, localCertFile)

    self.pullExternal(source)

    # Create occam/{uuid}:{revision} tag
    self.tag(uuid, source, revision=revision)

    # Create localhost:5000/{uuid}:{revision} tag
    self.tag(uuid, source, revision=revision, namespace='localhost:5000')

    if cert:
      # Remove certificate
      os.remove(localCertFile)
      os.rmdir(localCertPath)

    return True

  def build(self, obj, base, cwd, local=False):
    """
    Builds the given object within a docker container.
    """

    # Delete the previous image for this revision
    if not local:
      self.delete(obj)

    # Generate a Dockerfile
    self.generateDockerFileFor(obj, base, cwd, local)

    # Build and create the resulting volume
    command = self.buildCommandFor(obj, cwd, local)
    self.executeDockerCommand(command, cwd)

    # Build for local builds by invoking the build script
    if local:
      objectMetadata = obj.objectInfo()
      objectMetadata['revision'] = obj.revision

      commandToRun = self.commandFor(objectMetadata, 'build')

      docker_command = self.runCommandFor(obj, cwd,
                                          initialization={"command": commandToRun},
                                          extraOptions=['-i'],
                                          cwdMount=False,
                                          cwdMountAsObject=True,
                                          redirectOutput=False,
                                          local=True)
      self.executeDockerCommand(docker_command, cwd, wait=True)

    # TODO: We can delete the image without deleting the volume??
    return True

  def console(self, obj, cwd):
    """
    Runs an interactive shell for the given object.
    """

    # Object Initialization Pass
    #docker_command = self.runCommandFor(obj, cwd,
    #                                    initialization={"command": "/bin/bash"},
    #                                    extraOptions=['-i'],
    #                                    cwdMount=False,
    #                                    redirectOutput=False)
    #p = self.executeDockerCommand(docker_command, cwd, interactive=True)
    self.run(obj, cwd, console=True)

    return True

  def run(self, obj, cwd, console=False):
    """
    Runs the given object that was built previously on docker.
    """

    # TODO: Pull volumes?
    # TODO: Build Dependencies??
    # Run a VM with the built volume

    vmInfo = obj.objectInfo()
    uuid = vmInfo.get('id')

    # Create volumes
    volumes = self.rakeVolumes(vmInfo.get('input', [{}])[0])

    objInfo = vmInfo.get('input', [{}])[0]

    capabilities = vmInfo.get('capabilities', [])

    videoProcess = None
    videoProcessName = None
    vncPort = None

    if 'video' in capabilities:
      # Run x11 server and x11vnc

      # Grab a port
      vncPort = self.occam.network.allocatePort()
      Log.write("Running VNC on %s" % (vncPort))

      # Name the process
      videoProcessName = "occam-video-%s" % (vncPort)

      # Kill any currently running process with that name
      self.executeDockerCommand(['docker', 'rm', '-f', videoProcessName], cwd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

      # Run the process
      docker_command = ['docker', 'run', '-t', '--name', videoProcessName, '-p', '%s:5900' % (vncPort), 'occam/178494e0-6282-11e5-95cf-001fd05bb228:9803c096934c41bb782e39bfa0d87c436acfe32d', '/bin/bash', '-c', 'cd /occam/178494e0-6282-11e5-95cf-001fd05bb228-9803c096934c41bb782e39bfa0d87c436acfe32d; sh start.sh; sleep 3; sh start.sh']

      videoProcess = self.executeDockerCommand(docker_command, cwd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, wait=False)

    for volume in volumes:
      volumeName = "%s-%s" % (self.volumeNameFor(volume[0], revision=volume[1]), uuid)
      volume_command = self.buildVolumeCommandFor(volume[0], revision=volume[1], volumeName=volumeName)
      Log.write("Building volume for %s %s" % (volume[2].get('type', 'object'), volume[2].get('name', 'unknown')))
      if not self.executeDockerCommand(volume_command, cwd):
        # First attempt to delete the volume (maybe an old version of the volume exists somehow?)
        Log.write("Cleaning up old volume for %s %s" % (volume[2].get('type', 'object'), volume[2].get('name', 'unknown')))
        self.executeDockerCommand(['docker', 'rm', '-f', volumeName], cwd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        # Then attempt to build the object (maybe the object isn't built?)
        subObj = self.occam.objects.retrieve(volume[0], revision=volume[1])
        if not self.occam.built(subObj):
          Log.write("Building object %s %s" % (volume[2].get('type', 'object'), volume[2].get('name', 'unknown')))
          self.occam.build(subObj)
          build_path = self.occam.objects.createPathFor(subObj.objectInfo()['id'], subPath = "builds")
          build_path = os.path.join(build_path, subObj.revision)
          base = self.occam.objects.retrieve(objInfo.get('id'), revision=objInfo.get('revision'))
          self.build(subObj, base, build_path)

        Log.write("Reattempting to build volume for %s %s" % (volume[2].get('type', 'object'), volume[2].get('name', 'unknown')))
        if not self.executeDockerCommand(volume_command, cwd):
          # We tried, but failed.
          Log.error("Cannot build volume")
          return False

    # TODO: wait on VM events instead (and parallelize)

    # Send event
    # TODO: have video process start an X server immediately
    if videoProcess:
      Log.event('video', {
        "port": vncPort
      })

    # Run Sub-Object Initialization Passes
    subInitializations = self.subInitializationsFor(obj, objInfo)
    for item in subInitializations:
      Log.header("Running Sub-Object Initialization")

      extraOptions = []
      variables = {}

      if videoProcess:
        extraOptions += ['--volumes-from', videoProcessName]
        variables['DISPLAY'] = ':0'

      variables['HOME'] = '/local'

      docker_command = self.taskCommandFor(obj, item, cwd, extraOptions=extraOptions, variables=variables)
      self.executeDockerCommand(docker_command, cwd, wait=True)

    # Get the object info for the object to actually run
    runInfo = objInfo.get('input', [{}])[0]

    # Object Initialization Pass
    Log.header("Running Object Initialization")

    extraOptions = []
    variables = {}
    if videoProcess:
      extraOptions += ['--volumes-from', videoProcessName]
      variables['DISPLAY'] = ':0'

    variables['HOME'] = '/local'

    docker_command = self.taskCommandFor(obj, runInfo, cwd, extraOptions=extraOptions, variables=variables)
    self.executeDockerCommand(docker_command, cwd)

    # When we run an initialization pass, we will be able to read a run.json
    # which will tell us what the next commands to run will be.
    initializations = self.initializationsFor(runInfo, cwd)
    Log.write("Found initialization: %s" % (initializations))

    # Initialization will be an array of commands to sequentially run.
    index = None
    for item in initializations:
      Log.header("Running Object Pass")

      extraOptions = []
      variables = {}

      if videoProcess:
        extraOptions += ['--volumes-from', videoProcessName]
        variables['DISPLAY'] = ':0'

      variables['HOME'] = '/local'

      if console:
        item['command'] = '/bin/bash -c "echo %s; /bin/bash"' % (item['command'])
        extraOptions += ['-i']

      docker_command = self.taskCommandFor(obj, runInfo, cwd, initialization=item, initializationIndex=index, extraOptions=extraOptions, variables=variables)

      if console:
        self.executeDockerCommand(docker_command, cwd, wait=True, interactive=True)
      else:
        self.executeDockerCommand(docker_command, cwd, wait=True)

      index = (index or 0) + 1

    # Kill video process once the application terminates
    if videoProcess:
      #videoProcess.communicate()
      Log.write("Cleaning up VNC process")
      self.executeDockerCommand(['docker', 'rm', '-f', videoProcessName], cwd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
      self.occam.network.freePort(vncPort)

    # Delete Volumes (except those that are supposed to persist!)
    for volume in volumes:
      volumeName = "%s-%s" % (self.volumeNameFor(volume[0], revision=volume[1]), uuid)
      Log.write("Cleaning up volume for %s %s" % (volume[2].get('type', 'object'), volume[2].get('name', 'unknown')))
      self.executeDockerCommand(['docker', 'rm', '-f', volumeName], cwd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    return True

  def pullExternal(self, source):
    """
    Pulls the container from the given source found on a non-occam network.
    """

    command = ['docker', 'pull', source]
    code = subprocess.Popen(command).communicate()

  def makeInitializationPathFor(self, objectInfo, cwd, section='run'):
    # Gather object info for what just ran
    runInfo = objectInfo.get(section, {})

    # Retrieve the object tag for this invocation
    runObjectTag = str(runInfo.get('index', objectInfo.get('index')))

    # Determine the initialization path
    objectPath = os.path.join(cwd, "init", runObjectTag)

    if not os.path.exists(objectPath):
      Log.write("making initialization path %s" % (objectPath))
      os.mkdir(objectPath)

    # Determine the objects path
    objectPath = os.path.join(cwd, "objects", runObjectTag)

    if not os.path.exists(objectPath):
      Log.write("making object path %s" % (objectPath))
      os.mkdir(objectPath)

    return True

  def initializationsFor(self, objectInfo, cwd, section='run'):
    """
    Returns an array of commands for OCCAM to run. This command list is
    generated by the script given by the object as opposed to a straight
    command.
    """

    # This could be elsewhere!

    # Gather object info for what just ran
    runInfo = objectInfo.get(section, {})
    if not 'script' in runInfo:
      return []

    # Retrieve the object tag for this invocation
    runObjectTag = str(runInfo.get('index', objectInfo.get('index')))

    # Determine the initialization path
    objectPath   = os.path.join(cwd, "init", runObjectTag)
    initDataPath = os.path.join(objectPath, "run.json")

    Log.noisy("looking for an initialization file at %s" % (initDataPath))

    # Parse the json
    try:
      ret = json.load(open(initDataPath))
    except:
      ret = []

    # Force this to be an array
    if not isinstance(ret, list):
      ret = [ret]

    # TODO: check individual items to ensure they are dict's and remove them
    #       if not.

    return ret

  def securityOptionsFor(self, obj):
    """
    Returns the security options needed to run the given object.
    """

    ret = []

    apparmor = self.occam.capabilities.appArmor()

    if apparmor:
      # TODO: better apparmor options?
      ret += ["--security-opt", "apparmor:unconfined"]

    return ret

  def capabilitiesFor(self, obj):
    """
    Returns the capability list needed to run the given object.
    """
    ret = []

    # Allows ptrace
    ret += ["--cap-add", "SYS_PTRACE"]

    return ret

  def volumeNameFor(self, objOrUUID, revision=None):
    """
    Returns the docker volume name representing this object.
    """

    if not isinstance(objOrUUID, str):
      revision = objOrUUID.revision
      uuid = objOrUUID.ownerObjectInfo()['id']
    else:
      uuid = objOrUUID

    return "occam-%s-%s" % (uuid, revision)

  def imageNameFor(self, objOrUUID, revision=None):
    """
    Returns the docker image name representing this object.
    """

    if not isinstance(objOrUUID, str):
      revision = objOrUUID.revision
      uuid = objOrUUID.ownerObjectInfo()['id']
    else:
      uuid = objOrUUID

    if revision is None:
      return "occam/%s" % (uuid)

    return "occam/%s:%s" % (uuid, revision)

  def rakeLocalVolumes(self, obj, task_uuid):
    """
    Returns an array of strings of tuples that contain the uuid and revision
    and path of a cloned local copy of an object described by the given
    object or object metadata.
    """
    ret = []

    if isinstance(obj, dict):
      objectInfo = obj
      revision = objectInfo.get('revision')
    else:
      revision = obj.revision
      objectInfo = obj.objectInfo()

    if 'id' in objectInfo:
      uuid = objectInfo['id']
      version = None

      if 'owner' in objectInfo:
        # Owned objects are within their owner's volume
        uuid = objectInfo['owner']['id']

      if objectInfo.get('created', False):
        # Determine local path
        local_path = self.occam.objects.createPathFor(uuid, subPath = "local")
        local_path = os.path.join(local_path, revision)

        # Clone if not available
        # Append path
        ret.append([uuid, revision, local_path])

    # Recursively rake volumes of every input
    for input in objectInfo.get('input', []):
      ret.extend(self.rakeLocalVolumes(input, task_uuid))

    # Recursively rake volumes of every output
    for output in objectInfo.get('output', []):
      ret.extend(self.rakeLocalVolumes(output, task_uuid))

    # Pull out 'with' volumes
    # TODO: this

    # Return the list
    return ret

  def rakeVolumes(self, obj):
    """
    Returns an array of strings of docker image names that are used with the
    object described by the given object or object metadata.
    """
    ret = []

    if isinstance(obj, dict):
      objectInfo = obj
      revision = objectInfo.get('revision')
    else:
      revision = obj.revision
      objectInfo = obj.objectInfo()

    if 'id' in objectInfo:
      uuid = objectInfo['id']
      version = None

      if 'owner' in objectInfo:
        # Owned objects are within their owner's volume
        uuid = objectInfo['owner']['id']

      if not objectInfo.get('created', False):
        ret.append([uuid, revision, objectInfo])

    # Recursively rake volumes of every input
    for input in objectInfo.get('input', []):
      ret.extend(self.rakeVolumes(input))

    # Recursively rake volumes of every dependency
    for input in objectInfo.get('dependencies', []):
      ret.extend(self.rakeVolumes(input))

    # Recursively rake volumes of every output
    for output in objectInfo.get('output', []):
      ret.extend(self.rakeVolumes(output))

    # Pull out 'with' volumes
    # TODO: this

    # Return the list
    return ret

  def delete(self, obj):
    command = self.deleteVolumeCommandFor(obj)
    self.executeDockerCommand(command)

    command = self.deleteImageCommandFor(obj)
    self.executeDockerCommand(command)

  def guestRootFor(self, objOrUUID, revision=None):
    if not isinstance(objOrUUID, str):
      revision = objOrUUID.revision
      uuid = objOrUUID.objectInfo()['id']
    else:
      uuid = objOrUUID

    #if 'owner' in runInfo:
    #  run_obj_id = runInfo['owner']

    return "/occam/%s-%s" % (uuid, revision)

  def executeDockerCommand(self, command, cwd=None, stdout=None, stderr=None, wait=True, interactive=False):
    Log.noisy("Invoking native command: %s" % ' '.join(command))

    parseToLog = False

    if interactive == False:
      if stdout is None:
        # Capture stdout and replay it on the log (as stderr)
        stdout = subprocess.PIPE
        parseToLog = True

      if stderr is None:
        # Capture stderr and replay it on the log
        stderr = subprocess.PIPE
        parseToLog = True

    process = subprocess.Popen(command, cwd=cwd, stdout=stdout, stderr=stderr)
    if process.stdin:
      process.stdin.close()

    utfDecoder = codecs.getreader('utf-8')

    totalRead = 0

    if wait:
      if parseToLog:
        stdout = process.stdout
        stderr = process.stderr

        log = ""

        while True:
          readable, writable, exceptional = select.select([stdout, stderr], [], [stdout, stderr])

          numRead = 0

          for reader in readable:
            read = utfDecoder(reader).read(1)
            if (read == "\n"):
              Log.external(log, 'docker')
              log = ""
            else:
              log += read

            numRead += len(read)
            totalRead += len(read)

          if len(readable) == 0 and process.poll() is not None:
            break

          if numRead == 0:
            break

        if len(log) > 0:
          Log.external(log, 'docker')

      process.communicate()
      return process.returncode == 0
    else:
      return process

  def variablesFor(self, objectInfo, section='run'):
    ret = {}

    if section in objectInfo:
      runInfo = objectInfo[section]
      if 'index' in runInfo and 'type' in runInfo:
        run_type = runInfo['type']
        index = runInfo['index']

      run_obj_id = runInfo.get('id') or objectInfo.get('id')

      run_obj_id_actual = run_obj_id
      if 'owner' in runInfo:
        run_obj_id = runInfo['owner']

      run_revision = runInfo.get('revision') or objectInfo.get('revision')

      run_obj_type = runInfo.get('type') or objectInfo.get('type')
      run_obj_name = runInfo.get('name') or objectInfo.get('name')
      run_obj_index = runInfo.get('index')
      if run_obj_index is None:
        run_obj_index = objectInfo.get('index')

      ret['OCCAM_OBJECT_INDEX'] = str(run_obj_index)
      ret['OCCAM_OBJECT_TYPE']  = str(run_obj_type)
      ret['OCCAM_OBJECT_ID']    = str(run_obj_id)

    return ret

  def commandFor(self, objectInfo, section='run'):
    # AAAAHHHHHHHHHHHHHHH
    run_cmd = "/bin/true"

    obj_revision = objectInfo.get('revision')

    if section in objectInfo:
      runInfo = objectInfo[section]
      if 'index' in runInfo and 'type' in runInfo:
        run_type = runInfo['type']
        index = runInfo['index']

      if "name" in runInfo:
        run_name = runInfo["name"]

        if "type" in runInfo:
          run_type = runInfo["type"]

      run_obj_id = runInfo.get('id') or objectInfo.get('id')

      run_obj_id_actual = run_obj_id
      if 'owner' in runInfo:
        run_obj_id = runInfo['owner']

      run_revision = runInfo.get('revision') or objectInfo.get('revision')

      run_root = self.guestRootFor(run_obj_id, revision=run_revision)

      run_obj_type = runInfo.get('type') or objectInfo.get('type')
      run_obj_name = runInfo.get('name') or objectInfo.get('name')
      run_obj_index = runInfo.get('index')

      if run_obj_index is None:
        run_obj_index = objectInfo.get('index')

      if 'command' in runInfo:
        run_cmd = runInfo['command']
      elif 'script' in runInfo:
        language = runInfo["language"]
        filename = runInfo["script"]
        if "version" in runInfo:
          script_version = runInfo["version"]
        else:
          script_version = None

        if language == "python":
          if script_version is None:
            script_version = "2.7.6"

          run_cmd = "python %s/%s" % (run_root, filename)
        elif language == "ruby":
          if script_version is None:
            script_version = '2.0.0'
          run_cmd = "ruby %s/%s" % (run_root, filename)
        elif language == "perl":
          if script_version is None:
            script_version = '5.18.0'
          run_cmd = "perl %s/%s" % (run_root, filename)
        elif language == "bash":
          run_cmd = "bash %s/%s" % (run_root, filename)
        elif language == "sh":
          run_cmd = "sh %s/%s" % (run_root, filename)

    return run_cmd

  def deleteVolumeCommandFor(self, obj):
    # docker rm
    command = ["docker", "rm"]

    # image name to delete
    command += [self.volumeNameFor(obj)]

    return command

  def deleteImageCommandFor(self, obj):
    # docker rm
    command = ["docker", "rmi", "-f"]

    # image name to delete
    command += [self.imageNameFor(obj)]

    return command

  def generateDockerFileFor(self, obj, base, cwd, local=False):
    """
    This method will generate a Dockerfile in the given working directory based
    on the given object.
    """

    # Place the Dockerfile within the working directory given to us
    dockerFilePath = os.path.join(cwd, "Dockerfile")

    # The path the object lives in the container
    guestRoot = self.guestRootFor(obj)

    # Create the file
    f = open(dockerFilePath, 'w+')

    # DOCKER-VERSION - The necessary versioning
    f.write("# DOCKER-VERSION 0.11.1\n")

    objInfo = obj.objectInfo()
    metadata = obj.objectInfo().get('metadata', {})
    docker_metadata = metadata.get('docker', {})
    metadata_volumes = docker_metadata.get('VOLUME', [])
    buildInfo = objInfo.get('build', {})

    if 'using' in buildInfo:
      baseName = "occam/%s" % (buildInfo['using']['id'])

      if 'revision' in buildInfo['using']:
        baseName = baseName + ":" + buildInfo['using']['revision']
    else:
      architecture = buildInfo.get('architecture', objInfo.get('architecture', 'x86-64'))
      environment  = buildInfo.get('environment',  objInfo.get('environment', 'ubuntu:14.04'))

      buildingBaseImage = False

      if environment == 'docker':
        resources = objInfo.get('install', [])

        for resource in resources:
          if resource.get('type') == 'docker-container':
            buildingBaseImage = True
            baseName = resource.get('source')

      if not buildingBaseImage:
        # Base image is within the task list
        if base:
          baseName = self.imageNameFor(base)
        else:
          Log.error("cannot determine the base image for this docker build")
          return False

    # FROM - Specifies the image to base the container environment off of
    f.write("FROM %s\n" % (baseName))

    # USER - The username to run the commands as
    #f.write("USER occam\n")

    # ADD - Adds a file system volume that maps the host's cwd to a known root
    if not local:
      f.write("ADD . %s\n" % (guestRoot))

    objectMetadata = obj.objectInfo()
    objectMetadata['revision'] = obj.revision

    # RUN - Runs the listed command in the container
    if not local:
      commandToRun = self.commandFor(objectMetadata, 'build')
      invocation = ["/bin/bash", "-c", "cd %s; %s" % (self.guestRootFor(obj), commandToRun)]
      f.write('RUN %s\n' % (json.dumps(invocation)))

    # VOLUME - Suggests that the given path should be self-contained in a volume.
    f.write('VOLUME ["%s"]\n' % (guestRoot))

    # VOLUME from metadata of object
    if isinstance(metadata_volumes, list) and len(metadata_volumes) > 0:
      for volume in metadata_volumes:
        # TODO: sanitize??
        f.write('VOLUME ["%s"]\n' % (volume))


    # CMD - The command to run when the container is invoked. Which is NOTHING.
    f.write("CMD /bin/true\n")

    # Done
    f.close()

  def buildCommandFor(self, obj, cwd, local=False):
    """
    Returns the docker command which will build the given object.
    """

    objectInfo = obj.objectInfo()

    # docker run
    command = ["docker", "build", "--force-rm=true", "--rm=true"]

    # -t image name to build to
    if not local:
      command += ["-t", self.imageNameFor(obj)]
    else:
      cwdHash = base64.b32encode(os.path.realpath(cwd).encode('utf8')).decode('utf8').lower().replace('=', '')
      command += ["-t", self.imageNameFor(objectInfo['id'], None) + "-local-" + cwdHash]

    # path for the docker file
    command.append(cwd)

    return command

  def tag(self, objOrUUID, source, revision=None, namespace="occam"):
    if not isinstance(objOrUUID, str):
      revision = objOrUUID.revision
      uuid = objOrUUID.objectInfo()['id']
    else:
      uuid = objOrUUID

    command = ["docker", "tag", source, namespace + "/" + uuid + ":" + revision]
    code = subprocess.Popen(command).communicate()

    return code == 0

  def buildVolumeCommandFor(self, objOrUUID, revision=None, volumeName = None):
    imageName  = self.imageNameFor(objOrUUID, revision=revision)

    if volumeName is None:
      volumeName = self.volumeNameFor(objOrUUID, revision=revision)

    command = ["docker", "run", "--name", volumeName, "-t", imageName, "/bin/true"]

    return command

  def objectTagFor(self, obj, section='run', omitIndex=False):
    """
    Returns the object tag that uniquely identifies this object within the
    virtual machine.
    """

    if not isinstance(obj, dict):
      revision = obj.revision
      obj = obj.ownerObjectInfo()
    else:
      revision = obj.get('revision')

    runRevision = obj.get('revision', revision)
    runIndex    = obj.get('index', 0)
    runID       = obj.get('id')

    if 'owner' in obj:
      runID = obj['owner'].get('id', obj.get('id'))

    # Retrieve the object tag for this invocation
    if omitIndex:
      return "-".join([
          str(runID),
          str(runRevision)
        ])
    else:
      return str(runIndex)

  def mountPathFor(self, obj):
    runObjectTag = self.objectTagFor(obj, omitIndex=True)

    # Determine the mounted path
    return os.path.join("/", "occam", runObjectTag)

  def objectPathFor(self, obj, cwd, filename, section='run', local=False):
    runObjectTag = self.objectTagFor(obj, section)

    # Determine the initialization path
    objectPath = os.path.join("objects", runObjectTag)

    # Return the full path
    localObjectPath = os.path.join(objectPath, filename)

    if local:
      return localObjectPath
    else:
      return os.path.realpath(os.path.join(cwd, localObjectPath))

  def logPathFor(self, obj, cwd):
    """
    Returns the path of shared logs within shared virtual machines. The logs
    path maintains a token file for each running VM.
    """

    # Determine the log path
    logPath = os.path.join(cwd, "logs")

    if os.path.exists(logPath) and os.path.isdir(logPath) and os.path.islink(logPath):
      # Mount the symlink (this is the only symlink we allow)
      logPath = os.path.realpath(os.readlink(logPath))
    else:
      # There is no shared logs path
      logPath = None

    return logPath

  def subInitializationsFor(self, vmObj, objInfo, section='run'):
    """
    Returns a list of objects that need to be initialized prior to running
    the given object.
    """

    ret = []

    runInfo = objInfo.get(section, {})

    # Gather the running image
    inputs = objInfo.get('input', [])
    if len(inputs) > 0:
      input = inputs[0]

      # Look at every input to this
      inputs = input.get('input', [])

      for input in inputs:
        if 'run' in input and not 'command' in input['run']:
          ret.append(input)

    return ret

  def taskCommandFor(self, vmObj, objInfo, cwd, section='run', initialization=None, initializationIndex=None, extraOptions=None, variables=None, cwdMount=True, cwdMountAsObject=False):
    """
    Returns the docker command to run for the given object. It will run with
    the given working directory specified by cwd as the target of all output.
    """

    self.makeInitializationPathFor(objInfo, cwd)

    uuid = vmObj.objectInfo().get('id')

    if not isinstance(extraOptions, list):
      extraOptions = None

    if variables is None:
      variables = {}

    extraOptions = extraOptions or []

    runInfo = objInfo.get(section, {})

    securityOptions = self.securityOptionsFor(objInfo)
    capabilities    = self.capabilitiesFor(objInfo)

    # docker run
    command = ["docker", "run"]

    # --security-opt options
    command += securityOptions

    # --cap-add options
    command += capabilities

    # -v local job
    if cwdMount:
      command += ["-v", "%s:/local:rw" % (os.path.realpath(cwd))]

    # --volumes-from volumes
    volumes = self.rakeVolumes(objInfo)

    # -v volumes
    localVolumes = self.rakeLocalVolumes(objInfo, uuid)

    # Create volumes
    for volume in volumes:
      volumeName = "%s-%s" % (self.volumeNameFor(volume[0], revision=volume[1]), uuid)
      command += ["--volumes-from", volumeName + ":rw"]

    # Create local volumes
    for volume in localVolumes:
      volumeTag = "%s-%s" % (volume[0], volume[1])
      command += ["-v", "%s:/occam/%s" % (volume[2], volumeTag)]

    # -t for a pseudo terminal
    command += ["-t"]

    # Extra options
    command += extraOptions

    # Finally, the image name
    obj_uuid = objInfo.get('id')
    revision = objInfo.get('revision')
    obj_index = objInfo.get('index')
    command += [self.imageNameFor(obj_uuid, revision)]

    environmentVars = {}
    commandToRun = '/bin/true'

    # ultimate command to actually run in the container
    if not initialization is None:
      if 'command' in initialization:
        commandToRun = initialization['command']

        # OCCAM environment variables for running the command
        environmentVars = self.variablesFor(objInfo, section)
        environmentVars.update(initialization.get('variables', {}))
      else:
        Log.warn('initialization completed but command was not given by initialization script')
    else:
      # Gather the running image
      commandToRun = self.commandFor(objInfo, section)

      # OCCAM environment variables for running the command
      environmentVars = self.variablesFor(objInfo, section)

    environmentVars.update(variables)

    runEnvironment = ""
    for key, value in environmentVars.items():
      runEnvironment += "%s=%s " % (key, value)

    # Shared log path
    logPath = None
    if cwdMount:
      logPath = self.logPathFor(vmObj, cwd)
      if logPath:
        command += ["-v", "%s:/local/logs:rw" % (logPath)]

    # Local working path
    cwdLocal = "/local"
    if not cwdMount:
      cwdLocal = self.mountPathFor(vmObj)
    if cwdMountAsObject:
      cwdLocal = self.mountPathFor(obj)
      command += ["-v", "%s:%s:rw" % (os.path.realpath(cwd), cwdLocal)]

    # Determine object stdout/stderr paths
    extension = '.raw'
    if not initializationIndex is None:
      extension = '-%s.raw' % (str(initializationIndex))

    inputs = runInfo.get('input', [])
    outputPath = self.objectPathFor(objInfo, cwd, 'run-out%s' % (extension), local=True)
    errorPath  = self.objectPathFor(objInfo, cwd, 'run-err%s' % (extension), local=True)
    if len(inputs) > 0:
      outputPath = self.objectPathFor(inputs[0], cwd, 'run-out%s' % (extension), local=True)
      errorPath  = self.objectPathFor(inputs[0], cwd, 'run-err%s' % (extension), local=True)

    outputPath = os.path.join(cwdLocal, outputPath)
    errorPath  = os.path.join(cwdLocal, errorPath)

    if logPath:
      logCommand = "; echo {} > %s/done.json; rm %s/log.json" % (logPath, logPath)
    else:
      logCommand = ""

    redirectOutput = True
    if redirectOutput:
      redirectLine = "| tee %s 3>&1 1>&2 2>&3 | tee %s 3>&1 1>&2 2>&3 %s" % (outputPath, errorPath, logCommand)
    else:
      redirectLine = ""

    # TODO: sanitize commandToRun
    # TODO: run command to run as a whole tty so that all output is stored?

    # -u user id to run inside container
    #command += ["-u", self.occam.capabilities.userId()]
    userId  = self.occam.capabilities.userId()
    groupId = self.occam.capabilities.groupId()

    # We add a user to the container with the same privileges as the current user
    # That user will be in 'root' group inside the container but not outside
    invocation = ["/bin/bash", "-c", "groupadd -g %s occam; useradd -mu %s occam -g %s -G sudo,root; echo '%%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; exec su - occam -c 'cd %s; %s %s %s'" % (str(groupId), str(userId), str(groupId), cwdLocal, runEnvironment, commandToRun, redirectLine)]
    invocation = ["/bin/bash", "-c", "cd %s; %s %s %s" % (cwdLocal, runEnvironment, commandToRun, redirectLine)]
    command += invocation

    # Basically, look at 'run' or 'build' etc
    # If there is a 'command', use that (form to correct path)
    # If there is a 'with', look at that object and form that command first.
    # Run all inits for inputs the same way
    # We can put this in Object
    return command

  def runCommandFor(self, obj, cwd, info=None, section='run', initialization=None, initializationIndex=None, extraOptions=None, cwdMount=True, cwdMountAsObject=False, redirectOutput=True, variables=None, uuid=None, local=False):
    """
    Returns the docker command to run for the given object. It will run with
    the given working directory specified by cwd as the target of all output.
    """

    if uuid is None:
      uuid = obj.objectInfo().get('id')

    if not isinstance(extraOptions, list):
      extraOptions = None

    if variables is None:
      variables = {}

    extraOptions = extraOptions or []

    objectInfo = obj.objectInfo()

    if info is None:
      info = objectInfo

    runInfo = info.get(section, {})

    securityOptions = self.securityOptionsFor(obj)
    capabilities    = self.capabilitiesFor(obj)

    # docker run
    command = ["docker", "run"]

    # --security-opt options
    command += securityOptions

    # --cap-add options
    command += capabilities

    # -v local job
    if cwdMount:
      command += ["-v", "%s:/local:rw" % (os.path.realpath(cwd))]
    if cwdMountAsObject:
      cwdLocal = self.mountPathFor(obj)
      command += ["-v", "%s:%s:rw" % (os.path.realpath(cwd), cwdLocal)]

    # --volumes-from volumes
    volumes = self.rakeVolumes(obj.objectInfo().get(section, {}))

    # Create volumes
    for volume in volumes:
      volumeName = "%s-%s" % (self.volumeNameFor(volume[0], revision=volume[1]), uuid)
      command += ["--volumes-from", volumeName]

    # -t for a pseudo terminal
    command += ["-t"]

    # Extra options
    command += extraOptions

    # Finally, the image name
    uuid = runInfo.get('id', objectInfo.get('id'))
    revision = runInfo.get('revision', objectInfo.get('revision', obj.revision))
    if not local:
      command += [self.imageNameFor(uuid, revision)]
    else:
      cwdHash = base64.b32encode(os.path.realpath(cwd).encode('utf8')).decode('utf8').lower().replace('=', '')
      command += [self.imageNameFor(uuid, None) + "-local-" + cwdHash]

    environmentVars = {}
    commandToRun = '/bin/true'

    # ultimate command to actually run in the container
    if not initialization is None:
      if 'command' in initialization:
        commandToRun = initialization['command']

        # OCCAM environment variables for running the command
        inputs = runInfo.get('input', [])
        if len(inputs) > 0:
          input = inputs[0]
          environmentVars = self.variablesFor(input, section)
        environmentVars.update(initialization.get('variables', {}))
      else:
        Log.warn('initialization completed but command was not given by initialization script')
    else:
      # Gather the running image
      inputs = runInfo.get('input', [])
      if len(inputs) > 0:
        input = inputs[0]
        commandToRun = self.commandFor(input, section)

        # OCCAM environment variables for running the command
        environmentVars = self.variablesFor(input, section)

    environmentVars.update(variables)

    runEnvironment = ""
    for key, value in environmentVars.items():
      runEnvironment += "%s=%s " % (key, value)

    # Shared log path
    logPath = None
    if cwdMount:
      logPath = self.logPathFor(obj, cwd)
      if logPath:
        command += ["-v", "%s:/local/logs:rw" % (logPath)]

    # Local working path
    cwdLocal = "/local"
    if not cwdMount:
      cwdLocal = self.mountPathFor(obj)
    if cwdMountAsObject:
      cwdLocal = self.mountPathFor(obj)

    # Determine object stdout/stderr paths
    extension = '.raw'
    if not initializationIndex is None:
      extension = '-%s.raw' % (str(initializationIndex))

    inputs = runInfo.get('input', [])
    outputPath = self.objectPathFor(runInfo, cwd, 'run-out%s' % (extension), local=True)
    errorPath  = self.objectPathFor(runInfo, cwd, 'run-err%s' % (extension), local=True)
    if len(inputs) > 0:
      outputPath = self.objectPathFor(inputs[0], cwd, 'run-out%s' % (extension), local=True)
      errorPath  = self.objectPathFor(inputs[0], cwd, 'run-err%s' % (extension), local=True)

    if logPath:
      logCommand = "; echo {} > %s/done.json; rm %s/log.json" % (logPath, logPath)
    else:
      logCommand = ""

    if redirectOutput:
      redirectLine = "| tee %s 3>&1 1>&2 2>&3 | tee %s 3>&1 1>&2 2>&3 %s" % (outputPath, errorPath, logCommand)
    else:
      redirectLine = ""

    # TODO: sanitize commandToRun
    # TODO: run command to run as a whole tty so that all output is stored?

    # -u user id to run inside container
    #command += ["-u", self.occam.capabilities.userId()]
    userId  = self.occam.capabilities.userId()
    groupId = self.occam.capabilities.groupId()

    invocation = ["/bin/bash", "-c", "useradd -u %s occam; exec su - occam -c 'cd %s; %s %s %s'" % (str(userId), cwdLocal, runEnvironment, commandToRun, redirectLine)]
    invocation = ["/bin/bash", "-c", "useradd -u %s occam; cd %s; %s %s %s" % (str(userId), cwdLocal, runEnvironment, commandToRun, redirectLine)]
    invocation = ["/bin/bash", "-c", "groupadd -g %s occam; useradd -mu %s occam -g %s -G sudo,root; echo '%%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; exec su - occam -c 'cd %s; %s %s %s'" % (str(groupId), str(userId), str(groupId), cwdLocal, runEnvironment, commandToRun, redirectLine)]
    invocation = ["/bin/bash", "-c", "cd %s; %s %s %s" % (cwdLocal, runEnvironment, commandToRun, redirectLine)]
    command += invocation

    # Basically, look at 'run' or 'build' etc
    # If there is a 'command', use that (form to correct path)
    # If there is a 'with', look at that object and form that command first.
    # Run all inits for inputs the same way
    # We can put this in Object
    return command
