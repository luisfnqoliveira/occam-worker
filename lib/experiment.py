# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log    import Log
from lib.git    import Git
from lib.object import Object

import os
import json

from uuid import uuid1, uuid4

class Experiment():
  """
  This class represents an OCCAM experiment.
  """

  @staticmethod
  def create(path, name, belongsTo, root, storable=True, occam=None):
    """
    Creates a new experiment on disk.
    """

    Object.create(path, name, "experiment", root=root, build=False, occam=occam, belongsTo=belongsTo, createPath=True)
    return Experiment(path, occam=occam, root=root)

  def __init__(self, path, revision="HEAD", occam=None, root=None):
    """
    Creates an instance of a experiment wrapping an existing group at the
    given path.
    """

    self.object = Object(path, revision=revision, occam=occam, root=root)

  def __getattr__(self, method_name):
    """
    This will override method calls which don't exist on Experiment and
    pass them to the internal Object.
    """
    return getattr(self.object, method_name)

  def copyOnWrite(self, workset):
    myWorkset = self.workset()

    if not myWorkset.objectInfo()['id'] == workset.objectInfo()['id']:
      # Create a new ID and set new name
      info = self.objectInfo()
      obj_id = info.get('id')
      info['id'] = Experiment.uuid(info['name'])
      info['authors'] = []

      # Add belongsTo for objects with a 'within'
      info['belongsTo'] = {
        "type": workset.objectInfo()['type'],
        "id":   workset.objectInfo()['id']
      }

      info['clonedFrom'] = {
        "type": info['type'],
        "name": info['name'],
        "id":   obj_id,
        "revision": self.revision
      }

      obj = Experiment(self.path, revision=self.updateObject(info, "Cloned.")[0], occam=self.occam)
      self.occam.objects.store(obj)
      workset.updateDependency(object_type = 'experiment',
                               name        = info['name'],
                               revision    = obj.revision,
                               uuid        = obj_id,
                               new_uuid    = info['id'],
                               category    = 'contains')
      self.occam.objects.store(workset)

      return obj
    return self
