# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import json

from io import StringIO

# json logging
# {
#   "type": "normal",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }
# {
#   "type": "warning",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }

# Log types:

# normal   - average, useful log info about what's going on / what was requested
# debug    - noisy info that is less useful for the day to day. for devs
# error    - error conditions!
# warning  - warning condition
# external - from other subprocesses we don't control
# done     - when things finish successfully

class Log:
  class Header:
    INVALID = 0
    STATUS  = 1

  class Action:
    INVALID = 0

  class Type:
    # Invalid or unknown type
    INVALID  = None

    # Normal everyday messages (useful!) (*)
    # This is just the normal output. This may have an ID when the logged
    # output is known. For instance, we might see a "normal" log type with
    # an id of 1234 which might correspond to 'building object Foo'. It will
    # have a params key 'object' that describes the object the line says we
    # are building.
    NORMAL   = "normal"

    # Debugging messages (not useful. for devs) (*)
    # May contain an ID and some parameters, but probably won't have i18n
    # support.
    DEBUG    = "debug"

    # Error messages (oh no!) (!)
    ERROR    = "error"

    # Warning messages (oh meh?? maybe oh no?) (!)
    WARNING  = "warning"

    # External messages (docker being noisy) ([])
    # These messages generally do not have ids. They are stdout/stderr of a
    # subprocess that OCCAM spawns to do some related task or run a VM.
    EXTERNAL = "external"

    # Completion messages (yay. success!!) (*)
    # These messages will appear when a command completes successfully. They
    # will have an associated DoneType for an ID.
    DONE     = "done"

  class DoneTypes:
    INVALID = 0 # Invalid or unknown type

    RUN     = 1 # Completed a run task
    BUILD   = 2 # Completed a build task
    CONSOLE = 3 # Completed a console task

  first_line = False
  verbose = False
  store_output = False
  stored_output = StringIO()
  percentMessage = ""
  withinPercent = False

  jsonLogging = False

  @staticmethod
  def print(message, end="\n", file=sys.stdout):
    if Log.withinPercent:
      print(file=sys.stderr)
      Log.withinPercent = False

    print(message, file=file, end=end)

  @staticmethod
  def initialize(options):
    if options.verbose:
      Log.verbose = True
    if options.logtype == "json":
      Log.jsonLogging = True

  @staticmethod
  def header(message):
    if Log.jsonLogging:
      Log.writeJSON("header", message)
    else:
      if Log.first_line:
        Log.print("", file=sys.stderr)

      Log.first_line = True
      Log.print(message, file=sys.stderr)

  @staticmethod
  def output(message, padding="  ", end="\n"):
    Log.print(padding, end="", file=sys.stderr)
    sys.stderr.flush()
    Log.print(message, end=end)
    if Log.store_output:
      Log.stored_output.write(message + end)

  def storedOutput():
    output = Log.stored_output
    Log.stored_output = StringIO()

    output.seek(0, 0)
    return output.read()

  @staticmethod
  def writeJSON(type, message, params={}, id="0"):
    Log.print(json.dumps({
      "type": type,
      "message": message,
      "id": str(id),
      "params": params
    }), file=sys.stderr, end="\n")

  @staticmethod
  def write(message, end="\n"):
    if Log.jsonLogging:
      Log.writeJSON("normal", message)
    else:
      Log.print(" \x1b[1;37m*\x1b[0m %s" % (message), file=sys.stderr, end=end)

  @staticmethod
  def external(message, source):
    if Log.jsonLogging:
      Log.writeJSON("external", message, params={"source": source})
    else:
      Log.print(" \x1b[1;37m[\x1b[0m%s\x1b[1;37m]\x1b[0m: %s" % (source, message), file=sys.stderr)

  @staticmethod
  def noisy(message):
    if Log.verbose:
      if Log.jsonLogging:
        Log.writeJSON("debug", message)
      else:
        Log.print(" \x1b[38;5;240m* %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def warning(message):
    if Log.jsonLogging:
      Log.writeJSON("warning", message)
    else:
      Log.print(" \x1b[1;33m! Warning: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def error(message):
    if Log.jsonLogging:
      Log.writeJSON("error", message)
    else:
      Log.print(" \x1b[1;31m! Error: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def done(message):
    if Log.jsonLogging:
      Log.writeJSON("done", message)
    else:
      Log.print(" \x1b[1;32m* Done: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def event(message, values={}):
    # Only happens in JSON logging (or when verbose mode is set)
    if Log.jsonLogging:
      Log.writeJSON("event", message, params=values)
    elif Log.verbose:
      Log.print(" \x1b[38;5;240m+ Event: %s\x1b[0m" % (message), file=sys.stderr)

  @staticmethod
  def writePercentage(message):
    Log.percentMessage = message
    Log.write(message + " [   %]", end='')
    Log.withinPercent = True

  @staticmethod
  def updatePercentage(percent):
    # TODO: check position and reprint line if position is weird
    percentCode = str(int(percent)).rjust(3, ' ')
    print("\x1b[5D%s%%]" % (percentCode), file=sys.stderr, end='')
    sys.stderr.flush()

  @staticmethod
  def outputBlock(text, padding="  "):
    for line in text.split('\n'):
      printed = ""
      for word in line.split(' '):
        if printed != "" and len(printed) + len(word) > (80 - len(padding)):
          Log.output("%s" % (printed), padding="")
          printed = ""
        printed += word + " "
      Log.output("%s" % (printed), padding="")
