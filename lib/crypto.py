# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from Crypto.PublicKey import RSA
from Crypto import Random

class Crypto:
  @staticmethod
  def newKeyPair():
    random_generator = Random.new().read
    key = RSA.generate(2048, random_generator)
    publicKey = key.publickey().exportKey('PEM', pkcs=1)
    privateKey = key.exportKey('PEM', pkcs=1)

    return [publicKey, privateKey]
