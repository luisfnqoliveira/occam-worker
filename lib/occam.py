# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import yaml
import os
import re
import json
import sys
import time
import datetime

import codecs   # For downloading json text
import tempfile # For temporary directories
import shutil   # For deleting that temp dir (grr python)
import bcrypt   # For password hashing

# Object Types
from lib.workset    import Workset
from lib.group      import Group
from lib.object     import Object
from lib.person     import Person
from lib.experiment import Experiment
from lib.log        import Log
from lib.crypto     import Crypto

from lib.db     import DB

# Managers
from lib.node_manager       import NodeManager
from lib.object_manager     import ObjectManager
from lib.network_manager    import NetworkManager
from lib.resource_manager   import ResourceManager
from lib.capability_manager import CapabilityManager
from lib.backend_manager    import BackendManager

# Resource Handlers
from lib.resources.file     import File
from lib.resources.git      import Git
from lib.resources.docker   import Docker as DockerResource

# Backends
from lib.backends.docker    import Docker as DockerBackend

# SQL foo
from sqlalchemy.sql import or_

class Occam:
  def __init__(self, path='~/.occam', options=None):
    """
    Creates an instance representing a local OCCAM node.
    """
    self.options = options
    self.path    = os.path.expanduser(path)

    # TODO: handle development??
    self.environment = "production"

    # Initialize Managers
    # TODO: lazy load these?
    self.nodes        = NodeManager(self)
    self.objects      = ObjectManager(self)
    self.network      = NetworkManager(self)
    self.resources    = ResourceManager(self)
    self.backends     = BackendManager(self)
    self.capabilities = CapabilityManager(self)

    self.backends.register('docker',  DockerBackend)

    self.resources.register('git',              Git)
    self.resources.register('file',             File)
    self.resources.register('application/zip',  File)
    self.resources.register('docker-container', DockerResource)

  def configuration(self):
    # Create paths if they don't already exist
    if not os.path.exists(self.path):
      Log.noisy("creating directory %s" % (self.path))
      os.mkdir(self.path)

    config_path = os.path.join(self.path, "config.yml")

    if not os.path.exists(config_path):
      Log.noisy("creating a new configuration file")

      # Create from default config
      default_config_path = os.path.join(os.path.dirname(__file__), '..', 'config.yml.sample')
      shutil.copyfile(default_config_path, config_path)

      # Generate secrets
      f = open(config_path)
      config_data = f.read()
      f.close()

      import binascii

      secret = binascii.hexlify(os.urandom(32))
      config_data = config_data.replace("%secret1%", secret.decode('ascii'))

      secret = binascii.hexlify(os.urandom(32))
      config_data = config_data.replace("%secret2%", secret.decode('ascii'))

      f = open(config_path, 'w+')
      f.write(config_data)
      f.close()

    config_file = open(config_path)
    config = yaml.safe_load(config_file)[self.environment]

    # Default password scheme
    config['passwords'] = config.get('passwords') or {}
    passwords = config['passwords']

    passwords['scheme'] = passwords.get('scheme') or "bcrypt"
    passwords['rounds'] = passwords.get('rounds') or 20

    # Default paths
    config['paths'] = config.get('paths') or {}
    paths = config['paths']

    # TODO: just clean this up like an adult
    paths['objects']    = paths.get('objects')    or os.path.join(self.path, "objects")
    paths['local']      = paths.get('local')      or os.path.join(self.path, "local")
    paths['git']        = paths.get('git')        or os.path.join(self.path, "git")
    paths['docker']     = paths.get('docker')     or os.path.join(self.path, "docker")
    paths['hg']         = paths.get('hg')         or os.path.join(self.path, "hg")
    paths['invocation'] = paths.get('invocation') or os.path.join(self.path, "invocation")
    paths['svn']        = paths.get('svn')        or os.path.join(self.path, "svn")
    paths['builds']     = paths.get('builds')     or os.path.join(self.path, "builds")
    paths['runs']       = paths.get('runs')       or os.path.join(self.path, "runs")
    paths['store']      = paths.get('store')      or os.path.join(self.path, "store")
    paths['jobs']       = paths.get('jobs')       or os.path.join(self.path, "jobs")
    paths['nodes']      = paths.get('nodes')      or os.path.join(self.path, "nodes")
    paths['ports']      = paths.get('ports')      or os.path.join(self.path, "ports")

    return config

  def temporaryClone(self, uuid, revision, worksetUuid=None, worksetRevision=None):
    # We are adding this to an object that already exists
    # We need to create a temporary object to house it
    db_obj = self.objects.search(uuid=uuid).first()
    if db_obj is None:
      Log.error("cannot find object with id %s" % (uuid))
      return -1

    if revision is None:
      revision = db_obj.revision

    ownerId = db_obj.owner_uid or db_obj.uid

    # This is ultimately the object to append the new object to
    obj = Object(self.objects.pathFor(ownerId), occam=self, revision=revision, uuid=db_obj.uid)

    destObjectInfo = obj.objectInfo()

    dest_obj_name = destObjectInfo['name']
    dest_obj_type = destObjectInfo['type']

    # Find and clone workset
    workset = obj
    workset_path = self.objects.pathFor(ownerId)
    if not worksetUuid is None and dest_obj_type != "workset":
      db_workset_obj = self.objects.search(uuid=worksetUuid, object_type="workset").first()
      if db_workset_obj is None:
        Log.error("cannot find workset object with id %s" % (worksetUuid))
        return -1

      # This is ultimately the object to append the new object to
      workset_path = self.objects.pathFor(db_workset_obj.uid)
      workset = Object(workset_path, occam=self, revision=worksetRevision)
    else:
      worksetUuid = uuid
      worksetRevision = revision

    # Create temporary path
    path = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))

    # Pull workset and all objects at this revision

    # Get a reference to every parent object
    clonedObjects = []
    clonedObjects.append(obj)

    current = obj
    objectInfo = current.objectInfo()
    while not current is None and 'type' in objectInfo and objectInfo['type'] != 'workset':
      current = current.parent()
      if current:
        clonedObjects.append(current)
        objectInfo = current.objectInfo()

    # Clone workset
    workset.clone(path, revision=worksetRevision)

    # Clone all cloned objects
    current = workset
    clonePath = path
    for clonedObject in reversed(clonedObjects):
      clonedObjectID = clonedObject.objectInfo()['id']
      Log.noisy("attempting to clone %s" % clonedObjectID)
      current_db_obj = self.objects.search(uuid=clonedObjectID).first()

      # Find dependency in current object
      dependencies = current.objectInfo().get('contains') or []
      cloneRevision = 'HEAD'
      for dependency in dependencies:
        Log.write("looking at %s vs %s" % (dependency.get('id'), clonedObjectID))
        if dependency.get('id') == clonedObjectID:
          cloneRevision = dependency.get('revision') or 'HEAD'

          current = Object(self.objects.pathFor(current_db_obj.uid), occam=self, revision=cloneRevision)
          clonePath = os.path.join(clonePath, (dependency.get('type') or 'object') + '-' + (Object.slugFor(dependency.get('name'))))
          Log.write("cloning %s to %s" % (clonedObjectID, clonePath))
          current.clone(clonePath, revision=revision, increment=True)
          break

    workset = Object(workset_path, occam=self, revision=worksetRevision)
    obj = self.objectAt(clonePath, revision=revision, uuid=db_obj.uid)

    Log.write("Cloning %s %s to %s" % (objectInfo.get('type', 'unknown'), objectInfo.get('name', 'unknown'), clonePath))
    if workset.objectInfo()['type'] == "workset":
      workset = Workset(workset_path, occam=self, revision=worksetRevision)
      obj.root = workset

    return [workset, obj, path]

  def objectAt(self, path, revision="HEAD", searchFor=None, searchForUUID=None, recurse=False, uuid=None):
    """
    Returns an instance of the object at the given path. If searchFor is given,
    it will look for the given type of object in the given path and any parent
    directories until found. Returns None if nothing is found. Accepts a
    revision which defaults to HEAD.
    """

    path = os.path.realpath(path)

    if not os.path.exists(path):
      return None

    # Look for a workset in this path. Recurse up until it finds one.
    while path != "/":
      obj = Object(path, occam=self, revision=revision)
      if obj.objectInfo():
        info = obj.objectInfo()
        if ((not searchFor and not searchForUUID) or (not searchFor is None and info.get('type') == searchFor) or (not searchForUUID is None and info.get('id') == searchForUUID)):
          if info.get('type') == "workset":
            return Workset(path, occam=self, revision=revision)
          elif info.get('type') == "group":
            return Group(path, occam=self, revision=revision)
          elif info.get('type') == "experiment":
            return Experiment(path, occam=self, revision=revision)
          else:
            return Object(path, occam=self, revision=revision, uuid=uuid)
      else:
        if searchFor is None and searchForUUID is None and not recurse:
          return None

      path = os.path.realpath(os.path.join(path, ".."))

    if path == "/":
      return None

  def initialized(self):
    """
    Returns True when this OCCAM node is initialized.
    """
    return os.path.exists(self.path)

  def initialize(self):
    """
    Initializes a local OCCAM node, if necessary.
    """

    path_types = ['objects', 'jobs', 'store', 'git', 'hg', 'svn', 'builds']

    # Create paths if they don't already exist
    if not os.path.exists(self.path):
      Log.noisy("creating directory %s" % (self.path))
      os.mkdir(self.path)

    for path in path_types:
      full_path = os.path.realpath(self.configuration()['paths'].get(path))
      if not os.path.exists(full_path):
        Log.noisy("creating directory %s" % (full_path))
        os.mkdir(full_path)

    db = False
    DB.initialize(self.options, self.path)
    DB.create(self.path)

    db = True

    if db:
      system = DB.session().query(DB.System).first()

      if system is None:
        # Set up system record with default paths
        Log.header("Establishing occam system in %s" % (self.path))

        system = DB.System()

        system.curate_git   = 1
        system.curate_hg    = 1
        system.curate_svn   = 1
        system.curate_store = 1

        system.moderate_people  = 0
        system.moderate_objects = 0

        DB.session().add(system)
        DB.session().commit()

  def storeSVN(self, svn, uuid=None):
    """
    Retains a copy of the svn repository in occam object pool. Local repositories are on
    disk and can be altered at will by anybody. These are cloned from these
    persisted artifacts.
    """

    # Place it in: .occam/git/2-code/2-code/full-code
    uuid = uuid or SVN.uuid()

    # TODO: handle svn path vs object path vs build path!!!
    path = self.objects.createPathFor(uuid, subPath="svn")

    # Create object in path to hold the git repository metadata
    obj = Object.create(path, "svn_repository", "occam/svn", uuid=uuid, createPath=False, occam=self)
    info = obj.objectInfo()
    info['file'] = 'repository'
    obj.updateObject(info, 'Added repository.')

    repo_path = os.path.join(path, "repository")
    os.mkdir(repo_path)

    svn.checkout(repo_path)

    return obj, SVN(repo_path)


  def storeMercurial(self, mercurial):
    # TODO: this
    raise Exception('not implemented')

  def destroyLocalLink(self, path, person):
    """
    Destroys any local link (or none if it doesn't exist) between the person
    and the given path.
    """

  def createLocalLink(self, path, obj, person):
    """
    Creates a link between a local data object and a local path for a given
    person. This record will allow changes that happen within the system
    or seen by the outside to be reflected at the current path automatically
    in some situations.
    """
    local_link = DB.LocalLink()

    local_link.path = path

    workset_obj = self.objects.search(object_type='workset', uuid=obj.objectInfo()['id']).first()
    person_obj  = self.objects.search(object_type='person', uuid=person.objectInfo()['id']).first()

    # Attach the Workset/Person metadata objects to the Local Link
    local_link.workset_id = workset_obj.workset.id
    local_link.person_id  = person_obj.person.id

    Log.noisy("creating local link between workset (%s) and person (%s) at %s" % (workset_obj.id, person_obj.id, path))

    DB.session().add(local_link)
    DB.session().commit()

  def currentPerson(self):
    auth_token_path = os.path.join(self.path, "auth_token")

    if os.path.exists(auth_token_path):
      auth_token = json.load(open(auth_token_path, 'r'))

      if 'person' in auth_token:
        person_uid = auth_token['person']
        db_person = self.searchPeople(uuid=person_uid).first()
        if not db_person is None:
          return Person(self.objects.pathFor(db_person.uid), occam=self)

    return None

  def currentPersonId(self):
    auth_token_path = os.path.join(self.path, "auth_token")

    if os.path.exists(auth_token_path):
      auth_token = json.load(open(auth_token_path, 'r'))

      if 'person' in auth_token:
        person_uid = auth_token['person']
        db_person = self.searchPeople(uuid=person_uid).first()
        if not db_person is None:
          return db_person.id

    return None

  def authenticate(self, username=None, email=None, password=None):
    account = self.searchAccounts(username=username, email=email)
    account = account.first()

    if account is None:
      return False

    if account:
      return bcrypt.hashpw(password.encode('utf-8'), account.hashed_password.encode('utf-8')) == account.hashed_password.encode('utf-8')
    else:
      return False

  def addAuthentication(self, person_uuid, password, roles=[]):
    # Get system admin record
    system = DB.session().query(DB.System).first()

    # Set password (if exists)
    account = DB.Account()

    # Accounts are active by default, unless moderation says otherwise
    if system.moderate_accounts == 1:
      # We are moderating accounts
      account.active = 0
    else:
      # We allow all accounts
      account.active = 1

    # Default to no roles
    account.roles = ";;"

    # First account defaults to administrator role
    if DB.session().query(DB.Account).count() == 0:
      if not 'administrator' in roles:
        roles.append('administrator')

    # Add the roles designated by the admin table
    default_roles = ((system.default_roles or ";;")[1:-1] or "").split(';')
    for default_role in default_roles:
      if not default_role in roles:
        roles.append(default_role)

    # Add the semi-colon delimited list of roles
    # TODO: roles can be broken if they contain a ';'
    #       but maybe this is not a problem.
    account.roles = ';' + ';'.join(roles) + ';'

    db_obj = DB.session().query(DB.Object).filter(DB.Object.uid == person_uuid).first()
    person = db_obj.person

    if person:
      account.username  = person.username
      account.person_id = person.id

      # TODO: move this out of this file (move configuration to its own place as well)
      rounds = 12

      config_info = self.configuration()
      rounds = config_info['passwords']['rounds']

      # Create password auth
      account.hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(rounds, prefix=b"2a")).decode('utf-8')

      # Create private key
      keyPair = Crypto.newKeyPair()
      account.private_key = keyPair[1].decode('utf-8')
      person.public_key = keyPair[0].decode('utf-8')

      DB.session().add(account)
      DB.session().commit()

  def updateAllKeys(self):
    DB.initialize(self.options, self.path)

    people = DB.session().query(DB.Person)
    for person in people:
      account = person.account

      # Create private key
      keyPair = Crypto.newKeyPair()

      if account:
        account.private_key = keyPair[1].decode('utf-8')

      person.public_key = keyPair[0].decode('utf-8')

      DB.session().commit()

  def addPerson(self, name):
    """
    Create a new person.
    """

    uuid = Person.uuid()
    path = os.path.realpath(self.objects.createPathFor(uuid))

    person = Person.create(path, name, uuid=uuid, occam=self)

    self.objects.store(person)

    return Person(path, occam=self)

  def addWorkset(self, path, name):
    """
    Create a new workset within this occam instance at the given local path.
    """

    path = os.path.realpath(path)

    workset = Workset.create(path, name)

    self.objects.store(workset)

    return Workset(path, occam=self)

  def searchRuns(self, uuid):
    DB.initialize(self.options, self.path)

    return DB.session().query(DB.Run).filter(DB.Run.uid==uuid)

  def searchAccounts(self, username=None, email=None):
    DB.initialize(self.options, self.path)

    query = DB.session().query(DB.Account)

    # Search for exact username
    if not username is None:
      query = query.filter(or_(DB.Account.username==username))

    # Search for exact email
    if not email is None:
      query = query.filter(or_(DB.Account.email==email))

    return query

  def searchAccounts(self, name):
    DB.initialize(self.options, self.path)
    query = DB.session().query(DB.Account)
    query = query.filter(DB.Account.username == name)

    return query

  def searchPeople(self, uuid=None, name=None, keyword=None):
    DB.initialize(self.options, self.path)

    query = DB.session().query(DB.Object)

    # Search for exact values
    if not uuid is None:
      query = query.filter(DB.Object.uid==uuid)
    if not name is None:
      query = query.filter(or_(DB.Object.name==name))

    # Search for arbitrary values
    if not keyword is None:
      query = query.filter(or_(DB.Object.uid==keyword, DB.Object.name==keyword))

    return query

  def searchLocalLinks(self, workset_id, person_id):
    DB.initialize(self.options, self.path)

    query = DB.session().query(DB.LocalLink)
    query = query.filter_by(workset_id=workset_id, person_id=person_id)

    return query

  def addAuthor(self, workset, person):
    DB.initialize(self.options, self.path)
    db_workset = self.objects.search(object_type="workset", uuid=workset.objectInfo()['id']).first().workset
    db_person  = self.searchPeople(uuid=person.objectInfo()['id']).first().person
    db_obj = DB.Authorship(workset_id = db_workset.id,
                           person_id  = db_person.id)
    DB.session().add(db_obj)
    DB.session().commit()

  def addCollaborator(self, workset, person):
    DB.initialize(self.options, self.path)
    db_workset = self.objects.search(object_type="workset", uuid=workset.objectInfo()['id']).first().workset
    db_person  = self.searchPeople(uuid=person.objectInfo()['id']).first().person
    db_obj = DB.Collaboratorship(workset_id = db_workset.id,
                                 person_id  = db_person.id)
    DB.session().add(db_obj)
    DB.session().commit()

  def update(self, obj, build_path=None, localHash=None):
    # Prepare build directory
    uuid = obj.objectInfo()['id']
    obj_revision = obj.revision or obj.head()
    if obj_revision == "HEAD":
      obj_revision = obj.head()
    if localHash:
      localHash = None
    if not build_path:
      build_path = self.objects.pathFor(uuid, subPath='builds')
      build_path = os.path.join(build_path, obj_revision)

    Log.noisy("updating build contained in %s (%s)" % (build_path, localHash))

    if not os.path.exists(build_path):
      build_path = None

    if build_path is None:
      return self.build(obj, build_path=build_path, localHash=localHash)

    return True

  def console(self, object, build_path = None, localHash = None):
    """
    Dispatch the given task object.
    """

    taskInfo = object.objectInfo()

    backend = self.backends.handlerFor(taskInfo.get('backend'))

    if not backend:
      return False

    # Create a space to run the task (if not provided)
    run_path = None

    uuid = taskInfo.get('id')
    obj_revision = object.revision or object.head()
    if obj_revision == "HEAD":
      obj_revision = object.head()

    if run_path is None:
      run_path = self.objects.createPathFor(uuid, subPath='runs')
      run_path = os.path.join(run_path, obj_revision)
      Log.write("created path %s" % (run_path))

    # Clone repository at that revision
    if not os.path.exists(run_path):
      object = Object(object.path, revision=obj_revision, occam=self)
      object.clone(run_path, revision=obj_revision)

    self.createTaskPaths(object, run_path)

    # Call the backend to run the task
    backend.console(object, run_path)

    return run_path

  def built(self, obj):
    # Determines if the object is already built

    if 'build' in obj.objectInfo():
      # Determine the backend it should use
      environment  = obj.objectInfo().get('environment')
      architecture = obj.objectInfo().get('architecture')

      object_backend = self.backends.providing(environment, architecture)
      nativeBackend  = self.backends.providing(environment, architecture, recurse=True)

      if nativeBackend:
        return nativeBackend.exists(obj)
      elif object_backend:
        # TODO: security: recursion??
        return self.built(object_backend)
      else:
        Log.error("No backend found for %s on %s" % (environment, architecture))
        return False

  def build(self, obj, build_path=None, localHash=None, local=False):
    if isinstance(obj, dict):
      objType = obj.get('type')
      if objType != "occam/task":
        Log.error("can only build objects from within a task manifest")
        return False

      # Go through task list and build against the backend
      backendName = obj.get('backend')
      nativeBackend = self.backends.handlerFor(backendName)

      if nativeBackend is None:
        Log.error("cannot use backend %s" % (backendName))
        return False

      # Go through objects to run!
      runInfo = obj.get('input', [{}])[0]
      environmentObject = self.objects.retrieve(runInfo.get('id'))

      while True:
        objName = runInfo.get('name', 'unknown')

        db_obj = self.objects.search(runInfo.get('id')).first()
        if db_obj is None:
          # TODO: Error
          return None

        object = self.objects.retrieve(db_obj.uid, db_obj.object_type, owner_uuid=db_obj.owner_uid)
        objName = object.objectInfo()['name']

        if not nativeBackend.exists(object):
          # Prepare build directory
          build_path = None
          uuid = object.objectInfo()['id']
          obj_revision = object.revision or object.head()
          if obj_revision == "HEAD":
            obj_revision = object.head()

          if build_path is None:
            build_path = self.objects.createPathFor(uuid, subPath='builds')
            build_path = os.path.join(build_path, obj_revision)
            Log.noisy("creating path %s" % (build_path))

          # Clone repository at that revision
          if not os.path.exists(build_path):
            object = Object(object.path, revision=obj_revision, occam=self)
            object.clone(build_path, revision=obj_revision)

          cloned_obj = Object(build_path, revision=obj_revision, occam=self)

          # Install
          cloned_obj.install()
          subInfo = cloned_obj.objectInfo()

          Log.write("building %s against backend %s" % (objName, backendName))

          elapsed = time.perf_counter()
          date    = datetime.datetime.utcnow().isoformat()
          success = nativeBackend.build(cloned_obj, environmentObject, build_path, local)
          elapsed = time.perf_counter() - elapsed

          self.objects.appendInvocation(object, 'builds', {
            "time": elapsed,
            "date": date,
            "success": success,
            "backend": backendName
          }, revision=obj_revision)

        if 'provides' in runInfo and 'input' in runInfo:
          # Cool, input is the object to run within
          runInfo = runInfo['input'][0]
        else:
          break;

      return True

    # Prepare build directory
    uuid = obj.objectInfo()['id']
    obj_revision = obj.revision or obj.head()
    if obj_revision == "HEAD":
      obj_revision = obj.head()
    if build_path is None:
      build_path = self.objects.createPathFor(uuid, subPath='builds')
      build_path = os.path.join(build_path, obj_revision)
      Log.noisy("creating path %s" % (build_path))
    else:
      Log.write("building in %s" % (build_path))

    # Clone repository at that revision
    if not os.path.exists(build_path):
      obj = Object(obj.path, revision=obj_revision, occam=self)
      obj.clone(build_path, revision=obj_revision)

    cloned_obj = Object(build_path, revision=obj_revision, occam=self)

    # Install
    cloned_obj.install()
    objInfo = cloned_obj.objectInfo()

    environment  = objInfo.get('environment')
    architecture = objInfo.get('architecture')

    # Build
    if 'run' in objInfo or 'build' in objInfo:
      # Determine the backend it should use
      backend = self.backends.providing(environment, architecture)
      nativeBackend = self.backends.providing(environment, architecture, recurse=True)

      if nativeBackend is None:
        # No native backend. We failed.
        Log.error("No backend found for %s on %s" % (environment, architecture))
        return False

      elapsed = time.perf_counter()
      date    = datetime.datetime.utcnow().isoformat()
      success = nativeBackend.build(cloned_obj, backend, build_path, local)
      elapsed = time.perf_counter() - elapsed

      DB.initialize(self.options, self.path)
      system = DB.session().query(DB.System).first()
      host = system.host
      port = system.port

      self.objects.appendInvocation(obj, 'builds', {
        "origin": {
          "host": host,
          "port": port
        },
        "time": elapsed,
        "date": date,
        "success": success,
        "backend": nativeBackend.name()
      }, revision=obj_revision)

      return success

    return True

  def pullJob(self, updateStatus=None):
    session = self.databaseSession()
    job = session.query(DB.Job).filter_by(status           = "queued",
                                          codependant      = 0,
                                          has_dependencies = 0).limit(1).first()

    if not job is None and not updateStatus is None:
      update_query = session.query(DB.Job).filter_by(id = job.id, status = "queued")
      update_query = update_query.update({"status": updateStatus})
      session.flush()
      session.commit()

    return job

  def pullCodependantJobs(self, job):
    return DB.session().query(DB.Job).filter(DB.Job.id.in_(DB.session().query(DB.JobCodependency.depends_on_job_id).filter_by(job_id = job.id)))

  def completeJob(self, job):
    # Update dependent jobs (if any)
    total_dependencies = (DB.session().query(DB.Job).filter_by(id = DB.session().query(DB.JobDependency.depends_on_job_id).filter_by(job_id = DB.session().query(DB.JobDependency.job_id).filter_by(depends_on_job_id = job.id))).count())

    if total_dependencies == 1:
      master_job = (DB.session().query(DB.Job).filter_by(id = DB.session().query(DB.JobDependency.job_id).filter_by(depends_on_job_id = job.id))).first()
      master_job.has_dependencies = 0
      print("Clearing block to run job #" + str(master_job.id))

    # Update job status to finished
    job.status = "finished"

    DB.session().commit()

  def downloadText(self, url, accept='application/json', scheme=None, suppressError=False, cert=None):
    response, content_type, size = self.network.get(url, accept, scheme, suppressError, cert=cert)

    if response is None:
      return None

    data = None
    reader = codecs.getreader('utf-8')
    if content_type == "application/json":
      data = json.load(reader(response))
    else:
      data = reader(response).read()

    return data

  def databaseSession(self):
    DB.initialize(self.options, self.path)
    return DB.session()

  def isUUID(self, uuid):
    return isinstance(uuid, str) and len(uuid) == 36 and uuid[8]  == '-' \
                                                     and uuid[13] == '-' \
                                                     and uuid[18] == '-' \
                                                     and uuid[23] == '-'

  def taskObjectsFor(self, task):
    """
    Yields a list of objects that are involved in the given task.
    """

    ret = []

    inputs = task.get('input', [])
    for input in inputs:
      if not input.get('id') is None:
        ret.append(input)
      ret.extend(self.taskObjectsFor(input))

    return ret

  def taskFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None):
    """
    Generates a task manifest for the given object that will run the object
    on this system.
    """

    if not isinstance(object, dict):
      objInfo = object.objectInfo()
      ownerInfo = object.ownerObjectInfo()
      revision = object.revision
    else:
      objInfo = object
      ownerInfo = {}

    # Gather object information
    objName = objInfo.get('name', 'unknown')
    objType = objInfo.get('type', 'object')

    environment  = objInfo.get('environment')
    architecture = objInfo.get('architecture')

    # Gather providers and the native backend
    backendPath = self.backends.providerPath(environment, architecture, environmentGoal, architectureGoal)
    nativeBackend = backendPath[-1]

    if nativeBackend is None:
      # No native backend. We failed.
      Log.error("No backend found for %s on %s" % (environment, architecture))
      return None

    if isinstance(nativeBackend, Object):
      backendName = None
    else:
      backendName = nativeBackend.__class__.name()

    # Once we have a native backend, build up the task manifest to run
    # on top of that backend

    task = {
      "type": "occam/task",
      "name": "Virtual Machine to run %s with %s" % (objName, backendName),
      "backend": backendName,
      "id": Object.uuid(''),

      "input": [{}]
    }

    if generator:
      task['generator'] = generator.objectInfo()
      task['generator']['revision'] = generator.revision

    # We need to run each backend to map the native backend to the environment
    # of the requested object.

    runInfo = task['input'][0]
    currentRunInfo = runInfo

    capabilities = set([])

    # The starting index for tagging unique objects in the VM
    objectIndex = indexStart

    for backend in reversed(backendPath[0:-1]):
      Log.write("setting up %s" % (backend.objectInfo().get('name')))

      # Add to our task
      currentRunInfo.update(backend.objectInfo())
      currentRunInfo['revision'] = backend.revision
      currentRunInfo['input'] = [{}]
      if not 'index' in currentRunInfo:
        currentRunInfo['index'] = objectIndex
        objectIndex += 1
      currentRunInfo = currentRunInfo['input'][0]

      Log.write("seeing capability request for %s" % (backend.objectInfo().get('capabilities', [])))
      capabilities |= set(backend.objectInfo().get('capabilities', []))

    # This will be a collection of all capabilities claimed by the objects needed to run
    capabilities |= set(objInfo.get('capabilities', []))

    # Now we need to satisfy the capabilities for the task

    task['capabilities'] = list(capabilities)
    providers = []
    Log.write("Raking capabilities")
    for capability in task['capabilities']:
      Log.write("Fulfilling capability '%s'" % (capability))
      objs = self.objects.providersFor(capability=capability)
      if objs.count() > 0:
        provider = self.objects.retrieve(objs[0].uid)
        providerInfo = provider.objectInfo()
        if not providerInfo.get('id') in providers:
          Log.write("Found: %s" % (objs[0].name))
          capabilities |= set(providerInfo.get('capabilities', []))
          providers.append(providerInfo.get('id'))

          # Add provider object
          task['input'][0]['input'].append(providerInfo)
          task['input'][0]['input'][len(task['input'][0]['input'])-1]['index'] = objectIndex
          task['input'][0]['input'][len(task['input'][0]['input'])-1]['revision'] = provider.revision
          objectIndex += 1

          # TODO: the providers need to be ordered by dependency and capability relations
          #       for instance, fluxbox provides windowing but requires x11 first!

    task['capabilities'] = list(capabilities)

    # TODO: move the above capability satisfier somewhere else (object manager??)
    # TODO: ensure that provider object capabilities are also satisfied

    # Add forced inputs
    if inputs is not None and isinstance(inputs, list):
      Log.write("Adding input to task")
      objInfo['input'] = objInfo.get('input', [])
      if not isinstance(objInfo['input'], list):
        objInfo['input'] = []

      for input in inputs:
        inputInfo = input
        if not isinstance(input, dict):
          inputInfo = input.objectInfo()
          inputInfo['revision'] = input.revision

        # Give the input a relative identifier
        inputInfo['index'] = objectIndex
        objectIndex += 1

        objInfo['input'].append(inputInfo)

    if ownerInfo.get('id', objInfo.get('id')) != objInfo.get('id'):
      objInfo['owner'] = {'id': ownerInfo['id']}
    currentRunInfo.update(objInfo)
    currentRunInfo['revision'] = revision

    if not 'index' in currentRunInfo:
      currentRunInfo['index'] = objectIndex

    # Build using the task manifest (this will build each object that needs
    # to be built to work with this backend)

    # Establish the 'volume' and 'mounted' paths for every object so they
    # know where they, and their inputs, exist in their environments
    self.setVolumesInTask(task)
    self.parseReplaceInTask(task)

    Log.noisy(json.dumps(task, indent=2, separators=(',', ': ')))

    return task

  def buildTask(self, object, inputs=None):
    # Generate a task for the given object
    task = self.taskFor(object, inputs=inputs)
    if task is None:
      return None
    self.build(task)

    # Create a task object in a temporary directory
    tempPath = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
    taskObject = Object.create(tempPath, task['name'], task['type'], uuid=task['id'], createPath=False, occam=self, belongsTo=None, root=None, build=False)
    taskObject.updateObject(task, "Forms task object.")

    # Store that task object
    self.objects.store(taskObject)

    # Delete the temporary path
    if tempPath:
      Log.noisy("removing %s" % tempPath)
      shutil.rmtree(tempPath)

    # Return the formed task object
    return self.objects.retrieve(task['id'])

  def rakeTaskObjects(self, objInfo):
    """
    Returns an array of every object metadata for the given task metadata.
    """

    ret = []

    for input in objInfo.get('input', []):
      ret.append(input)
      ret += self.rakeTaskObjects(input)

    return ret

  def splitKeys(self, key):
    keys = re.split(r"([^\\])\.", key)

    new_keys = []
    for i in range(0, len(keys)-1, 2):
      new_keys.append((keys[i] + keys[i+1]).replace('\\', ''))

    new_keys.append(keys[len(keys)-1].replace('\\', ''))

    return new_keys

  def parseReplaceInTask(self, objInfo):
    """
    This method goes through the task and parses the 'replace' sections. These
    are usually used to form commands from substring replacements while looking
    at input objects, etc.
    """

    if 'run' in objInfo and 'command' in objInfo['run']:
      runInfo = objInfo['run']

      replacements = runInfo.get('replace', [])
      for replacement in replacements:
        key = replacement.get('key')
        value = replacement.get('value')

        if key and value:
          keys = self.splitKeys(value)

          info = objInfo
          for item in keys:
            if info:
              if isinstance(info, list):
                item = int(item)
                info = info[item]
              elif item in info:
                info = info[item]
              else:
                info = None

          if info:
            value = info
          else:
            value = replacement.get('default', '')

          runInfo['command'] = runInfo['command'].replace(key, value)
          Log.write("Updating run command to: %s" % runInfo['command'])

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('input', [])
    if isinstance(inputs, list):
      for input in inputs:
        self.parseReplaceInTask(input)

    # Do the outputs, too
    outputs = objInfo.get('output', [])
    if isinstance(outputs, list):
      for output in outputs:
        self.parseReplaceInTask(output)

  def setVolumesInTask(self, objInfo, rootBasepath = None, basepath=None, rootMountPathIndex=None, mountPathIndex=None):
    """
    This method takes a task's metadata and adds 'volume' and 'mounted' fields
    based on any 'basepath' fields it finds.
    """

    # Start at the root of the virtual machine and go through recursively
    # to any leaves.
    mountId = objInfo.get('id')
    if 'owner' in objInfo:
      mountId = objInfo['owner'].get('id', objInfo.get('id'))

    if rootBasepath is None:
      rootBasepath = objInfo.get('basepath', None)

    if basepath is None:
      basepath = rootBasepath;

    if mountPathIndex is None:
      mountPathIndex = [0]

    if rootMountPathIndex is None:
      rootMountPathIndex = [0]

    # Set the volume and mounted for this entry
    if rootBasepath is not None and basepath is not None:
      rootMountPath = rootBasepath.get('mount', '/')
      if not isinstance(rootMountPath, str) and isinstance(rootMountPath, list):
        index = rootMountPathIndex[0]
        if index < len(rootMountPath):
          rootMountPath = rootMountPath[index]
          rootMountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          rootMountPath = "/"
      else:
        rootMountPath = rootBasepath.get('separator', '/').join(
          [ rootMountPath, "%s-%s" % (mountId, objInfo.get('revision')) ])

      mountPath = basepath.get('mount', '/')
      if not isinstance(mountPath, str) and isinstance(mountPath, list):
        index = mountPathIndex[0]
        if index < len(mountPath):
          mountPath = mountPath[index]
          mountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          mountPath = "/"
      else:
        mountPath = basepath.get('separator', '/').join(
          [ mountPath, "%s-%s" % (mountId, objInfo.get('revision')) ])

      localPath = rootBasepath.get('local', '/local')
      localPath = os.path.join(localPath, "objects", str(objInfo.get('index', 0)))

      objInfo['paths'] = {
        "volume": rootMountPath,
        "mount":  mountPath,

        "output": localPath,
        "local":  basepath.get('local', '/local')
      }

    if 'basepath' in objInfo:
      basepath = objInfo.get('basepath', basepath)

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('input', [])
    if isinstance(inputs, list):
      for input in inputs:
        self.setVolumesInTask(input, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Do the outputs, too
    outputs = objInfo.get('output', [])
    if isinstance(outputs, list):
      for output in outputs:
        self.setVolumesInTask(output, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

  def createTaskPaths(self, object, run_path):
    # Set up task directories

    taskInfo = object.objectInfo()

    paths = ['init', 'objects', 'outputs']
    for path in paths:
      realPath = os.path.join(run_path, path)
      if not os.path.exists(realPath):
        os.mkdir(realPath)

      # For each object in the task, add a directory (uuid-revision-index) to each
      Log.write("Setting up other paths")
      for metadata in self.rakeTaskObjects(taskInfo):
        revision = metadata.get('revision')
        index    = metadata.get('index')
        uuid     = metadata.get('id')

        token    = "%s-%s-%s" % (uuid, revision, index)

        tokenPath = os.path.join(realPath, str(index))
        Log.noisy("creating path for %s" % (tokenPath))

        if not os.path.exists(tokenPath):
          os.mkdir(tokenPath)

  def runTask(self, object):
    """
    Dispatch the given task object.
    """

    taskInfo = object.objectInfo()

    backend = self.backends.handlerFor(taskInfo.get('backend'))

    if not backend:
      return False

    # Create a space to run the task (if not provided)
    run_path = None

    uuid = taskInfo.get('id')
    obj_revision = object.revision or object.head()
    if obj_revision == "HEAD":
      obj_revision = object.head()

    if run_path is None:
      run_path = self.objects.createPathFor(uuid, subPath='runs')
      run_path = os.path.join(run_path, obj_revision)
      Log.write("created path %s" % (run_path))

    # Clone repository at that revision
    if not os.path.exists(run_path):
      object = Object(object.path, revision=obj_revision, occam=self)
      object.clone(run_path, revision=obj_revision)

    self.createTaskPaths(object, run_path)

    # Call the backend to run the task
    backend.run(object, run_path)

    return run_path
