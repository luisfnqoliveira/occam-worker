#!/usr/bin/env python

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Executes as a separate script that will start the given command in
# the background. This script must be reliable!

import os
import time
import signal
import sys
import resource       # Resource usage information.

from daemon   import Daemon
from optparse import OptionParser

def main(argv=None):
  if argv is None:
    argv = sys.argv

  parser = OptionParser()
  parser.add_option("-d", "--dispatcher",  action  = "store",
                                           type    = "int",
                                           dest    = "dispatcherProcessId",
                                           help    = "defines the process id of the dispatcher to signal")
  parser.add_option("-q", "--workerPath",  action  = "store",
                                           type    = "string",
                                           dest    = "workerPath",
                                           help    = "defines the path for the worker recordkeeping")
  parser.add_option("-w", "--workingPath", action  = "store",
                                           type    = "string",
                                           dest    = "workingDir",
                                           help    = "defines the working path of the job")
  parser.add_option("-r", "--root",        action  = "store",
                                           type    = "string",
                                           dest    = "rootPath",
                                           help    = "defines the root path for the occam job log")
  parser.add_option("-j", "--job",         action  = "store",
                                           type    = "int",
                                           dest    = "job",
                                           help    = "the job id associated with this process")
  parser.add_option("-c", "--command",     action  = "append",
                                           default = [],
                                           type    = "string",
                                           dest    = "commands",
                                           help    = "the command to execute")
  opts, args = parser.parse_args()

  if opts.job is None or opts.workingDir is None or opts.rootPath is None or not opts.commands:
    print("Job id, Root Path, Command, and Working Directory required")
    return 0

  if not os.path.exists(opts.workingDir):
    print("Working directory %s does not exist" % opts.workingDir)
    return 0
  if not os.path.exists(opts.workerPath):
    print("Worker path directory %s does not exist" % opts.workerPath)
    return 0
  elif not os.path.exists(opts.rootPath):
    print("Making jobs directory: %s" % (opts.rootPath))
    os.mkdir(opts.rootPath)

  retCode = Daemon(opts.workingDir, "%s/job-%s" % (opts.rootPath, opts.job), False).create()

  # Record our process id
  log(opts, retCode)

  # Exec
  spawn(opts)

  # Finish
  finish(opts)

  return 0

def log(opts, code):
  """Log the process parameters to a specific, unique job file.
  """
  procParams = """
{
  "job_id":             "%s",
  "exit_code":          "%s",
  "process_id":         "%s",
  "parent_process_id":  "%s",
  "parent_group_id":    "%s",
  "session_id":         "%s",
  "user_id":            "%s",
  "effective_user_id":  "%s",
  "real_group_id":      "%s",
  "effective_group_id": "%s"
}
""" % (opts.job, code, os.getpid(), os.getppid(), os.getpgrp(), os.getsid(0),
                       os.getuid(), os.geteuid(), os.getgid(),  os.getegid())

  print("Logging to %s/job-%s.json" % (opts.rootPath, opts.job))
  open("%s/job-%s.json" % (opts.rootPath, opts.job), "w").write(procParams + "\n")
  open("%s/../../current.json" % (opts.rootPath), "w").write(procParams + "\n")

  return code

def spawn(opts):
  for command in opts.commands:
    print("!!Running %s" % (command))
    os.system(command)
    print("!!Done")

  return 0

def finish(opts):
  maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
  if (maxfd == resource.RLIM_INFINITY):
    maxfd = MAXFD

  os.closerange(3, maxfd)

  time.sleep(1)

  # Delete the job file
  print("Removing %s/running/%s" % (opts.workerPath, opts.job))
  os.remove("%s/running/%s" % (opts.workerPath, opts.job))

  os.kill(int(opts.dispatcherProcessId), signal.SIGUSR1)

  return 0

if __name__ == "__main__":
  sys.exit(main())
