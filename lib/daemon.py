# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

class Daemon:
  # User permissions for the daemon
  # Prohibit: User none, Group write/execute, World all
  UMASK = 0o037

  # Number of possible file descriptors
  MAXFD = 1024

  # Get the null device for stdin
  if (hasattr(os, "devnull")):
    DEV_NULL = os.devnull
  else:
    DEV_NULL = "/dev/null"

  def __init__(self, workingDir, logFile, appendProcId=False):
    """Sets up the options for our daemon process to be created with the
    create() function
    """

    self.workingDir   = workingDir
    self.logFile      = logFile
    self.appendProcId = appendProcId

  def create(self):
    """Detach our process from the controlling terminal or session and run it
    in the background as a daemon.
    """

    # Fork ourselves twice and then orphan ourselves by ignoring S_NOHUP
    try:
      pid = os.fork()
    except OSError as e:
      raise(Exception, "%s [%d]" % (e.strerror, e.errno))

    if (pid == 0):
      os.setsid()

      try:
        pid = os.fork()
      except OSError as e:
        raise(Exception, "%s [%d]" % (e.strerror, e.errno))

      if (pid == 0):
        # Change our working directory
        os.chdir(self.workingDir)

        # Change our user
        os.umask(Daemon.UMASK)
      else:
        os._exit(0)
    else:
      os._exit(0)

    # get the number of possible fds we have open
    import resource
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if (maxfd == resource.RLIM_INFINITY):
      maxfd = MAXFD

    # close stdin, stdout, etc (close all open files)
    for fd in range(0, maxfd):
      try:
        os.close(fd)
      except(OSError):
        pass

    # redirect stdin to /dev/null
    os.open(Daemon.DEV_NULL, os.O_RDWR)

    # redirect stdout to occam-worker.log
    if self.appendProcId:
      os.open("%s-%s.log" % (self.logFile, os.getpid()), os.O_RDWR | os.O_CREAT)
    else:
      os.open("%s.log" % (self.logFile), os.O_RDWR | os.O_CREAT)

    # redirect stderr to occam-worker.log
    os.dup2(1, 2)

    return(0)
