# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import re
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from uuid import uuid1
from uuid import uuid4

from lib.log            import Log

from datetime import datetime

import subprocess

# TODO: respond to error codes and return False on errors

class SVN:
  """
  This support class will handle retrieving OCCAM metadata from objects given
  a revision and a object path.
  """

  @staticmethod
  def create(path):
    """
    Creates a svn repository for a new artifact.
    """

    Log.noisy("creating svn repository in %s" % (path))

    p = subprocess.Popen(['git', 'init'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Remove silly 'master' branch. Masters are a terrible, oppressive idea.
    # They do not exist, and should not exist, within a distributed system.
    # Give it a random identifier >:]
    p = subprocess.Popen(['git', 'checkout', '-b', str(uuid4())], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    p = subprocess.Popen(['git', 'branch', '-d', 'master'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    return Git(path)

  @staticmethod
  def exists(path):
    return os.path.exists(os.path.join(path, '.git'))

  def __init__(self, path, revision="HEAD", uuid=None):
    if not revision:
      revision = "HEAD"

    self.uuid = uuid
    self.revision = revision

    if re.match(r'^[a-z]+://', path):
      # Open a remote git repository (by cloning locally)
      tmpdir = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      p = subprocess.Popen(['git', 'clone', path, tmpdir], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=tmpdir).wait()
      self.tmp  = True
      self.path = tmpdir
    else:
      # Open a local git repository (by referencing it directly)
      self.tmp  = False
      self.path = path

    # TODO: check for revision errors, throw exception

  def __del__(self):
    if self.tmp:
      shutil.rmtree(self.path)

  @staticmethod
  def hasRevision(path, revision="HEAD"):
    return Git.fullRevision(path, revision) != False

  @staticmethod
  def fullRevision(path, revision="HEAD"):
    p = subprocess.Popen(['git', 'rev-parse', revision], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=path)
    errorCode = p.wait()
    if errorCode != 0:
      return False

    return p.stdout.read().decode('utf-8').strip()

  @staticmethod
  def uuid():
    """
    Creates a uuid for this git repository.
    """

    return "occam-git-%s" % (uuid1())

  def retrieveJSON(self, filepath):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """
    p = subprocess.Popen(['git', 'show', self.revision + ":./" + filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    reader = codecs.getreader('utf-8')
    try:
      object_info = json.load(reader(p.stdout))
    except:
      object_info = {}
    p.wait()

    return object_info

  def retrieveFile(self, filepath):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """
    p = subprocess.Popen(['git', 'show', self.revision + ":./" + filepath], stdout=subprocess.PIPE, cwd=self.path)
    ret = p.stdout.read()
    code = p.wait()
    if code != 0:
      ret = ""

    return ret

  def name(self):
    return self.objectInfo().get("name") or ""

  def type(self):
    return self.objectInfo().get("type") or ""

  def authors(self):
    return self.objectInfo().get("authors") or []

  def description(self):
    return self.objectInfo().get("description") or ""

  def objectInfo(self):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """

    return self.retrieveJSON('object.json')

  def configurations(self):
    objectInfo = self.objectInfo()
    if "configurations" in objectInfo:
      return objectInfo["configurations"]
    else:
      return []

  def outputs(self):
    objectInfo = self.objectInfo()
    if "outputs" in objectInfo:
      return objectInfo["outputs"]
    else:
      return []

  def inputs(self):
    objectInfo = self.objectInfo()
    if "inputs" in objectInfo:
      return objectInfo["inputs"]
    else:
      return []

  # TODO: rename to installInfo
  def installPackages(self):
    objectInfo = self.objectInfo()
    if "install" in objectInfo:
      if isinstance(objectInfo["install"], list):
        return objectInfo["install"]
      else:
        return [objectInfo["install"]]
    else:
      return []

  def buildInfo(self):
    return self.objectInfo().get("build") or {}

  def runInfo(self):
    return self.objectInfo().get("run") or {}

  def input(self, name, type):
    return {}

  def schema(self):
    objectInfo = self.objectInfo()

  def commit(self, message, filepath='-a'):
    Log.noisy("commiting '%s' to %s" % (message, self.path))
    p = subprocess.Popen(['git', 'commit', filepath, '--file=-'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.path)
    p.stdin.write(message.encode('utf-8'))
    p.stdin.close()
    p.wait()

    revision = self.head()

    return revision

  def add(self, filename='*'):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'add', filename], stdout=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def head(self):
    return Git.fullRevision(self.path)

  def parent(self):
    """
    Returns the parent revision.
    """

    return Git.fullRevision(self.path, "HEAD^")

  def checkout(self, to):
    p = subprocess.Popen(['svn', 'checkout', self.path, to], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

  def addRemote(self, name, path):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'remote', 'add', name, path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def rmRemote(self, name):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'remote', 'rm', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def fetch(self, name):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'fetch', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def fetchRemoteBranches(self, name):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'branch', '--track', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()
    p = subprocess.Popen(['git', 'fetch', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def fetchPath(self, path):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'fetch', '--', path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def branch(self):
    """
    Returns the name of the current branch.
    """
    # TODO: handle errors
    p = subprocess.Popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return p.stdout.read().decode('utf-8').strip()

  def addBranch(self, name):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'branch', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def switchBranch(self, name):
    # TODO: handle errors
    p = subprocess.Popen(['git', 'checkout', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def checkoutBranch(self, branch_name, old_branch_name=None, remote_name=None):
    # TODO: handle errors
    if old_branch_name is None:
      old_branch_name = branch_name

    if remote_name is None:
      p = subprocess.Popen(['git', 'checkout', '-b', branch_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    else:
      p = subprocess.Popen(['git', 'checkout', '-b', branch_name, '%s/%s' % (remote_name, old_branch_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def reset(self, revision, hard=True):
    if hard:
      p = subprocess.Popen(['git', 'reset', "--hard", revision], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    else:
      p = subprocess.Popen(['git', 'reset', revision], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def stash(self):
    p = subprocess.Popen(['git', 'stash'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True

  def stashPop(self):
    p = subprocess.Popen(['git', 'stash', 'pop'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.path)
    p.wait()

    return True
