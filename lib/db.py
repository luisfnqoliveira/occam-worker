# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SQLAlchemy Stuff
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy                 import create_engine
from sqlalchemy.orm             import sessionmaker
from sqlalchemy                 import Index, func

Base = declarative_base()

# Standard imports
import yaml
import os
import shutil

# Logging and Configuration
from lib.log import Log
from lib.config import Config

# Database Records
from models.object            import Object
from models.session           import Session
from models.job               import Job
from models.job_dependency    import JobDependency
from models.job_codependency  import JobCodependency
from models.person            import Person
from models.bookmark          import Bookmark
from models.review_capability import ReviewCapability
from models.experiment        import Experiment
from models.group             import Group
from models.workset           import Workset
from models.system            import System
from models.account           import Account
from models.run               import Run
from models.object_input      import ObjectInput
from models.object_output     import ObjectOutput
from models.collaboratorship  import Collaboratorship
from models.authorship        import Authorship
from models.local_link        import LocalLink
from models.node              import Node
from models.viewer            import Viewer
from models.provider          import Provider
from models.resource          import Resource
from models.association       import Association
from models.recently_used     import RecentlyUsed

class DB:
  engine = None
  db = None
  url = None

  # Only database records imported here will be created in the database
  # upon an 'occam initialize'
  Object           = Object
  Workset          = Workset
  Group            = Group
  Experiment       = Experiment
  System           = System
  Account          = Account
  Person           = Person
  Authorship       = Authorship
  Collaboratorship = Collaboratorship
  LocalLink        = LocalLink
  ObjectOutput     = ObjectOutput
  ObjectInput      = ObjectInput
  Run              = Run
  Node             = Node
  Job              = Job
  Provider         = Provider
  JobDependency    = JobDependency
  JobCodependency  = JobCodependency
  ReviewCapability = ReviewCapability
  Bookmark         = Bookmark
  Viewer           = Viewer
  Resource         = Resource
  Association      = Association
  RecentlyUsed     = RecentlyUsed

  @staticmethod
  def initialize(options, path=None):
    if DB.db:
      return

    if path is None:
      # Default path is ~/.occam
      path = os.path.realpath(os.path.join(os.path.expanduser("~"), ".occam"))

    Log.noisy("Initializing connection to database")

    config = Config.load(options, path)

    db_filename = 'occam'
    if options and hasattr(options, 'test') and options.test:
      db_filename = 'occam-test'

    db_config = config['database']
    adapter = db_config['adapter']

    db_url = db_config.get('uri')

    if adapter == "sqlite3":
      db_url = 'sqlite:///%s/%s.db' % (path, db_filename)
    elif adapter == "postgres":
      db_url = db_config.get('uri')

    DB.adapter = adapter
    DB.url = db_url
    DB.engine = create_engine(db_url)

    Session = sessionmaker(bind=DB.engine)

    DB.db = Session()

    Log.noisy("Database connection established")
    return DB.db

  @staticmethod
  def close():
    DB.db.close()
    DB.db = None
    DB.engine = None

  @staticmethod
  def create(path):
    Log.write("creating tables")
    Base.metadata.create_all(DB.engine)

    # Create Indexes
    if DB.adapter != "sqlite":
      Index('object_name_index', func.lower(Object.name))

  @staticmethod
  def session():
    return DB.db
