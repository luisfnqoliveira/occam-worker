# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log    import Log

import os
import subprocess

class CapabilityManager:
  """
  This OCCAM manager handles querying for system properties.
  """

  def __init__(self, occam):
    """
    Initialize the node manager.
    """

    self.occam   = occam
    self.options = occam.options

    self.apparmor = None

  def appArmor(self):
    """
    Returns True when AppArmor is present on the system.
    """
    if self.apparmor is None:
      ret = False

      apparmor_path = "/sys/module/apparmor/parameters/enabled"
      if os.path.exists(apparmor_path):
        f = open(apparmor_path, 'r')
        if f.read().strip() == "Y":
          ret = True

      self.apparmor = ret

    return self.apparmor

  def userId(self):
    """
    Returns the current user ID for the running occam process.
    """

    # TODO: platform independence (for Windows??):

    p = subprocess.Popen(['id', '-u', os.getenv('USER')], stdout=subprocess.PIPE)
    user_id = p.stdout.read().decode('utf-8').strip()
    p.wait()

    return user_id

  def groupId(self):
    """
    Returns the current user's primary group ID for the running occam process.
    """

    # TODO: platform independence (for Windows??):

    p = subprocess.Popen(['id', '-g', os.getenv('USER')], stdout=subprocess.PIPE)
    group_id = p.stdout.read().decode('utf-8').strip()
    p.wait()

    return group_id
