# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import re

from lib.git import Git
from lib.log import Log

from io import BytesIO

# For pulling network http resources
from ftplib import FTP

try:
  # Python 3
  from urllib.request import Request, URLopener, urlopen, HTTPError
  from http.client import HTTPConnection
  from urllib.parse import urlparse
except:
  # Python 2
  from urllib2 import Request, URLopener, urlopen, HTTPError
  from urllib2 import urlparse
  from httplib import HTTPConnection

# For HTTPS using stored certificates
import ssl
import socket

class HTTPSConnection(HTTPConnection):
  pass

class NetworkManager:
  """
  This OCCAM manager handles downloading resources from the network.
  """

  def __init__(self, occam):
    """
    Initialize the network manager.
    """

    self.occam   = occam
    self.options = occam.options
    self.ports = {}
    self.portStart = 30001
    self.portEnd = 31000

    # Ensure that the port manager path is created
    self.portPath = self.occam.configuration()['paths']['ports']
    if not os.path.exists(self.portPath):
      os.mkdir(self.portPath);

  def getFTP(self, url):
    """
    Retrieves the given document over FTP.
    """

    urlparts = urlparse(url)
    path = os.path.dirname(urlparts.path)
    filename = os.path.basename(urlparts.path)

    ftp = FTP(urlparts.hostname)
    Log.write(ftp.login())

    # Force binary mode
    Log.write(ftp.sendcmd("TYPE I"))

    # Change directory
    Log.write(ftp.cwd(path))

    # Retieve file
    socket, size = ftp.ntransfercmd('RETR %s' % (filename))

    # We must maintain the ftp connection and the socket for the incoming buffer
    class Socket:
      def __init__(self, socket, ftp):
        self.socket = socket
        self.ftp    = ftp

      def read(self, *args):
        return self.socket.recv(*args)

    # Return the buffer as a byte stream
    return Socket(socket, ftp), 'application/octet-stream', size

  def getHTTP(self, url, accept, scheme=None, suppressError=False, doNotVerify=False, cert=None):
    """
    Retrieves the given document over HTTP.
    """

    data = None

    response = None
    content_type = None

    request = Request(url)
    if scheme is not None:
      request.type = scheme
    request.add_header('Accept', accept)

    try:
      response = urlopen(request, cafile=cert)
      content_type = dict(response.info()).get('Content-Type')

      try:
        content_type = content_type[0:content_type.index(';')]
      except:
        pass
    except HTTPError as e:
      if e.code == 404:
        if not suppressError:
          Log.error("cannot find resource: HTTP code %s" % (e.code))
      else:
        if not suppressError:
          Log.error("cannot open resource: HTTP code %s" % (e.code))
    #except:
    #  Log.error("cannot open resource: %s" % (sys.exc_info()[0]))
    #  if not suppressError:
    #    Log.error("cannot open resource: %s" % (sys.exc_info()[0]))

    try:
      size = int(response.headers.get('Content-Length'))
    except:
      size = 0

    return response, content_type, size

  def get(self, url, accept='application/json', scheme=None, suppressError=False, doNotVerify=False, cert=None):
    """
    Retrieves the given document over HTTP or FTP.
    """

    urlparts = urlparse(url)

    if scheme is None:
      if urlparts.scheme == "":
        scheme = 'http'
      else:
        scheme = urlparts.scheme

    if scheme == "ftp":
      return self.getFTP(url)
    else:
      return self.getHTTP(url, accept, scheme, suppressError, doNotVerify, cert)

  def post(self, url, data={}, accept='application/json'):
    pass

  def isGitAt(self, url, cert=None):
    """
    Returns True when the given url points to a git repository.
    """
    return Git.isAt(url, cert=cert)

  def isObjectAt(self, url):
    pass

  def isURL(self, url):
    return re.match(r'^[a-z]+://', url)

  def allocatePort(self):
    """
    Allocates a free port from the ranged configured on the system to be used
    by OCCAM. This ensures that ports are not used more than once by an OCCAM
    process or virtual machine.
    """

    ret = None
    for port in range(self.portStart, self.portEnd):
      if not port in self.ports:
        path = os.path.join(self.portPath, str(port))
        if not os.path.exists(path):
          f = open(str(path), 'w+')
          f.close()

          self.ports[port] = True
          ret = port

          break

    return ret

  def freePort(self, port):
    """
    Deallocates a port that was previously allocated by a call to allocatePort.
    """

    if port in self.ports:
      path = os.path.join(self.portPath, str(port))
      if os.path.exists(path):
        os.remove(path)

      del self.ports[port]

    return True
