# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import yaml

class Config:
  data = None

  @staticmethod
  def load(options=None, path=None):
    """
    Returns the configuration file.
    """

    if Config.data is None:
      if path is None:
        # Default path is ~/.occam
        path = os.path.realpath(os.path.join(os.path.expanduser("~"), ".occam"))

      # Look at config.yml
      config_path = os.path.join(path, 'config.yml')
      if not os.path.exists(config_path):
        # Create from default config
        default_config_path = os.path.join(os.path.dirname(__file__), '..', 'config.yml.sample')
        shutil.copyfile(default_config_path, config_path)

      config_file = open(config_path)

      environment = "production"
      if options and options.development:
        environment = "development"

      Config.data = yaml.safe_load(config_file)[environment]

    return Config.data
