# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil

from lib.db     import DB
from lib.log    import Log
from lib.object import Object

import uuid as UUID

class ResourceManager:
  """
  This OCCAM manager handles resource installation. This is the process
  OCCAM uses to resolve "install" sections of any Object.

  When a resource is newly pulled from source, the uuid and revision/hash
  are not known. So, it is pulled to a temporary place near where the
  resource would be stored and then moved when the name is known. An Object
  and Resource record is created as necessary.
  """

  def __init__(self, occam):
    """
    Initialize the resource manager.
    """

    self.occam    = occam
    self.options  = occam.options
    self.handlers = {}

  def uuid(self, source):
    """
    Creates a namespaced uuid based on the given resource source location.
    """

    return str(UUID.uuid3(namespace=UUID.NAMESPACE_URL, name=source))

  def register(self, resourceType, handlerClass):
    """
    Adds a new resource type.
    """
    self.handlers[resourceType] = handlerClass

  def handlerFor(self, resourceType):
    """
    Returns an instance of a handler for the given type.
    """

    if not resourceType in self.handlers:
      # TODO: warning?
      resourceType = 'file'

    return self.handlers[resourceType](self.occam)

  def createObjectRecord(self, resourceType, uuid, name, source):
    """
    Adds a record of this resource object to the database. The Object record
    will have the type of 'resource' and have the resource field set to the
    type of resource ('git', 'file', etc.)
    """

    # Get a Database instance
    session = self.occam.databaseSession()

    # Retrieve any existing Object record
    obj = self.retrieveObjectRecord(resourceType, uuid)

    # If the Object record doesn't exist, create it
    if obj is None:
      obj = DB.Object()

      # Assign the uuid given to us
      obj.uid = uuid

      # Assign the default Object type for resources
      obj.object_type = "resource"
      obj.object_type_safe = "resource"

      # Assign the specific resource type (git, file, etC)
      # This will allow the system to know it is wrapping a resource on disk
      obj.resource = resourceType

      # Assign the given name
      obj.name = name

      # Assign the original source (provenance)
      obj.source = source

      # Commit
      session.add(obj)
      session.commit()

    return obj

  def createResourceRecord(self, resourceType, obj, revision):
    """
    Adds a record for this instantiation of a resource. It will be attached to
    the given object record. The new record will record the source of the data
    for the original resource and the revision (hash, etc) for the resource
    as given by the handler.
    """

    # Get a Database instance
    session = self.occam.databaseSession()

    # Retrieve any existing resource
    resource = self.retrieveResourceRecord(resourceType, obj.uid, revision)

    # If the record doesn't exist, create it
    if resource is None:
      resource = DB.Resource()

      # Attach it to the existing resource Object record
      resource.occam_object_id = obj.id

      # Tag the revision or hash for lookups later
      resource.revision = revision

      # Commit
      session.add(resource)
      session.commit()

    return obj

  def retrieveObjectRecord(self, resourceType=None, uuid=None, source=None):
    """
    Retrieves the database record that describes the resource given by its
    resourceType and/or uuid or source.

    If the given resource is not known, then None is returned.
    """

    session = self.occam.databaseSession()

    query = session.query(DB.Object)
    if not resourceType is None:
      query = query.filter_by(resource=resourceType)
    if not uuid is None:
      query = query.filter_by(uid=uuid)
    if not source is None:
      query = query.filter_by(source=source)

    return query.first()

  def retrieveResourceRecord(self, resourceType, uuid, revision):
    """
    Retrieves the database record that describes the instantiation of the given
    resource given its resourceType, uuid, and revision.

    If the resource is not known specifically for this revision, None is
    returned.
    """

    obj = self.retrieveObjectRecord(resourceType=resourceType, uuid=uuid)

    session = self.occam.databaseSession()

    query = session.query(DB.Resource)
    query = query.filter_by(revision=revision, occam_object_id=obj.id)

    return query.first()

  def retrieve(self, uuid, revision):
    """
    Retrieves the resource given by the uuid and revision.
    """
    obj = self.retrieveObjectRecord(uuid=uuid)

    resourceType = None

    if not obj is None:
      resourceType = obj.resource

    if resourceType is None:
      return None

    handler = self.handlerFor(resourceType)
    return handler.retrieve(uuid, revision)

  def pull(self, resourceInfo):
    """
    Returns the object for the specfied resource, creating and storing it if
    necessary.
    """

    install_type = resourceInfo.get('type', 'object')
    uuid         = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')

    if uuid is None:
      uuid = self.uuid(source)

    handler = self.handlerFor(install_type)

    # Pull Object for this resource, if it exists
    obj = self.retrieveObjectRecord(install_type, uuid)

    # If Object does not exist, create the Object
    if obj is None:
      obj = self.createObjectRecord(install_type, uuid, name, source)

    # Pull Resource for this resource, if it exists
    resource = self.retrieveResourceRecord(install_type, uuid, revision)

    # Detect if the resource exists
    path = handler.retrieve(uuid, revision)

    # Create a place to put the resource, if it doesn't exist
    new = False
    if path is None:
      path = self.occam.objects.createPathFor("temporary-" + str(UUID.uuid4()), subPath=handler.path())
      Log.noisy("Creating a new resource in %s" % (path))
      new  = True

    # Pull the resource
    revision, subDependencies = handler.pull(uuid, revision, name, source, to, path)

    # If a new resource, move the resource to its proper location
    if new:
      properPath = handler.pathFor(uuid, revision)
      shutil.move(path, properPath)
      Log.noisy("Moving resource to %s" % (properPath))

      # Create new Resource record
      self.createResourceRecord(install_type, obj, revision)

    return obj, revision, subDependencies

  def pullAll(self, obj):
    """
    Returns an array of Objects for pulled objects. This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section.
    """

    ret = []

    objectInfo = obj.objectInfo()

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    resourceList = resources

    while len(resourceList) > 0:
      dependencies = []

      for resourceInfo in resourceList:
        new_obj, revision, subDependencies = self.pull(resourceInfo)

        # Remove all known subDependencies
        unknownDependencies = []

        for subDependency in subDependencies:
          found = False
          for resource in resources:
            if (resource.get('type')   == subDependency.get('type')   and
                resource.get('source') == subDependency.get('source') and
                resource.get('to')     == subDependency.get('to')):
              found = True

          for resource in ret:
            if (resource.get('type')   == subDependency.get('type')   and
                resource.get('source') == subDependency.get('source') and
                resource.get('to')     == subDependency.get('to')):
              found = True

          if not found:
            unknownDependencies.append(subDependency)

        dependencies.extend(unknownDependencies)

        if new_obj is None:
          Log.error("Could not pull %s resource." % (resourceInfo.get('type', 'unknown')))
          new_info = {}
        else:
          new_info = resourceInfo.copy()
          new_info.update({
                  'id': new_obj.uid,
            'revision': revision
          })

        ret.append(new_info)

      # TODO: handle duplicates so as to not infinite loop, or import a limit
      resourceList = dependencies

    return ret

  def install(self, obj, resourceInfo, path):
    """
    Installs the given resource to the given path.
    """

    if resourceInfo is None:
      return

    # Get resource Type
    resourceType = resourceInfo.get('type', 'file')
    handler = self.handlerFor(resourceType)

    path = os.path.join(path, resourceInfo.get('to', 'package'))

    Log.write("Installing %s resource to %s" % (resourceType, path))

    uuid = resourceInfo.get('id')
    revision = resourceInfo.get('revision')

    resourcePath = None

    if not revision is None and not uuid is None:
      resourcePath = handler.retrieve(uuid, revision)
      Log.noisy("Installing resource from %s" % (resourcePath))

    if not resourcePath is None:
      return handler.install(uuid, revision, path, resourceInfo)

    Log.error("Failed to find resource")

    return False

  def installAll(self, obj, path):
    """
    Returns an array of Objects for pulled objects. Will create them as they
    are needed. This array, like pullAll, correspond directly to the list of
    resources to install in the given Object's 'install' section. This method
    will install the resources to the given path.
    """

    resources = self.pullAll(obj)

    for resource in resources:
      new_obj = self.install(obj, resource, path)

    return resources

  def cloneAll(self, obj):
    """
    Returns an array of resource infos for pulled objects. This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section. It will clone the resource when possible and update that section to
    point to the cloned version. Otherwise, it will leave the resource info
    alone.
    """

    ret = []

    objectInfo = obj.objectInfo()

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    for resourceInfo in resources:
      new_info = self.clone(resourceInfo)
      ret.append(new_info)

    return ret

  def clone(self, resourceInfo):
    """
    Will clone the given resource, if necessary. If not necessary, will just
    return the resource as is. Generally, this is useful for cloning objects
    that have a git repository attached. When you clone that object, you mean
    to modify the code, so you must also fork the repositories.
    """

    install_type = resourceInfo.get('type', 'object')
    uuid         = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')

    handler = self.handlerFor(install_type)
    if handler.clonable():
      newResourceInfo = handler.clone(uuid, revision, name, source, to)
      newInfo = resourceInfo.copy()

      # Overwrite identifying tags in the new resource info structure
      for tag in ['id', 'revision']:
        newInfo[tag] = newResourceInfo.get(tag, newInfo.get(tag))

      return newInfo
    else:
      return resourceInfo

  def update(self):
    """
    Updates the given resource via the source. Pulls in any new information or
    changes from the source or from node mirrors. Returns True if there are
    any updated changes.
    """

    return False

  def commitAll(self, obj):
    """
    This is given a local object. Will go through the object's resources in
    its 'install' section and for each decide if the value currently on disk in
    the object's path has changed. It will return an updated resource list
    containing new revisions while storing the changes in the store.
    """

    resources = obj.objectInfo().get('install', [])
    if not isinstance(resources, list):
      resources = [resources]

    dirty = False

    ret = []

    for resourceInfo in resources:
      # Look at that directory (if it exists) and check to see
      # if the resource has changed

      path = os.path.join(obj.path, resourceInfo.get('to', 'package'))

      newResourceInfo, changed = self.commit(resourceInfo, path)
      if changed:
        ret.append(newResourceInfo)
        dirty = True
      else:
        ret.append(resourceInfo)

    return ret, dirty

  def commit(self, resourceInfo, path):
    """
    """

    install_type = resourceInfo.get('type', 'object')
    uuid         = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')

    # Call the resource handler's commit method
    handler = self.handlerFor(install_type)
    newResourceInfo, dirty = handler.commit(uuid, revision, name, source, path)

    # Create a new resource tag based on the old
    newInfo = resourceInfo.copy()

    # Update revision
    newInfo['revision'] = newInfo.get('revision', '')
    newInfo['revision'] = newResourceInfo.get('revision', newInfo['revision'])

    # Return the updated resource tag
    return newInfo, dirty
