# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pymongo import MongoClient

from lib.log import Log

class DocumentStore:
  engine = None
  db = None

  def initialize(options):
    # TODO: read from config.yml
    if DocumentStore.db:
      DocumentStore.close()

    Log.noisy("Initializing connection to document store")

    # Default host/port
    host = 'localhost'
    port = 27017
    name = 'occam'

    if options.mongo_host:
      host = options.mongo_host

    if options.mongo_port:
      port = options.mongo_port

    if options.mongo_name:
      name = options.mongo_name

    DocumentStore.engine = MongoClient(host, port)
    DocumentStore.db = DocumentStore.engine[name]

    return DocumentStore.db

  def close():
    DocumentStore.engine.close()
    DocumentStore.db = None
  
  # as a class level function, self as a param?
  def ordered_hash(self,value):
    for k, v in value.items():
      if isinstance(value[k], dict):
        value[k] = DocumentStore.ordered_hash(value[k])

    value["__ordering"] = list(value.keys())

    return value

  def session():
    return DocumentStore.db
