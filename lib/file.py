# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import re
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from uuid import uuid1
from uuid import uuid4

from lib.log            import Log

from datetime import datetime

# TODO: respond to error codes and return False on errors

class File:
  """
  This support class will handle retrieving OCCAM metadata from objects given
  a revision and a object path.
  """

  def __init__(self, path, revision="HEAD", uuid=None):
    if not revision:
      revision = "HEAD"

    self.uuid = uuid
    self.revision = revision

    if re.match(r'^[a-z]+://', path):
      # Path is a network point
      self.path = tmpdir
      self.local = False
    else:
      # Path is local
      self.path = path
      self.local = True

  @staticmethod
  def uuid():
    """
    Creates a uuid for this file.
    """

    return "occam-file-%s" % (uuid1())

  def retrieveJSON(self, filepath):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """
    fullpath = os.path.join(self.path, filepath)
    if self.local:
      reader = codecs.getreader('utf-8')
      f = open(fullpath, "r")
      try:
        object_info = json.load(reader(f))
      except:
        object_info = {}
      f.close()
    else:
      # Download the file at that path
      pass

    return object_info

  def retrieveFile(self, filepath):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """
    fullpath = os.path.join(self.path, filepath)
    if self.local:
      reader = codecs.getreader('utf-8')
      if os.path.exists(fullpath):
        f = open(fullpath, "r")
        try:
          ret = f.read()
        except:
          ret = ""
        f.close()
      else:
        ret = ""
    else:
      # Download the file at that path
      pass

    return ret

  def name(self):
    return self.objectInfo().get("name") or ""

  def type(self):
    return self.objectInfo().get("type") or ""

  def objectInfo(self):
    """
    Returns the object metadata for a particular revision of an object that is
    local on the disk or remote. If it is remote, it has to fetch the history.
    """

    return Git(self.path).retrieveJSON('object.json')

  def head(self):
    return Git.fullRevision(self.path)

  def pullFile(self, url):
    pass

  def clone(self, to):
    if self.local:
      # Clone local file contents
      shutil.copytree(self.path, to)
    else:
      # Download all files to destination
      pass
