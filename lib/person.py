# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log        import Log
from lib.git        import Git
from lib.group      import Group
from lib.object     import Object

import os
import json

from uuid import uuid4

class Person():
  """
  This class represents an OCCAM person.
  """

  @staticmethod
  def create(path, name, uuid=None, occam=None):
    """
    Creates a person.
    """

    Object.create(path, name, "person", uuid=Person.uuid(), root=None, build=False, occam=occam, belongsTo=None, createPath=True)
    return Person(path, occam=occam, root=None)

  @staticmethod
  def uuid():
    """
    Creates a uuid for this person.
    """

    return str(uuid4())

  def __init__(self, path, occam=None, revision="HEAD", root=None):
    """
    Creates an instance of a person wrapping an existing person at the
    given path. It can take any path and will search for the person in
    parent paths until finding a person object.
    """

    self.object = Object(path, revision=revision, occam=occam, root=root)

  def __getattr__(self, method_name):
    """
    This will override method calls which don't exist on Experiment and
    pass them to the internal Object.
    """
    return getattr(self.object, method_name)
