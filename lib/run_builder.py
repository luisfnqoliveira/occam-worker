# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
import itertools
import functools
import shutil
import collections

from time import sleep

from lib.log                import Log
from lib.git                import Git
from lib.commands.configure import Configure

class RunBuilder:
  """
  This class will handle expanding experiment workflows into runs.
  """

  def __init__(self, args, options, workingPath = ".", occam = None):
    """
    Construct a run builder class.
    """

    self.args        = args
    self.options     = options
    self.workingPath = workingPath
    self.occam       = occam

  def do(self, recursive=False):
    ret = []

    path = self.workingPath or "."

    path = os.path.realpath(path)

    Log.header("Building Runs for Experiment")
    # We turn 'experiment' objects into many 'run' objects
    # We do this by looking at each object in the workflow and
    # each configuration. For any configuration with a range of
    # values, we split every possible permutation into a 'run'

    # Look for object.json?
    json_path = "%s/object.json" % (path)
    if os.path.exists(json_path):
      obj_info = json.load(open(json_path))
    else:
      obj_info = {}

    obj_type = obj_info.get('type') or "experiment"

    if len(self.args) > 2:
      obj_type = self.args[1]
      name = self.args[2]
    elif len(self.args) > 1:
      obj_type = None
      name = self.args[1]
    else:
      name = obj_info.get('name')

    # We only operate on 'run' objects
    if not obj_type == "experiment":
      Log.error("cannot find a experiment object in %s" % (path))
      exit(-1)
      return

    # Make a runs directory
    runs_path = os.path.join(path, "runs")
    if not os.path.exists(runs_path):
      Log.write("Creating runs directory")
      os.mkdir(runs_path)

    # Pull out connections
    self.job_count = 0
    all_connections = []

    workflow = obj_info.get('workflow')
    if workflow:
      all_connections = workflow.get('connections') or []

    total_runs = 1
    prepared_configurations = []
    configuration_index = 0
    connection_index = 0

    local_objects = None

    for connection in all_connections:
      if connection["object"].get('created', False):
        #prepared_configurations.append([])
        connection_index += 1
        continue
      elif connection["object"]["local"]:
        # Find local object
        if local_objects is None:
          workset = self.occam.objectAt(path, searchFor='workset')
          if not workset is None:
            local_objects = workset.objects()
          else:
            parent_obj = self.occam.objectAt(os.path.join(path, '..'), recurse=True)
            local_objects = [parent_obj]
        object = None
        for local_object in local_objects:
          local_object_info = local_object.objectInfo()
          if local_object_info["id"] == connection["object"]["id"]:
            object = local_object
            break

        if object is None:
          Log.error("cannot find attached object in the workset: %s %s" % (connection["object"]["type"], connection["object"]["name"]))
          exit(-1)
          return

        object_path = object.path
      else:
        object = self.occam.objects.search(uuid = connection["object"]["id"]).first()

        if object is None:
          Log.error("cannot find attached object: %s %s" % (connection["object"]["type"], connection["object"]["name"]))
          exit(-1) # TODO: remove this
          return -1

        object = self.occam.objects.retrieve(object.uid, object.object_type)

      objectInfo = object.objectInfo()

      # Pull out permutations of configurations for each connection
      # TODO: configurations based on revision
      for configuration in (objectInfo.get("configurations") or []):
        prepared_configurations.append([connection_index])

        if "file" in configuration:
          configuration_file_name = configuration["file"]

        schema = configuration.get('schema') or {}
        if not isinstance(schema, dict):
          schema = object.git.retrieveJSON(schema)

        # Pull configuration either from the file
        if not configuration_file_name is None:
          configuration_file_path = os.path.join(path, "config", str(connection_index), configuration_file_name)
          if os.path.exists(configuration_file_path):
            input_date = os.path.getmtime(configuration_file_path)
            configuration_info = json.load(open(configuration_file_path, 'r'))
          else:
            # TODO: handle thisssss!
            Log.error("cannot find configuration file %s for attached object: %s %s" % (configuration_file_name, connection["object"]["type"], connection["object"]["name"]))
            exit(-1) # TODO: remove this (have caller handle error)
            return -1

        # Rake ranges to find all ranged values and return a list of them
        ranges = Configure.rake_ranges(configuration_info, schema)

        # Determine every unique value for each configuration option
        ranges = Configure.form_range(ranges, schema)

        # Explode these into new configurations
        if len(ranges) == 0:
          num_ranges = 0
        else:
          # Multiply all possible values to determine the number of possible combinations
          num_ranges = functools.reduce(lambda x, y: x*y, [len(x) for x in ranges])

        ranges = itertools.product(*ranges)

        # For the product of all ranges, create a configuration
        if num_ranges == 0:
          num_ranges = 1

          Log.write("No configuration ranges; making a single run")

          # Just plop down the configuration as is into the run directory
          run_path = os.path.join(path, "runs", str(0))
          if not os.path.exists(run_path):
            Log.write("Creating run #%s" % (str(0)))
            os.mkdir(run_path)

          run_configurations_path = os.path.join(run_path, "config")
          if not os.path.exists(run_configurations_path):
            os.mkdir(run_configurations_path)

          run_configurations_path = os.path.join(run_configurations_path, str(connection_index))
          if not os.path.exists(run_configurations_path):
            os.mkdir(run_configurations_path)

          run_configuration_file_path = os.path.join(run_configurations_path, configuration_file_name)

          prepared_configurations[configuration_index].append(configuration_file_name)

          if not os.path.exists(run_configuration_file_path) or (os.path.getmtime(run_configuration_file_path) < input_date):
            f = open(run_configuration_file_path, 'w+')
            f.write(json.dumps(configuration_info))
            f.close()
            ret.append(run_path)
          else:
            Log.write("run unchanged")
        else:
          Log.write("Generating %s runs based on configuration ranges" % (num_ranges))

          run_index = 0
          # Take the configuration, substitute out each range, write it out
          for current_range in ranges:
            for item in current_range:
              def deepUpdate(d, u):
                for k, v in u.items():
                  if isinstance(v, collections.Mapping):
                    r = deepUpdate(d.get(k, {}), v)
                    d[k] = r
                  else:
                    d[k] = u[k]
                return d
              deepUpdate(configuration_info, item["hash"])
            run_path = "%s/runs/%s" % (path, run_index)
            if not os.path.exists(run_path):
              Log.write("Creating run #%s" % (run_index))
              os.mkdir(run_path)
            run_configurations_path = "%s/config" % (run_path)
            if not os.path.exists(run_configurations_path):
              os.mkdir(run_configurations_path)

            run_configurations_path = os.path.join(run_configurations_path, str(connection_index))
            if not os.path.exists(run_configurations_path):
              os.mkdir(run_configurations_path)

            run_configuration_file_path = os.path.join(run_configurations_path, configuration_file_name)

            if not os.path.exists(run_configuration_file_path) or (os.path.getmtime(run_configuration_file_path) < input_date):
              f = open(run_configuration_file_path, 'w+')
              f.write(json.dumps(configuration_info))
              f.close()
              ret.append(run_path)
            else:
              Log.write("Run %s unchanged" % (run_index))

            prepared_configurations[configuration_index].append(configuration_file_name)

            run_index += 1

        total_runs = total_runs * num_ranges
        configuration_index += 1
      connection_index += 1

    # Right now, we have all unique configurations kinda just thrown into
    # run directories. For instance, if we had 2 options for configuration A,
    # 4 for configuration B, and 1 for C, we'd have this:
    # runs/0/config/a0, b0, c0
    # runs/1/config/a1, b1
    # runs/2/config/  , b2
    # runs/3/config/  , b3

    if len(prepared_configurations) == 0:
      # Create at least one run
      run_index = 0
      run_path = os.path.join(path, "runs", str(run_index))
      if not os.path.exists(run_path):
        Log.write("Creating run #%s" % (run_index))
        os.mkdir(run_path)

      run_configurations_path = os.path.join(run_path, "config")
      if not os.path.exists(run_configurations_path):
        os.mkdir(run_configurations_path)

      ret.append(run_path)
    else:
      # So, we need to fill out those runs
      for prepared_configuration in prepared_configurations:
        prepared_connection_index = prepared_configuration[0]
        run_count = len(prepared_configuration)-1
        if run_count > 0:
          for run_index in range(run_count, total_runs):
            # retrieve path to configuration file to copy
            src_run_index = run_index % run_count
            configuration_file_name = prepared_configuration[src_run_index+1]

            # copy this file over to the run configurations path
            # creating runs as necessary
            src_run_path = os.path.join(path, "runs", str(src_run_index))
            src_run_configurations_path = os.path.join(src_run_path, "config", str(prepared_connection_index))

            run_path = os.path.join(path, "runs", str(run_index))
            if not os.path.exists(run_path):
              Log.write("Creating run #%s" % (run_index))
              os.mkdir(run_path)

            run_configurations_path = os.path.join(run_path, "config")
            if not os.path.exists(run_configurations_path):
              os.mkdir(run_configurations_path)

            Log.write("Writing out configurations for workflow node %s" % (str(prepared_connection_index)))
            run_configurations_path = os.path.join(run_configurations_path, str(prepared_connection_index))
            if not os.path.exists(run_configurations_path):
              os.mkdir(run_configurations_path)

            src_configuration_path = os.path.join(src_run_configurations_path, configuration_file_name)
            dst_configuration_path = os.path.join(run_configurations_path, configuration_file_name)
            Log.noisy("Copying from %s to %s" % (src_configuration_path, dst_configuration_path))

            # copy
            input_date = os.path.getmtime(src_configuration_path)
            if not os.path.exists(dst_configuration_path) or (os.path.getmtime(dst_configuration_path) < input_date):
              shutil.copyfile(src_configuration_path, dst_configuration_path)
              ret.append(run_path)
            else:
              Log.write("Run %s unchanged" % (run_index))

    return ret
