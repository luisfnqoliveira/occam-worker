# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Command generator algorithm:
#
# Run commands are determined at the leaves of the VM and go up through the
# parents to the root of the VM.
#
# If the 'run' section has a 'command' and a 'replace' then iterate through the
# 'replace' section and do a substring replacement in the order they are listed
# here.
#
# The 'key' of an entry in the 'replace' list will contain a substring within
# 'command' that will be replaced. The 'value' consequently will contain a key
# string ('input.0.run.command' for instance) that will identify an entry within
# the VM relative to this object. If that entry is not found, the 'default'
# entry within the replace entry will be used instead. If there is no 'default'
# then it will be replaced with an empty string.
#
# {
#   ...
#   "input": [
#     {
#       "run": {
#         "command": "FOO.PCX"
#       }
#     }
#   ],
#   "run": {
#     "command": "BAR.EXE $input$",
#     "replace": [
#       {
#         "key":     "$input$",
#         "value":   "input.0.run.command",
#         "default": "EXAMPLE.PCX"
#       }
#     ]
#   }
# }

import os
import copy
import sys
import json
import shutil

from time import sleep

from lib.log             import Log
from lib.git             import Git
from lib.object          import Object
from lib.experiment      import Experiment

class TaskBuilder:
  """
  This class will handle expanding experiment runs into tasks.
  """

  def __init__(self, args, options, workingPath = ".", occam = None):
    """
    Construct a task builder class.
    """

    self.args        = args
    self.options     = options
    self.workingPath = workingPath
    self.occam       = occam

  def do(self, recursive=False, runPaths=None):
    errors = []
    path = self.workingPath or "."

    Log.header("Building Tasks for Run")
    # We turn 'run' objects into many 'task' objects
    # Task objects are executed in turn to execute a Flow.
    # Tasks can be parallized when they run.

    # Look for object.json?
    experiment_obj = Experiment(path, occam=self.occam)
    obj_info = experiment_obj.objectInfo()

    obj_type = obj_info.get('type') or "run"
    experiment_info = obj_info
    experiment_uuid = obj_info.get('id')

    if len(self.args) > 2:
      obj_type = self.args[1]
      name = self.args[2]
    elif len(self.args) > 1:
      obj_type = None
      name = self.args[1]
    else:
      name = obj_info.get('name')

    # We only operate on 'run' objects
    if not obj_type == "experiment":
      Log.error("cannot find a run object in %s" % (path))
      exit(-1)
      return [], []

    # Pull out connections
    self.job_count = 0
    self.vm_count = 0
    self.paths = []

    # Every VM has objects, initializations, and configurations paths.
    # The "objects" path contains the mounted volumes with object data.
    # The "initializations" path contains the reported information from the
    #   object when it is run. Its 'run.json'
    # The "configurations" path contains, for each object, its input.
    def initVM(info, run_path, vm_path):
      # Create VM object at path
      vm_object = Object.create(vm_path, "generated", "occam/task", occam=self.occam, createPath=False)

      object_path = os.path.join(vm_path, "objects")
      if not os.path.exists(object_path):
        Log.write("Creating object directory for vm")
        os.mkdir(object_path)

      init_path = os.path.join(vm_path, "init")
      if not os.path.exists(init_path):
        Log.write("Creating initializations directory for vm")
        os.mkdir(init_path)

      configuration_path = os.path.join(vm_path, "config")
      if not os.path.exists(configuration_path):
        Log.write("Creating configurations directory for vm")
        os.mkdir(configuration_path)

      return vm_object

    # This will create and place all configurations into their respective
    # places within the VM environment (which is being staged on local disk)
    # This exhibits how one goes through the run information and determines
    # which objects are present. We will also do this later for when we build
    # the VM itself to know what objects to build and mount.
    def setupVM(info, run_path, vm_path):
      object_token = str(info['index'])

      # Create init/configuration paths

      init_path = os.path.join(vm_path, "init", object_token)
      if not os.path.exists(init_path):
        Log.noisy("creating init directory for %s in vm" % (object_token))
        os.mkdir(init_path)
        f = open(os.path.join(init_path, '.marker'), 'w+')
        f.close()

      object_path = os.path.join(vm_path, "objects", object_token)
      if not os.path.exists(object_path):
        Log.noisy("creating working directory for %s in vm" % (object_token))
        os.mkdir(object_path)
        f = open(os.path.join(object_path, '.marker'), 'w+')
        f.close()

      # Copy configurations into the VM staging area
      run_configurations_path = os.path.join(run_path, "config", str(info['index']))
      configuration_path = os.path.join(vm_path, "config", object_token)
      if os.path.exists(run_configurations_path) and not os.path.exists(configuration_path):
        Log.noisy("creating configuration directory for object in vm")
        Log.write("Copying configurations into vm from %s to %s" % (run_configurations_path, configuration_path))
        shutil.copytree(run_configurations_path, configuration_path)
      elif not os.path.exists(configuration_path):
        Log.noisy("no configurations found in %s" % (run_configurations_path))
        Log.noisy("creating configuration directory for object in vm (%s)" % (configuration_path))
        os.mkdir(configuration_path)

      # Make a directory for every object
      for input in info.get('input', []):
        setupVM(input, run_path, vm_path)

      # Make a directory for every output
      for output in info.get('output', []):
        setupVM(output, run_path, vm_path)

    # This function will spawn a job (run object) given the metadata given.
    def spawnJob(info, run_path, corunning = False, dependsOn = None, coDependsOn = None):
      jobs_path = os.path.join(run_path, "jobs")
      if not os.path.exists(jobs_path):
        Log.noisy("creating jobs directory")
        os.mkdir(jobs_path)

      job_path = os.path.join(jobs_path, str(self.job_count))
      if not os.path.exists(job_path):
        Log.noisy("creating job %s directory" % (self.job_count))
        os.mkdir(job_path)
        self.paths.append(job_path)

      vms_path = os.path.join(job_path, "vms")
      if not os.path.exists(vms_path):
        Log.noisy("creating vms directory")
        os.mkdir(vms_path)

      vm_path = os.path.join(vms_path, str(self.vm_count))
      if not os.path.exists(vm_path):
        Log.noisy("creating vm %s directory" % (self.vm_count))
        os.mkdir(vm_path)

      # Initialize the VM path
      vm_object = initVM(info, run_path, vm_path)

      # Plop down configurations for each object used
      setupVM(info, run_path, vm_path)

      job = vm_object.objectInfo()

      # Determine VM build requirements

      job['generator'] = {
        "id": experiment_info['id'],
        "type": "experiment",
        "name": experiment_info['name'],
        "revision": experiment_obj.revision
      }

      if dependsOn:
        job['dependsOn'] = dependsOn

      if coDependsOn:
        job['coDependsOn'] = coDependsOn

      # Establish the workset of the VM
      if 'workset' in experiment_info:
        job['workset'] = experiment_info.get('workset', {})

      taskInfo = self.occam.taskFor(info, info['revision'], indexStart=1000, generator=experiment_obj)
      if taskInfo is None:
        # Attempt to resolve the issue in building a VM
        return False

      job.update(taskInfo)

      # Write out VM object metadata
      Log.write("Writing out vm #%s object.json" % (self.vm_count))
      vm_object.updateObject(job, "Writing out VM")
      vm_object.git.add('config')
      vm_object.git.add('init')
      vm_object.git.add('objects')
      Log.write("Committing configurations/initializations for the vm")
      vm_object.commit(message='Adding configurations.')

      Log.write("VM Object: %s %s committed at revision %s" % (vm_object.objectInfo().get('type', 'unknown'),
                                                               vm_object.objectInfo().get('name', 'unknown'),
                                                               vm_object.revision))

      if corunning:
        self.vm_count += 1
      else:
        self.job_count += 1
        self.vm_count = 0

      return True

    # Parse the current connection metadata from the experiment workflow.
    # This will recursively trigger. Each connection will call this function on
    # its own inputs.
    def parseConnection(connection, connection_index, all_connections):
      ret = {}

      # Get object (if available)
      if not connection["object"].get('created', False):
        objectUUID = connection["object"]["id"]
        obj = self.occam.objects.retrieve(objectUUID, connection["object"]["type"], revision = connection["object"].get('revision'))
        objectInfo = obj.objectInfo()
        ownerInfo = obj.ownerObjectInfo()
      else:
        objectInfo = connection["object"]
        ownerInfo = objectInfo

      if connection["object"].get('local', False):
        # Hmm, we need the local revision for this object
        obj = self.occam.objectAt(path, searchFor=connection["object"]["type"])
        obj.revision = obj.storeLocalChanges()
        connection["object"]["revision"] = obj.revision

      objectInputs = objectInfo.get('inputs', [])

      ret = copy.deepcopy(objectInfo)

      if 'configurations' in ret:
        del ret['configurations']

      ret["index"] = connection_index

      if connection.get('files', False):
        ret["files"] = connection['files']

      for configuration in objectInfo.get('configurations', []):
        if not 'configurations' in ret:
          ret['configurations'] = {}
        ret['configurations'][configuration['name']] = configuration['file']

      if ownerInfo.get('id') != objectInfo.get('id'):
        ret['owner'] = ownerInfo.get('id')

      if connection["object"].get('created', False):
        ret['created'] = True
        ret['id'] = Object.uuid(ret['type'])
        # Create the object
        createdObjectPath = self.occam.objects.createPathFor(ret['id'])
        createdObject = Object.create(createdObjectPath, ret.get('name', "generated"), ret.get('type', 'object'), uuid=ret['id'], occam=self.occam, createPath=False)
        ret['revision'] = createdObject.revision
        self.occam.objects.store(createdObject)

      if 'revision' in connection["object"]:
        ret['revision'] = connection['object']['revision']

      if 'id' in connection["object"]:
        ret['id'] = connection['object']['id']

      # Related objects
      corunnables = []
      inputs = []

      # Gather inputs
      inputConnections = []
      subConnectionIndex = 0
      for subConnection in all_connections:
        if subConnection.get('to', -1) == connection_index:
          inputConnections.append(subConnection)

          # Look up what type of input this is and if it is valid.
          # If we are creating an object as intermediary, determine that.

          subInfo = parseConnection(subConnection, subConnectionIndex, all_connections)

          # Grab input object (if available)
          if not subConnection['object'].get('created', False):
            inputObjectUUID = subConnection["object"]["id"]
            inputObject = self.occam.objects.retrieve(inputObjectUUID, subConnection['object'].get('type'), revision = subConnection["object"].get('revision'))
            inputObjectInfo = inputObject.objectInfo()
            inputOutputs = inputObjectInfo.get('outputs', [])
          else:
            inputOutputs = []
            inputObjectInfo = subConnection["object"]

          # Look up the input description from the original object.
          # We need to find out if this is a shared object.
          inputInfo = None
          for currentInputInfo in objectInputs:
            if 'id' in currentInputInfo:
              if currentInputInfo['id'] == inputObjectInfo.get('id'):
                inputInfo = currentInputInfo
                break
            elif currentInputInfo['type'] == inputObjectInfo['type'] or currentInputInfo['type'] == "occam/runnable":
              inputInfo = currentInputInfo
              break

          if not 'run' in subInfo:
            if 'corunning' in subInfo:
              if not "corunning" in ret:
                ret["corunning"] = []
              ret['corunning'].extend(subInfo['corunning'])
              del subInfo['corunning']

          # Look up the output description from the original object.
          # We need to find out if this is a shared object.
          outputInfo = None
          for currentOutputInfo in inputOutputs:
            if 'id' in currentOutputInfo:
              if currentOutputInfo['id'] == ret.get('id'):
                outputInfo = currentOutputInfo
                break
            elif currentOutputInfo['type'] == ret['type']:
              outputInfo = currentOutputInfo
              break

          # Creating this object
          if connection["object"].get('created', False):
            # OK. Make sure we have this output
            # TODO: eh, make sure this actually works
            subInfo['output'] = []
            outputData = ret.copy()
            # Outputs cannot be in this because it may circularly reference itself within inputs. etc.
            if 'outputs' in outputData:
              del outputData['outputs']
            subInfo['output'].append(outputData)

          if inputInfo and (inputInfo.get('contained', False) or subConnection['object'].get('created', False)):
            # Do not spawn a job for this. This is purely input.

            # Then assign this object is an input.
            if not "input" in ret:
              ret["input"] = []
            subInfo = subInfo.copy()
            subInfo['contained'] = True
            ret["input"].append(subInfo)

            # However, if that input is corunning against something else,
            #          this object must corun with that object also.
            if 'corunning' in subInfo:
              if not "corunning" in ret:
                ret["corunning"] = []

              ret['corunning'].extend(copy.deepcopy(subInfo['corunning']))
          elif inputInfo and inputInfo.get('fifo', False):
            # Spawn a corunning task to create this object
            if not subConnection['object'].get('created', False):
              # Add ourselves as an output?
              subInfo['output'] = []
              subInfo['output'].append(ret)
              del subInfo['output']
              if not "corunning" in ret:
                ret["corunning"] = []
              ret["corunning"].append(subInfo)
            else:
              # We need to add the corunning spaces
              if not "corunning" in ret:
                ret["corunning"] = []
              if not "input" in ret:
                ret["input"] = []

              # We inherit any corunning tasks that have propaged through the
              # created object.
              if 'corunning' in subInfo:
                ret["corunning"].extend(subInfo.get("corunning", []))
                del subInfo['corunning']

              # Then the object that is created is the input to the current object
              ret["input"].append(subInfo)
          elif outputInfo and outputInfo.get('fifo', False):
            # Spawn a corunning task to create this object
            if not subConnection['object'].get('created', False):
              subInfo['output'] = []
              subInfo['output'].append(ret)
              del subInfo['output']
              if not "corunning" in ret:
                ret["corunning"] = []
              ret["corunning"].append(subInfo)

              # We must also corun with their corunning objects
              if 'corunning' in subInfo:
                for corunningObject in subInfo['corunning']:
                  ret['corunning'].append(corunningObject)

                del subInfo['corunning']

              # TODO: They need to know they corun with us
          else:
            # Spawn a task to run the input
            if not "input" in ret:
              ret["input"] = []
            ret["input"].append(subInfo)
            # However, if that input is corunning against something else,
            #          this object must corun with that object also.
            if 'corunning' in subInfo:
              if not "corunning" in ret:
                ret["corunning"] = []
              ret['corunning'].extend(subInfo['corunning'])
              del subInfo['corunning']

        subConnectionIndex += 1

      return ret

    # Make a runs directory
    runs_path = os.path.join(os.path.realpath(path), "runs")
    if not os.path.exists(runs_path):
      Log.noisy("creating runs directory")
      os.mkdir(runs_path)

    for sub_path in os.listdir(runs_path):
      if not sub_path == "." or sub_path == "..":
        run_path = os.path.join(runs_path, sub_path)

        if not runPaths is None and not run_path in runPaths:
          continue

        if os.path.isdir(run_path):
          # Spawn tasks for this run

          workflow = obj_info.get('workflow')
          all_connections = []
          if workflow:
            connections = workflow.get('connections') or []
            # TODO: refactor

            root_connection = None
            for connection in connections:
              all_connections.append(connection)
              if connection.get("to", -1) == -1:
                root_connection = connection

            runInfo = parseConnection(root_connection, 0, all_connections)

            # Spawn jobs!
            #if 'run' in runInfo:
              #spawnJob(runInfo, run_path)
            def prune(a, b):
              for k,v in b.items():
                if isinstance(v, dict):
                  prune(a[k], b[k])
                if isinstance(v, list):
                  for index in range(len(v)):
                    prune(a[k][index], b[k][index])
                if not k in ['input', 'corunning', 'id', 'index', 'created']:
                  del a[k]
            #foo = copy.deepcopy(runInfo)
            #prune(foo, runInfo)

            def spawnInner(info, run=True, corunning=False, dependsOn=None, coDependsOn=None):
              runNext = run
              thisJob = {
                "job_index": self.job_count,
                "vm_index":  self.vm_count
              }

              if 'run' in info:
                if run:
                  runNext = False

              for inputObject in info.get('input', []):
                if 'run' in inputObject:
                  spawnInner(inputObject, runNext, dependsOn=thisJob)
                elif inputObject.get('created', False):
                  for subInputObject in inputObject.get('input', []):
                    if 'run' in subInputObject:
                      spawnInner(subInputObject, True, dependsOn=thisJob)

              for corunningObject in info.get('corunning', []):
                if 'run' in corunningObject:
                  spawnInner(corunningObject, run, True, coDependsOn=thisJob)

              if 'run' in info and run:
                if not spawnJob(info, run_path, corunning, dependsOn=thisJob, coDependsOn=thisJob):
                  return info

              return True

            result = spawnInner(runInfo, True)
            if not result is True:
              Log.error("Could not spawn a job: %s" % result)
              errors.append(runInfo)

          Log.write("Building tasks for run")
          self.job_count = 0

    return self.paths, errors
