# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import shutil
import os
import hashlib

from lib.object import Object
from lib.log    import Log

class File:
  """
  This is an OCCAM resource engine for handling repeatable file retrieval
  and storage.
  """

  def __init__(self, occam):
    self.occam = occam
    self.options = occam.options

  def path(self):
    """
    Returns the storage section these resources will be stored under. If this
    is 'git', for instance, resources will be stored under:
    <OCCAM_HOME>/git/01/23/<0123... uuid>
    """

    return 'store'

  def pathFor(self, uuid, revision=None):
    """
    Returns the path to the stored file on disk (if it would exist) for the
    given file revision. If revision is not specified, it returns the path to
    the file's revision list.

    If you want the exact path to the data, use File.retrieve
    """
    filePath = self.occam.objects.pathFor(uuid, subPath='store')

    if revision:
      filePath = os.path.join(filePath, revision)

    return filePath

  def store(self, uuid):
    pass

  def retrieve(self, uuid, revision):
    """
    Retrieves the path of the stored data at the given uuid at the given
    revision.
    """

    objPath = self.occam.objects.pathFor(uuid)
    obj = self.occam.objects.retrieve(uuid)

    filePath = self.pathFor(uuid, revision)
    filePath = os.path.join(filePath, 'data')

    if self.exists(uuid, revision):
      return filePath

    return None

  def install(self, uuid, revision, path, resourceInfo):
    """
    Installs the file stored as the given uuid and revision to the given path.
    """

    filePath = self.retrieve(uuid, revision)

    if not filePath is None:
      # TODO: error if destination path already exists??
      try:
        Log.noisy("copying file from %s to %s" % (filePath, path))
        shutil.copyfile(filePath, path)
      except:
        Log.error("Failed to install file resource.")
        return False

      actions = resourceInfo.get('actions', {})
      if not isinstance(actions, dict):
        actions = {}

      if resourceInfo and "unpack" in actions:
        Log.write("unpacking file")
        destination = actions['unpack']
        destination = os.path.join(os.path.dirname(path), destination)

        os.system('unzip -o "%s" -d "%s"' % (path, destination))

      return True

    return False

  def pull(self, uuid, revision, name, source, to, path, cert=None):
    """
    Retrieves and stores the given file resource according to the given resource
    info. Returns a reference of the Object in the store.
    """

    name = name or "File Download"

    file = None
    file_obj = None

    if uuid is not None:
      file = self.retrieve(uuid, revision)

    filehash = revision

    if file is None and source is not None:
      # Download file
      Log.write("Downloading resource from %s" % (source))

      reader, content_type, size = self.occam.network.get(source, accept='*/*', cert=cert)

      if reader is None:
        return None, []

      filePath = os.path.join(path, 'data')

      bytesTotal = size

      if not bytesTotal:
        bytesTotal = 1

      bytesSoFar = 0
      percentage = 0

      Log.writePercentage("Downloading resource from source (size: %s)" % (bytesTotal))

      digest = hashlib.sha256()

      with open(filePath, 'wb+') as f:
        bytesRead = 1
        while bytesRead > 0:
          chunk = reader.read(8196)
          bytesRead = len(chunk)
          bytesSoFar += bytesRead
          digest.update(chunk)
          f.write(chunk)
          # TODO: division by zero
          Log.updatePercentage((bytesSoFar / bytesTotal) * 100)
      f.close()

      filehash = digest.hexdigest()

      # The File revision is the hash of the file?
      # Create the object for the file. Store the file and hash.
      # Use the object revision to find the file??

      # File is not stored.

    return filehash, []

  def exists(self, uuid, revision=None):
    """
    Returns True if a file of the given revision is stored in the object store.
    If revision is not specified, it just looks for the presence of any
    revisions.
    """
    filePath = self.pathFor(uuid, revision)

    # If the revision is None, then it doesn't exist
    if revision is None:
      return None

    return os.path.exists(filePath)

  def update(self, uuid, source):
    """
    """

    # File update can be handled by looking at cache info and content-length
    # We should pull a HEAD and check the headers
    # When we find a new version, we should pull it down and give back the
    # revision.

    return False

  def clonable(self):
    """
    Files are not clonable.
    """

    return False

  def clone(self, uuid, revision, name, source, to):
    """
    File are not cloned. This just returns True.
    """

    return True

  def currentRevision(self, path):
    """
    Returns the current revision of the object. This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """

  def commit(self, uuid, revision, name, source, path):
    """
    Will commit the resource at the current path to the store as an updated
    revision if it has changed. It will return the updated resource info.
    """

    return {}, False
