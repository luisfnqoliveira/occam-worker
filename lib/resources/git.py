# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from lib.object import Object
from lib.log    import Log
from lib.git    import Git as GitRepo

# urljoin for resolving relative urls
try: from urllib.parse import urljoin
except ImportError: from urlparse import urljoin

from uuid import uuid4

class Git:
  """
  This is an OCCAM resource engine for handling repeatable Git retrieval
  and storage.
  """

  def __init__(self, occam):
    self.occam = occam
    self.options = occam.options

  def path(self):
    """
    Returns the storage section these resources will be stored under. If this
    is 'git', for instance, resources will be stored under:
    <OCCAM_HOME>/git/01/23/<0123... uuid>
    """

    return 'git'

  def pathFor(self, uuid, revision=None):
    """
    Returns the path to the stored git repository on disk (if it would exist)
    for the given uuid.
    """

    return self.occam.objects.pathFor(uuid, subPath='git')

  def store(self, git, uuid, revision, name, source, to, path):
    """
    Retains a copy of the git repository in occam object pool. Local repositories are on
    disk and can be altered at will by anybody. These are cloned from these
    persisted artifacts.
    """

    # Place it in: .occam/git/2-code/2-code/full-code
    git_path = path #self.occam.objects.createPathFor(uuid, subPath="git")

    # Clone into the git store
    git.submoduleInit()
    git.submoduleUpdate()
    git.clone(git_path)

    # Create object in path to hold the git repository metadata
    obj_path = self.occam.objects.createPathFor(uuid)

    # Look at submodules
    config = git.config()
    gitmodules = git.gitmodules()
    dependencies = []

    # Archive submodules
    for key in config:
      if key.startswith('submodule '):
        tokens = list(filter(None, key.split(' ')))
        if len(tokens) > 1:
          submoduleName = tokens[1]
          submoduleURL = config[key].get('url')

          if key in gitmodules:
            submodulePath = gitmodules[key]['path']

          # Store submodule
          Log.write("Found submodule %s at %s" % (submoduleName, submoduleURL))

          submoduleInfo = {
            'name': '%s (%s)' % (name, submoduleName),
            'source': submoduleURL,
            'type': 'git',
            'revision': git.submoduleRevisionFor(submodulePath),
            'id': str(uuid4()),
            'to': os.path.join(to, submodulePath)
          }

          dependencies.append(submoduleInfo)

    return None, GitRepo(git_path), dependencies

  def retrieve(self, uuid, revision):
    """
    Retrieves the stored git repository at the given uuid at the given
    revision.
    """

    gitPath = self.pathFor(uuid, revision)
    obj = self.occam.objects.retrieve(uuid)

    if self.exists(uuid, revision):
      return gitPath

    return None

  def install(self, uuid, revision, path, resourceInfo):
    """
    Installs the git repository via the given uuid and revision to the given
    path.
    """

    git_path = self.occam.objects.pathFor(uuid, subPath="git")
    git = GitRepo(git_path, revision=revision)
    Log.noisy("Cloning git repository at revision %s" % (revision))
    git.clone(path)
    git.reset(revision)

    installedGit = GitRepo(path)
    installedGit.submoduleInit()
    installedGit.submoduleUpdate()

    # Get the object metadata for this git repository
    obj = self.occam.objects.retrieve(uuid)
    objInfo = obj.objectInfo()

    # Ensure we also link up submodules
    submodules = installedGit.gitmodules()
    config     = installedGit.config()

    source = resourceInfo.get('source', '')
    if not source.endswith("/"):
      # It needs to end with a '/' for urljoin to work properly
      source = source + "/"

    for key in submodules:
      if key.startswith('submodule '):
        tokens = list(filter(None, key.split(' ')))
        if len(tokens) > 1:
          submoduleName = tokens[1]
          submoduleURL = submodules[key].get('url')

          # Reform URL relative to this source
          submoduleURL = urljoin(source, submoduleURL)

          # Get the resource object by searching by URL
          submoduleObject = self.occam.resources.retrieveObjectRecord('git', source=submoduleURL)
          if not submoduleObject is None:
            submoduleID = submoduleObject.uid
            internalURL = self.occam.objects.pathFor(submoduleID, 'git')

            # Store submodule
            Log.noisy("Pointing submodule %s to %s" % (submoduleName, submoduleID))

            config['submodule %s' % (submoduleName)] = {}
            config['submodule %s' % (submoduleName)]['url'] = internalURL

    # Update git config to point to our version of any known submodules
    # May break when submodules are unknown
    installedGit.configUpdate(config)

    return True

  def pull(self, uuid, revision, name, source, to, path, cert=None):
    """
    Retrieves and stores the given git resource according to the given resource
    info. Returns a reference of the Object in the store. The certificate path
    necessary to retrieve the resource over HTTPS can be given.
    """

    name = name or "Git Repository"

    git = None
    gitPath = None
    dependencies = []
    git_obj = None
    if uuid is not None:
      git_path = self.retrieve(uuid, revision)

    if git_path is None and source is not None:
      # Git repository is not stored.

      # Create local repository
      if self.occam.network.isGitAt(source, cert=cert):
        git = GitRepo(source, cert=cert)
        git_obj, git, dependencies = self.store(git, uuid, revision, name, source, to, path)
        revision = revision or git.head()
        #branch_name = "%s-%s" % (objectInfo.get('id'), revision)
        #git.addBranch(branch_name)

    return revision, dependencies

  def exists(self, uuid, revision=None):
    """
    Returns True if a git repository containing the given revision is stored
    in the object store. If revision is not specified, it just looks for the
    presence of the git repository.
    """

    gitPath = self.pathFor(uuid, revision)
    gitPath = os.path.join(gitPath, '.git')
    return os.path.exists(gitPath)

  def update(self, uuid, source):
    """
    Updates the git repository object stored in the git store.
    """

    # Git updates just pull down any new commits into a random branch name.
    # These commits will be preserved by the branching semantics of git.
    # We can then return a new HEAD, if there is one.

    info = obj.objectInfo()
    object_type = info['type']

    if info.get('storable') == False:
      return

    uuid = info['id']
    path = self.git.objects.pathFor(uuid, 'git')

    # Get the repository's actual file path in the git store
    repo_path = os.path.join(path, info['file'])

    # Interface to Git for this repository in the git store
    store_git = GitRepo(repo_path)

    Log.noisy("fetching from %s to %s" % (git.path, repo_path))

    # Create random remote and branch names
    remote_name = str(uuid4())
    new_branch_name = str(uuid4())

    # Add the remote to the local git repository
    Log.noisy("adding remote %s -> %s" % (remote_name, git.path))
    store_git.addRemote(remote_name, git.path)

    # Fetch local branches
    store_git.fetchRemoteBranches(remote_name)

    # Checkout that branch from the remote
    Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, git.branch()))
    store_git.checkoutBranch(new_branch_name, git.branch(), remote_name)

    # Remove generated remote name
    store_git.rmRemote(remote_name)

    # Done

  def clonable(self):
    """
    Git repositories are indeed clonable.
    """

    return True

  def clone(self, uuid, revision, name, source):
    """
    Clones the repository and returns a new resource tag.
    """

    Log.write("Forking git repository %s (%s)" % (name, uuid))

    # Git updates just pull down any new commits into a random branch name.
    # These commits will be preserved by the branching semantics of git.
    # We can then return a new HEAD, if there is one.

    path = self.occam.objects.pathFor(uuid, 'git')

    # Interface to Git for this repository in the git store
    store_git = GitRepo(path)

    # Create a new identifier
    newUUID = Object.uuid('')

    # Create git object
    self.store(store_git, newUUID, revision, name, source, to)

    # Done
    return {
      'id':       newUUID,
      'revision': revision,
      'name':     name,
      'source':   source
    }

  def currentRevision(self, path):
    """
    Returns the current revision of the object. This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """
    git = GitRepo(path)
    if git is None:
      return None

    return git.head()

  def commit(self, uuid, revision, name, source, path):
    """
    Will commit the resource at the current path to the store as an updated
    revision if it has changed. It will return the updated resource info.
    """

    dirty = False

    git = GitRepo(path)

    newRevision = self.currentRevision(path)

    # If there is a problem, just say it didn't change
    if newRevision is None or git is None:
      newRevision = revision

    # If it changed, push new content up to the store
    if newRevision != revision:
      Log.write("Pushing updated git content")
      dirty = True

      # Git updates just pull down any new commits into a random branch name.
      # These commits will be preserved by the branching semantics of git.

      store_path = self.occam.objects.pathFor(uuid, 'git')

      # Interface to Git for this repository in the git store
      store_git = GitRepo(store_path)

      Log.noisy("fetching from %s to %s" % (git.path, store_path))

      # Create random remote and branch names
      remote_name = str(uuid4())
      new_branch_name = str(uuid4())

      # Add the remote to the local git repository
      Log.noisy("adding remote %s -> %s" % (remote_name, git.path))
      store_git.addRemote(remote_name, git.path)

      # Fetch local branches
      store_git.fetchRemoteBranches(remote_name)

      # Checkout that branch from the remote
      Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, git.branch()))
      store_git.checkoutBranch(new_branch_name, git.branch(), remote_name)

    return {
      'id':       uuid,
      'revision': newRevision,
      'name':     name,
      'source':   source
    }, dirty
