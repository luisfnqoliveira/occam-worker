# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import shutil
import codecs
import json
import os
import uuid as UUID

from lib.object import Object
from lib.log    import Log

class Docker:
  """
  This is an OCCAM resource engine for handling repeatable file retrieval
  and storage.
  """

  def __init__(self, occam):
    self.occam = occam
    self.options = occam.options

  def path(self):
    """
    Returns the storage section these resources will be stored under. If this
    is 'git', for instance, resources will be stored under:
    <OCCAM_HOME>/git/01/23/<0123... uuid>
    """

    return 'docker'

  def pathFor(self, uuid, revision=None):
    """
    Returns the path to the stored metadata on disk (if it would exist) for the
    given container revision. If revision is not specified, it returns the path
    to the file's revision list.
    """
    filePath = self.occam.objects.pathFor(uuid, subPath='docker')
    if revision:
      filePath = os.path.join(filePath, revision)
      filePath = os.path.join(filePath, 'metadata.json')
    return filePath

  def store(self, uuid):
    pass

  def retrieve(self, uuid, revision):
    """
    Retrieves the stored data at the given uuid at the given revision.
    """

    objPath = self.occam.objects.pathFor(uuid)
    obj = self.occam.objects.retrieve(uuid)

    filePath = self.pathFor(uuid, revision)

    if self.exists(uuid, revision):
      return filePath

    return None

  def install(self, uuid, revision, path, resourceInfo):
    """
    Installs the file stored as the given uuid and revision to the given path.
    """

    # Just check if it exists
    return self.exists(uuid, revision)

  def pull(self, uuid, revision, name, source, to, path, cert=None):
    """
    Retrieves and stores the given file resource according to the given resource
    info. Returns a reference of the Object in the store.
    """

    name = name or "Docker Container"

    metadataPath = None

    if uuid is not None:
      metadataPath = self.retrieve(uuid, revision)

    if metadataPath is None and source is not None:
      # We do not have the resource asked for
      # Do we get it from a node, or from a external source?

      dockerHandler = self.occam.backends.handlerFor("docker")

      if self.occam.network.isURL(source):
        # Pull from node using the backend puller
        dockerHandler.pull(source, uuid, revision=revision, cert=cert)
      else:
        # Pull Docker container from source (use backend puller)
        Log.noisy("pulling docker container at %s" % (source))
        dockerHandler.pullExternal(source)

      # Create an object

      filePath = self.occam.objects.createPathFor(uuid, subPath='docker')
      objPath = self.occam.objects.createPathFor(uuid)

      revision = str(UUID.uuid4())

      # We need to tag this docker container to a canonical name (if not already??)
      # Creates: occam/{uuid}:{revision}
      self.occam.backends.handlerFor("docker").tag(uuid, source, revision=revision)

      filePath = path

      Log.write("creating docker metadata resource in %s" % (filePath))

      # Create metadata file
      filePath = os.path.join(filePath, 'metadata.json')
      with open(filePath, 'w+') as f:
        metadata = {
        }
        f.write(json.dumps(metadata))

    return revision, []

  def exists(self, uuid, revision=None):
    """
    Returns True if a file of the given revision is stored in the object store.
    If revision is not specified, it just looks for the presence of any
    revisions.
    """
    filePath = self.pathFor(uuid, revision)
    return os.path.exists(filePath)

  def update(self, uuid, source):
    """
    """

    # File update can be handled by looking at cache info and content-length
    # We should pull a HEAD and check the headers
    # When we find a new version, we should pull it down and give back the
    # revision.

    return False

  def clonable(self):
    """
    Docker containers are not clonable.
    """

    return False

  def clone(self, uuid, revision, name, source):
    """
    Docker containers are not cloned. This just returns True.
    """

    return True

  def currentRevision(self, path):
    """
    Returns the current revision of the object. This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """

  def commit(self, uuid, revision, name, source, path):
    """
    Will commit the resource at the current path to the store as an updated
    revision if it has changed. It will return the updated resource info.
    """

    return {}, False
