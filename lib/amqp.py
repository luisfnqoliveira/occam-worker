# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import rabbitpy
import traceback
import sys
import shlex

from lib.log import Log
from lib.cli import CLI

class AMQP:
  def __init__(self, options, occam):
    self.options = options
    self.occam = occam

  def run(self):
    Log.write("Running AMQP Message Server")
    Log.store_output = True
    queue_name = 'occam-command'
    with rabbitpy.Connection('amqp://occam:occam@127.0.0.1:5672/%2F') as conn:
      with conn.channel() as channel:
        Log.write("looking for messages on %s" % (queue_name))
        queue = rabbitpy.Queue(channel, queue_name)
        queue.declare()

        # Exit on CTRL-C
        while True:
          try:
            # Consume the message
            for message in queue:
              Log.write("Message received on queue %s" % (queue_name))
              command = message.body.decode('utf-8')
              response_queue = message.properties['reply_to']
              try:
                Log.write("Running %s" % command)
                if not CLI.execute(shlex.split(command)) == 0:
                  Log.error("failure")
              except:
                e = sys.exc_info()[0]
                print('Some other error')
                print(traceback.format_exc())
              response = Log.storedOutput()
              queue = rabbitpy.Queue(channel, response_queue)
              queue.declare()

              reply = rabbitpy.Message(channel, response)
              reply.publish('', response_queue)
              message.ack()
          except(KeyboardInterrupt):
            print('Exited consumer')
            break
          except:
            e = sys.exc_info()[0]
            print('Some other error')
            print(traceback.format_exc())
