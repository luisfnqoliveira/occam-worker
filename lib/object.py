# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log  import Log
from lib.git  import Git
from lib.file import File
from lib.svn  import SVN
from lib.db   import DB

import re
import os
import json

from uuid import uuid1
from uuid import uuid4

import subprocess

class Object():
  """
  This class represents a basic OCCAM object.
  """

  @staticmethod
  def create(path, name, object_type, uuid=None, createPath=True, occam=None, belongsTo=None, root=None, build=True):
    # Form basic object
    if uuid is None:
      uuid = Object.uuid(object_type)

    object_json = {
      "name": name,
      "type": object_type,
      "id":   uuid,
    }

    # Add workset field. The 'workset' is the root object for knowing
    # the access privilege of the object.
    if root and root.objectInfo().get('type', 'workset'):
      object_json['workset'] = {
        "type": root.objectInfo()['type'],
        "id":   root.objectInfo()['id']
      }

    # Add belongsTo field. This determines the object that contains this one.
    if belongsTo:
      object_json['belongsTo'] = {
        "type": belongsTo.objectInfo()['type'],
        "id":   belongsTo.objectInfo()['id']
      }

    # Create directory, if it doesn't exist, warn if it does
    if createPath:
      if os.path.exists(path):
        Log.warning("directory %s already exists" % (path))
      else:
        Log.noisy("creating directory %s" % (path))
        os.mkdir(path)
    elif not os.path.exists(path):
      raise Exception("path does not exist")

    # Create group object if it doesn't exist
    group_object_path = os.path.join(path, "object.json")

    f = open(group_object_path, "w")
    f.write(json.dumps(object_json, indent=2, separators=(',', ': ')))
    f.close()

    # Create .gitignore
    f = open("%s/.gitignore" % (path), 'w+')
    f.close()

    # Create git repository in group path
    git = Git.create(path)

    # Initial commit to get a revision
    git.add('*')
    git.commit("created %s %s" % (object_type, name))

    # Pull revision
    revision = git.head()
    Log.noisy("committed to repository as %s" % (revision))

    return Object(path, occam=occam, root=root)

  @staticmethod
  def uuid(object_type):
    """
    Creates a uuid for this object.
    """

    return str(uuid1())

  @staticmethod
  def slugFor(object_type):
    return object_type.replace('/', '-').lower()

  @staticmethod
  def slugUUID(object_type, uuid):
    return "%s-%s" % (Object.slugFor(object_type), uuid)

  def __init__(self, path, revision="HEAD", occam=None, root=None, uuid=None):
    """
    Creates an instance of an object wrapping an existing object at the
    given path.
    """

    self.revision  = revision or "HEAD"
    self.path      = path
    self.root      = root
    self.occam     = occam
    self.info      = None
    self.infoRevision = None
    self.ownerInfo = None
    self.uuid      = uuid

    if re.match(r'^[a-z]+://', path):
      byURL = True
    else:
      byURL = False

    if not byURL:
      self.path = os.path.realpath(self.path)

    if not byURL and not os.path.exists(self.path):
      raise Exception("path not found: %s" % (self.path))

    if byURL:
      # Negotiate for a git repo or json document
      data = self.occam.downloadText(self.path, 'application/json')

      if isinstance(data, dict):
        # Is an object json document
        self.git = None
        self.info = data
      else:
        self.git = Git(self.path, self.revision)
    elif Git.exists(self.path):
      self.git = Git(self.path, self.revision)
    else:
      self.git = None

    if self.git is not None and self.revision == "HEAD":
      self.revision = self.head()

  def head(self):
    """
    Returns the revision of HEAD for this Object.
    """
    return self.git.head()

  def parentRevision(self):
    return self.git.parent()

  def ownerObjectInfo(self):
    """
    Pull object info from the owner. If the object is unowned, then
    it will pull its own object info.
    """

    if self.git:
      self.ownerInfo = self.ownerInfo or self.git.objectInfo()
    else:
      self.ownerInfo = self.objectInfo()
    return self.ownerInfo.copy()

  def objectInfo(self):
    """
    Pull object info from revision
    """

    if self.revision == self.infoRevision and self.info is not None:
      return self.info

    self.infoRevision = self.revision

    if self.git:
      self.info = self.info or self.git.objectInfo()
      if self.uuid and self.info.get('id') != self.uuid:
        info = self.info
        self.info = {}
        for derivative in info.get('includes', []):
          if derivative.get('id') == self.uuid:
            new_info = info.copy()

            if 'includes' in new_info:
              del new_info['includes']
            if 'subtype' in new_info:
              del new_info['subtype']
            if 'inputs' in new_info:
              del new_info['inputs']
            if 'outputs' in new_info:
              del new_info['outputs']
            if 'install' in new_info:
              del new_info['install']
            if 'build' in new_info:
              del new_info['build']
            if 'run' in new_info:
              del new_info['run']
            if 'configurations' in new_info:
              del new_info['configurations']
            if 'include' in new_info:
              del new_info['include']

            new_info.update(derivative)
            self.info = new_info
            break

      return self.info.copy()
    else:
      # Pull from local
      if not self.info is None:
        return self.info.copy()

      objectInfoPath = os.path.join(self.path, 'object.json')
      if os.path.exists(objectInfoPath):
        f = open(objectInfoPath, 'r')
        self.info = json.load(f)
        f.close()

        if self.uuid and self.info['id'] != uuid:
          info = self.info
          self.info = {}
          for derivative in info.get('includes', []):
            if derivative.get('id') == self.uuid:
              new_info = info.copy()

              del new_info['includes']
              del new_info['inputs']
              del new_info['outputs']
              del new_info['install']
              del new_info['build']
              del new_info['run']
              del new_info['configurations']
              del new_info['include']

              new_info.update(derivative)
              self.info = new_info
              break

        return self.info.copy()
      else:
        {}

  def placeWithin(self, workset, obj=None):
    info = workset.objectInfo()
    workset_id = info.get('id')

    # Create branch for this placement
    branch_name = "within-workset-%s" % (workset_id)

    if obj:
      obj_info = obj.objectInfo()
      branch_name = "%s-%s" % (branch_name, obj_info['id'])

    Log.noisy("creating branch %s" % (branch_name))

    self.git.addBranch(branch_name)
    self.git.switchBranch(branch_name)

  def sync(self, revision):
    # TODO: Get rid of old objects??
    info = self.objectInfo()
    uuid = info['id']

    # Reset this object
    self.revision = revision
    self.info = None

    objectPath = self.occam.objects.pathFor(uuid)
    Log.write('syncing object path = %s @%s' % (objectPath, revision))

    # Change local view to the view of the object store
    self.git.addRemote('store', objectPath)
    self.git.fetch('store')
    self.git.reset(revision)

    # Go through and pull down dependencies
    info = self.objectInfo()
    Log.write(str(info))

    for dependency in (info.get('dependencies') or []):
      # Figure out the directory name
      dependency_uuid = dependency.get('id')
      directory_name = "%s-%s" % (Object.slugFor(dependency['type']), Object.slugFor(dependency['name']))

      path = os.path.join(self.path, directory_name)
      current_path = path
      obj = None

      incremental = 1
      while os.path.exists(current_path):
        # If it exists, determine if it is already this dependency
        obj = Object(current_path, occam=self.occam)
        sub_info = obj.objectInfo()
        if sub_info and sub_info['id'] == dependency_uuid:
          break

        # If it isn't, increment the directory name, repeat
        obj = None
        current_path = "%s-%s" % (path, str(incremental))
        incremental += 1

      if not os.path.exists(current_path):
        # If not found, create that directory and pull the object
        git = Git(self.occam.objects.pathFor(dependency_uuid))
        git.clone(to=current_path)

      # current_path is now a path to *this* dependency
      # Recurse the sync to the dependency
      obj = Object(current_path, occam=self.occam, root=self.root)
      Log.write('recursing to object %s in %s' % (obj.objectInfo()['id'], current_path))
      obj.sync(dependency['revision'])

  def experiments(self):
    return []

  def groups(self):
    return []

  def objects(self):
    return []

  def parent(self, revision=None):
    objectInfo = self.objectInfo()
    if 'belongsTo' in objectInfo:
      belongsToUUID = objectInfo['belongsTo']['id']
      parentObject = self.occam.objectAt(os.path.join(self.path, '..'), revision=revision)
      if parentObject and parentObject.objectInfo()['id'] == belongsToUUID:
        return parentObject
      else:
        db_obj = self.occam.objects.search(uuid=belongsToUUID).first()
        return self.occam.objects.retrieve(db_obj.uid, db_obj.object_type, revision=revision)
    else:
      return self.occam.objectAt(os.path.join(self.path, '..'), recurse=False)

  def databaseRecord(self):
    return self.occam.objects.search(uuid=self.objectInfo().get('id')).first()

  def lockedRecord(self):
    return self.occam.objects.search(uuid=self.objectInfo().get('id')).with_lockmode("update").first()

  def workset(self):
    """
    This function will return the Object for the workset containing the current
    object.
    """

    parent = self.parent()

    while parent and not parent.objectInfo()['type'] == 'workset':
      parent = parent.parent()

    # If our parent search doesn't work out, then just look at the workset
    # tag if it exists. We do this after a parent search so we can find the
    # workset on disk as a parent directory. This method below will always
    # pull the workset from the object store.
    if parent is None:
      if 'workset' in self.objectInfo():
        worksetUUID = self.objectInfo()['workset'].get('id')
        worksetRevision = self.objectInfo()['workset'].get('revision')
        db_obj = self.occam.objects.search(uuid=worksetUUID).first()
        return self.occam.objects.retrieve(db_obj.uid, db_obj.object_type, revision=worksetRevision)

    return parent

  def clone(self, path, revision=None, increment=False, create=False, cloneResources=True):
    """
    Explicitly clones a copy of this object to the given path. If create
    is True, we assign a new UUID to this object and mark it as cloned.
    Otherwise, we just pull down a copy of the object. Changes will be
    merged into the object itself.

    cloneResources: when True, this will clone source repositories as well.
    """

    path = os.path.realpath(path)

    if revision is None:
      revision = self.revision

    if increment and os.path.exists(path):
      path = path + '-1'
      index = 1
      while os.path.exists(path):
        index += 1
        path = path[0:path.rindex('-')] + '-' + str(index)

    if not os.path.exists(os.path.join(path, '.git')):
      Log.noisy("cloning object to %s" % (path))
      self.git.clone(path)
      git = Git(path)
      if not revision is None:
        Log.noisy("cloning object at revision %s" % (revision))
        git.reset(revision)
      # Create branch to retain any new revisions
      if not create:
        branch_name = str(uuid4())
        Log.noisy("creating branch %s" % (branch_name))
        git.addBranch(branch_name)
        git.switchBranch(branch_name)

      obj = Object(path, revision=revision, occam=self.occam)
      if create:
        info = obj.objectInfo()
        old_id = info['id']
        info['id'] = Object.uuid(info['type'])
        info['clonedFrom'] = {
          "type": self.objectInfo()['type'],
          "name": self.objectInfo()['name'],
          "id":   old_id,
          "revision": revision
        }

        if cloneResources:
          resourceInfo = self.occam.resources.cloneAll(obj)
          info['install'] = resourceInfo

        obj.updateObject(info, 'cloned from %s; new id %s' % (old_id, info['id']))
      return obj
    else:
      return False

  def purgeGenerates(self):
    info = self.objectInfo()

    info['generates'] = []
    self.updateObject(info, 'Clearing any generated objects.')

    return True

  def updateObject(self, info, message):
    """
    This method will replace the object metadata with the given metadata. It
    will commit that change in the log with the given message. It will update
    the database normalization as well.
    """

    # Write out the new object metadata
    f = open('%s/object.json' % (self.path), 'w+')
    f.write(json.dumps(info, indent=2, separators=(',', ': ')))
    f.close()

    # Update our view of ourselves
    self.info = info

    new_revisions = self.commit(message, 'object.json')

    # Update normalized view
    self.revision = new_revisions[0]
    self.occam.objects.updateNormalization(self);

    return new_revisions

  def commit(self, message="OCCAM automated commit", filepath="-a"):
    """
    Commits the current state of the Object on disk with the given commit
    message.
    """

    info = self.objectInfo()
    revision = self.git.commit(message, filepath)

    ret = [revision]

    # Update the folks
    refs = self.updateParents()
    for ref in refs:
      ret.append(ref)

    # Update cache
    self.updateStore()

    return ret

  def updateParents(self):
    info = self.objectInfo()

    ret = []

    # Update parent
    parent = self.parent()
    if parent:
      parent_revision = parent.updateDependency(info['id'], info['type'], info['name'], self.head(), category="contains")
      for ref in parent_revision:
        ret.append(ref)

    return ret

  def updateStore(self):
    """
    Updates OCCAM stored object.
    """

    self.occam.objects.update(self)

  def install(self):
    object_info = self.objectInfo()

    resources = self.occam.resources.installAll(self, self.path)

    save_object_info = False

    repositories = object_info.get('install', [])
    for resourceInfo, info in zip(resources, repositories):
      if resourceInfo is None:
        continue

      uuid = resourceInfo.get('id')
      revision = resourceInfo.get('revision')

      # Apply id/revision
      if info.get('id') != uuid or info.get('revision') != revision:
        info['id'] = uuid
        info['revision'] = revision
        save_object_info = True

    # New resources
    if len(resources) > len(repositories):
      Log.write("Discovered new resources")
      for resourceInfo in resources[len(repositories):]:
        Log.write("Found new resource: %s" % (resourceInfo.get('name')))
        repositories.append(resourceInfo)
        save_object_info = True

    if save_object_info:
      message = "Updated object's install repositories id/revisions."
      object_info['install'] = repositories
      self.updateObject(object_info, message)

  def build(self):
    # Just delegate building to the docker backend
    self.occam.backends.handlerFor("docker").build(self, self.path)
    #Docker.build(self.objectInfo()["type"], self.objectInfo()["name"], self.revision, self.path)

  def update(self):
    # Just delegate updating/building to the docker backend
    #if not Docker.update(self.objectInfo()["type"], self.objectInfo()["name"], self.revision, self.path):
    self.build()

  def fullRevision(self, revision):
    return Git.fullRevision(self.git.path, revision)

  def dependency(self, uuid, category='dependencies'):
    info = self.objectInfo()
    info[category] = info.get(category, [])

    for dependency in info[category]:
      if dependency['id'] == uuid:
        return dependency

    return None

  def updateDependency(self, uuid, object_type=None, name=None, revision=None, path=None, category='dependencies'):
    info = self.objectInfo()
    info[category] = info.get(category, [])

    ret = None
    for dependency in info[category]:
      if dependency['id'] == uuid:
        if name:
          dependency['name'] = name
        if revision:
          dependency['revision'] = revision
        if object_type:
          dependency['type'] = object_type
        if path:
          dependency['local'] = True
          dependency['path'] = path
        else:
          if 'local' in dependency:
            del dependency['local']

        ret = dependency

    return self.updateObject(info, 'updates dependency %s %s' % (object_type, name))

  def addDependency(self, uuid, object_type, name, revision, path=None, category='dependencies'):
    info = self.objectInfo()
    info[category] = info.get(category, [])

    ret = {
      'name':     name,
      'type':     object_type,
      'id':       uuid,
      'revision': revision
    }

    if not path is None:
      ret['local'] = True
      ret['path'] = path

    if not self.dependency(uuid):
      info[category].append(ret)

    return self.updateObject(info, 'adds %s %s as a dependency' % (object_type, name))

  def storeLocalChanges(self):
    # We are doing a temporary store of this object and its repositories
    # This happens when we are doing an updated build of a local object
    # To do this, we must commit all changes to each repository this object
    # is using. Reset those changes (to look like what it used to be) And then
    # commit the revision changes to the object itself, and storing that.
    # We will store the updated history to a new random branch in the git
    # repository managing those changes.

    info = self.objectInfo()
    info['install'] = info.get('install') or []
    repos = info['install']
    for repo in repos:
      # Pull out the name of the directory the repo is installed to
      directory = repo.get('to') or 'package'

      # This is the local path to the repository
      repo_path = os.path.join(self.path, directory)

      # This is an interface to the Git tool for this local repository
      repo_git = Git(repo_path)

      # This is the object in the object store that maintains this repository
      repo_obj = self.occam.objects.retrieve(repo['id'], "occam-git")

      if repo_obj is None:
        # The git repo is not stored on our system
        # Create the repo object and pull from the global url
        # Or discover the repo object at other sites
        repo_url = repo["git"]
        git = Git(repo_url)
        git_obj, git = self.occam.storeGit(git, repo.get('id'), repo.get('name'), repo.get('source'))
        revision = repo.get('revision') or git.head()
        branch_name = "%s-%s" % (info.get('id'), revision)
        git.addBranch(branch_name)
        if not "id" in repo:
          repo['id'] = git_obj.objectInfo()['id']
          message = message + "Creates git repository object %s\n" % (repo['id'])
          save_object_info = True
          # TODO: save!
        repo_obj = git_obj

      # The current revision if the repository
      repo_revision = repo_git.head()

      # We will commit any and all local changes to indexed files
      repo_git.commit("Committing partial changes for an updated local build", '-a')

      repo['revision'] = repo_git.head()
      Log.write("storing local change to repo in %s (%s -> %s)" % (repo.get('to') or "package", repo_revision, repo['revision']))

      # We will now store these changes in the global repository in our git store
      self.occam.updateGit(repo_obj, repo_git)

      # We will reset the local view to the point it was when we started
      repo_git.reset(repo_revision, hard=False)

    # Stash changes (only necessary if object.json has changed?)
    # TODO: what would we do if somebody 'git added' object.json at this point??
    self.git.stash()
    revision = self.head()
    self.updateObject(info, 'Committing git revision updates for updated local build')
    self.git.reset(revision, hard=True)
    revision = self.head()
    self.git.stashPop()

    return revision

  def relationList(self):
    """
    Returns a list of uuids that relate to this object.
    """

    ret = []

    object_info = self.objectInfo()
    parent = object_info.get('belongsTo', {})

    if isinstance(parent, dict):
      parent = parent.get('id')

    if parent is not None:
      ret.append(parent)

    # TODO: gather parents of parents??
    #       this might be something that object resolvers need to solve

    # Dependencies
    dependencies = object_info.get('dependencies', [])
    for dependency in dependencies:
      if isinstance(dependency, dict):
        dependency = dependency.get('id')

      if dependency is not None and self.occam.isUUID(dependency):
        ret.append(dependency)

    # Authors
    people = object_info.get('authors', [])
    for person in people:
      if isinstance(person, dict):
        person = person.get('id')

      if person is not None and self.occam.isUUID(person):
        ret.append(person)

    # Collaborators
    people = object_info.get('collaborators', [])
    for person in people:
      if isinstance(person, dict):
        person = person.get('id')

      if person is not None and self.occam.isUUID(person):
        ret.append(person)

    # Generated Objects
    generates = object_info.get('generates', [])
    for generated in generates:
      if isinstance(generated, dict):
        generated = generated.get('id')

      if generated is not None and self.occam.isUUID(generated):
        ret.append(generated)

    # Install section (repos)
    installs = object_info.get('install', [])
    for install_info in installs:
      if isinstance(install_info, dict):
        repo_id = install_info.get('id')
        if repo_id is not None and self.occam.isUUID(repo_id):
          ret.append(repo_id)

    # Workflow objects
    workflow = object_info.get('workflow', {})
    connections = workflow.get('connections', [])
    for connection in connections:
      obj = connection.get('object', {})
      id = obj.get('id')
      if id is not None and self.occam.isUUID(id):
        ret.append(id)

    # Build section (using, base)
    # TODO

    return ret

  def runCommand(self):
    """
    Returns the command that this object requests to be executed.
    """

    info = self.objectInfo()
    if "run" in info:
      if "index" in info["run"] and "type" in info["run"]:
        run_type = info["run"]["type"]
        index = info["run"]["index"]

      if "script" in info["run"]:
        language = info["run"]["language"]
        filename = info["run"]["script"]
        if "version" in info["build"]:
          script_version = info["run"]["version"]
        else:
          script_version = None

        if "name" in info["run"]:
          run_name = info["run"]["name"]

          if "type" in info["run"]:
            run_type = info["run"]["type"]

        run_obj_id = Object.slugUUID(info['run'].get('type', info.get('type')), info['run'].get('id', info.get('id')))
        run_obj_id_actual = run_obj_id
        if 'owner' in info['run']:
          run_obj_id = info['run']['owner']

        run_root = "/occam/%s" % (run_obj_id)
        run_root = run_root.replace(' ', '-')

        run_obj_type = info["run"].get('type') or obj_type
        run_obj_name = info["run"].get('name') or obj_name
        run_obj_revision = info["run"].get('revision') or obj_revision
        run_obj_index = info["run"].get('index') or info.get('index')

        if run_obj_index is None:
          run_environment = "OCCAM_OBJECT_TYPE=%s" % (run_obj_type)
        else:
          run_environment = "OCCAM_OBJECT_TYPE=%s OCCAM_OBJECT_INDEX=%s" % (run_obj_type, run_obj_index)

        if language == "python":
          if script_version is None:
            script_version = "2.7.6"

          run_cmd = "pyenv local %s; %s python %s/%s" % (script_version, run_environment, run_root, filename)
        elif language == "ruby":
          if script_version is None:
            script_version = '2.0.0'
          run_cmd = "rbenv local %s; %s ruby %s/%s" % (script_version, run_environment, run_root, filename)
        elif language == "perl":
          if script_version is None:
            script_version = '5.18.0'
          run_cmd = "plenv local %s; %s perl %s/%s" % (script_version, run_environment, run_root, filename)
        elif language == "bash":
          run_cmd = "%s bash %s/%s" % (run_environment, run_root, filename)
        elif language == "sh":
          run_cmd = "%s sh %s/%s" % (run_environment, run_root, filename)

