# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import os
import json

from lib.db     import DB
from lib.log    import Log
from lib.git    import Git
from lib.object import Object

# For pulling network http resources
try:
  from urllib.parse import urlparse
except:
  from urllib2 import urlparse

class NodeManager:
  """
  This OCCAM manager handles the Node listing, Node discovery, and Node lookup.
  """

  CERTIFICATE_FILENAME = 'ssl.crt'

  def __init__(self, occam):
    """
    Initialize the node manager.
    """

    self.occam   = occam
    self.options = occam.options

  def partialFromURL(self, url):
    """
    This method will take a fully realized URL to a resource on an OCCAM node
    and return the canonical name (domain,port) for that node. Port will be
    omitted when it is a normal port (80, 443)
    """
    if url is None:
      url = ""

    # Make sure it *is* a full URL
    url = self.urlFromPartial(url)

    # urlparse works great on real URLs
    urlparts = urlparse(url)

    # we want the host:port (unless port is 80 or 443)
    url = urlparts.hostname

    if urlparts.port and urlparts.port != 80 and urlparts.port != 443:
      url = url + "," + str(urlparts.port)

    return url

  def urlFromPartial(self, url, scheme="https"):
    """
    This method will return a full url from any partial url given as a Node uri.

    You can give the method a 'scheme' which will be applied to the URL if it
    does not have a scheme already.
    """
    if url is None:
      url = ""

    urlparts = urlparse(url)
    if urlparts.scheme == "":
      url = "%s://%s" % (scheme, url)
      urlparts = urlparse(url)

    if urlparts.netloc == "":
      return None

    return url

  def hostAndPortFromURL(self, url):
    """
    Returns a pair representing the host and pair respectively.
    """
    urlparts = urlparse(url)
    host = urlparts.netloc.split(':')[0]

    port = "80"
    host = urlparts.netloc.split(':')[0]
    if ':' in urlparts.netloc:
      port = urlparts.netloc.split(':')[1]
    elif scheme == "https":
      port = "443"

    return [host, port]

  def search(self, url):
    """
    Returns the database entry for the given node url.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    session = self.occam.databaseSession()
    node = session.query(DB.Node).filter(DB.Node.host==host).first()

    return node

  def retrieveInfo(self, url):
    """
    Polls and retrieves the node info from the Node.
    """

    url = self.urlFromPartial(url)

    # Query the url and retrieve node information
    node_info = self.occam.downloadText(url, scheme='https', suppressError=True)
    scheme = "https"

    # Negotiate for HTTPS
    # TODO: Flag on the system that never allows non-HTTPS
    if node_info is None:
      scheme = "http"
      node_info = self.occam.downloadText(url, scheme='http', suppressError=True)
      if node_info is None:
        return None

    if not isinstance(node_info, dict):
      # We can't query node information, but that's OK
      node_info = {}

    node_info['httpSSL'] = scheme == "https"

    # Get certificate
    self.retrieveCertificate(url)

    return node_info

  def pathFor(self, url):
    """
    Retrieves the system path for the given node.
    """
    nodePath = self.occam.configuration()['paths'].get('nodes')
    nodePath = os.path.join(nodePath, self.partialFromURL(url))

    return nodePath

  def createPathFor(self, url):
    nodePath = self.occam.configuration()['paths'].get('nodes')
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    nodePath = os.path.join(nodePath, self.partialFromURL(url))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    return nodePath

  def retrieveCertificate(self, url):
    """
    Retrieves the public key from the given node.
    """

    url = self.urlFromPartial(url)
    url = url + "/public_key"

    # Download that key
    response, content_type, size = self.occam.network.get(url, doNotVerify=True)
    reader = codecs.getreader('utf-8')

    certPath = os.path.join(self.createPathFor(url), self.CERTIFICATE_FILENAME)
    with open(certPath, 'w+') as f:
      f.write(reader(response).read())

  def certificatePathFor(self, url):
    """
    Gets the certificate path for the given node url.
    """

    nodePath = self.pathFor(url)
    nodePath = os.path.join(nodePath, self.CERTIFICATE_FILENAME)

    return nodePath

  def discover(self, url):
    """
    This method will lookup or append a new Node record for the given address.
    Returns None when the node cannot be reached.
    """

    url = self.urlFromPartial(url)
    host, port = self.hostAndPortFromURL(url)

    existing_node = self.search(url)

    Log.noisy("attempting to discover node at %s" % (url))

    node_info = self.retrieveInfo(url)
    if node_info is None:
      Log.error("No response from the node specified")
      return None

    # Acquire database
    session = self.occam.databaseSession()

    # Look up any information we have on the Node

    # Create a record for the Node, if it doesn't exist
    if existing_node:
      # Update Node
      Log.write("updating a known node")
      db_node = existing_node
    else:
      # Create Node
      Log.write("found a new node")
      db_node = DB.Node()

    # Update/Establish Node information in the database
    db_node.name = node_info.get('name', "OCCAM Node")
    db_node.host = host
    db_node.http_port = port
    if node_info['httpSSL']:
      db_node.http_ssl = 1
    else:
      db_node.http_ssl = 0

    # Add the DB record to the transaction
    session.add(db_node)

    # Commit the transaction
    session.commit()

    # Return the Node record
    return db_node

  def urlForNode(self, node, path):
    """
    Returns a URL for the given path for the given node.
    """

    scheme = "https"
    if node.http_ssl == 0:
      scheme = "http"

    host = node.host
    port = node.http_port

    return "%s://%s:%s/%s" % (scheme, host, port, path)

  def urlForObject(self, node, uuid, revision=None, subPath='objects'):
    """
    Returns a URL for the given object id for the given node.
    """

    path = "%s/%s" % (subPath, uuid)

    if not revision is None:
      path = "%s/%s" % (path, revision)

    return self.urlForNode(node, path)

  def findObject(self, uuid):
    """
    When we are in a bind to find an object, we can call this method to go
    and query for which node has this object. Will return either an empty
    array when the search comes up empty, or at least one Node which has the
    requested object.
    """

    # Naive, at the moment. Just go through the node list and query.

    session = self.occam.databaseSession()
    for node in session.query(DB.Node):
      Log.write("looking in %s (%s)" % (node.name, node.host))

      url = self.urlForObject(node, uuid)
      result = self.occam.downloadText(url)

      if result is not None:
        Log.write("found object at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def pullAllObjects(self, uuid, objectList=None):
    """
    This will replicate the object and all dependencies from any node which has
    them. If the object cannot be found, this returns None. Otherwise, returns the object.
    """

    # TODO: handle error when some objects can't be found

    if objectList is None:
      objectList = []

    if not uuid in objectList:
      obj = self.pullObject(uuid)

      if obj is None:
        return None

      objectList.append(uuid)

      # Get relations, and pull those
      relationList = obj.relationList()
      for uuid in relationList:
        Log.write("pulling related object %s" % (uuid))
        related_object = self.pullAllObjects(uuid, objectList)
    else:
      Log.write("already pulled")
      return None

    return obj

  def pullObject(self, uuid):
    """
    This will replicate the object by retrieving it from any node which has it.
    If the object cannot be found, this returns None. Otherwise, returns the
    Object.
    """

    # Retrieve a list of nodes with this object
    nodes = self.findObject(uuid)

    # If no node has the object, we fail out
    if len(nodes) == 0:
      return None

    # Pull the object from that node
    return self.pullObjectFrom(nodes[0], uuid)

  def pullObjectFrom(self, node, uuid, revision=None):
    """
    This will replicate the object by retrieving it from the given node.
    When the replication fails, this returns Node. Otherwise, returns the
    Object. If revision is given, it will pull at least that revision. If
    revision is None, it will replicate all revisions.
    """

    objectPath = self.urlForObject(node, uuid)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    Log.write("cloning object history")

    # Clone a temporary copy of this object
    git = Git(objectPath, cert=cert)

    # Create an Object based on that clone
    obj = Object(git.path, occam=self.occam)

    Log.write("storing %s %s in local object repository" % (obj.objectInfo()['type'], obj.objectInfo()['name']))

    # Copy the object information to the object store
    self.occam.objects.store(obj)

    # Retrieve the stored version of the Object
    objectInfo = obj.objectInfo()
    obj = self.occam.objects.retrieve(objectInfo['id'], objectInfo['type'], 'HEAD')

    # Is it a backed object?
    if not objectInfo.get('backed') is None:
      revision = obj.revision
      self.pullResourceFrom(node, uuid, revision)

    # Is it a built object?
    if not objectInfo.get('build') is None:
      revision = obj.revision
      self.pullBackendFrom(node, uuid, revision)

    # Pull invocation data
    self.pullInvocationDataFrom(node, obj)

    # Return the now replicated object
    return obj

  def pullInvocationDataFrom(self, node, obj, categories=None):
    """
    Pulls invocation data from the given node and object. This will pull
    invocation data from the given set. By default, it will pull "builds",
    "output", and "generated" from the node.
    """

    if categories is None:
      categories = ['builds', 'output', 'generated']

    uuid = obj.objectInfo().get('id')

    # Determine the URL for the invocations
    objectPath = self.urlForObject(node, uuid) + "/invocations/"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    # Pull each category in turn
    for category in categories:
      invocationPath = objectPath + category

      response, content_type, size = self.occam.network.get(invocationPath, 'application/json', cert=cert)

      # Parse the json from the stream
      reader = codecs.getreader('utf-8')
      try:
        invocation = json.load(reader(response))
      except:
        invocation = {}

      # Write out response to invocations path
      self.occam.objects.mergeInvocation(obj, category, invocation)

  def pullBackendFrom(self, node, uuid, revision=None):
    """
    This will pull any pre-built backend images for the given object
    from the given node.
    """

    # TODO: pull a backend list from the node to know what exists

    # Right now, assume a docker pre-built image exists
    dockerHandler = self.occam.backends.handlerFor('docker')
    objectPath = self.urlForObject(node, uuid, revision)
    cert = self.certificatePathFor(objectPath)

    return dockerHandler.pull(objectPath, uuid, revision=revision, cert=cert)

  def taskFor(self, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture):
    """
    This will ask known nodes how it would construct a VM for the given object
    for the given backend. It returns a VM object with enough metadata to
    discover required resources to replicate the VM on this node.
    """

    # Naive, at the moment. Just go through the node list and query.

    session = self.occam.databaseSession()
    for node in session.query(DB.Node):
      Log.write("looking in %s (%s)" % (node.name, node.host))

      return self.taskForFrom(node, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture)

    return None

  def taskForFrom(self, node, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture):
    """
    This will ask the given node how it would construct a VM for the given
    object for the given backend. It returns a VM object with enough metadata
    to discover required resources to replicate the VM on this node.
    """

    taskPath = self.urlForNode(node,
      "task?fromEnvironment=%s&fromArchitecture=%s&toEnvironment=%s&toArchitecture=%s" % (
        fromEnvironment, fromArchitecture, toEnvironment, toArchitecture
      )
    )

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    return self.occam.downloadText(taskPath, cert=cert)

  def pullResourceFrom(self, node, uuid, revision=None):
    """
    This will replicate the resource given by the uuid from the given node.
    When the replication fails, this returns None. Otherwise, returns the
    Object.
    """

    obj = self.occam.objects.retrieve(uuid)
    git = None
    if obj is None:
      # If the object describing the resource does not exist, fail.
      return None

    resourceType = obj.objectInfo()['backed']

    handler = self.occam.resources.handlerFor(resourceType)

    objectPath = self.urlForObject(node, uuid, revision=revision, subPath='resources/%s' % resourceType)
    Log.write("Pulling resource at %s" % (objectPath))

    name = obj.objectInfo()['name']

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    handler.pull(uuid, revision, name, objectPath, cert=cert)

    # Return the now replicated object and the Git object
    return obj, git
