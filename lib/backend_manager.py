# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from lib.log    import Log
from lib.object import Object

class BackendManager:
  """
  This OCCAM manager handles backend invocations. A backend is a service that
  OCCAM can use to build and run objects. For instance, a backend can wrap a
  virtual machine such that objects can be built and run within that
  environment. Some objects may be able to run on most backends, some on just
  a few, and some backends are decidedly relegated to run older archived
  programs.
  """

  def __init__(self, occam):
    """
    Initialize the backend manager.
    """

    self.occam    = occam
    self.options  = occam.options
    self.handlers = {}

  def register(self, backendName, handlerClass):
    """
    Adds a new backend type.
    """
    self.handlers[backendName] = handlerClass

  def handlerFor(self, backendName):
    """
    Returns an instance of a handler for the given type.
    """

    if not backendName in self.handlers:
      Log.error("backend %s not known" % (backendName))
      return None

    return self.handlers[backendName](self.occam)

  def providerPath(self, environment, architecture, environmentGoal=None, architectureGoal=None):
    """
    Provides an array of providers that will map the given environment and
    architecture to a native backend. The last item in the array will be a
    native backend instance. Every other item will, in turn, be an object that
    provides one step of an environment.

    For instance, if we want to run a "dos" object that runs on "x86", we will
    invoke this command with environment and architecture given accordingly.
    The native environment might be "linux" on "ARM", so we must find a set
    of objects that will give us that environment. This method will return the
    hypothetical array of objects that will get us there. The first object
    might be DOSBox, which provides the "dos"/"x86" environment but runs on
    "ubuntu:14.04"/"x86-64". So the next object in the array might be an
    ubuntu distribution providing "ubuntu:14.04"/"x86-64". So we will need
    something to run that distribution, therefore the next object might be
    VirtualBox, providing the "*"/"x86-64" environment. Let's assume we can
    run VirtualBox natively. In this case, our search is complete.
    """

    ret = []

    while True:
      provider = self.providing(environment, architecture)

      ret.append(provider)

      if environmentGoal is None or architectureGoal is None:
        if not isinstance(provider, Object):
          break
      else:
        if environment == environmentGoal and architecture == architectureGoal:
          break;

      environment  = provider.objectInfo().get('environment')
      architecture = provider.objectInfo().get('architecture')

    return ret

  def providing(self, environment, architecture, recurse=False):
    """
    Returns an instance of a backend handler that can manage the
    given environment/architecture combination. When recurse is True, when the
    backend is not a native backend, the method will recurse until a native
    backend is chosen.
    """

    backend       = None
    objectBackend = None

    # Go through the available backends and find one that provides the
    # requested environment/architecture combination

    for k, v in self.handlers.items():
      if v.canProvide(environment, architecture):
        Log.write("Using backend '%s'" % (k))
        backend = v(self.occam)
        break

    if backend:
      return backend

    # No native backend was found for this environment. Try to find a path
    # to a native backend.

    # Go through available provider objects to find something that can
    # handle the requested requirements (which in turn will require
    # some environment of its own)

    providers = self.occam.objects.providersFor(environment, architecture)

    for provider in providers:
      # Check for ability to use this object
      Log.write("Using backend '%s' (non-native)" % (provider.name))
      objectBackend = self.occam.objects.retrieve(provider.uid)
      break

    if objectBackend:
      if not recurse:
        return objectBackend
      else:
        environment  = objectBackend.objectInfo().get('environment')
        architecture = objectBackend.objectInfo().get('architecture')

        return self.providing(environment, architecture, recurse)

    # Nothing can handle this environment/architecture combination
    return None
