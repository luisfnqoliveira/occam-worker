# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

class Configuration:
  def __init__(self, opts):
    '''
    Creates an instance of a Configuration object for the specified input.
    '''
    self.filename = "input.json"
    self.options = opts

    f = open(self.filename)
    self.info = json.load(f)
    f.close()

    if not (opts.value is None):
      self.info[opts.key] = opts.value

  def filename(self):
    '''
    Returns the filename to be used as the configuration json file.
    '''
    return self.filename

  def hash(self):
    '''
    Returns a dictionary of the configuration info.
    '''
    return self.info

  def write(self):
    '''
    Writes out the configuration to the filename specified when this object
    was constructed.
    '''
    f = open(self.filename, 'w+')
    f.write(json.dumps(self.info))
    f.close()

  def print(self):
    '''
    Prints out the configuration to standard out.
    '''
    self.__print(self.info, "")

  def __print(self, items, indent):
    for k, v in items.items():
      if isinstance(v, dict):
        if (k == self.options.key or self.options.key is None):
          print("%s: " % (k))
          self.__print(v, indent + "  ")
      else:
        if (k == self.options.key or self.options.key is None):
          print("%s%s: %s" % (indent, k, v))
