# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling

from subprocess import Popen, PIPE
from hashlib import sha256

from lib.log    import Log
from lib.object import Object

from models.recipe  import Recipe

from lib.db  import DB

# TODO:
# 1. record tags in object metadata for installed repositories
# 2. use tags recorded in object metadata to install repositories
# 3. tarballs will use hashes to request particular tarballs (512-bit SHA)

class Install:
  def __init__(self, opts, args, occam):
    self.args    = args
    self.options = opts
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    return parser

  def do(self, path=None):
    Log.header("Installing Object")

    if path is None:
      path = "."

    if len(self.args) > 2:
      obj_type = self.args[1]
      name = self.args[2]
    elif len(self.args) > 1:
      obj_type = None
      name = self.args[1]
    else:
      obj_type = None
      name = None

    if obj_type and name:
      db_object = self.occam.objects.search(object_type=obj_type, name=name).first()
      object = Object(self.occam.objectPath(db_object.uid), occam=self.occam)
    else:
      object = Object(path, occam=self.occam)

    object_info = object.objectInfo()
    object.install()
