# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import getpass     # For entering a password for person create
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.workset    import Workset
from lib.occam      import Occam
from lib.group      import Group
from lib.object     import Object
from lib.person     import Person
from lib.experiment import Experiment

from uuid import uuid1

from lib.log import Log

from subprocess import Popen, PIPE

class Sync:
  """
  This command resets the view of a workset/group/experiment to a particular
  revision. It pulls in a local view for every dependency as well.
  """
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object to add the new object as a dependency")
    return parser

  def do(self):
    # Hopefully we are in a workset

    path = '.'

    workset = self.occam.objectAt(path, searchFor='workset')

    revision = self.options.revision

    if revision is None:
      db_obj = self.occam.objects.search(uuid=workset.objectInfo()['id']).first()
      revision = db_obj.revision

    workset.sync(revision)

    Log.done("successfully synchronized")
