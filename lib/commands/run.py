# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
import shutil
import re
import glob

from datetime import datetime
from time import sleep
from uuid import uuid1

from lib.db                import DB
from lib.log               import Log
from lib.object            import Object
from lib.experiment        import Experiment
from lib.task_builder      import TaskBuilder
from lib.run_builder       import RunBuilder

import subprocess

# TODO: add --stdout option to put the program's own stdout as our stdout
#       we default to the structured output as standard out.

class Run:
  """
  This class will handle running experiments and spawning experiment processes.
  """

  def __init__(self, opts, args, occam, workingPath = "."):
    """
    Construct a runner class.
    """
    self.options     = opts
    self.args        = args
    self.occam       = occam
    self.workingPath = workingPath

  @staticmethod
  def initParser(parser):
    """
    Amend the specific arguments for this command.
    """

    # TODO: add a --worker option to choose which worker server to initiate the job
    parser.add_option("-d", "--dispatch", action = "store_true",
                                          dest   = "dispatch",
                                          help   = "whether or not to dispatch the jobs on the worker queue")
    parser.add_option("-r", "--revision", action = "store",
                                          dest   = "revision",
                                          help   = "use the given revision of the object to add the new object as a dependency")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the experiment id to attach the workflow object to")
    parser.add_option("-w", "--within",   action = "store",
                                          dest   = "within_object",
                                          help   = "the workset id that contains the experiment")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the workset revision")
    parser.add_option("-n", "--no-rake-output", action = "store_true",
                                                dest   = "no_rake_output",
                                                help   = "will not rake and store output in the experiment")
    parser.add_option("-i", "--input",          action = "store",
                                                dest   = "input_object",
                                                help   = "the id of an object to run as input")
    parser.add_option("-I", "--input-revision", action = "store",
                                                dest   = "input_revision",
                                                help   = "the revision of the object to run as input")
    parser.add_option("-a", "--as",        action = "store",
                                           dest   = "author_uuid",
                                           help   = "the id of the person that creates this object")
    parser.add_option("-p", "--pull",      action = "store_true",
                                           dest   = "pull_objects",
                                           help   = "will pull any necessary objects needed to run this object")
    #parser.add_option("-b", "--backend", action = "store",
    #                                     dest   = "backend",
    #                                     help   = "the backend to use")
    parser.add_option("-e", "--environment", action = "store",
                                             dest   = "environment",
                                             help   = "the environment to target the VM")
    parser.add_option("-c", "--architecture", action = "store",
                                              dest   = "architecture",
                                              help   = "the architecture to target the VM")
    return parser

  def determineRevision(self):
    """
    This function determines which revision of the running object is being
    requested from the arguments.
    """

    if self.options.revision:
      revision = self.options.revision
    else:
      revision = None

    return revision

  def determineWorkset(self, object):
    """
    This function will determine which workset is the ultimate owner of the
    object to be run. (if any)
    """

    return object.workset()

  def determineObject(self, revision):
    """
    This function will determine which object is to be run.
    """

    tempPath = None

    if self.options.to_object:
      # TODO: handle when workset isn't found

      # if it is a occam/task, we should pull out and clone the generator (experiment)
      db_obj = self.occam.objects.search(self.options.to_object).first()
      if db_obj is None:
        return None

      obj = self.occam.objects.retrieve(db_obj.uid, db_obj.object_type, owner_uuid=db_obj.owner_uid)
      if obj is None:
        return None
      obj_info = obj.objectInfo()

      experiment_uuid = self.options.to_object
      experiment_revision = revision
      generatedObject = False

      if obj_info['type'] == "occam/task":
        generatedObject = True
        experiment_uuid = obj_info['generator'].get('id')
        experiment_revision = obj_info['generator'].get('revision')

      workset, experiment, workset_path = self.occam.temporaryClone(experiment_uuid, experiment_revision, self.options.within_object, self.options.within_revision)

      if experiment is None:
        return None

      tempPath = workset_path
      path = experiment.path

      if generatedObject:
        # Add the generated object!
        path = os.path.join(experiment.path, obj_info.get('id'))
        obj.clone(path, revision)

      object_path = path

      dest_obj_type = experiment.objectInfo()['type']
      dest_obj_name = experiment.objectInfo()['name']
      Log.noisy("running %s %s in %s" % (dest_obj_type, dest_obj_name, path))

    else:
      # We are running the current object
      path = "."

      experiment = self.occam.objectAt(path, revision=revision)

    experiment.temporaryPath = tempPath
    return experiment

  def rakeOutputsFor(self, objectInfo, vmPath, first=True):
    """
    Given an object metadata, this function returns an array of metadata
    representing the outputs generated by this object.
    """

    outputs = []

    object_token = "%s %s" % (objectInfo.get('type', 'unknown'), objectInfo.get('name', 'unknown'))
    object_path = os.path.join(vmPath, "objects", str(objectInfo['index']))
    init_path = os.path.join(vmPath, "initializations", "%s-%s-%s" % (objectInfo['id'], objectInfo['revision'], objectInfo['index']), "run.json")

    if 'run' in objectInfo or first:
      outputs.append({
        'name': 'stdout for %s' % (object_token),
        'file': '%s/run-out.raw' % (object_path),
        'type': 'text/plain',
        'origin': objectInfo.get('id'),
        'originRevision': objectInfo.get('revision')
      })

      outputs.append({
        'name': 'stderr for %s' % (object_token),
        'file': '%s/run-err.raw' % (object_path),
        'type': 'text/plain',
        'origin': objectInfo.get('id'),
        'originRevision': objectInfo.get('revision')
      })

      if os.path.exists(init_path):
        f = open(init_path, 'r')
        init_info = json.load(f)
        f.close()

        if 'finish' in init_info:
          outputs.append({
            'name': 'stdout (Finish) for %s' % (object_token),
            'file': '%s/run-finish-out.raw' % (object_path),
            'type': 'text/plain',
            'origin': objectInfo.get('id'),
            'originRevision': objectInfo.get('revision')
          })

          outputs.append({
            'name': 'stderr (Finish) for %s' % (object_token),
            'file': '%s/run-finish-err.raw' % (object_path),
            'type': 'text/plain',
            'origin': objectInfo.get('id'),
            'originRevision': objectInfo.get('revision')
          })

    for output in objectInfo.get('outputs', []):
      if 'pattern' in output:
        Log.write("Found output pattern %s" % (output['pattern']))
        glob_pattern = os.path.join(object_path, output['pattern'])
        for subfile in glob.glob(glob_pattern):
          output_info = {}
          output_info.update(output)

          output_info.update({
            'name': output.get('name', os.path.splitext(os.path.basename(subfile))[0]),
            'type': output.get('type', 'text/plain'),
            'object': self.occam.objects.retrieve(objectInfo['id'], objectInfo['type'], revision=objectInfo['revision']),
            'schema': output.get('schema'),
            'origin': objectInfo.get('id'),
            'originRevision': objectInfo.get('revision')
          })

          if os.path.isdir(subfile):
            output_json_path = os.path.join(subfile, 'object.json')
            if os.path.exists(output_json_path):
              f = open(output_json_path, 'r')
              info = json.load(f)
              output_info.update(info)
              f.close()
            output_info.update(info)
            output_info['createIn'] = subfile
          else:
            output_info['file'] = subfile

          if 'subtype' in output:
            output_info['subtype'] = output.get('subtype')

          # TODO: ensure there is some record of the task invocation

          Log.write("Found output: %s" % (output_info['name']))
          outputs.append(output_info)
      elif 'file' in output or 'createIn' in output:
        output_info = {}
        output_info.update(output)
        output_info.update({
          'name': output.get('name', 'output'),
          'type': output.get('type', 'text/plain'),
          'object': self.occam.objects.retrieve(objectInfo['id'], objectInfo['type'], revision=objectInfo['revision']),
          'schema': output.get('schema'),
          'origin': objectInfo.get('id'),
          'originRevision': objectInfo.get('revision')
        })

        if 'subtype' in output:
          output_info['subtype'] = output.get('subtype')

        if 'file' in output:
          output_info['file'] = os.path.join(object_path, output.get('file'))

        # TODO: ensure there is some record of the task invocation

        Log.write("Found output: %s" % (output_info['name']))
        outputs.append(output_info)

    return outputs

  # Capture output for this vm:
  def captureOutput(self, vm_path, generator, task, task_inputs, workset):
    ret = []
    outputs = []
    objects = self.occam.rakeTaskObjects(task_inputs)

    # Look at each object in the task
    for obj in objects:
      # Look at output for this object
      object_path = os.path.join(vm_path, "objects", str(obj['index']))
      object_token = "%s %s" % (obj.get('type', 'unknown'), obj.get('name', 'unknown'))

      new_outputs_path = os.path.join(object_path, "outputs")
      if not os.path.exists(new_outputs_path):
        os.mkdir(new_outputs_path)

      generator = obj

      outputs = self.rakeOutputsFor(obj, vm_path)

      # For each output generated for this object,
      # Create an object for it.
      for output in outputs:
        Log.write("Looking at output")
        if ('file' in output and output['file']) or ('createIn' in output and output['createIn']):
          # Create a UUID for this output
          output['id'] = Object.uuid('')

          uuid = output['id']

          # Create an object path for this output
          output_path = ""
          if 'file' in output and os.path.exists(output['file']):
            # Copy data
            output_path = os.path.join(new_outputs_path, uuid)
            if not os.path.exists(output_path):
              os.mkdir(output_path)

            shutil.copy(output['file'], output_path)
          elif 'createIn' in output and os.path.exists(os.path.join(object_path, output['createIn'])):
            output_path = os.path.join(new_outputs_path, uuid)

            # We will create the object within the given path
            template_path = os.path.join(object_path, output['createIn'])
            shutil.copytree(template_path, output_path)
          else:
            # Hmm, output not found
            if 'file' in output:
              Log.warning("no output file '%s' found" % (output['file']))
            elif 'createIn' in output:
              Log.warning("no output path '%s' found" % (os.path.join(object_path, output['createIn'])))
            else:
              Log.warning("do not understand the output: " % (output.get('name', "unknown")))

            continue

          if output.get('schema'):
            # TODO: remove git usage here
            schema = output['object'].git.retrieveJSON(output['schema'])

            # TODO: sanitize
            schema_output_path = os.path.join(output_path, output['schema'])
            f = open(schema_output_path, 'w+')
            f.write(json.dumps(schema))
            f.close()

          # Create object
          # (this will commit the files in the existing path to the object!)
          output_name = output.get('name', 'unknown')
          existingObjectInfo = {}
          existingObjectInfoPath = os.path.join(output_path, 'object.json')
          if os.path.exists(existingObjectInfoPath):
            f = open(existingObjectInfoPath)
            existingObjectInfo = json.load(f)
            f.close()

            if 'name' in existingObjectInfo:
              output_name = existingObjectInfo['name']

          outputObject = Object.create(output_path, output_name, output['type'], uuid=uuid, createPath=False, occam=self.occam, root=workset)

          # store objects
          outputInfo = outputObject.objectInfo()
          # TODO: Should this be 'experiment' type? or
          #       should it reiterate the generator of the task?
          # that is: outputInfo['generator'] = task_info.get('generator', { ... what is already there ... })
          # (this is for provenance)

          # Create the output object based on the object known
          if 'object' in output:
            del output['object']
          if 'id' in output:
            # Don't allow us to overwrite id
            del output['id']
          if 'createIn' in output:
            # Add the files specified by "path" to the output object
            Log.write("committing files in %s to object" % (output['createIn']))
            outputObject.git.add('.')
            del output['createIn']
          if 'file' in output:
            # Add the files specified by "path" to the output object
            outputInfo['file'] = os.path.basename(output['file'])
            del output['file']
          if 'schema' in output and output['schema']:
            outputInfo['schema'] = os.path.basename(output['schema'])
            del output['schema']

          # Merge output and outputInfo
          outputInfo.update(output)

          # Merge output and the existing object info
          outputInfo.update(existingObjectInfo)

          outputInfo['generator'] = {}
          outputInfo['generator']['object'] = generator.copy()
          outputInfo['generator']['task']   = task.objectInfo().copy()

          # Amend the information to tag the revision of the generating object
          outputInfo['generator']['task'].update({
            "revision": task.revision
          })

          # Update the output object
          outputObject.updateObject(outputInfo, 'Creating Output Object')

          # Install resources of object
          outputObject.install()

          # Get initial object revision (we will store this in Experiment)
          generate_obj_revision = outputObject.head()

          # Update generating object to refer to output object
          generate_obj_info = outputInfo.copy()
          generate_obj_info['revision'] = generate_obj_revision

          # Append this output object information. We will return all of the
          # output object metadata which will eventually be stored in the
          # generating object.
          ret.append(generate_obj_info)

    return ret

  def runVMs(self, jobPath, workset, experiment, run_db_records):
    tasks = []

    # Look for all of the vms in this job path
    vms_path = os.path.join(jobPath, "vms")

    # Get workset/experiment database entry
    workset_db    = self.occam.objects.search(workset.objectInfo()['id']).first()
    experiment_db = self.occam.objects.search(experiment.objectInfo()['id']).first()

    if os.path.exists(vms_path):
      # For each vm, spawn it!
      for vm_path in os.listdir(vms_path):
        vm_path = os.path.realpath(os.path.join(vms_path, vm_path))
        if os.path.isdir(vm_path):
          # Gather run index and job index
          drive, path_and_file = os.path.splitdrive(vm_path)
          dirs = path_and_file.split(os.sep)
          vm_index  = int(dirs[-1])
          job_index = int(dirs[-3])
          run_index = int(dirs[-5])

          # Spawn VM (or Job)
          if self.options.dispatch:
            # Create Run to contain the jobs for this run_index, if there is not one already
            run = run_db_records.get(run_index)

            if run is None:
              # Create a Run
              run = DB.Run()
              run.uid = str(uuid1())
              run.archived = 0
              run.published = datetime.utcnow()
              run.updated = datetime.utcnow()

              # Determine the person creating these runs
              if self.options.author_uuid:
                # TODO: handle when person is not found
                runner_id = self.occam.searchPeople(self.options.author_uuid).first().person.id
              else:
                runner_id = self.occam.currentPersonId()

              if runner_id is None:
                Log.error("Cannot find the author of this run")

              run.workset_id = workset_db.workset.id
              run.experiment_id = experiment_db.experiment.id
              run.workset_revision = workset.revision
              run.experiment_revision = experiment.revision
              run.person_id = runner_id

              DB.session().add(run)
              DB.session().flush()

              run_db_records[run_index] = run

            vm_obj = Object(vm_path, occam=self.occam)
            Log.write("dispatching job %s" % (vm_obj.objectInfo().get('id')))

            # Create job to run this vm, codependant with other vms in this job
            job = DB.Job()
            job.run_index = run_index
            job.vm_index  = vm_index
            job.job_index = job_index
            job.task_uid = vm_obj.objectInfo().get('id')
            job.status = "queued"
            job.kind = "run"

            job.run_id = run.id

            job.workset_id = workset_db.workset.id
            job.experiment_id = experiment_db.experiment.id
            job.workset_revision = workset.revision
            job.experiment_revision = experiment.revision
            job.person_id = run.person_id

            # Rake running connections:
            main_connection = vm_obj.objectInfo().get('input', [{}])[0].get('input', [{}])[0].get('index', 0)
            job.connections = ";%s;" % (main_connection)

            job.has_dependencies = 0
            job.codependant = 0

            job.command = "run"

            # Commit Job/Relations

            DB.session().add(job)
            DB.session().commit()

            # TODO: handle codependencies
            # Build JobCoDependency / JobDependency relations
            if job_index > 0:
              jobInfo = vm_obj.objectInfo().get('dependsOn')
              lastJob = DB.session().query(DB.Job).filter_by(run_id = run.id,
                                                             job_index = jobInfo["job_index"],
                                                             vm_index  = jobInfo["vm_index"]).first()
              Log.write("Job Dependency")
              job.has_dependencies = 1

              jobDependencyRecord = DB.JobDependency()

              jobDependencyRecord.job_id            = job.id
              jobDependencyRecord.depends_on_job_id = lastJob.id

              DB.session().add(jobDependencyRecord)
              DB.session().commit()

            # Create a Job Spawn job that all jobs depend on!!
          else:
            Log.write("running %s" % vm_path)

            # Ensure a flush and finalize any pending DB transactions
            # (some databases will do some aggressive locking and fail our spawned tasks)
            DB.session().commit()

            command = ['occam', 'run']

            if self.options.verbose:
              command.append('-v')

            if self.options.logtype == "json":
              command.append('-L')
              command.append('json')

            # Ensure that we run VM objects within the same workset
            # This ensures that output generated by these VMs will be attached
            # to that workset (for access permission purposes)
            if self.options.within_object:
              command.append('--within')
              command.append(self.options.within_object)

            if self.options.within_revision:
              command.append('--within-revision')
              command.append(self.options.within_revision)

            p = subprocess.Popen(command, cwd=vm_path)

            tasks.append(p)

    return tasks

  def runTask(self, object):
    obj_revision = object.revision
    object_path = object.path
    local_build = False
    tempPath = None
    object_info = object.objectInfo()

    docker_backend = self.occam.backends.handlerFor('docker')

    if not local_build and tempPath == False:
      workset, obj, workset_path = self.occam.temporaryClone(object_info['id'], obj_revision, None, None)
      tempPath = workset_path
      path = obj.path
    else:
      obj = Object(object.path, revision=object.revision, occam=self.occam)
      obj_revision = obj.revision

    Log.noisy("attempting to build object at %s" % (object_path))

    localHash = None
    if local_build:
      # TODO: hash path
      localHash = "local-%s" % "abc"
      obj.revision = obj.storeLocalChanges()
      obj_revision = obj.revision

    if local_build and self.occam.update(obj, build_path = object.path, localHash=localHash):
      Log.noisy("built object locally in %s" % (object_path))
    else:
      # Try building

      # We need to make sure dependent objects are built

      if 'id' in obj.objectInfo()['run']:
        run_obj = self.occam.objects.retrieve(obj.objectInfo()['input'][0]['id'], obj.objectInfo()['input'][0]['type'], revision=obj.objectInfo()['input'][0]['revision'])

        if not docker_backend.exists(run_obj):
          self.occam.build(run_obj)

      self.occam.build(obj, build_path=object.path, localHash=localHash)

    # Ensure this revision is stored in the occam store

    # Create directories for output
    hashed_name = "%s-%s" % (object_info["id"], obj.revision)
    relative_path = hashed_name

    input_object_path = os.path.join(object.path, "objects")
    if not os.path.exists(input_object_path):
      Log.write("creating objects path for task: %s" % (input_object_path))
      os.mkdir(input_object_path)

    input_object_path = os.path.join(input_object_path, relative_path)
    if not os.path.exists(input_object_path):
      Log.write("creating objects path for task: %s" % (input_object_path))
      os.mkdir(input_object_path)

    # We need to make sure dependent objects are built
    run_obj = self.occam.objects.retrieve(obj.objectInfo()['input'][0]['id'], obj.objectInfo()['input'][0]['type'], revision=obj.objectInfo()['input'][0]['revision'])
    if not docker_backend.exists(run_obj):
      self.occam.build(run_obj)

    for inputInfo in obj.objectInfo()('input', [])[0].get('input', []):
      if 'revision' in inputInfo:
        input_obj = self.occam.objects.retrieve(inputInfo['id'], inputInfo['type'], revision=inputInfo['revision'])
        if not docker_backend.exists(input_obj):
          self.occam.build(input_obj)

    docker_backend.run(obj, object.path)

    # Ok. Print out the output results.
    outputs = object_info.get('outputs') or []
    for output in outputs:
      # determine path
      if 'file' in output:
        # determine data file location
        output_path = os.path.join(object.path, output['file'])

        # open
        f = open(output_path)
        output = json.load(f)
        f.close()

        Log.header("Output")
        Log.output(str(output))

    # Rake VM (or Job)
    vm_path = object.path

    vmObject = Object(vm_path, occam=self.occam)
    vmObject.temporaryPath = tempPath

    return vmObject

  def do(self, recursive=False, pull=True):
    # Get the revision of the object to run
    revision = self.determineRevision()

    # Get the object to run
    object   = self.determineObject(revision)

    if object is None:
      if self.options.to_object and self.options.pull_objects and pull:
        # Retrieve the object
        Log.write("Could not find the object. Attempting to pull the object.")
        self.occam.nodes.pullAllObjects(self.options.to_object)

        # Retry running the object
        return self.do(pull=False)

      Log.error("Cannot find the specified object.")
      return -1;

    revision = object.revision

    masterTempPath = object.temporaryPath

    # Get the workset for this object
    workset = self.determineWorkset(object)
    if workset:
      Log.write("Writing outputs to workset %s (%s)" % (workset.objectInfo().get('name', 'unknown'),
                                                        workset.objectInfo().get('id')))

    # Get the object info
    object_info = object.objectInfo()
    obj_type = object_info.get('type', 'object')
    obj_name = object_info.get('name', 'unnamed')

    if obj_type == "experiment":
      Log.header("Running experiment set")
    elif obj_type == "group":
      Log.header("Running group")
    elif obj_type == "workset":
      Log.header("Running workset")
    else:
      Log.header("Running %s object %s" % (obj_type, obj_name))

    # For objects, we go through and collect all sibling objects that have
    # runnable workflows.

    # Determine the number of 'Run' sets determined by a workflow using the
    # RunBuilder
    paths = []
    errors = []

    if 'workflow' in object_info:
      paths = RunBuilder(self.args, self.options, object.path, self.occam).do()

    # Determine the number of 'Task' sets for each Run using the TaskBuilder
    if len(paths) > 0:
      paths, errors = TaskBuilder(self.args, self.options, object.path, self.occam).do(runPaths=paths)

    # For keeping track of dispatching jobs within the same run
    run_db_records = {}

    if len(errors) > 0:
      Log.error("Cannot run: cannot build tasks.")
      for obj in errors:
        Log.write("Attempt to determine how to build a task for %s %s" % (obj.get('type'), obj.get('name')))
        # TODO: update for more than one backend
        docker_backend = self.occam.backends.handlerFor('docker')
        backend = docker_backend.provides()[0]
        hypotheticalTask = self.occam.nodes.taskFor(backend[0], backend[1], obj.get('environment'), obj.get('architecture'))
        objs = self.occam.taskObjectsFor(hypotheticalTask)
        for obj in objs:
          Log.write("Need object %s %s" % (obj.get('type'), obj.get('name')))
          if self.options.pull_objects:
            # Pull this object
            self.occam.nodes.pullAllObjects(obj.get('id'))
            return self.do()
      return -1

    # Dispatch/queue or immediately run the created tasks
    for jobPath in paths:
      Log.write("running job %s" % jobPath)
      tasks = self.runVMs(jobPath, workset, object, run_db_records)

      # Wait for all tasks to complete
      for task in tasks:
        task.wait()

    inputs = None
    if self.options.input_object:
      input = self.occam.objects.retrieve(self.options.input_object, revision=self.options.input_revision)

      if input:
        inputs = []
        inputs.append(input)
        Log.write("Adding input: %s %s" % (input.objectInfo().get('type', 'object'), input.objectInfo().get('name', 'unknown')))

    if len(paths) == 0:
      # Re-read object info if we want a different revision
      if obj_type != 'occam/task':
        # Generate a task if this object is not a task!!
        task_object = self.occam.buildTask(object, inputs=inputs)
        if task_object is None:
          Log.error("Cannot run this object.")
          return -1
      else:
        task_object = object

      Log.write("Running task...")

      # Run task
      runPath = self.occam.runTask(task_object)

      Log.write("Task Completed")

      # Look for outputs
      Log.header("Raking outputs")
      Log.write("path: %s" % (runPath))
      generated_outputs = self.captureOutput(runPath, object, task_object, task_object.objectInfo().get('input', [])[0], workset)

      object_info = object.objectInfo()

      generatorObject = None
      generatorId = task_object.objectInfo().get('generator', {}).get('id')
      generatorRevision = task_object.objectInfo().get('generator', {}).get('revision')
      if generatorId:
        generatorObject = self.occam.objects.retrieve(generatorId, revision=generatorRevision)

      if generatorObject:
        generatorObjectInfo = generatorObject.objectInfo()

      for output in generated_outputs:
        outputObject = self.occam.objects.retrieve(output['origin'], revision=output['originRevision'])
        outputObjectInfo = outputObject.objectInfo()

        invocationRecord = {
          "origin": {
            "id": outputObjectInfo['id'],
            "name": outputObjectInfo.get('name', 'unknown'),
            "type": outputObjectInfo.get('name', 'object'),
            "revision": outputObject.revision
          },
          "object": {
            "id": output['id'],
            "revision": output['revision'],
            "name": output.get('name', 'unknown'),
            "type": output.get('type', 'object')
          }
        }

        if generatorObject:
          Log.write("Adding %s output '%s' (%s) to %s %s (%s)" % (output['type'], output['name'], output.get('id', "!!!"), generatorObjectInfo['type'], generatorObjectInfo['id'], generatorObject.revision))

          # Add subtype, file, schema, size to invocation record
          if 'subtype' in outputObjectInfo:
            invocationRecord['origin']['subtype'] = outputObjectInfo['subtype']

          if 'file' in outputObjectInfo:
            invocationRecord['origin']['file'] = outputObjectInfo['file']

          if 'schema' in outputObjectInfo:
            invocationRecord['origin']['schema'] = outputObjectInfo['schema']

          if 'size' in outputObjectInfo:
            invocationRecord['origin']['size'] = outputObjectInfo['size']

          if 'subtype' in output:
            invocationRecord['object']['subtype'] = output['subtype']

          if 'file' in output:
            invocationRecord['object']['file'] = output['file']

          if 'schema' in output:
            invocationRecord['object']['schema'] = output['schema']

          if 'size' in output:
            invocationRecord['object']['size'] = output['size']

          self.occam.objects.appendInvocation(generatorObject, 'output', invocationRecord)
        Log.write("Adding %s output '%s' (%s) to %s %s (%s)" % (output.get('type', 'unknown'), output.get('name', 'unknown'), output.get('id', "!!!"), outputObjectInfo.get('type', 'unknown'), outputObjectInfo.get('id', 'unknown'), outputObject.revision))
        self.occam.objects.appendInvocation(outputObject, 'generated', invocationRecord['object'])
      Log.done("Finished running %s %s" % (object.objectInfo().get('type', 'object'), object.objectInfo().get('name', 'unknown')))

    if masterTempPath:
      Log.write("removing %s" % masterTempPath)
      shutil.rmtree(masterTempPath)
