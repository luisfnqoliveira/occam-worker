# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import json        # For parsing recipes
import collections # For OrderedDict

from lib.log            import Log
from lib.git            import Git
from lib.object         import Object

class Append:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-u", "--unique",     action = "store_true",
                                            dest   = "forceUnique",
                                            help   = "When given, it will only add the item if it doesn't already exist.")
    parser.add_option("-a", "--at",         action = "store",
                                            dest   = "at_index",
                                            help   = "Specifies the index to add the new item to the array.")
    parser.add_option("-i", "--input-type",  action  = "store",
                                             dest    = "input_type",
                                             default = "text",
                                             help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the object uuid to build")
    parser.add_option("-w", "--within",   action = "store",
                                          dest   = "within_object",
                                          help   = "the id of the object to clone this object within")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the revision of the object to clone this object within")
    return parser

  def do(self):
    key = None
    value = None

    if len(self.args) > 2:
      key = self.args[1]
    else:
      return -1
    if len(self.args) == 3:
      value = self.args[2]
    elif len(self.args) > 3:
      Log.error("too many arguments.")
      return -1

    # Form value
    if self.options.input_type == 'json':
      # TODO: Catch exception
      value = json.loads(value)

    revision = self.options.revision or "HEAD"
    if self.options.revision:
      revision = self.options.revision

    if self.options.to_object:
      # Get the object from the object store
      # Create a space for it
      obj_id = self.options.to_object

      workset, obj, path = self.occam.temporaryClone(self.options.to_object, revision, self.options.within_object, self.options.within_revision)
      tempPath = True
    else:
      # Look in the current path
      obj = Object('.', occam=self.occam)

    # Get object metadata
    obj_info = obj.objectInfo()

    # Split keys by '.'
    def splitKey(k):
      keys = re.split(r"([^\\])\.", k)
      new_keys = []
      for i in range(0, len(keys)-1, 2):
        new_keys.append((keys[i] + keys[i+1]).replace('\\', ''))
      new_keys.append(keys[len(keys)-1].replace('\\', ''))

      return new_keys

    keys = splitKey(key)

    current = obj_info

    for k in keys[0:-1]:
      Log.noisy("traversing key %s" % (k))
      if isinstance(current, list):
        current = current[int(k)]
      else:
        current = current.get(k)

      if current is None:
        Log.error("Cannot find key %s within %s" % (k, key))
        return -1

    Log.noisy("final key %s" % (keys[-1]))
    current[keys[-1]] = current.get(keys[-1], [])

    forceUnique = self.options.forceUnique

    at_index = None
    if self.options.at_index:
      at_index = int(self.options.at_index)

    if forceUnique and (not value in current[key]):
      if at_index is not None:
        current[keys[-1]].insert(at_index, value)
      else:
        current[keys[-1]].append(value)
      revisions = obj.updateObject(obj_info, 'appending unique value to %s' % (key))
      Log.write("New revision: ", end="")
      Log.output(revisions[0], padding="")
      if (len(revisions) > 1):
        Log.write("New workset revision: ", end="")
        Log.output(revisions[-1], padding="")
      Log.done("appended %s to %s" % (value, key))
    elif not forceUnique:
      if at_index is not None:
        current[keys[-1]].insert(at_index, value)
      else:
        current[keys[-1]].append(value)
      revisions = obj.updateObject(obj_info, 'appending value to %s' % (key))
      Log.write("New revision: ", end="")
      Log.output(revisions[0], padding="")
      if (len(revisions) > 1):
        Log.write("New workset revision: ", end="")
        Log.output(revisions[-1], padding="")
      if at_index is not None:
        Log.done("Appended %s to %s at %s" % (value, key, at_index))
      else:
        Log.done("Appended %s to %s" % (value, key))
    else:
      Log.warning("Value already in list %s" % (value, key))

    return 0
