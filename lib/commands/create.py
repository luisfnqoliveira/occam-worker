# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling

from lib.workset        import Workset
from lib.log            import Log

class Create:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-d", "--no-db", action = "store_true",
                                       dest   = "no_db",
                                       help   = "does not create database entry")
    return parser

  def do(self):
    Log.header("Creating workset")

    name = None
    if len(self.args) > 1:
      name = self.args[1]

    path = "."
    if len(self.args) > 2:
      path = self.args[2]
      Log.write("placing object in %s" % (path))
    else:
      path = "%s/%s" % (path, name)

    if name is None:
      Log.error("no name given")
      return -1

    # Create the workset
    workset = self.occam.addWorkset(path, name)
    Log.done("Successfully created workset %s" % (name))
