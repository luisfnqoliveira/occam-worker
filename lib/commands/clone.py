# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import shutil

from subprocess import Popen, PIPE

# For pulling network http resources
try:
  from urllib.request import Request, urlopen  # Python 3
except:
  from urllib2 import Request, urlopen  # Python 2

from urllib.error import HTTPError  # Python 3

from lib.log    import Log
from lib.object import Object
from lib.person import Person
from lib.workset import Workset

class Clone:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision", action = "store",
                                          dest   = "revision",
                                          help   = "the revision of the object to use")
    parser.add_option("-l", "--linked",   action = "store_true",
                                          dest   = "linked",
                                          help   = "whether or not to link to the original object instead of creating a new object with a new uuid")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the object uuid to build")
    parser.add_option("-p", "--temporary",action = "store_true",
                                          dest   = "temporary",
                                          help   = "create a temporary path (you are responsible for deleting)")
    parser.add_option("-s", "--store",    action = "store_true",
                                          dest   = "store",
                                          help   = "build the clone in the object store instead of making a local path")
    parser.add_option("-a", "--as",       action = "store",
                                          dest   = "author_uuid",
                                          help   = "the id of the person that clones this object")
    parser.add_option("-w", "--within",   action = "store",
                                          dest   = "within_object",
                                          help   = "the id of the object to clone this object within")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the revision of the object to clone this object within")
    return parser

  def do(self):
    Log.header("Cloning object")

    tempPath = False

    obj_name = None
    obj_type = None
    obj_id   = None

    revision = None

    path = None

    if self.options.revision:
      revision = self.options.revision

    if self.options.to_object:
      obj_id = self.options.to_object
      if len(self.args) > 1:
        obj_name = self.args[1]

    else:
      if len(self.args) > 1:
        obj_id = self.args[1]

      if len(self.args) > 2:
        path = self.args[2]

      if len(self.args) > 2 and obj_id is None:
        obj_type = self.args[1]
        obj_name = self.args[2]
        obj_id   = None

        path = None

        if len(self.args) > 3:
          path = self.args[3]

    if obj_id is None and obj_type is None:
      Log.error("no object id given")
      return -1

    # Find referred object
    obj = self.occam.objects.retrieve(obj_id, revision=revision)

    if obj is None:
      Log.error("object not found")
      return -1

    if revision is None:
      revision = obj.revision

    obj_id   = obj.objectInfo()['id']
    obj_type = obj.objectInfo()['type']
    if obj_name is None:
      obj_name = obj.objectInfo()['name']

    if path is None:
      path = os.path.join('.', obj_type + '-' + obj_name)

    path = os.path.realpath(path)

    # Clone
    # Look at type

    new_obj = None

    # Add the current person as the author
    if self.options.author_uuid:
      # TODO: handle when person is not found
      person = Person(self.occam.objects.pathFor(self.options.author_uuid), occam=self.occam)
    else:
      person = self.occam.currentPerson()

    primary_obj = self.occam.objectAt(self.occam.objects.pathFor(obj_id))

    if self.options.store:
      # Make a temporary space for the destination workset (if needed)
      if self.options.within_object:
        # We make a clone of the dependant object
        # TODO: get workset_uuid and workset_revision
        db_within_obj = self.occam.objects.search(self.options.within_object).first()
        within_obj = self.occam.objects.retrieve(self.options.within_object, db_within_obj.object_type, revision=self.options.within_revision)
        workset = within_obj.workset()
        workset_uuid = workset.objectInfo()['id']
        workset_revision = workset.revision
        workset_obj, within_obj, path = self.occam.temporaryClone(self.options.within_object, self.options.within_revision, worksetUuid=workset_uuid, worksetRevision=workset_revision)

        # And we will clone in the new object
        new_obj = primary_obj.clone(os.path.join(within_obj.path, 'cloned-experiment'), revision=revision, increment=True, create=False)
      else:
        # Or simply make a temporary space for the cloned object
        workset_obj, new_obj, path = self.occam.temporaryClone(obj_id, revision)

      if not self.options.linked:
        # Create a new ID and set new name
        info = new_obj.objectInfo()
        info['id'] = Object.uuid(new_obj.objectInfo()['type'])
        info['name'] = obj_name
        info['authors'] = []

        # Add belongsTo for objects with a 'within'
        if self.options.within_object:
          db_within_obj = self.occam.objects.search(self.options.within_object).first()
          info['belongsTo'] = {
            'type': db_within_obj.object_type,
            'id': db_within_obj.uid
          }

        info['clonedFrom'] = {
          "type": primary_obj.objectInfo()['type'],
          "name": primary_obj.objectInfo()['name'],
          "id":   obj_id,
          "revision": revision
        }

        # Will store the new object
        if new_obj.objectInfo()['type'] == "workset":
          new_obj = Workset(new_obj.path, revision=new_obj.updateObject(info, "Cloned.")[0][0], occam=self.occam)

          # Update Authorship
          new_obj.addAuthor(person)
        else:
          newRevision = new_obj.updateObject(info, "Cloned.")[0][0]
          new_obj = Object(new_obj.path, revision=newRevision, occam=self.occam)
          self.occam.objects.store(new_obj)
          workset_obj.addDependency(object_type = 'experiment',
                                    name        = obj_name,
                                    revision    = new_obj.revision,
                                    uuid        = info['id'],
                                    category    = 'contains')
          self.occam.objects.store(workset_obj)

      tempPath = True
    elif obj_type == 'workset':
      # We should not be in a workset
      # For worksets, clone whole structure at the given revision
      #  - Recurse for inner objects (do we clone those too?)
      pass
    elif obj_type == 'group':
      # We should be in a workset/group
      pass
    elif obj_type == 'experiment':
      # We should be in a workset/group
      pass
    else:
      new_obj = obj.clone(path, create=True, increment=True, cloneResources=True)

    if new_obj is None:
      Log.error("could not clone")
      return -1

    Log.write("Cloned object as revision %s to %s" % (obj.revision, path))
    new_id = new_obj.objectInfo()['id']
    if self.options.temporary:
      Log.write('temp path: ', end='')
      Log.output(path, padding="")
    else:
      Log.write('New Id: ', end='')
      Log.output(new_id, padding="")

    if tempPath and not self.options.temporary:
      Log.noisy("removing %s" % path)
      shutil.rmtree(path)
    else:
      # Apply dependency information to current object!
      current_object = self.occam.objectAt('.')
      if current_object and current_object.objectInfo()['type'] in ['workset', 'group', 'experiment']:
        current_object.addDependency(uuid        = new_id,
                                     object_type = obj_type,
                                     name        = obj_name,
                                     revision    = new_obj.head())

    Log.done("Cloned %s %s" % (obj_type, obj_name))
