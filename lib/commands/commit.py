# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import getpass     # For entering a password for person create
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.db         import DB
from lib.workset    import Workset
from lib.occam      import Occam
from lib.group      import Group
from lib.object     import Object
from lib.person     import Person
from lib.experiment import Experiment

from uuid import uuid1

from lib.log import Log

from subprocess import Popen, PIPE

class Commit:
  """
  This commits the object's revision to the tree.
  """
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    return parser

  def do(self):
    path = '.'
    Log.header('Committing object')

    obj = self.occam.objectAt(path)

    if obj is None:
      Log.error('No object in the current path.')
      return -1

    # Update resources
    objInfo = obj.objectInfo()

    Log.write("Committing resources")
    resources, dirty = self.occam.resources.commitAll(obj)

    if dirty:
      objInfo['install'] = resources
      obj.updateObject(objInfo, "Committing resources.")

    obj.updateParents()
    obj.updateStore()

    self.occam.objects.updateNormalization(obj);

    # Set base revision to this!
    db_obj = self.occam.objects.search(uuid=obj.objectInfo()['id']).first()

    if db_obj.revision != obj.head():
      # Write changed revision to the database
      db_obj.revision = obj.head()

      DB.session().add(db_obj)
      DB.session().commit()
    else:
      Log.write('No changes.')

    Log.write('Current revision: ', end='')
    Log.output(db_obj.revision, padding='')

    Log.done("Updated object %s" % objInfo.get('id'))
