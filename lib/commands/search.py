# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.log            import Log
from lib.db             import DB
from lib.git            import Git

class Search:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-t", "--type",  action = "store",
                                       dest   = "object_type",
                                       help   = "search for objects with the specified type")
    parser.add_option("-n", "--name",  action = "store",
                                       dest   = "name",
                                       help   = "search for objects with the specified name")
    parser.add_option("-u", "--uuid",  action = "store",
                                       dest   = "uuid",
                                       help   = "search for the object with the specified uuid")
    return parser

  def do(self):
    keyword = None
    if len(self.args) > 1:
      keyword = self.args[1]

    if self.options.object_type == "person":
      objs = self.occam.searchPeople(name = self.options.name,
                                     uuid = self.options.uuid,
                                     keyword = keyword)
    else:
      objs = self.occam.objects.search(object_type = self.options.object_type,
                                      name        = self.options.name,
                                      uuid        = self.options.uuid)

    Log.header("Listing Found Objects")
    for obj in objs:
      Log.output(obj.name)
      Log.output(obj.uid, padding="    uuid: ")

    Log.write("found %s object(s)" % (objs.count()))

    return 0
