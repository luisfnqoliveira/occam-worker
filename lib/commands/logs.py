# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import re
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)
import yaml        # Output in YAML
import select
import math
import time        # Sleep between polls

from lib.log            import Log
from lib.db             import DB
from lib.git            import Git
from lib.object         import Object

from models.system           import System

class Logs:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-j", "--job",  action = "store",
                                      dest   = "job",
                                      help   = "the job id to pull the current log from")
    parser.add_option("-t", "--tail", action = "store_true",
                                      dest   = "tail",
                                      help   = "when specified, will continuously update with new information until the job is complete")
    parser.add_option("-s", "--seek", action = "store",
                                      type   = "int",
                                      dest   = "seek",
                                      help   = "which byte position to start printing from")
    return parser

  def do(self):
    if self.options.job is None:
      Log.error("Must specify job id with --job argument")
      return -1

    # Look up the job record
    DB.initialize(self.options)
    job_db = DB.session().query(DB.Job).filter_by(id = self.options.job).first()
    if job_db is None:
      Log.error("Cannot find job with id %s" % (self.options.job))
      return -1

    # If the job is located at this node, list the log
    # The path is <JOB_PATH>/<RADIX>/job-<JOB_ID>
    # where RADIX is the order of hundreds of the job id
    #      (89 -> 0, 123 -> 100, 499 -> 400, etc)
    jobs_path = self.occam.configuration()['paths'].get("jobs")
    job_radix = math.floor(job_db.id / 100) * 100
    job_path  = os.path.join(jobs_path, str(job_radix), "job-%s" % str(job_db.id))
    if not os.path.exists(job_path):
      Log.error("Cannot find log for job with id %s" % (self.options.job))
      return -1

    log_path = os.path.join(job_path, "job-%s.log" % (str(job_db.id)))
    if not os.path.exists(log_path):
      Log.error("Cannot find log for job with id %s" % (self.options.job))
      return -1

    f = open(log_path, 'r')

    # Seek through the file (other tools, such as the web portal, require this)
    if not self.options.seek is None:
      f.seek(self.options.seek)

    log = f.read()
    Log.output(log, end="", padding="")

    sleepTime = 0.1

    # If tail is specified, continue to print any new information
    if not self.options.tail is None and job_db.status == "running":
      while True:
        readable, writable, exceptional = select.select([f], [], [f])

        for reader in readable:
          log = f.read()
          if len(log) == 0:
            time.sleep(sleepTime)
            sleepTime *= 2
            if sleepTime > 3:
              sleepTime = 3
          else:
            sleepTime = 0.1
            Log.output(log, end="", padding="")

    # Close the log
    f.close()
