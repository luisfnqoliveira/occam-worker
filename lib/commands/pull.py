# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.commands.install      import Install
from lib.commands.build        import Build
from lib.db             import DB
from lib.git            import Git
from lib.object         import Object

from datetime import datetime

from subprocess import Popen, PIPE

# For pulling network http resources
try:
  from urllib.request import Request, urlopen  # Python 3
except:
  from urllib2 import Request, urlopen  # Python 2

from urllib.error import HTTPError  # Python 3

from lib.log import Log

class Pull:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-t", "--task",      action = "store_true",
                                           dest   = "pull_task",
                                           help   = "will pull any necessary objects needed to run this object")
    return parser

  def do(self):
    Log.header("Pulling object")

    git_path = None

    # Grab the URL from the command line arguments
    url = None
    if len(self.args) > 1:
      url = self.args[1]

    if url is None:
      Log.error("no object ID or url given")
      return -1

    # TODO: object at git path
    # TODO: object at local path

    if self.occam.isUUID(url):
      obj = self.occam.nodes.pullAllObjects(url)
    elif self.occam.network.isURL(url):
      # pull directly from this url
      # TODO
      Log.warning("not implemented")
      return -1
    else:
      # pull from path
      obj = Object(url, occam=self.occam)
      self.occam.objects.store(obj)

    if obj is None:
      Log.error("could not pull the object")
      return -1

    # Pull any objects determined by other nodes to be necessary to build/run
    # this one
    if self.options.pull_task:
      # TODO: update for more than one backend
      docker_backend = self.occam.backends.handlerFor('docker')
      backend = docker_backend.provides()[0]
      objInfo = obj.objectInfo();
      hypotheticalTask = self.occam.nodes.taskFor(backend[0], backend[1],
                           objInfo.get('environment'),
                           objInfo.get('architecture'))
      objs = self.occam.taskObjectsFor(hypotheticalTask)
      for discoveredObj in objs:
        Log.write("Discovered object %s %s" % (discoveredObj.get('type'),
                                               discoveredObj.get('name')))

        # Pull this object
        self.occam.nodes.pullAllObjects(discoveredObj.get('id'))

    Log.done("discovered %s %s" % (obj.objectInfo()['type'], obj.objectInfo()['name']))
    return 0
