# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling

from lib.db             import DB
from lib.workset        import Workset

from models.system           import System

from lib.log import Log

class Status:
  """
  This class handles listing the current context and workset from the
  command-line.
  """
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-g", "--groups",      action = "store_true",
                                             dest   = "groups",
                                             help   = "just lists groups")
    parser.add_option("-e", "--experiments", action = "store_true",
                                             dest   = "experiments",
                                             help   = "just lists experiments")
    parser.add_option("-p", "--paths",       action = "store_true",
                                             dest   = "paths",
                                             help   = "lists directory names instead of object names")
    return parser

  def findContextWithPaths(self,p1):
    pass

  def do(self):
    # Look for context. Which workset are we in?
    # Base it off the current directory and go up the path until we find a base
    # object we understand
    path = "."

    # Gather our context
    obj = self.occam.objectAt(path)

    # Retrieve the workset metadata
    workset = self.occam.objectAt(path, searchFor='workset')

    if workset is None and obj is None:
      Log.error("not within a workset or object")
      return -1

    groups      = obj.groups()
    experiments = obj.experiments()
    objects     = obj.objects()

    if self.options.groups:
      for group in groups:
        if self.options.paths:
          Log.output(group.path, padding="")
        else:
          Log.output(group.objectInfo()["name"], padding="")
      return 0
    elif self.options.experiments:
      for experiment in experiments:
        if self.options.paths:
          Log.output(experiment.path, padding="")
        else:
          Log.output(experiment.objectInfo()["name"], padding="")
      return 0

    Log.header(Log.Header.STATUS)

    # for every group, list the group/etc we are in
    current_obj = obj
    while not current_obj is None:
      object_info = current_obj.objectInfo()
      Log.write("in %s %s (%s) @ %s" % (object_info["type"], object_info["name"], object_info["id"], current_obj.head()))
      current_obj = current_obj.parent()

    # Print out groups
    if len(groups) > 0:
      Log.header("Seeing groups")
      for group in groups:
        if self.options.paths:
          Log.write(group.path)
        else:
          Log.write(group.objectInfo()["name"])

    # Print out experiments
    if len(experiments) > 0:
      Log.header("Seeing experiments")
      for experiment in experiments:
        if self.options.paths:
          Log.write(experiment.path)
        else:
          Log.write(experiment.objectInfo()["name"])

    # Print out objects
    if len(objects) > 0:
      Log.header("Seeing objects")
      for obj in objects:
        if self.options.paths:
          Log.write(obj.path)
        else:
          object_info = obj.objectInfo()
          Log.write("%s (%s) [%s] @ %s" % (object_info.get('name'),
                                           object_info.get('type'),
                                           object_info.get('state'),
                                           object_info.get('revision')))

    # If within an experiment, print out the workflow
    # TODO: refactor for workflow objects
    object_info = obj.objectInfo()
    if "workflow" in object_info and "connections" in object_info["workflow"]:
      Log.header("Workflow")
      i = 0
      for connection in object_info["workflow"]["connections"]:
        revision = connection["object"].get("revision") or ""
        if revision != "":
          revision = " @ %s" % revision
        if connection["object"].get("local"):
          location = "local"
        else:
          location = "global"
        if connection["object"].get('created', False):
          location = "generated"
        Log.write("%s - %s %s%s [%s]" % (i, connection["object"]["type"], connection["object"]["name"], revision, location))
        i += 1

    return 0
