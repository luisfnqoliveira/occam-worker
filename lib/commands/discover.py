# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from lib.log import Log

class Discover:
  """
  This command will discover an OCCAM node that this node may use in the
  future to discover objects.

  Usage:

  occam discover <HOST>
  occam discover <HOST>:<PORT>

  Examples:

  Discovery by hostname:

  $ occam discover occam.cs.pitt.edu

  Discovery by ip address and port:

  $ occam discover 10.0.0.1:9292
  """

  def __init__(self, opts, args, occam):
    """
    Instantiate this command handler.
    """

    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    """
    Add specific command line options.
    """

    return parser

  def do(self):
    """
    Perform the command.
    """

    name = None
    url = None

    if len(self.args) > 1:
      url = self.args[1]

    db_node = self.occam.nodes.discover(url)

    if db_node is None:
      Log.error("could not verify the OCCAM server as given")
      return -1
    name = db_node.name

    if db_node is None:
      Log.error("Could not create the Node")
      return -1

    Log.done("Successfully discovered %s" % (name))
    return 0
