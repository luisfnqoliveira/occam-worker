# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import re
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)
import yaml        # Output in YAML

from lib.log            import Log
from lib.db             import DB
from lib.git            import Git
from lib.object         import Object

from models.system           import System

class Set:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-i", "--input-type",  action  = "store",
                                             dest    = "input_type",
                                             default = "text",
                                             help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the object uuid to build")
    parser.add_option("-w", "--within",   action = "store",
                                          dest   = "within_object",
                                          help   = "the id of the object to clone this object within")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the revision of the object to clone this object within")
    return parser

  def do(self):
    # Determine the revision we will use
    revision = self.options.revision or "HEAD"

    obj_id = None
    tempPath = False
    path = None

    key = None
    value = None

    if self.options.revision:
      revision = self.options.revision

    if self.options.to_object:
      obj_id = self.options.to_object

      workset, obj, path = self.occam.temporaryClone(self.options.to_object, revision, self.options.within_object, self.options.within_revision)
      tempPath = True
    else:
      path = "."
      obj = Object('.', occam=self.occam)

    if obj is None and obj_id is None: #and obj_type is None and obj_name is None:
      Log.error("no object id given")
      return -1

    # Takes a key specified in the argument list, and turns it into an array
    # that follows the hierarchy.
    def splitKey(k):
      keys = re.split(r"([^\\])\.", k)
      new_keys = []
      for i in range(0, len(keys)-1, 2):
        new_keys.append((keys[i] + keys[i+1]).replace('\\', ''))
      new_keys.append(keys[len(keys)-1].replace('\\', ''))

      return new_keys

    new_data = {}

    # TODO: error when len(self.args) is not even
    for i in range(1, len(self.args), 2):
      k = self.args[i]
      if len(self.args) > (i+1):
        v = self.args[i+1]

        # Form value
        if self.options.input_type == 'json':
          # TODO: Catch exception
          v = json.loads(v)
      else:
        v = None

      keys = splitKey(k)

      hash = new_data

      for subkey in keys[0:-1]:
        if isinstance(hash, list):
          subkey = int(subkey)

        if not subkey in hash:
          hash[subkey] = {}

        hash = hash[subkey]

      if v is None:
        Log.write("Removing %s" % (keys[-1]))
      else:
        Log.write("Setting %s to %s" % (keys[-1], str(v)[0:50]))
      hash[keys[-1]] = v

    if not new_data is None:
      obj_info = obj.objectInfo()

      def setIterate(subhash, info):
        if isinstance(subhash, dict):
          it = subhash.items()
        else:
          it = enumerate(subhash)

        for key, value in it:
          if isinstance(info, list):
            key = int(key)

            if key >= len(info) or key < 0:
              Log.warn("Index not valid.")
              return

          if not value is None:
            if isinstance(info, dict) and not key in info:
              # We just add it all
              info[key] = value
            else:
              if isinstance(value, dict):
                if not isinstance(info, dict) and not isinstance(info, list):
                  # Replace the scalar value with a hash
                  info[key] = {}
                setIterate(value, info[key])
              else:
                info[key] = value
          elif isinstance(info, list):
            # Removes the item from the list
            del info[key]
          elif key in info:
            # Removes the key from the hash
            del info[key]

      setIterate(new_data, obj_info)

      revisions = obj.updateObject(obj_info, 'changing value of %s' % (self.args[1]))

      Log.write("New revision: ", end="")
      Log.output(revisions[0], padding="")
      if (len(revisions) > 1):
        Log.write("New workset revision: ", end="")
        Log.output(revisions[-1], padding="")

      Log.done("Updated values in %s %s" % (obj_info.get('type', 'unknown'),
                                            obj_info.get('name', 'unknown')))
      return 0
    else:
      Log.error("There is no new data.")
      return -1
