# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import json
import codecs
import os

from lib.log import Log

class Test:
  """
  This class will handle the command that will run the tests for occam-worker.
  """

  def __init__(self, opts, args, occam):
    """
    Construct a test command class.
    """
    self.options     = opts
    self.args        = args
    self.occam       = occam


  @staticmethod
  def initParser(parser):
    """
    Amend the specific arguments for this command.
    """

    parser.add_option("-d", "--dirty", action = "store_true",
                                       dest   = "dirty",
                                       help   = "will display without colors and pretty text")

    parser.add_option("-c", "--color", action = "store_true",
                                       dest   = "color",
                                       help   = "will display with colors")

    parser.add_option("-j", "--json", action = "store_true",
                                      dest   = "json",
                                      help   = "will output as json")

    return parser

  def pathToObject(self, path):
    """
    Turns a path "tests/commands/test_run.py" into the python module path lib, commands, run.py
    """

    parts = path.split(os.sep)
    parts[0] = 'lib'
    parts[-1] = parts[-1][5:]

    return parts

  def correctFunctionName(self, string):
    """
    Will turn something like DetermineObject into determineObject.
    """

    if len(string) > 0:
      string = string[0].lower() + string[1:]

    return string

  def underscoreStringToDescription(self, string):
    parts = string.split('_')

    ret = parts[0]
    for piece in parts[1:]:
      ret = ret + " " + piece

    return ret

  def testStringToObject(self, string):
    ret = []
    for item in string.split('::'):
      if item.startswith('Test'):
        item = item[4:]
      elif item.startswith('test_'):
        item = item[5:]
        item = self.underscoreStringToDescription(item)

        # Assume last item was a function name
        if len(ret) > 0:
          ret[-1] = self.correctFunctionName(ret[-1])
      else:
        item = None

      if not item is None:
        ret.append(item)

    return ret

  def interpretLine(self, line):
    if len(line) == 0:
      return []

    result = line[0]

    if len(line) == 1:
      return [result]

    line = line[2:]

    if result == " ":
      return [result, line]

    parts = line.split('::', 1)
    path = parts[0]
    parts = parts[1]
    objectParts = self.pathToObject(path)
    return [result] + objectParts + self.testStringToObject(parts)

  def outputLineAsJSON(self, stack, parts):
    result = parts[0]
    if result == " ":
      return stack

    parts = parts[1:]
    for i, part in enumerate(parts):
      if len(stack) <= i or stack[i] != part:
        preceding = ""
        succeeding = ": {"
        symbol = ""
        color = "0"

        if i == len(parts) - 1:
          pass

        # Output closing braces for all parts of the stack we are now leaving
        for j in reversed(range(i,len(stack)-1)):
          end = "}"
          if j == len(stack) - 2:
            end = "]"
          Log.output("\n" + ("  " * (j+1)) + end, end="", padding="")

        if len(stack)-1 >= i and i > 0:
          preceding = ","

        if i == len(parts)-2:
          succeeding = ": ["
        if i == len(parts)-1:
          part = {
            "message": part,
            "result":  result
          }
          succeeding = ""

        Log.output(preceding + "\n" + ("  " * (i+1)) + json.dumps(part) + succeeding, end="", padding="")
        stack = stack[0:i]
        stack.append(part)
    return stack

  def outputLine(self, stack, parts):
    result = parts[0]
    if result == " ":
      return stack

    parts = parts[1:]
    for i, part in enumerate(parts):
      if len(stack) <= i or stack[i] != part:
        preceding = ""
        succeeding = ""
        symbol = ""
        color = "0"

        if i == len(parts) - 1:
          symbol = result

          if result == ".":
            color = "1;32"
            if not self.options.dirty:
              symbol = "✓"
          elif result == "F":
            color = "1;31"
            if not self.options.dirty:
              symbol = "×"

          if self.options.color or not self.options.dirty:
            preceding = "\x1b[" + color + "m"
            succeeding = "\x1b[0m"

          part = symbol + " " + part

        Log.output(("  " * i) + preceding + part + succeeding, padding="")
        stack = stack[0:i]
        stack.append(part)

    return stack

  def parseStream(self, reader, json=False):
    output = ""
    stack = ["__root__"]

    if json:
      Log.output("{", padding="", end="")

    try:
      reading = True
      while reading:
        stream = reader.read(128)
        if stream is None or len(stream) < 128:
          reading = False
        output = output + stream

        # Interpret py.test report
        lines = output.split('\n')

        for line in lines[0:-1]:
          allParts = self.interpretLine(line)
          if json:
            stack = self.outputLineAsJSON(stack, allParts)
          else:
            stack = self.outputLine(stack, allParts)

        output = lines[-1]
    except:
      output = ""

    if json:
      for j in reversed(range(0,len(stack)-1)):
        end = "}"
        if j == len(stack) - 2:
          end = "]"
        Log.output("\n" + ("  " * (j+1)) + end, padding="", end="")
      Log.output("\n}", padding="")

  def do(self):
    """
    Run the command.
    """

    Log.header("Running tests")

    # Run py.test and send its report to stderr and throw away stdout
    our_path = os.path.join(os.path.dirname(__file__), '..', '..')
    p = subprocess.Popen(['py.test', '-q', '--result-log', '/dev/stderr'], stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, cwd=our_path)

    # We will read stderr as a unicode stream
    reader = codecs.getreader('utf-8')(p.stderr)

    # We will form an output and parse each line
    self.parseStream(reader, json=self.options.json)

    p.wait()
