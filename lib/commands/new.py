# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import getpass     # For entering a password for person create
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.workset    import Workset
from lib.occam      import Occam
from lib.git        import Git
from lib.group      import Group
from lib.object     import Object
from lib.person     import Person
from lib.experiment import Experiment

from uuid import uuid1

from lib.log import Log

from subprocess import Popen, PIPE

# TODO: handle when group already exists or inconsistency on disk
# TODO: add .occam clone/copy/backup of group (especially when removed)

class New:
  """
  This class adds a new object to the current container object, generally a workset.
  It will first have to find out the context (which workset are we in) and then
  do the appropriate set-up to create the new type of object requested.
  """
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object to add the new object as a dependency")
    parser.add_option("-p", "--password",  action = "store",
                                           dest   = "password",
                                           help   = "the password to use to create a new person object")
    parser.add_option("-a", "--as",        action = "store",
                                           dest   = "author_uuid",
                                           help   = "the id of the person that creates this object")
    parser.add_option("-i", "--internal",  action = "store_true",
                                           dest   = "internal",
                                           help   = "whether or not to just create this in the repository without also creating a directory on disk")
    parser.add_option("-t", "--to",        action = "store",
                                           dest   = "to_object",
                                           help   = "the object to add the new object as a dependency")
    parser.add_option("-w", "--within",    action = "store",
                                           dest   = "within_object",
                                           help   = "the object to add the new object as a dependency")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the object to add the new object as a dependency")
    parser.add_option("-o", "--role",            action  = "append",
                                                 dest    = "roles",
                                                 default = [],
                                                 type    = str,
                                                 help    = "adds the given role to a new person object")
    return parser

  def do(self):
    # Look for context. Which workset are we in?
    # Base it off the current directory and go up the path until we find a base
    # object we understand

    type = None
    if len(self.args) > 1:
      type = self.args[1]

    name = None
    if len(self.args) > 2:
      name = self.args[2]

    path = None
    revision = None
    workset = None
    obj = None

    if type is None:
      raise Exception("no type of object given")

    if name is None:
      if type == "id":
        # Print out a new uuid
        Log.output(Object.uuid(''))
        return 0

      raise Exception("no name given")

    if self.options.revision:
      revision = self.options.revision

    tempPath = False

    if self.options.to_object:
      # TODO: handle when workset isn't found
      workset, obj, path = self.occam.temporaryClone(self.options.to_object, revision, self.options.within_object, self.options.within_revision)
      tempPath = True
      dest_obj_type = obj.objectInfo()['type']
      dest_obj_name = obj.objectInfo()['name']
      Log.noisy("adding %s %s to %s %s at %s in %s" % (type, name, dest_obj_type, dest_obj_name, revision, path))
    elif self.options.internal:
      tempPath = True
      path = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
    else:
      # We are adding to the current object
      path = "."

    safe_type = type.replace('/', '-').replace(' ', '-')
    safe_name = name.replace('/', '-').replace(' ', '-')

    base_path = path
    path = os.path.realpath(path)
    if len(self.args) > 3:
      path = self.args[3]
      Log.write("placing object in %s" % (path))

    Log.header("Creating new %s" % (type))
    object_path = os.path.realpath(os.path.join(path, "%s-%s" % (safe_type, safe_name)))

    if type == "workset":
      # Workset creation
      object_path = os.path.realpath(os.path.join(path, safe_name))
      workset = self.occam.addWorkset(object_path, safe_name)

      # Add the current person as the author
      if self.options.author_uuid:
        # TODO: handle when person is not found
        person = Person(self.occam.objects.pathFor(self.options.author_uuid), occam=self.occam)
      else:
        person = self.occam.currentPerson()

      Log.output(workset.objectInfo()['id'])

      if person:
        if base_path == '.':
          # TODO: handle case where we create local links elsewhere (internal = false)
          self.occam.createLocalLink(workset.path, workset, person)
        workset.addAuthor(person)
        Log.done("Successfully created workset %s as %s" % (name, person.objectInfo()['name']))
      else:
        Log.done("Successfully created workset %s" % (name))

      return 0
    elif type == "person":
      # We need a special case for 'people'
      accounts = self.occam.searchAccounts(name)
      if accounts.count() > 0:
        Log.error("Username already taken.")
        return -1

      # Get a password

      password = None
      if self.options.password:
        password = self.options.password
      else:
        password = getpass.getpass('\npassword for %s: ' % (name))

      roles = self.options.roles
      obj = self.occam.addPerson(name)
      self.occam.addAuthentication(obj.objectInfo()['id'], password, roles)
      Log.write("new person uuid: ", end="")
      Log.output(obj.objectInfo()['id'], padding="")

      Log.done("Successfully created %s" % (name))
      return 0

    # Get the current workset
    if workset is None:
      workset = self.occam.objectAt(path, searchFor='workset')

    # Get the current object
    if obj is None:
      obj = self.occam.objectAt(path, recurse=True)
      if not obj is None:
        obj.root = workset

    parent = self.occam.objectAt(path, recurse=True)

    container_object = None
    if not workset is None:
      container_object = workset.objectInfo()

    if type == "collaborator" or type == "author":
      Log.header("Adding new %s" % (type))

      # Search for the person by their name (or just use the id??)
      person = self.occam.searchPeople(keyword = name).first()
      # TODO: handle ambiguity by listing choices with ids!
      if person is None:
        Log.error("cannot find the person %s" % (name))
        return -1

      person = Person(person.path)

      try:
        if type == "collaborator":
          done = workset.addCollaborator(person)
        elif type == "author":
          done = workset.addAuthor(person)
      except Exception as e:
        msg = e
        Log.error("failure: %s" % (msg))
        return -1

      if done:
        Log.done("added %s as a %s to workset %s" % (name, type, container_object['name']))

      return 0

    if container_object and container_object['type'] == "workset":
      if "name" in container_object:
        Log.write("adding to workset %s" % (container_object["name"]))

    # Create directory, if it doesn't exist, warn if it does
    if os.path.exists(object_path):
      Log.error("directory %s already exists" % (object_path))
      return -1
    else:
      if obj:
        if type == 'group':
          new_obj = obj.addGroup(name)
        elif type == 'experiment':
          new_obj = obj.addExperiment(name, path)
        else:
          new_obj = obj.addObject(name, type)
        new_revision = obj.git.head()
        Log.output("%s" % (new_revision))
      else:
        # Create basic object
        new_obj = Object.create(object_path, name, type, root=workset)

    Log.output("%s" % (new_obj.objectInfo()['id']))

    if tempPath:
      Log.noisy("removing %s" % path)
      shutil.rmtree(path)

    Log.done("Successfully created %s %s" % (type, name))

    return 0
