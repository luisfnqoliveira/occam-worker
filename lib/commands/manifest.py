# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pprint import pprint

import os
import sys
import json
import shutil
import re

from datetime import datetime
from time import sleep
from uuid import uuid1

from lib.db                import DB
from lib.log               import Log
from lib.object            import Object
from lib.experiment        import Experiment
from lib.task_builder      import TaskBuilder
from lib.run_builder       import RunBuilder

import subprocess

class Manifest:
  """
  This class will handle generating a VM manifest for a given object or for a
  given target environment.
  """

  def __init__(self, opts, args, occam):
    """
    Construct a runner class.
    """
    self.options     = opts
    self.args        = args
    self.occam       = occam

  @staticmethod
  def initParser(parser):
    """
    Amend the specific arguments for this command.
    """

    parser.add_option("-r", "--revision", action = "store",
                                          dest   = "revision",
                                          help   = "the revision of the object the manifest would run")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the object that the manifest would run")
    parser.add_option("-e", "--environment", action = "store",
                                             dest   = "environment",
                                             help   = "the environment to target the VM")
    parser.add_option("-a", "--architecture", action = "store",
                                              dest   = "architecture",
                                              help   = "the architecture to target the VM")
    parser.add_option("-E", "--target-environment", action = "store",
                                             dest   = "target_environment",
                                             help   = "the environment the VM would run on")
    parser.add_option("-A", "--target-architecture", action = "store",
                                              dest   = "target_architecture",
                                              help   = "the architecture the VM would run on")
    return parser

  def do(self, recursive=False):
    environment  = self.options.environment
    architecture = self.options.architecture

    objectId = self.options.to_object
    objectRevision = self.options.revision

    objectInfo = None

    targetEnvironment  = self.options.target_environment
    targetArchitecture = self.options.target_architecture

    if environment is None or architecture is None:
      # Pull the environment and architecture from the object
      if objectId is None:
        Log.error("Either an object uuid or an explicit environment/architecture is required.")
        return -1

      object = self.occam.objects.retrieve(objectId, revision=objectRevision)
      environment = object.objectInfo().get('environment')
      architecture = object.objectInfo().get('architecture')
      objectInfo = object.objectInfo()

      if environment is None or architecture is None:
        Log.error("Specified object does not have an environment or architecture listed.")
        return -1

    if objectInfo is None:
      objectInfo = {
        "environment":  environment,
        "architecture": architecture
      }

    # Generate task manifest
    taskInfo = self.occam.taskFor(objectInfo, environmentGoal=targetEnvironment, architectureGoal=targetArchitecture)

    # Output the task json
    Log.output(json.dumps(taskInfo))

    Log.done("Generated task manifest")
    return 0
