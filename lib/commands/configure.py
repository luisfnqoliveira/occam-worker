# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)
import re          # For parsing validator tests
import math        # For validator tests

from copy import deepcopy

from lib.log            import Log
from lib.git            import Git
from lib.db             import DB
from lib.object         import Object
from lib.experiment     import Experiment

class Configure:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-d", "--description",  action = "store_true",
                                              dest   = "description",
                                              help   = "prints out a description of the configuration option")
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object to add the new object as a dependency")
    parser.add_option("-t", "--to",        action = "store",
                                           dest   = "to_object",
                                           help   = "the object to add the new object as a dependency")
    parser.add_option("-w", "--within",    action = "store",
                                           dest   = "within_object",
                                           help   = "the object to add the new object as a dependency")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the object to add the new object as a dependency")
    parser.add_option("--remove-files", action = "store_true",
                                        dest   = "remove_files",
                                        help   = "remove all of the current files currently attached as input to this object")
    parser.add_option("-f", "--file", action  = "append",
                                      dest    = "files",
                                      default = [],
                                      type    = str,
                                      help   = "adds the given path within the object as an input")
    return parser

  class Tokens:
    def __init__(self):
      self.tokens = []

    def push(self, token, data=None):
      self.tokens.append(token)
      if token == "identifier" or token == "number":
        self.tokens.append(data)

    def tokenize(self, str):
      last_character = ''
      state = 0
      token = ""

      for c in str:
        if state == 0:
          # Reading identifier
          if re.match(r'[*+-\/()%=<>!]', c):
            # Switch states
            state = 1

            # Push token
            if re.match(r'^\d+$', token):
              self.push("number", token)
            elif (token == "x" or token == "log2" or token == "floor"):
              self.push("identifier", token)
            elif len(token):
              return False

            token = ""
            last_character = ""
        elif state == 1:
          # Reading symbol
          if c == '*' and last_character == '*' and len(token) == 1:
            # **
            pass
          elif (c == '=' and (last_character == '<' or
                              last_character == '>' or
                              last_character == '=' or
                              last_character == '!') and len(token) == 1):
            # ==, <=, >=, !=
            pass
          elif re.match(r'\d|\w', c):
            # Switch states
            state = 0

            # Push token
            self.push(token)
            token = ""
            last_character = ""
          elif (re.match(r'[*+-\/()%=<>!]', c)):
            # Push token
            self.push(token)
            token = ""
            last_character = ""

        if c == ' ':
          self.push(token)
          token = ""
          last_character = ""
        else:
          token = token + c
          last_character = c

      self.push(token)

      return self.tokens

  def compile(code):
    tokens = Configure.Tokens().tokenize(code)

    lastToken = ""
    leftParens = 0
    output = ""

    for token in tokens:
      if lastToken == "identifier":
        if token == "log2":
          output += "log2"
        elif token == "floor":
          output += "floor"
        elif token == "ceiling":
          output += "ceiling"
        elif token == "log":
          output += "log"
        elif token == "x":
          output += "x"
      elif lastToken == "number":
        output += "(" + token + ")"
      elif (token == "identifier" or token == "number"):
        pass
      elif token == "==":
        output += "=="
      else:
        if token == "(":
          leftParens += 1
        elif token == ")":
          if leftParens < 1:
            return False
          leftParens -= 1
        output = output + token

      lastToken = token

    def ceiling(x):
      return math.ceil(x)

    def floor(x):
      return math.floor(x)

    def log2(x):
      return math.log(x,2)

    def log(x,y):
      return math.log(x,y)

    eval_context = {
      'floor':   floor,
      'ceiling': ceiling,
      'log2':    log2,
      'log':     log,
    }

    return output, eval_context

  def execute(code, value):
    output, eval_context = Configure.compile(code)

    # TODO: is this safe???
    return eval(output, {'x': value}, eval_context)

  def validate(value, schema):
    # Pull out ranged value, if ranged
    if isinstance(value, str) and schema['type'] in ['int', 'float'] and (value.find('...', 1) >= 0 or value.find(',', 1) >= 0):
      # Split on commas, and then split by ranges
      ret = [False, "Range is invalid."]
      for section in value.split(','):
        parts = [x.strip() for x in section.split(':')]
        step = "x+1"
        if len(parts) > 1:
          section = parts[0]
          step = parts[1]

        span = [x.strip() for x in section.split('...')]
        start = span[0]
        if len(span) > 1:
          final = span[1]
        else:
          final = span[0]

        # Validate that there is a valid entry within the range
        iterations = 0

        i = int(start)
        passed = False
        lastError = [False, "Range does not include any items."]
        while i <= int(final) and iterations < 50:
          lastError = Configure.validate(i, schema)
          if lastError[0] == True:
            passed = True
            break

          i = Configure.execute(step, i)
          iterations += 1

        ret[0] = passed

        if ret[0] == False:
          if start != final:
            ret[1] = "range does not include a valid value: " + lastError[1]
          return ret

      ret[1] = value
      return ret

    type = schema.get('type')
    if type is None:
      type = schema

    # Type check
    if isinstance(type, list):
      # Array (enum)
      if not value in type:
        return [False, "must be one of the following: %s" % (type)]
    elif type == "int":
      # Integer type
      try:
        value = int(value)
      except(ValueError):
        return [False, "must be an integer"]
    elif type == "float":
      # Floating Point type
      try:
        value = float(value)
      except(ValueError):
        return [False, "must be a float"]
    elif type == "boolean":
      # Floating Point type
      if value in ["true", "yes", "on"]:
        value = True
      elif value in ["false", "no", "off"]:
        value = False
      else:
        return [False, "must be a boolean: %s" % (value)]

    # Validations
    validations = schema.get('validations') or []
    for validation in validations:
      if 'min' in validation:
        # Min test
        if type == "int":
          minimum = int(validation['min'])
        elif type == "float":
          minimum = float(validation['min'])

        if value < minimum:
          return [False, "must be at least %s" % (minimum)]

      if 'max' in validation:
        # Max test
        if type == "int":
          maximum = int(validation['max'])
        elif type == "float":
          maximum = float(validation['max'])

        if value > maximum:
          return [False, "must be at most %s" % (maximum)]

      if 'test' in validation:
        # Coded test
        if not Configure.execute(validation['test'], value):
          return [False, validation.get('message') or "did not validate %s" % (validation['test'])]

    return [True, value]

  def do(self):
    Log.header("Configuring Attached Workflow Object")

    # Pull out the object revision to change
    if self.options.revision:
      revision = self.options.revision

    tempPath = False
    path = None
    experiment = None

    # Look for context. Are we in an experiment?
    if self.options.to_object:
      # TODO: handle when workset isn't found
      workset, experiment, workset_path = self.occam.temporaryClone(self.options.to_object, revision, self.options.within_object, self.options.within_revision)
      path = experiment.path
      tempPath = True
    else:
      # We are adding to the current object
      path = "."

      experiment = self.occam.objectAt(path)

    new_data = None
    key = None
    files = self.options.files

    if len(self.args) > 2:
      key = self.args[2]

    # Takes a key specified in the argument list, and turns it into an array
    # that follows the hierarchy.
    def splitKey(k):
      keys = re.split(r"([^\\])\.", k)
      new_keys = []
      for i in range(0, len(keys)-1, 2):
        new_keys.append((keys[i] + keys[i+1]).replace('\\', ''))
      new_keys.append(keys[len(keys)-1].replace('\\', ''))

      return new_keys

    if len(self.args) > 3:
      new_data = {}

      # TODO: error when len(self.args) is not even
      for i in range(2, len(self.args), 2):
        k = self.args[i]
        v = self.args[i+1]
        keys = splitKey(k)

        hash = new_data

        for subkey in keys[0:-1]:
          if not subkey in hash:
            hash[subkey] = {}
          hash = hash[subkey]

        hash[keys[len(keys)-1]] = v

    if experiment is None or not experiment.objectInfo()['type'] == 'experiment':
      Log.error("Not within an experiment.")
      return -1

    object_json = experiment.objectInfo()

    if not "type" in object_json or object_json["type"] != "experiment":
      Log.error("not within an experiment.")
      return -1

    if not "workflow" in object_json or not "connections" in object_json["workflow"]:
      Log.error("experiment has no workflow.")
      return -1

    # Get the workflow node (connection index) to configure
    if len(self.args) > 1:
      node_id = self.args[1]
    else:
      Log.error("node identifier required")
      return -1

    if len(object_json["workflow"]["connections"]) <= int(node_id):
      Log.error("node identifier invalid")
      return -1
    connection = object_json["workflow"]["connections"][int(node_id)]

    type = None
    name = None
    revisions = None
    if "object" in connection:
      type = connection["object"].get("type")
      name = connection["object"].get("name")
      obj_revision = connection["object"].get('revision') or ""
      uuid = connection["object"].get('id')

      if not "configuration" in connection["object"]:
        object = self.occam.objects.search(uuid=uuid).first()
        if object is None:
          Log.error("cannot find %s object %s" % (type, name))
          exit(-1)
          return

        objectPath = self.occam.objects.pathFor(object.uid)
        obj = Object(objectPath, revision=obj_revision, occam=self.occam)
        if Git.fullRevision(objectPath, obj_revision):
          # Open the object within git repo from a revision
          objectInfo = obj.objectInfo()
        else:
          # If we cannot find a git repo, just open the object as is.
          f = open('%s/object.json' % (objectPath))
          objectInfo = json.load(f)
          f.close()

        # Pull out configuration schema for this object
        def set_default(schema, current):
          for k in (schema.get("__ordering") or schema.keys()):
            v = schema[k]
            if "type" in v and not isinstance(v["type"], dict):
              # Item
              if "default" in v:
                if "type" in v:
                  # Ensure the correct type, just in case
                  if v["type"] == "int":
                    v["default"] = int(v["default"])
                  elif v["type"] == "float":
                    v["default"] = float(v["default"])
                current[k] = v["default"]
              else:
                Log.warning("no default for key %s" % (k))
            else:
              # Group
              if not k in current:
                current[k] = {}
              set_default(v, current[k])

        configurations_path = os.path.join(path, "config")
        if not os.path.exists(configurations_path):
          Log.write("creating configurations directory")
          os.mkdir(configurations_path)

        configurations_path = os.path.join(configurations_path, node_id)
        if not os.path.exists(configurations_path):
          Log.write("creating object configuration directory")
          os.mkdir(configurations_path)

        # For every configuration available for this object, create a config
        # file containing the defaults when the file doesn't already exist.
        for configuration in (objectInfo.get('configurations') or []):
          ret = {}
          configuration_filename = configuration['file']
          configuration_name = configuration['name']
          schema = configuration['schema']

          if isinstance(schema, str):
            schema = obj.git.retrieveJSON(schema)

          # TODO: handle when schema_file does not exist
          #       this is when it wants configuration placed inside the
          #       object.json of the runner

          configuration_file_path = os.path.join(configurations_path, configuration_filename)

          if not os.path.exists(configuration_file_path):
            Log.write("adding default configuration %s for %s object %s" % (configuration_name, type, name))

            set_default(schema, ret)

            f = open(configuration_file_path, 'w+')
            f.write(json.dumps(ret))
            f.close()

            Log.write("writing %s" % (configuration_file_path))

            # Update object
            # TODO: remove this git nonsense
            experiment.purgeGenerates()

            experiment.git.add(configurations_path)
            revisions = experiment.commit('Updates configuration')
          else:
            Log.write("configuration %s already exists. doing nothing." % (configuration_name))

        # List configuration option/all options
        for configuration in (objectInfo.get('configurations') or []):
          configuration_filename = configuration['file']

          # When editing values, we will need the schema to validate them
          if not new_data is None:
            schema = configuration['schema']

            if isinstance(schema, str):
              schema = obj.git.retrieveJSON(schema)

          if key is None:
            Log.header(configuration['name'])

          # TODO: handle when file does not exist this is when it wants
          # configuration placed inside the object.json of the runner

          configuration_file_path = os.path.join(configurations_path, configuration_filename)

          if os.path.exists(configuration_file_path):
            config = json.load(open(configuration_file_path, 'r'))

            def search_hash(hash, schema_hash, find_hash={}, remaining={}):
              for k, v in hash.items():
                if isinstance(v, dict):
                  if k in find_hash:
                    search_hash(v, schema_hash[k], find_hash[k], remaining[k])
                    if not remaining[k]:
                      Log.write("ok %s: %s" % (k, remaining[k]))
                      del remaining[k]
                  else:
                    search_hash(v, schema_hash[k], find_hash, remaining)
                elif k in find_hash:
                  # Edit configuration
                  if not new_data is None:
                    value = find_hash[k]
                    if not isinstance(value, dict):
                      # Validate
                      test, value = Configure.validate(value, schema_hash[k])
                      if test == False:
                        Log.error("value does not validate: %s" % (value))
                      else:
                        hash[k] = value

                      if not k in remaining:
                        # TODO: handle ambiguity of configuration parameters
                        pass
                      else:
                        Log.write("found %s" % (k))
                        del remaining[k]

                  Log.header(configuration['name'])

                  # Print value
                  Log.write("%s: %s" % (k, hash[k]))

                  if new_data is None and self.options.description:
                    # Print description
                    if "description" in schema[k]:
                      Log.header("Description")
                      first_line = True
                      for line in schema_hash[k]["description"].split('\n'):
                        printed = ""
                        for word in line.split(' '):
                          if printed != "" and len(printed) + len(word) > 65:
                            if first_line:
                              Log.write("description: %s" % (printed))
                              first_line = False
                            else:
                              Log.write("           : %s" % (printed))
                            printed = ""
                          printed += word + " "
                        if first_line:
                          Log.write("description: %s" % (printed))
                          first_line = False
                        else:
                          Log.write("           : %s" % (printed))

                    # Print validations
                    if "validations" in schema[k]:
                      Log.header("Validations")
                      for validation in schema[k]["validations"]:
                        if "min" in validation:
                          Log.write("Must be at least %s" % (validation["min"]))
                        if "max" in validation:
                          Log.write("Must be at most %s" % (validation["max"]))
                        if "test" in validation:
                          Log.write(validation.get("message") or validation["test"])

              return ret

            def output_hash(hash, indent=""):
              for k, v in hash.items():
                if isinstance(v, dict):
                  Log.write("%s%s:" % (indent, k))
                  output_hash(v, indent + "  ")
                else:
                  Log.write("%s%s: %s" % (indent, k, v))

            def reportMissingKeys(data, key = ""):
              for k, v in data.items():
                new_key = key + "." + k
                if key == "":
                  new_key = k

                if isinstance(v, dict):
                  reportMissingKeys(data[k], new_key)
                else:
                  Log.warning("could not find configuration key %s" % (new_key))

            if key is None:
              # Print all options
              output_hash(config)
            else:
              # Search for and print the requested key
              if not new_data is None:
                remaining = deepcopy(new_data)
                Log.write("%s" % (remaining))
                search_hash(config, schema, new_data, remaining)
                if remaining:
                  reportMissingKeys(remaining)
                # TODO: handle not finding a key or having a failed validation and do not
                #       commit the object

                # Update object
                experiment.purgeGenerates()

                # Save updated configuration file
                f = open(configuration_file_path, 'w+')
                Log.write("writing out configuration in %s within %s" % (configuration_file_path, configurations_path))
                f.write(json.dumps(config))
                f.close()
                Log.write(config)

                experiment.git.add(configurations_path)
                revisions = experiment.commit('Updates configuration', configurations_path)
          else:
            Log.warning("could not find configuration for %s" % (configuration_name))

    if len(files) > 0:
      # Add the files to the configuration
      connection = object_json["workflow"]["connections"][int(node_id)]

      connection['files'] = connection.get('files', [])

      if self.options.remove_files:
        connection['files'] = []
        Log.write("Setting files as input to %s." % (connection.get('object', {}).get('name', 'unknown')))
      else:
        Log.write("Adding files as input to %s." % (connection.get('object', {}).get('name', 'unknown')))

      connection['files'] = connection['files'] + files

      revisions = experiment.updateObject(object_json, 'Adds files to input')
    elif self.options.remove_files:
      # Remove the files for the given connection
      connection = object_json["workflow"]["connections"][int(node_id)]

      if 'files' in connection:
        del connection['files']

        revisions = experiment.updateObject(object_json, 'Removes files from input')


    if not revisions is None:
      Log.write("New revision: ", end="")
      Log.output(revisions[0], padding="")
      if (len(revisions) > 1):
        Log.write("New workset revision: ", end="")
        Log.output(revisions[-1], padding="")
    Log.done("Configured %s object %s" % (type, name))

    return 0

  # Returns a set of hashes for each permutation of the given ranges.
  def form_range(values, schema=None):
    set = []
    for current_range in values:
      keys   = current_range[0].split('.')
      values = current_range[1]

      key_set = []

      # Find definition in schema
      definition = schema
      hash = {}
      current = hash
      last = None
      last_key = None

      # Go down the configuration tree and find the key
      for k in keys:
        current[k] = {}
        last = current
        current = current[k]

        definition = definition[k]
        last_key = k

      for pair in values:
        start = int(pair[0])
        step = "x+1"
        if len(pair) == 1:
          final = start
        elif len(pair) == 2:
          final = int(pair[1])
        else:
          final = int(pair[1])
          step  = pair[2]

        # Find first possible value
        while start <= final:
          passed = True
          if "validations" in definition:
            for validation in definition["validations"]:
              if "test" in validation:
                result = Configure.validate(str(start), definition)
                if result[0] == False:
                  passed = False
                  break

          if passed:
            break
          else:
            start += 1

        i = start
        iterations = 0
        while(i <= final and iterations < 50):
          passed = True

          if i != start:
            if "validations" in definition:
              for validation in definition["validations"]:
                if "test" in validation:
                  result = Configure.validate(i, definition)
                  if result[0] == False:
                    passed = False
                    break

          if passed:
            last[last_key] = i
            key_set.append({
              "hash":  deepcopy(hash),
              "value": i,
              "name":  current_range[0]
            })

          i = Configure.execute(step, i)

      set.append(key_set)

    return set

  # Reports all of the ranged values which we will replace when we generate
  # new configurations.
  #
  # Returns an array of tuples of the form [full_key, range]
  #   ie ['1.foo.bar', [2,3]]
  #   which means range from 2 to 3 in key foo.bar in object id 1
  def rake_ranges(hash, schema=None, nesting='', key=None, blank=False, values=None):
    if values is None:
      values = []
    hash = hash or {}

    new_key = nesting
    if not key is None and not nesting is None:
      if len(nesting) > 0:
        new_key = "%s.%s" % (nesting, key)
      else:
        new_key = key

    if schema is None and key is None:
      # We are looking at a blank object config... go through every object in the connection
      # Each object will have one or more configurations, go through those as well
      # Type check against all values

      # Fail on unknown objects (or try to resolve them?)
      for k, v in hash.items():
        connection_id = int(k)
        connection = DB.session().query(Connection).filter_by(id=connection_id).first()
        if connection is None:
          del hash[k]
        else:
          # Get the object for this connection
          object = connection.object

          # Go through each configuration
          for sub_k, sub_v in hash[k].items():
            configuration_id = int(sub_k)
            configuration = DB.session().query(Configuration).filter_by(id=configuration_id).first()

            if configuration is None:
              del hash[k][sub_k]
            else:
              Configure.rake_ranges(hash[k][sub_k], configuration.schema, "#{connection_id}.#{configuration_id}", None, blank, values)
    elif isinstance(schema, dict):
      if "type" in schema and not isinstance(schema["type"], dict):
        # Output form input
        value = ""
        if schema["type"] == "long" or schema["type"] == "port":
          schema["type"] = "int"

        if schema["type"] == "array" or schema["type"] == "hex":
          schema["type"] = "string"

        if schema["type"] == "array" or schema["type"] == "file":
          schema["type"] = "string"

        if "default" in schema:
          value = schema["default"]

        if not hash is None and key in hash:
          value = hash[key]

          if isinstance(value, str) and schema['type'] in ['int', 'float'] and (value.find('...', 1) >= 0 or value.find(',', 1) >= 0):
            config_tuple = [new_key, []]

            # Split on commas, and then split by ranges
            for section in value.split(','):
              config_values = []

              parts = [x.strip() for x in section.split(':')]
              step = "x+1"
              if len(parts) > 1:
                section = parts[0]
                step    = parts[1]

              parts = [x.strip() for x in section.split('...')]
              if len(parts) == 1:
                parts = [parts[0], parts[0]]

              config_values.extend(parts)
              config_values.append(step)

              config_tuple[1].append(config_values)
            values.append(config_tuple)
      else:
        # Go through group
        for k in schema.keys():
          if k == "__ordering":
            continue

          v = schema[k]
          if isinstance(v, dict):
            if not hash is None and not key is None and (not key in hash or hash[key] == "") and blank:
              next

            if not hash is None and not key is None:
              if key in hash:
                Configure.rake_ranges(hash[key], v, new_key, k, blank, values)
            elif key is None:
              Configure.rake_ranges(hash, v, new_key, k, blank, values)
    elif isinstance(hash, list):
      # TODO: add a "+" button to allow values of this type to be
      # appended. This is a 'more than 1' type of configuration
      pass
    else:
      # Shouldn't happen
      pass

    return values
