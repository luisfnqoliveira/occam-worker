# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.log            import Log
from lib.db             import DB

class Detach:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision", action = "store",
                                          dest   = "revision",
                                          help   = "use the given revision of the object to add the new object as a dependency")
    parser.add_option("-t", "--to",       action = "store",
                                          dest   = "to_object",
                                          help   = "the experiment id to attach the workflow object to")
    parser.add_option("-w", "--within",   action = "store",
                                          dest   = "within_object",
                                          help   = "the workset id that contains the experiment")
    parser.add_option("-W", "--within-revision", action = "store",
                                                 dest   = "within_revision",
                                                 help   = "the workset revision")
    return parser

  def do(self):
    Log.header("Detaching Object")

    # Look for context. Are we in an experiment?
    path = None
    tempPath = False

    if len(self.args) > 1:
      node_id = int(self.args[1])
    else:
      Log.error("node identifier required")
      return -1

    revision = None
    if self.options.revision:
      revision = self.options.revision

    experiment = None
    if self.options.to_object:
      # TODO: handle when workset isn't found
      workset, experiment, path = self.occam.temporaryClone(self.options.to_object, revision, self.options.within_object, self.options.within_revision)
      tempPath = True
    else:
      # We are adding to the current object
      path = "."

      experiment = self.occam.objectAt(path)

    if not experiment.objectInfo()['type'] == 'experiment':
      Log.error("not within an experiment.")
      return -1

    #experiment = experiment.copyOnWrite(workset)
    experiment_info = experiment.objectInfo()

    if not "type" in experiment_info or experiment_info["type"] != "experiment":
      Log.error("not within an experiment.")
      return -1

    if not "workflow" in experiment_info or not "connections" in experiment_info["workflow"]:
      Log.error("experiment has no workflow.")
      return -1

    # Remove the node
    # Error if that node does not exist

    if len(experiment_info["workflow"]["connections"]) <= node_id:
      Log.error("node identifier invalid")
      return -1

    def detachNode(index):
      removed_connection = experiment_info["workflow"]["connections"].pop(index)

      # Reform connection indices throughout
      subindex = 0
      for connection in experiment_info['workflow']['connections']:
        if connection['to'] is None:
          pass
        elif connection['to'] > index:
          connection['to'] -= 1
        elif connection['to'] == index:
          connection['to'] = None
        subindex += 1

      # Remove any connections that rely on this connection
      commit_message = ""
      subindex = 0
      for connection in experiment_info['workflow']['connections']:
        if connection['to'] == None:
          commit_message += detachNode(subindex)
        subindex += 1

      type = None
      name = None
      if "object" in removed_connection:
        type = removed_connection["object"].get("type")
        name = removed_connection["object"].get("name")
        revision = removed_connection["object"].get("revision")

      Log.write("Detached %s %s (%s)" % (type, name, revision))

      commit_message += 'detached %s %s (%s)\n\n' % (type, name, revision)
      return commit_message

    # Detach the node requested
    commit_message = detachNode(node_id)

    # Commit change to experiment
    commit_message += 'OCCAM automated commit'
    revisions = experiment.updateObject(experiment_info, commit_message)

    Log.write("New revision: ", end="")
    Log.output(revisions[0], padding="")
    if (len(revisions) > 1):
      Log.write("New workset revision: ", end="")
      Log.output(revisions[-1], padding="")

    if tempPath:
      Log.noisy("removing %s" % path)
      shutil.rmtree(path)

    Log.done("Detached node %s" % (node_id))
    return 0
