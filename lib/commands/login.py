# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import getpass

from lib.db             import DB

from models.system      import System

from lib.log import Log

class Login:
  """
  This class adds a new object to the current container object, generally a workset.
  It will first have to find out the context (which workset are we in) and then
  do the appropriate set-up to create the new type of object requested.
  """
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-n", "--new", action = "store_true",
                                     dest   = "new",
                                     help   = "creates a new account")
    return parser

  def is_authenticated():
    occam_path = os.path.expanduser('~/.occam')
    if not os.path.exists(occam_path):
      os.mkdir(occam_path)

    token_path = "%s/auth_token" % (occam_path)
    if not os.path.exists(token_path):
      return False

    # TODO: open token and check the credentials
    return True

  def do(self):
    Log.header("Authenticating")

    username = None
    if len(self.args) > 1:
      username = self.args[1]
    else:
      Log.error("must enter a username: occam auth <USERNAME>")
      return -1

    Log.write("waiting for password")

    password = None
    password = getpass.getpass('\npassword for %s (typing will be hidden): ' % (username))

    # Determine if credentials match
    if not self.occam.authenticate(username, password=password):
      Log.error("username or password is invalid")
      return -1

    # Add a token in some path?
    occam_path = os.path.expanduser('~/.occam')
    if not os.path.exists(occam_path):
      os.mkdir(occam_path)

    # Add auth token
    token_path = "%s/auth_token" % (occam_path)
    sig_path   = "%s/auth_token.sig" % (occam_path)

    account = self.occam.searchAccounts(username=username).first()

    token = {
      "username": username,
      "person": account.person.object.uid,
      "timestamp": "foo"
    }

    # TODO: sign this token
    f = open(token_path, 'w+')
    f.write(json.dumps(token))
    f.close()

    Log.done("successfully authenticated as %s" % (username))
