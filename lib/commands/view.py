# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import re
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)
import yaml        # Output in YAML

from lib.log            import Log
from lib.db             import DB
from lib.git            import Git
from lib.object         import Object

from models.system           import System

class View:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision",  action = "store",
                                           dest   = "revision",
                                           help   = "use the given revision of the object")
    parser.add_option("-u", "--url",       action = "store",
                                           dest   = "url",
                                           help   = "view a remote object at the given URL")
    return parser

  def do(self):
    # Determine the revision we will use
    revision = self.options.revision or "HEAD"

    obj_id = None
    key = None

    if len(self.args) == 2:
      obj_id = self.args[1]
    elif len(self.args) > 2:
      obj_id = self.args[1]
      if len(self.args) == 3:
        key = self.args[2]

    if len(self.args) > 3:
      Log.error("too many arguments")
      return -1

    if self.options.url:
      # Find specific object
      object_path = self.options.url

      # Create Object from URL:
      obj = Object(object_path, revision=revision, occam=self.occam)

    elif obj_id:
      # Query for object by id
      object = self.occam.objects.search(uuid=obj_id).first()
      if object is None:
        Log.error("cannot find object with id %s" % (obj_id))
      object_path = self.occam.objects.pathFor(object.uid)

      # Retrieve metadata
      obj = Object(object_path, revision=revision, occam=self.occam)
    else:
      obj = Object('.', revision=revision, occam=self.occam)

    obj_info = obj.objectInfo()

    if not key is None:
      match = re.match(r'^(.*)\[(\d+)\]$', key)

      index = None
      if match:
        key = match.group(1)
        index = int(match.group(2))

      if key in obj_info:
        item = obj_info[key]
        if not index is None:
          if not isinstance(item, list):
            Log.error("%s is not a list" % (key))
            return -1
          if len(item) <= index:
            Log.error("%s has a length of %s; a valid index is between 0 and %s" % (key, len(item), len(item)-1))
            return -1
          item = item[index]

        if isinstance(item, str):
          Log.outputBlock(item, padding="")
        else:
          Log.output(yaml.dump(item, default_flow_style=False), padding="")
      else:
        Log.warning("%s not found in %s %s" % (key, obj_info.get('type'), obj_info.get('name')))
    else:
      Log.header("Listing Information for Object")
      Log.output(yaml.dump(obj_info, default_flow_style=False), padding="")
