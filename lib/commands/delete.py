# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories

from lib.db             import DB
from lib.object         import Object
from lib.log import Log

class Delete:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam

  @staticmethod
  def initParser(parser):
    return parser

  def do(self):
    Log.header("Deleting object")

    git_path = None

    uuid = None
    if len(self.args) > 1:
      uuid = self.args[1]

    object = self.occam.objects.search(uuid=uuid).first()

    if object is None:
      Log.error("Cannot find %s" % (uuid))
      return -1

    Log.write("Deleting %s" % (object.name))

    try:
      obj = Object(object.path, occam=self.occam)
      self.occam.deleteObject(obj)
    except:
      # Still have to delete the orphaned DB entry
      DB.session().delete(object)
      DB.session().commit()
