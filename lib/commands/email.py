# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import uuid
# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

from lib.log            import Log
from lib.db              import DB

from datetime import datetime,timedelta

'''
	(occam) email passreset/activate username --baseurl BASEURL
'''
class Email(object):
	"""docstring for Email"""
	def __init__(self, opts, args, occam):
		self.options = opts
		self.args    = args
		self.occam   = occam
		if not hasattr(self.options,'base_url'):
			self.options.base_url = 'localhost:9292'

	@staticmethod
	def initParser(parser):
		parser.add_option("-b", "--baseurl",	action = "store",
												dest   = "base_url",
												help   = "base url of the website")
		return parser

	def do(self):
		if len(self.args) != 3:
			Log.error('invalid parameters to send a email')
			return -1

		account = self.occam.searchAccounts(username=self.args[2])
		account = account.first()
		if account and account.email:
			if self.args[1] == 'passreset':
				self.send_password_change_token(account)
			elif self.args[1] == 'activate':
				self.send_account_activate_token(account)
			else:
				Log.error('invalid email purpose')
				return -1
		else:
			Log.error('invalid account or account has no email')
			return -1

		Log.done('Email for %s is sent'%(self.args[1]))
		return 0

	def __send_message(person_or_account, sbj, bd):
		msg = MIMEText(bd)
		# me == the sender's email address
		# you == the recipient's email address
		msg['Subject'] = sbj
		msg['From'] = 'chobitscxd@gmail.com'
		msg['To'] = person_or_account.email

		# Send the message via our own SMTP server.
		s = smtplib.SMTP('localhost')
		s.send_message(msg)
		s.quit()

	def send_password_change_token(self, account):
		token = Email.__generate_token()
		account.reset_token = token
		# 24 hours until expired
		account.expired = datetime.now() + timedelta(days=1)
		DB.session().add(account)
		DB.session().commit()
		Log.output("OCCAM: Password Reset\n\nHi, %s, use this link to reset your password: %s/pass_reset/%s/%s"
					%(account.username,self.options.base_url,account.username,token))
		Email.__send_message(account, 	"OCCAM: Password Reset",
								"Hi, %s, use this link to reset your password: %s/pass_reset/%s/%s"
								%(account.username,self.options.base_url,account.username,token)
					)

	def send_account_activate_token(self, account):
		token = Email.__generate_token()
		account.reset_token = token
		# 24 hours until expired
		account.expired = datetime.now + timedelta(days=1)
		DB.session().add(account)
		DB.session().commit()
		Log.output("OCCAM: Account Activation\n\nHi, %s, use this link to activate your account: %s/activate_account/%s/%s"
					%(account.username,self.options.base_url,account.username,token))
		Email.__send_message(account, 	"OCCAM: Account Activation",
								"Hi, %s, use this link to activate your account: %s/pass_reset/%s/%s"
								%(account.username,self.options.base_url,account.username,token)
					)
	def send_workset_done(self, person, workset):
		Email.__send_message(person, "OCCAM: Workset Done", "%s (rev. %s)"
							%(workset.name,workset.revision))

	def send_experiment_done(self, person, experiment):
		Email.__send_message(person, "OCCAM: Experiment Done", "%s (rev. %s) in %s"
					%(experiment.name,experiment.revision,experiment.workset.name))

	def __generate_token():
		return uuid.uuid4().hex
