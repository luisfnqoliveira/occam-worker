# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import codecs
import sys
import json        # For parsing recipes
import collections # For OrderedDict
import base64      # For name mangling
import tempfile    # For temporary directories
import shutil      # For deleting that temp dir (grr python)

from lib.db             import DB
from lib.git            import Git
from lib.log import Log

from models.system           import System
from models.object           import Object

from datetime import datetime

from subprocess import Popen, PIPE

class Update:
  def __init__(self, opts, args, occam):
    self.options = opts
    self.args    = args
    self.occam   = occam
    self.workingPath = "."

  @staticmethod
  def initParser(parser):
    parser.add_option("-r", "--revision", action = "store",
                                          dest   = "revision",
                                          help   = "the revision of the object to use")
    return parser

  def do(self):
    path = None

    Log.header("Updating Object Build")

    local_build = False

    # Determine object from givens
    obj_revision = ""
    if len(self.args) > 2:
      obj_type = self.args[1]
      name = self.args[2]
      uuid = None
    elif len(self.args) > 1:
      obj_type = None
      name = self.args[1]
      uuid = None
    else:
      obj_type = None
      name = None
      uuid = None

    try:
      if self.options.revision:
        obj_revision = self.options.revision
    except:
      obj_revision = ""

    if obj_type is None and name:
      path = name
    elif obj_type and name:
      obj = self.occam.objects.search(object_type = obj_type,
                                      name        = name,
                                      uuid        = uuid).first()
      if obj is None:
        Log.error("cannot find buildable object")
        return -1

      if obj_revision == "":
        obj_revision = obj.revision
        print(obj_revision)

      path = obj.path
    elif path is None:
      path = "."
      Log.write("Updating a local version of this object.")
      local_build = True

    # TODO: object.json wrt revision
    # TODO: pull object path from DB when name/type/revision specified
    # TODO: do a build if a build doesn't already exist
    # TODO: accept path as positional argument
    object_path = path

    # Find root of object to build
    obj = self.occam.objectAt(object_path, revision=obj_revision)
    object_path = obj.path

    # Re-read object info if we want a different revision
    if obj_revision:
      obj_revision = Git.fullRevision(object_path, obj_revision)
      if obj_revision:
        objectInfo = Git(object_path, obj_revision).objectInfo()
      else:
        objectInfo = None

      if not objectInfo:
        Log.error("revision not found")
        return -1
      tag = obj_revision
    else:
      # Acquire a revision tag representing "up-to-date"
      rev_parse = Popen(['git', 'rev-parse', 'HEAD'], stdout=PIPE, cwd=object_path)
      rev_parse.wait()
      tag = rev_parse.stdout.read().decode('utf-8').strip()

    localHash = None
    #if local_build:
      # TODO: proper hash
      #localHash = "local-abc"

    if local_build and self.occam.update(obj, build_path = path, localHash=localHash):
      Log.done('Finished Local Update of %s %s' % (obj.objectInfo()["type"], obj.objectInfo()["name"]))
    elif not local_build and self.occam.update(obj):
      Log.done('Finished Updating %s %s' % (obj.objectInfo()["type"], obj.objectInfo()["name"]))
