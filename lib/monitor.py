# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import subprocess
import os
import sys
import time
import datetime

class Monitor:
  def __init__(self, pid):
    self.pid = pid
    self.count = 0
    self.stats = {}
    self.vmpeak_max = 0
    self.vmpeak_average = 0
    self.vmsize_max = 0
    self.vmsize_average = 0
    self.vmhwm_max = 0
    self.vmhwm_average = 0
    self.vmrss_max = 0
    self.vmrss_average = 0
    self.vmdata_max = 0
    self.vmdata_average = 0

  def status(self):
    self.count = self.count + 1

    command = "/proc/" + str(self.pid) + "/status"

    stats = {}
    try:
      f = open(command, "r")

      # Parse proc file
      while True:
        string = f.readline()

        # Checks for end of file
        if len(string) == 0:
          break

        array = string.split(":")
        stats[array[0]] = array[1].strip()
    except:
      print("Failed to open proc stats file.")

    # Merge Statistics
    self.stats.update(stats)

    # Compute Incremental Statistics
    self.vmpeak()
    self.vmsize()
    self.vmhwm()
    self.vmrss()
    self.vmdata()
    return stats

  def vmpeak(self):
    temp_vmpeak = self.__parse('VmPeak')

    if temp_vmpeak > self.vmpeak_max:
      self.vmpeak_max = temp_vmpeak

    self.stats["VmPeakMax"] = self.max_vmpeak()

    self.vmpeak_average = self.vmpeak_average + temp_vmpeak
    self.stats["VmPeakAvg"] = self.average_vmpeak()

  def max_vmpeak(self):
    return self.vmpeak_max

  def average_vmpeak(self):
    return self.vmpeak_average/self.count

  def vmsize(self):
    temp_vmsize = self.__parse('VmSize')

    if temp_vmsize > self.vmsize_max:
      self.vmsize_max = temp_vmsize
    self.stats["VmSizeMax"] = self.max_vmsize()

    self.vmsize_average = self.vmsize_average + temp_vmsize
    self.stats["VmSizeAvg"] = self.average_vmsize()

  def max_vmsize(self):
    return self.vmsize_max

  def average_vmsize(self):
    return self.vmsize_average / self.count

  def vmhwm(self):
    temp_vmhwm = self.__parse('VmHWM')

    if temp_vmhwm > self.vmhwm_max:
      self.vmhwm_max = temp_vmhwm
    self.stats["VmHWMMax"] = self.max_vmhwm()

    self.vmhwm_average = self.vmhwm_average + temp_vmhwm
    self.stats["VmHWMAvg"] = self.average_vmhwm()

  def max_vmhwm(self):
    return self.vmhwm_max

  def average_vmhwm(self):
    return self.vmhwm_average / self.count

  def vmrss(self):
    temp_vmrss = self.__parse('VmRSS')

    if temp_vmrss > self.vmrss_max:
      self.vmrss_max = temp_vmrss
    self.stats["VmRSSMax"] = self.max_vmrss()

    self.vmrss_average = self.vmrss_average + temp_vmrss
    self.stats["VmRSSAvg"] = self.average_vmrss()

  def max_vmrss(self):
    return self.vmrss_max

  def average_vmrss(self):
    return self.vmrss_average / self.count

  def vmdata(self):
    temp_vmdata = self.__parse('VmData')

    if temp_vmdata > self.vmdata_max:
      self.vmdata_max = temp_vmdata
    self.stats["VmDataMax"] = self.max_vmdata()

    self.vmdata_average = self.vmdata_average + temp_vmdata
    self.stats["VmDataAvg"] = self.average_vmdata()

  def max_vmdata(self):
    return self.vmdata_max

  def average_vmdata(self):
    return self.vmdata_average / self.count

  def __parse(self, key):
    if key in self.stats:
      array = self.stats[key].strip().split(" ")
      return int(array[0])
    return 0
