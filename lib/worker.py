# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import shlex
import threading

import time
from datetime import date

from time import sleep

from lib.cli            import CLI
from lib.db             import DB
from lib.daemon         import Daemon
from lib.signal         import Signal
from lib.occam          import Occam
from lib.log            import Log

class Worker:
  def __init__(self, options, occam):
    self.occam   = occam
    self.options = options
    self.workingPath = occam.path

    if self.options.rootPath is None:
      # TODO: SYSTEM PATH
      self.options.rootPath = occam.configuration()['paths'].get('jobs')
      self.options.rootPath = os.path.realpath(self.options.rootPath)

    if not os.path.exists(self.options.rootPath):
      Log.write("Making jobs directory: %s" % (self.options.rootPath))
      os.mkdir(self.options.rootPath)

    self.workersPath = "%s/workers" % (self.options.rootPath)
    self.runningPath = "%s/running" % (self.options.rootPath)

    if not os.path.exists(self.workersPath):
      Log.write("Making workers directory: %s" % (self.workersPath))
      os.mkdir(self.workersPath)

    if not os.path.exists(self.runningPath):
      Log.write("Making running directory: %s" % (self.runningPath))
      os.mkdir(self.runningPath)

    if self.options.daemon:
      DB.close()
      retCode = Daemon(self.options.rootPath, "%s/occam-worker" % (self.workersPath), True).create()
      # Reopen the database in the daemon process
      DB.initialize(self.options)

    # Open document store
    #DocumentStore.initialize(self.options)

    # Open memcache access
    #Memcache.initialize(self.options)

    self.lock = threading.Lock()
    # TODO: do this better. python doesn't let us release a lock more than
    #       once. wtf.
    self.lock_lock = threading.Lock()

    self.signaller = Signal({
      'finished': self.signal,
      'worker':   self,
    })

    retCode = 0

    procParams = """
    {
      "exit_code":          "%s",
      "process_id":         "%s",
      "parent_process_id":  "%s",
      "parent_group_id":    "%s",
      "session_id":         "%s",
      "user_id":            "%s",
      "effective_user_id":  "%s",
      "real_group_id":      "%s",
      "effective_group_id": "%s"
    }
    """ % (retCode, os.getpid(), os.getppid(), os.getpgrp(), os.getsid(0),
                    os.getuid(), os.geteuid(), os.getgid(),  os.getegid())

    open("%s/occam-worker-%s.json" % (self.workersPath, os.getpid()), "w").write(procParams + "\n")

  def createJobPath(self, job):
    # Jobs are stored as [jobs path]/0/job-1, [jobs path]/100/job-123, etc
    # That is, they are divided into groups of 100
    major_id = job.id - (job.id % 100)

    major_path = os.path.join(self.options.rootPath, str(major_id))
    job_path   = os.path.join(major_path, "job-%s" % (str(job.id)))

    if not os.path.exists(major_path):
      print("Making job group directory: %s" % (major_path))
      os.mkdir(major_path)

    if not os.path.exists(job_path):
      print("Making job directory: %s" % (job_path))
      os.mkdir(job_path)

    return job_path

  def run(self):
    while True:
      if not self.idle():
        if self.options.verbose:
          print("No work")
        sleep(3)

  # Called when the worker is idle
  def idle(self):
    # Yank a queued job and set it to running
    job = self.occam.pullJob('running')
    if not job is None:
      self.perform(job)

      # Mark Job

      # Finish Job

      # Hold Steady
      self.lock.acquire()
      self.lock.release()
      return True

    return False

  # Initialize the jobs
  def initJob(self, job):
    # Print to log
    print("Started job #" + str(job.id))

    # Establish start time
    start_time = time.time()

    # Create a directory for the job to run in with the format:
    # {occam-root}/%yyyy/%mm/%dd/job-{job id}
    jobWorkingPath = self.createJobPath(job)

    # Afterward, this directory will have a 'job-{job id}.json' and a
    # 'job-{job id}.log' which details its running environment and its
    # output respectively.

    # Create job running file
    print("Writing file")
    jobFile = "%s/%s" % (self.runningPath, str(job.id))
    open(jobFile, "w+").close()
    self.files.append([job, jobFile])
    Log.noisy("adding file: %s" % (jobFile))
    Log.noisy(self)
    Log.noisy(self.files)
    print("Ok")

    # Build runs path!
    run_path = jobWorkingPath
    run_path = os.path.join(run_path, "runs")
    if not os.path.exists(run_path):
      os.mkdir(run_path)
    run_path = os.path.join(run_path, str(job.run_index))
    if not os.path.exists(run_path):
      os.mkdir(run_path)
    run_path = os.path.join(run_path, "jobs")
    if not os.path.exists(run_path):
      os.mkdir(run_path)
    run_path = os.path.join(run_path, str(job.job_index))
    if not os.path.exists(run_path):
      os.mkdir(run_path)
    run_path = os.path.join(run_path, "vms")
    if not os.path.exists(run_path):
      os.mkdir(run_path)
    run_path = os.path.join(run_path, str(job.vm_index))
    if not os.path.exists(run_path):
      os.mkdir(run_path)

    # Clone task to vm path
    vm_object = self.occam.objects.retrieve(job.task_uid, "occam-task")
    vm_object.clone(path=run_path, create=False, increment=False)

    self.tasks.append([job, job.command, jobWorkingPath, run_path])

  # Do the given job
  def perform(self, job):
    # Set up wait semaphore
    self.lock.acquire()

    # Remember the current job
    self.job = job
    self.tasks = []
    self.files = []

    # We need to spawn all co-dependant jobs
    codependants = self.occam.pullCodependantJobs(job)
    for codependant in codependants:
      print("start")
      self.initJob(codependant)
      print("end")

    self.initJob(job)

    # Create a directory for the job to run in with the format:
    # {occam-root}/%yyyy/%mm/%dd/job-{job id}
    jobWorkingPath = self.createJobPath(job)

    # Tell world we are starting these jobs
    DB.session().commit()

    jobInfoPath = "%s/current.json" % (self.options.rootPath)
    jobLogFilePath = "%s/job-%s.log" % (jobWorkingPath, job.id)
    outputLogFilePath = "%s/output.raw" % (jobWorkingPath)
    open(outputLogFilePath, "w+").close()

    # Run the jobs
    for task in self.tasks:
      # Execute Job
      path = task[2]
      job_path = task[3]

      os.chdir(path)
      occam_opts = ""

      Log.write("performing: %s" % (job.command))
      script = "python %s/spawn.py -q %s -d %s -r %s -j %s -w %s -c '%s/../occam.py %s'" % (
          os.path.dirname(__file__), # Launch spawn.py from occam-worker source
          self.options.rootPath,     # -q {Worker path}
          os.getpid(),               # -d {dispatcher process id}
          path,                      # -r {job working directory}
          job.id,                    # -j {job id}
          job_path,                  # -w {working directory for running job}
          os.path.dirname(__file__), # Our library path
          task[1])

      print("Running %s" % (script))

      # Do process spawning/monitoring in a separate space
      os.system(script)

      # Success
      return True

      #if not CLI.execute(shlex.split(task[1].command)):
        #self.fail(task[0])

    def tail(f, window=20):
      """
      Returns the last `window` lines of file `f` as a list.
      """
      if window == 0:
        return []
      BUFSIZ = 1024
      f.seek(0, 2)
      bytes = f.tell()
      size = window + 1
      block = -1
      data = []
      while size > 0 and bytes > 0:
        if bytes - BUFSIZ > 0:
          # Seek back one whole BUFSIZ
          f.seek(block * BUFSIZ, 2)
          # read BUFFER
          data.insert(0, f.read(BUFSIZ).decode("utf-8"))
        else:
          # file too small, start from begining
          f.seek(0,0)
          # only read what was not read
          data.insert(0, f.read(bytes).decode("utf-8"))

        linesFound = data[0].count('\n')
        size -= linesFound
        bytes -= BUFSIZ
        block -= 1
      # Remove everything up to the first newline
      data = ''.join(data)
      return data[(data.find('\n')+1):]

    child_pid = None
    monitor = None
    stats = {}

    # Wait for signal meaning the job is done
    print("Waiting")

    # Wait until the semaphore is unlocked
    # Look at all children
    while self.lock.locked():
      if child_pid is None:
        try:
          spawned = json.load(open(jobInfoPath))
          child_pid = spawned["process_id"]
          monitor = Monitor(child_pid)
          print("Child process: %s" % (child_pid))
        except:
          child_pid = None

      if child_pid is not None:
        # Monitoring/output pushing
        stats = monitor.status()

      # Tail the job output to memcache
      if os.path.exists(jobLogFilePath):
        jobFile = open(jobLogFilePath, "rb")
        stats["job_log"] = tail(jobFile)
        jobFile.close()
      if os.path.exists(outputLogFilePath):
        jobFile = open(outputLogFilePath, "rb")
        stats["output_log"] = tail(jobFile)
        jobFile.close()

      Memcache.set("job_%s" % (self.job.id), stats)
      sleep(0.5)

    if child_pid is None:
      monitor = Monitor(self.child_pid)

    stats = monitor.status()

    if os.path.exists(jobLogFilePath):
      jobFile = open(jobLogFilePath, "rb")
      stats["job_log"] = tail(jobFile)
      jobFile.close()
    if os.path.exists(outputLogFilePath):
      jobFile = open(outputLogFilePath, "rb")
      stats["output_log"] = tail(jobFile)
      jobFile.close()

    #Memcache.set("job_%s" % (self.job.id), stats)

    # Record time
    #end_time = time.time()

    #elapsed_time = end_time - start_time
    #print("Elapsed Time: %s seconds" % (elapsed_time))

    #self.job.elapsed_time = elapsed_time

    # Commit the job updates
    DB.session().commit()

    # Perform other scripts
    for task in self.tasks:
      task[1].finish(task[0], jobWorkingPath)

    DB.session().commit()

  def fail(self, job):
    if self.options.verbose:
      print("Failed job #" + str(job.id))

    # Update job to failed
    job.status = "failed"

  def signal(self):
    """ This will be called when the dispatched process wishes to communicate.
    """

    # Determine which file exists
    job = None
    for file in self.files:
      if not os.path.isfile(file[1]):
        job  = file[0]
        path = file[1]
        break

    if job == None:
      print("Signal from unknown job!")
      return

    print("Signal from job %s" % (job.id))

    # Remove that checked file
    self.files[:] = [x for x in self.files if not x[1] == path]

    # Retrieve the child pid
    try:
      spawned = json.load(open("%s/current.json" % (self.options.rootPath)))

      # Log
      self.child_pid = spawned["process_id"]
      print("Message from child [%s]" % (self.child_pid))
    except:
      print("Problem reading %s/current.json." % (self.options.rootPath))

    # Determine which job this child represents

    # Remove log
    try:
      os.remove("%s/current.json" % (self.options.rootPath))
    except:
      print("Problem deleting %s/current.json." % (self.options.rootPath))

    job.log_file = "%s/job-%s.json" % (self.options.rootPath, job.id)

    # Mark job as finished
    self.finish(job)

    # Unlock?
    self.lock_lock.acquire()
    if len(self.files) == 0:
      # Release poll lock to poll once more
      if self.lock.locked():
        self.lock.release()
    self.lock_lock.release()

  def finish(self, job):
    if self.options.verbose:
      print("Finished job #" + str(job.id))

    self.occam.completeJob(job)
