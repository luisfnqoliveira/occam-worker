# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2016 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from lib.db         import DB
from lib.git        import Git
from lib.log        import Log
from lib.object     import Object
from lib.group      import Group
from lib.experiment import Experiment
from lib.workset    import Workset
from lib.person     import Person

from uuid import uuid4

# For pulling network http resources
import codecs
try:
  from urllib.request import Request, urlopen, HTTPError  # Python 3
  from urllib.parse import urlparse # Python 3
except:
  from urllib2 import Request, urlopen, HTTPError  # Python 2
  from urllib2 import urlparse # Python 2

class ObjectManager:
  """
  This OCCAM manager handles the Object listing, Object discovery, and Object
  lookup.
  """

  def __init__(self, occam):
    """
    Initialize the object manager.
    """

    self.occam   = occam
    self.options = occam.options

  def viewersFor(self, objectType, objectSubType=None):
    """
    Returns the objects that view the given object type.
    """

    session = self.occam.databaseSession()

    viewer_query = session.query(DB.Viewer.occam_object_id)
    object_query = session.query(DB.Object)

    viewers = viewer_query.filter_by(views_type = objectType)
    if objectSubType:
      viewers = viewers.filter_by(views_subtype = objectSubType)

    objects = object_query.filter(DB.Object.id.in_(viewers))

    return objects

  def providersFor(self, environment=None, architecture=None, capability=None):
    """
    Returns the objects that provide for the given environment and architecture.
    """

    session = self.occam.databaseSession()

    provider_query = session.query(DB.Provider.occam_object_id)
    object_query   = session.query(DB.Object)

    providers = provider_query.filter_by(environment  = environment,
                                         architecture = architecture,
                                         capability   = capability)
    objects   = object_query.filter(DB.Object.id.in_(providers))

    return objects

  def search(self, uuid=None, name=None, object_type=None):
    """
    """

    session = self.occam.databaseSession()

    query = session.query(DB.Object)

    if not uuid is None:
      query = query.filter_by(uid=uuid)
    if not name is None:
      if object_type == "person":
        # TODO: handle searching both name and username?
        query = query.filter_by(username=name)
      else:
        query = query.filter_by(name=name)
    if not object_type is None:
      query = query.filter_by(object_type_safe=Object.slugFor(object_type))

    return query

  def retrieve(self, uuid, object_type="object", revision="HEAD", owner_uuid=None):
    """
    Returns an object instance representing the given stored OCCAM object. Will
    return a Group, Experiment, Workset, or generic Object respective of the
    object type.
    """

    if object_type == "occam-git":
      subPath = "git"
    else:
      subPath = "objects"

    if owner_uuid is None:
      owner_uuid = uuid

    path = self.pathFor(owner_uuid, subPath=subPath)

    if os.path.exists(path):
      if object_type == 'group':
        return Group(path, revision=revision, occam=self.occam)
      elif object_type == 'experiment':
        return Experiment(path, revision=revision, occam=self.occam)
      elif object_type == 'workset':
        return Workset(path, revision=revision, occam=self.occam)
      else:
        return Object(path, revision=revision, occam=self.occam, uuid=uuid)

    # Object not found
    return None

  def updateNormalization(self, obj, objectInfo=None, db_obj=None, owner_db_obj=None):
    """
    This method will look at the given object info and update the database
    record for this object to reflect that information. This includes the
    revision of the object.
    """

    Log.write("Updating database record.")

    # Retrieve the object's database record (unless given)
    if db_obj is None:
      db_obj = obj.databaseRecord()

    if db_obj is None:
      Log.error("Cannot find the database record for this object.")
      return -1

    # Get object info (unless given)
    if objectInfo is None:
      objectInfo = obj.objectInfo()

    # Handle object ownership
    if not owner_db_obj is None:
      db_obj.owner_uid = owner_db_obj.uid

    # Set the fields
    db_obj.name             = objectInfo.get('name', 'unknown')
    db_obj.description      = objectInfo.get('description', '')
    db_obj.object_type      = objectInfo.get('type', 'object')
    db_obj.object_type_safe = Object.slugFor(objectInfo.get('type', 'object'))
    db_obj.organization     = objectInfo.get('organization')
    db_obj.subtype          = objectInfo.get('subtype')
    db_obj.uid              = objectInfo.get('id')
    db_obj.revision         = obj.revision;

    # Pull out tags
    db_obj.tags = ";" + ";".join(objectInfo.get('tags', [])) + ";"

    # Pull out capabilities
    db_obj.tags = ";" + ";".join(objectInfo.get('capabilities', [])) + ";"

    # Pull out env/arch
    db_obj.environment  = objectInfo.get('environment')
    db_obj.architecture = objectInfo.get('architecture')

    # Detect a 'runnable' object
    if 'run' in objectInfo and db_obj.object_type != "person":
      db_obj.runnable = 1
    else:
      db_obj.runnable = 0

    # Pull out configuration
    db_obj.configures = objectInfo.get('configures', {}).get('id')

    for derivative in objectInfo.get('includes', []):
      Log.write("Storing included object %s %s" % (derivative.get('type', 'unknown'),
                                                   derivative.get('name', 'unknown')))

      new_info = objectInfo.copy()

      if 'includes' in new_info:
        del new_info['includes']
      if 'subtype' in new_info:
        del new_info['subtype']

      if 'inputs' in new_info:
        del new_info['inputs']
      if 'outputs' in new_info:
        del new_info['outputs']
      if 'install' in new_info:
        del new_info['install']
      if 'build' in new_info:
        del new_info['build']
      if 'run' in new_info:
        del new_info['run']
      if 'configurations' in new_info:
        del new_info['configurations']
      if 'contains' in new_info:
        del new_info['contains']

      new_info.update(derivative)
      derivative = new_info

      derivativeObject = self.retrieve(derivative.get('id'))

      db_obj_sub = None

      if derivativeObject:
        db_obj_sub = derivativeObject.databaseRecord()

      if db_obj_sub is None:
        db_obj_sub = DB.Object()

      # Recurse to pull out the derived object information
      self.updateNormalization(obj, derivative, db_obj_sub, db_obj)

    # Rake 'provides' and add Provider records
    self.updateProviders(obj, db_obj)

    # Rake 'views' and add Viewer records
    self.updateViewers(obj, db_obj)

    # Rake 'outputs' and add ObjectOutput records
    self.updateObjectOutputs(obj, db_obj)

    # Rake 'inputs' and add ObjectInput records
    self.updateObjectInputs(obj, db_obj)

    session = self.occam.databaseSession()
    session.add(db_obj)
    session.commit()
    session.flush()

    return 0

  def updateViewers(self, obj, db_obj = None):
    """
    This method will update or add Viewer records for the given object. It
    will create one record for every object type listed in the given object's
    'views' field in its metadata.
    """

    Log.noisy("Checking for any updated Viewers")

    info = obj.objectInfo()
    session = self.occam.databaseSession()

    if db_obj is None:
      db_obj = obj.databaseRecord()

    # Store a token for any Viewer object
    if not 'views' in info:
      return 0

    views = info.get('views', [])
    if not isinstance(views, list):
      views = [views]

    # For every object type this object can view, make a new record as long as
    # a record doesn't already exist.
    for objectType in views:
      if not isinstance(objectType, dict):
        objectType = {
          "type": objectType
        }

      # Query current viewers
      viewers = self.viewersFor(objectType.get('type'), objectType.get('subtype'))

      newViewer = True

      for viewer in viewers:
        if db_obj.id == viewer.id:
          newViewer = False

      if newViewer:
        # Creates a new Viewer record referring to this object
        Log.write("Creating a Viewer record for %s" % objectType)

        db_viewer = DB.Viewer()
        db_viewer.views_type      = objectType.get('type')
        db_viewer.views_subtype   = objectType.get('subtype')
        db_viewer.occam_object_id = db_obj.id
        db_viewer.revision        = db_obj.revision

        session.add(db_viewer)

  def updateProviders(self, obj, db_obj = None):
    """
    This method will update or add Provider records for the given object.
    """

    info = obj.objectInfo()
    session = self.occam.databaseSession()

    if db_obj is None:
      db_obj = obj.databaseRecord()

    # Store a token for any Provider of an environment/architecture
    if not 'provides' in info:
      return 0

    provides = info.get('provides', {})
    if isinstance(provides, list):
      if len(provides) >= 2:
        provides = {
          "environment":  provides[0],
          "architecture": provides[1]
        }
      else:
        provides = {}

    environment  = info['provides'].get('environment')
    architecture = info['provides'].get('architecture')
    capabilities = info['provides'].get('capabilities', [])

    if environment is None and architecture is None and len(capabilities) == 0:
      return

    # Query provider
    providers = self.providersFor(environment, architecture, None)

    newProvider = True

    for provider in providers:
      if db_obj.id == provider.id:
        newProvider = False

    if newProvider:
      # Store new Environment/Architecture Provider
      if environment or architecture:
        Log.noisy("storing new provider for %s/%s" % (environment, architecture))

        db_provider = DB.Provider()

        db_provider.occam_object_id = db_obj.id

        db_provider.environment  = environment
        db_provider.architecture = architecture

        session.add(db_provider)

    for capability in capabilities:
      providers = self.providersFor(environment, architecture, capability)

      newProvider = True

      for provider in providers:
        if db_obj.id == provider.id:
          newProvider = False

      # Store New Capability Provider
      if newProvider:
        Log.noisy("storing new provider for capability %s" % (capability))
        db_provider = DB.Provider()

        db_provider.occam_object_id = db_obj.id

        db_provider.capability   = capability
        db_provider.environment  = environment
        db_provider.architecture = architecture

        session.add(db_provider)

  def updateObjectInputs(self, obj, db_obj = None):
    """
    This method will add ObjectInput records for the given object.
    """

    info = obj.objectInfo()
    session = self.occam.databaseSession()

    if db_obj is None:
      db_obj = obj.databaseRecord()

    db_inputs = session.query(DB.ObjectInput).filter_by(occam_object_id=db_obj.id)
    db_inputs.delete()

    inputs = info.get('inputs', [])
    for input in inputs:
      # Look up any existing object input
      db_input = DB.ObjectInput()

      # Update the record
      db_input.revision = obj.revision
      db_input.object_type = input.get('type')
      db_input.object_type_safe = Object.slugFor(input.get('type'))

      db_input.occam_object_id = db_obj.id
      db_input.subtype = input.get('subtype')

      # Mark whether or not the input is sharable.
      # When objects are sharable, the object that generates the
      # input (and that input itself) must be present or connectable
      # in some way within the same VM.
      if 'fifo' in input and input['fifo'] == True:
        db_input.fifo = 1
      else:
        db_input.fifo = 0

      session.add(db_input)

  def updateObjectOutputs(self, obj, db_obj = None):
    """
    This method will add ObjectOutput records for the given object.
    """

    info = obj.objectInfo()
    session = self.occam.databaseSession()

    if db_obj is None:
      db_obj = obj.databaseRecord()

    db_outputs = session.query(DB.ObjectOutput).filter_by(occam_object_id=db_obj.id)
    db_outputs.delete()

    outputs = info.get('outputs', [])
    for output in outputs:
      db_output = DB.ObjectOutput()
      db_output.revision = obj.revision
      db_output.object_type = output.get('type')
      db_output.object_type_safe = Object.slugFor(output.get('type'))

      db_output.occam_object_id = db_obj.id
      db_output.subtype = output.get('subtype')

      # Mark whether or not the output is sharable.
      # When objects are sharable, the object they are input to must
      # be present or connectable in some way within the same VM.
      if 'fifo' in output and output['fifo'] == True:
        db_output.fifo = 1
      else:
        db_output.fifo = 0

      session.add(db_output)

  def invocationPathFor(self, obj, category="output"):
    """
    Returns the path to the invocation data. For more information about
    invocation data, see description of method 'invocationDataFor.'
    """

    # Retrieve object uuid
    uuid = obj.objectInfo().get('id')

    if uuid is None:
      return None

    # Find the full path to the stored invocation metadata
    # Ex: .occam/invocation/2-code/2-code/full-code
    path = self.createPathFor(uuid, subPath='invocation')
    filePath = os.path.join(path, "%s.json" % (category))

    return filePath

  def invocationDataFor(self, obj, category="output"):
    """
    Returns the invocation data for this object. Invocation data is data that
    is not part of the object, but rather a record of what this object has done
    over time.

    Examples of invocation data are objects that have been generated (both uuid
    and revisions), what backend methods have successfully been used to run the
    object, and any performance information known for the object.
    """

    # Retrieve path to invocation metadata
    filePath = self.invocationPathFor(obj, category)

    # Open and parse the invocation metadata
    try:
      f = open(filePath, 'r')
      ret = json.load(f)
      f.close()
    except:
      # File not found or corrupt in some way
      ret = {}

    # Return the data
    return ret

  def mergeInvocation(self, obj, category, items):
    """
    Will merge the given invocation data for the given category with the
    invocation data current stored for the given object. Returns the newly
    merged invocation data.
    """

    # Retrieve invocation data
    data = self.invocationDataFor(obj, category)

    for revision, value in items.items():
      if revision in data:
        if isinstance(data[revision], list):
          if (isinstance(value, list)):
            value = data[revision] + value
          else:
            value = data[revision]
        elif isinstance(data[revision], dict):
          data[revision].update(value)
          value = data[revision]

      data[revision] = value

    # Update the invocation
    self.storeInvocation(obj, category, data)

    # Returns invocation data
    return data

  def appendInvocation(self, obj, category, item, revision=None):
    """
    Adds the given item to the given category within the stored invocation file.
    For more information about an invocation file, see the description of
    method 'invocationDataFor'.

    Ex: appendInvocation(some_obj, "generated", {
      "id":   "<uuid>",
      "revision": "fedcba",
      "name": "foo",
      "type": "application/json"
    })

    This would add that the given object has generated an object with id <uuid>
    seen with the revision "fedcba". The given object was at revision "abcdef"
    when it generated the new object.

    The items in the invocation file have arbitrary structure. All categories
    are arrays, but you can place any data within them.
    """

    if revision is None:
      revision = obj.revision

    # Retrieve invocation data
    data = self.invocationDataFor(obj, category)

    # Ensure that revision exists
    data[revision] = data.get(revision, [])

    # Ensure category is array
    if not isinstance(data[revision], list):
      # Abort!
      return None

    # Append the data item
    data[revision].append(item)

    # Update the invocation
    self.storeInvocation(obj, category, data)

    # Returns invocation data
    return data

  def updateInvocation(self, obj, category, item, revision=None):
    """
    """

    if revision is None:
      revision = obj.revision

    # Retrieve invocation data
    data = self.invocationDataFor(obj, category)

    # Ensure that revision exists
    data[revision] = data.get(revision, [])

    # Ensure category is a dictionary
    if not isinstance(data[revision], dict):
      # Abort!
      return None

    # Append the data item
    data[revision].update(item)

    # Update the invocation
    self.storeInvocation(obj, category, data)

    # Returns invocation data
    return data

  def storeInvocation(self, obj, category="output", data={}):
    """
    Completely replaces the stored invocation data with the given data.
    Returns None when the data cannot be stored. Otherwise, returns the
    data that was stored.
    """

    # Retrieve path to invocation metadata
    filePath = self.invocationPathFor(obj, category)

    # Open and parse the invocation metadata
    ret = data
    try:
      f = open(filePath, 'w+')
      f.write(json.dumps(data))
      f.close()
    except:
      # File not found or corrupt in some way
      ret = None

    # Return the data stored (or None)
    return ret

  def store(self, obj, public=True):
    """
    Retains a copy of the object in occam object pool. Local objects are on
    disk and can be altered at will by anybody. These objects are the actual
    persisted artifacts.
    """

    info = obj.objectInfo()
    revision = obj.head()
    object_type = info['type']

    if info.get('storable') == False:
      return

    # Place it in: .occam/objects/2-code/2-code/full-code
    uuid = info.get('id') or Object.uuid(info['type'], info['name'])
    path = self.createPathFor(uuid)

    if not path == os.path.realpath(obj.path):
      # If it is not already stored
      Log.noisy("cloning backup repository at %s" % (path))
      Git(obj.path).clone(path)

    # Add Database Entry
    session = self.occam.databaseSession()

    system = session.query(DB.System).first()

    db_obj = session.query(DB.Object).filter_by(uid=info['id']).first()

    # If it doesn't exist, create a record for it
    if db_obj is None:
      Log.noisy('storing Object record in db')
      db_obj = DB.Object()

      if object_type != "person" and system.moderate_objects == 1:
        db_obj.active = 0
      else:
        db_obj.active = 1

      if public:
        db_obj.private = 0
      else:
        db_obj.private = 1

      session.add(db_obj)
      session.commit()
      session.flush()

      self.updateNormalization(obj, info, db_obj)

      # Gather object outputs / inputs

      # Rake 'outputs' and add ObjectOutput records
      self.updateObjectOutputs(obj, db_obj)

      # Rake 'inputs' and add ObjectInput records
      self.updateObjectInputs(obj, db_obj)

      # Creates special records for special objects
      if object_type == "person":
        Log.noisy('Storing Person record in db')

        db_person_obj = DB.Person()
        db_person_obj.occam_object_id = db_obj.id
        db_person_obj.username = info['name']
        session.add(db_person_obj)
      elif object_type == "workset":
        Log.noisy('Storing Workset record in db')
        db_workset_obj = DB.Workset()

        db_workset_obj.occam_object_id = db_obj.id
        session.add(db_workset_obj)
      elif object_type == "group":
        Log.noisy('Storing Group record in db')
        db_group_obj = DB.Group()
        db_group_obj.occam_object_id = db_obj.id
        session.add(db_group_obj)
      elif object_type == "experiment":
        Log.noisy('Storing Experiment record in db')
        db_experiment_obj = DB.Experiment()
        db_experiment_obj.occam_object_id = db_obj.id
        session.add(db_experiment_obj)

      session.commit()
      session.flush()

    # Rake 'provides' and add Provider records
    self.updateProviders(obj, db_obj)

    # Rake 'views' and add Viewer records
    self.updateViewers(obj, db_obj)

    return db_obj

  def createPathFor(self, uuid, subPath='objects'):
    """
    Creates a path to store an object with the given uuid.
    """

    if uuid is None:
      return None

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    path = self.occam.configuration()['paths'].get(subPath)

    if path is None:
      return None

    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code1)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, code2)
    if not os.path.exists(path):
      os.mkdir(path)
    path = os.path.join(path, uuid)
    if not os.path.exists(path):
      os.mkdir(path)

    return path

  def pathFor(self, uuid, subPath='objects'):
    """
    Returns the path the object of the given uuid would be stored.
    """

    code = uuid
    code1 = code[0:2]
    code2 = code[2:4]

    # Objects are in: .occam/objects/<type>/<code[0:2]>/<code[2:4]>/full-code
    object_path = self.occam.configuration()['paths'].get(subPath)
    if object_path is None:
      return None
    return os.path.realpath(os.path.join(object_path, code1, code2, uuid))

  def delete(self, obj):
    # Builds path
    uuid = obj.objectInfo()['id']
    build_path = self.pathFor(uuid, subPath='builds')
    if os.path.exists(build_path):
      shutil.rmtree(build_path)

    # Remove local content
    Log.write("Deleting content at %s" % (obj.path))
    if os.path.exists(obj.path):
      shutil.rmtree(obj.path)

    # Remove object and all associated records in DB
    object = self.search(uuid=obj.objectInfo()['id']).first()

    session = self.occam.databaseSession()
    session.delete(object)
    session.commit()

    # Remove docker vm images / volumes
    # TODO: ARGH
    docker_name = Docker.docker_name(object.object_type, object.revision)
    os.system("docker rmi occam/%s" % (docker_name))

  def update(self, obj):
    """
    Updates the persisted version of the given object in the OCCAM store.
    """

    session = self.occam.databaseSession()

    info = obj.objectInfo()
    object_type = info['type']

    if info.get('storable') == False:
      return

    uuid = info['id']
    path = self.pathFor(uuid)

    # TODO: is this necessary or wanted?
    #if (path == os.path.realpath(obj.path)):
      # Already stored
      #return True

    if not os.path.exists(path):
      # Object has been destroyed. Store this object!
      return self.store(obj)

    # Update DB revision, if we had the old revision
    # This way, we have an understanding of 'base'
    db_obj = self.search(uuid=uuid, object_type=object_type).first()
    if not db_obj is None:
      Log.noisy("updating revision in DB: %s %s: PARENT REVISION = %s, DB REVISION = %s" % (obj.objectInfo()['type'], obj.objectInfo()['name'], obj.parentRevision(), db_obj.revision))
      if db_obj.revision == obj.parentRevision():
        db_obj.revision = obj.head()
        Log.noisy('updating %s to %s' % (uuid, db_obj.revision))
        session.commit()

    git = Git(path)
    obj_path = self.pathFor(uuid)
    Log.noisy("fetching from %s to %s." % (obj_path, path))
    remote_name = str(uuid4())
    new_branch_name = str(uuid4())
    Log.noisy("adding remote %s -> %s" % (remote_name, obj_path))
    git.addRemote(remote_name, obj.path)
    git.fetchRemoteBranches(remote_name)
    Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, obj.git.branch()))
    git.checkoutBranch(new_branch_name, obj.git.branch(), remote_name)
    git.rmRemote(remote_name)

    # Update Local Links for this object
    root = None
    if object_type == "workset":
      root = obj
    elif not obj.root is None:
      root = obj.root

    if not root is None:
      Log.write("syncing with root: %s" % (root.objectInfo()['id']))

      db_obj = self.search(uuid=root.objectInfo()['id'], object_type='workset').first()
      for link in self.occam.searchLocalLinks(workset_id=db_obj.id, person_id=self.occam.currentPersonId()):
        Log.write('linking with %s' % (link.path))
        # Compare revision of local link with this revision, if they are the same, update this
        if not os.path.exists(link.path):
          continue

        local_git = Git(link.path)
        Log.write('%s == %s' % (root.parentRevision(), local_git.head()))
        if root.parentRevision() == local_git.head() or root.head() == local_git.head():
          # Sync the workset!
          Log.write('syncing workset at %s' % (link.path))
          Object(link.path, occam=self.occam, root=root).sync(root.head())

    return True
